<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Denver');
		//$authPermissionID = $this->Permission_model->authPermissionID($this->session->userdata('user_id'));
		//$this->setData('authPermissionID', $authPermissionID);
	}

	public function index(){
		$this->load->library('user_agent');
		$user = $this->Admin_model->getUser($postData=[]);

		$profile = "Profile";

		$data = array(
			'page_description'  => $profile,
			'page_head' 		=> $profile,
			'page_keyword' 		=> $profile,
			'page' 				=> $profile,
			'page_title' 		=> $profile,
			'user'				=> $user,
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
		);
		$this->load->view('admin/profile/profile_view', $data);
	}
	public function show_profile(){
		echo json_encode($this->Profile_model->show_profile($this->session->userdata('user_id')));
	}
}
