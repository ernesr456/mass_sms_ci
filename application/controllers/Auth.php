<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Denver');
	}
	
	public function index(){
		if ($this->session->userdata('is_logged_in')) {
			redirect('/admin');
		}else{
			$data = array(
				'page_description' 	=> 'Login page',
				'page' 			   	=> 'login',
				'page_keyword' 		=> 'Login page',
				'page_title' 		=> 'Login page',
			);
			$this->load->view('admin/login_view', $data);
		}
	}

	public function login_user(){
		/*
			Error List:
			0 - No Error
			1 - Too Many Login Attempts
			2 - Bad Credentials
		*/
		if ($this->input->post()){
			// check if have login attempts
			$postData = $this->input->post();
			// check the auth
			$auth = $this->Auth_model->login($postData);

			if ($auth) {
				echo json_encode($auth);
			}else if ($auth==3) {
				$username = $this->db->escape(strip_tags($postData["username"]));
				$sql = "SELECT * from users where username =".$username. "OR email =".$username;
				$query = $this->db->query($sql);
				$row = $query->row_array();
				$name = $row['name'];
				$id = $row['id'];
				$email = $row['email'];
				$link = base_url()."auth/verification?id=".$id."&type=verification";
				$subject = "My Soapbox: Verify Email";
				echo json_encode($this->template($name,$link,$id,$email,$subject));
			}else{
				echo json_encode($auth);
			}

			// if ($auth===true) {
			// 	echo json_encode(true);
			// }else if($auth ==3){
			// 	$username = $this->db->escape(strip_tags($postData["username"]));
			// 	$sql = "SELECT * from users where username =".$username. "OR email =".$username;
			// 	$query = $this->db->query($sql);
			// 	$row = $query->row_array();
			// 	$name = $row['name'];
			// 	$id = $row['id'];
			// 	$email = $row['email'];
			// 	$link = base_url()."auth/verification?id=".$id."&type=verification";
			// 	$subject = "My Soapbox: Verify Email";
			// 	echo json_encode($this->template($name,$link,$id,$email,$subject));
			// }else if($auth===false){
			// 	echo json_encode(false);
			// }
		}else{ echo json_encode(false); }
	}

	public function verification(){
		if ($this->session->userdata('is_logged_in')) {
			redirect('/admin');
		}
		else {
			/*
			Error List:
			0 - No Error
			1 - Too Many Login Attempts
			2 - Bad Credentials
			*/

			// set the initial login attempts
			$error = "";
			if (isset($_GET['type'])) {
				$id = $_GET['id'];
				if ($_GET['type']=='confirm_pass') {
					$pass = $this->Admin_model->confirm($id);
					if ($pass==true) {
						$pass = $this->Admin_model->delete_request($id);
					}else{
						$error = 2;
					}
				}else if($_GET['type']=='verification'){
					$verify = $this->Admin_model->verification($id);
				}
			}
			$data = array(
				'page_description' 	=> 'Login page',
				'page' 			   	=> 'login',
				'page_keyword' 		=> 'Login page',
				'page_title' 		=> 'Login page',
				'error'				=> $error
			);
			$this->load->view('admin/verify_view', $data);
		}

	}
	public function change_pass(){
		$postData = $this->input->post();
		$data = $postData['data'];
		$id = $data[0]['value'];
		$sql = "SELECT * from users where id =$id";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		$name = $row['name'];
		$email = $row['email'];
		$link = base_url()."auth/verification?id=".$id."&type=confirm_pass";
		$subject = "My Soapbox: Confirmation Password Changed";
		$auth = $this->Admin_model->change_pass($data);
		echo json_encode($auth);
		if ($auth==true) {
			$email = $this->template($name,$link,$id,$email,$subject);
		}else{

		}
	}
	public function password_reset(){
		if ($this->session->userdata('is_logged_in')) {
			redirect('/admin');
		}
		else {
			/*
			Error List:
			0 - No Error
			1 - Too Many Login Attempts
			2 - Bad Credentials
			*/
			if ($this->input->post()){
				// check if have login attempts
				$postData = $this->input->post();
				// check the auth
				$data = array(
					'page_description' 	=> 'Reset Password Page',
					'page' 			   	=> 'Reset Password',
					'page_keyword' 		=> 'Reset Password page',
					'page_title' 		=> 'Reset Password page',
					'authpage'			=> true,
					'error'				=> 0,
				);
				$this->load->view('admin/reset_password', $data);
			} else {
				$data = array(
					'page_description' 	=> 'Login page',
					'page' 			   	=> 'Login',
					'page_keyword' 		=> 'Login page',
					'page_title' 		=> 'Login page',
					'authpage'			=> true,
					'error'				=> 0,
				);
				$this->load->view('admin/reset_password', $data);
			}
		}
	}
	public function create_account(){
		if ($this->input->post()){
			$postData = $this->input->post();
			echo json_encode($this->Auth_model->register($postData));
			
		}
	}
	public function update_account(){
		if ($this->input->post()){ 
			$postData = $this->input->post();
			$get_id = $postData['get_id'];
			$updatecontact = $this->Admin_model->updateUserbyAdmin($get_id, $postData);
			echo json_encode($updatecontact);
		}
	}
	public function register(){
		$data = array(
			'page_description' 	=> 'Register page',
			'page' 			   	=> 'register',
			'page_keyword' 		=> 'Register page',
			'page_title' 		=> 'Register page',
			'authpage'			=> true,
			'error'				=> 0,
		);
		$this->load->view('admin/register_view', $data);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

    public function get_key(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$recover = $this->Admin_model->get_key($postData);
			$email = $postData['email'];
			if ($recover == true) {
				$sql = "SELECT id,name from users where email = '$email'";
				$query = $this->db->query($sql);
				if ($query->num_rows()>0) {
					$row = $query->row_array();
	        		$id = $row['id'];
	        		$name = $row['name'];
					$link = base_url()."auth/password_reset?id=".$id;
					$subject = "My Soapbox: Password Recovery";
					$this->Auth_model->template($name,$link,$id,$email,$subject);
				}
			}
			echo json_encode($recover);
		}
	}

	public function template($name,$link,$verification_key,$email,$subject){
		$template="";
		$message="";
		$message1="";
		$button="";
		$name = "Get MySoapBox";
		$title = "";
		$message2="";
		$smtp="";
		$smtp_email ="";
		$from 		= 'noreply@getmysoapbox.com';

		if ($subject=="My Soapbox: Verify Email") {
			$title = "Verify your Email Address";
			$message = '
				Welcome to <strong><a href="https://massci.techonsolutions.us/" rel="noopener" style="text-decoration: none; color: #3e6edf;" target="_blank" title="My Soapbox">My Soapbox</a></strong>
			';

			$message1="
				Please click the button below to verify your email address.
			";
			$button = "CONFIRM EMAIL";
			$message2="If you did not sign up to My Soapbox,Please ignore this email";
		}else if($subject=="My Soapbox: Confirmation Password Changed"){
			$title = "Confirm to Change Password";
			$message.=
			'
			<p style="text-align: center;">You changed your password to confirm that was you!</p>
                <br>
                <p style="text-align: center;">Simply click on the button to a confirm</p>
			'
			;
			$button = "Confirm to set new password";
			$message2.='
                <p style="text-align: center;">If you didn&apos;t ask to change your password don&apos;t worry, your password is still safe and you can ignore this email.</p>

			';

		}
		else if($subject=="My Soapbox: Password Recovery"){
			$title = "Password Recovery";
			$message.=
			"
			<p>We received request to reset your password of your My Soapbox account, We're to help!</p>
                <br>
                <p>Simply click on the button to a new password</p>
			"
			;
			$button = "Set a New Password";
			$message2.='
                <p style="text-align: center;">If you didn&apos;t ask to change your password don&apos;t worry, your password is still safe and you can ignore this email.</p>

			';
		}
		
		$template .='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml"><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><meta content="width=device-width" name="viewport"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><title></title><style type="text/css">body{margin:0;padding:0}table,td,tr{vertical-align:top;border-collapse:collapse}*{line-height:inherit}a[x-apple-data-detectors=true]{color:inherit!important;text-decoration:none!important}</style><style id="media-query" type="text/css">@media (max-width:520px){.block-grid,.col{min-width:320px!important;max-width:100%!important;display:block!important}.block-grid{width:100%!important}.col{width:100%!important}.col>div{margin:0 auto}img.fullwidth,img.fullwidthOnMobile{max-width:100%!important}.no-stack .col{min-width:0!important;display:table-cell!important}.no-stack.two-up .col{width:50%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num8{width:66%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num3{width:25%!important}.no-stack .col.num6{width:50%!important}.no-stack .col.num9{width:75%!important}.video-block{max-width:none!important}.mobile_hide{min-height:0;max-height:0;max-width:0;display:none;overflow:hidden;font-size:0}.desktop_hide{display:block!important;max-height:none!important}}</style></head><body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #ffffff;"><table bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; width: 100%;" valign="top" width="100%"><tbody><tr style="vertical-align: top;" valign="top"><td style="word-break: break-word; vertical-align: top;" valign="top"><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;"><img align="center" alt="Image" border="0" class="center autowidth" src="'.base_url().'assets/Email_image/sms2.jpg" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 500px; display: block;" title="Image" width="500"/></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.8; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 25px;"><p style="font-size: 30px; line-height: 1.8; word-break: break-word; text-align: center; mso-line-height-alt: 54px; margin: 0;"><span style="font-size: 30px;"><strong>'.$title.'</strong></span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"></div><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 24px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 29px; margin: 0;"><span style="font-size: 24px;">'.$message.'</span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message1.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#3e6edf;border-radius:60px;-webkit-border-radius:60px;-moz-border-radius:60px;width:auto; width:auto;;border-top:1px solid #3e6edf;border-right:1px solid #3e6edf;border-bottom:1px solid #3e6edf;border-left:1px solid #3e6edf;padding-top:5px;padding-bottom:5px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><a href="'.$link.'" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #3e6edf; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; width: auto; width: auto; border-top: 1px solid #3e6edf; border-right: 1px solid #3e6edf; border-bottom: 1px solid #3e6edf; border-left: 1px solid #3e6edf; padding-top: 5px; padding-bottom: 5px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">'.$button.'</span></span></a></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:40px;padding-right:10px;padding-bottom:40px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message2.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div></td></tr></tbody></table></body></html>
        ';

        $send_grid_email = $this->config->item('send_grid_email');
        $send_grid_pass = $this->config->item('send_grid_pass');

		return $this->send_now($email,$subject,$template,$send_grid_email,'My Soapbox');
	}

	public function send_now($email, $subject, $body, $from_name, $title){

        $this->email->from($from_name, $title); 
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($body);
        return ($this->email->send()) ? 3 : 5;
	}

	public function add_new_user(){
		if($this->session->userdata('is_logged_in')){
			if($this->input->post()){
				echo"a";
				$res = $this->Auth_model->add_update_this_user();
				if($res['code']=="200"){
					$id 	  = $res['data'];
					$name     = $this->input->post('name');
					$email    = $this->input->post('email');
					$link	  = base_url()."auth/verification?id=".$id."&type=verification";
					$subject  = "My Soapbox: Verify Email";
					$sent 	  = $this->Auth_model->template_user($name,$link,$id,$email,$subject);
					echo ($sent) ? json_encode($this->Auth_model->response('200', 'New user was successfully created. Please check the email for verification', '')) : json_encode($this->Auth_model->response('201', 'A problem occured while sending an email, please try login again.', '')); 
				}else{
					echo json_encode($res);
				}
			}
		}else{ echo 0; }
	}

	public function update_this_user(){
		if($this->session->userdata('user_id')&&$this->session->userdata('is_logged_in')){
			if($this->input->post()){
				
				echo json_encode($this->Auth_model->add_update_this_user());
			}
		}else{ echo 0; }
	}

	public function get_user(){
		if($this->session->userdata('user_id')){
			if($this->input->post()){
				echo json_encode($this->Auth_model->get_this_user($this->input->post('id')));
			}
		}else{ echo 0; }
	}

	public function change_selected_users_status(){
		if($this->session->userdata('user_id')){
			$id 	= explode(',',$this->input->post('id'));
			$status = $this->input->post('status');
			echo $this->Auth_model->change_selected_users_status($id, $status) ? 1 : 0;
		}else{ echo 0; }
	}

	public function update_user_status(){
		if($this->session->userdata('user_id')){
			if($this->input->post()){
				echo $this->Auth_model->update_user_status() ? 1 : 0;
			}
		}else{ echo 0; }
	}

	public function get_user_lists(){
		if($this->session->userdata('user_id')){
            $limit 			= $this->input->post('length');
            $offset 		= $this->input->post('start');
            $search 		= $this->input->post('search');
            $order 			= $this->input->post('order');
            $draw 			= $this->input->post('draw');
            $role 			= $this->input->post('user_role');
            $column_order 	= array('a.id','a.name','a.username','a.email','a.user_group','a.created_by','a.status', 'a.id');
            $list 			= $this->Auth_model->get_user_lists('users',$column_order, $limit, $offset ,$search, $order, $role);
            $output = array(
                "draw" 				=> $draw,
                "recordsTotal" 		=> $list['count_all'],
                "recordsFiltered"	=> $list['count'],
                "data" 				=> $list['data']
            );
            echo json_encode($output);
        }else{
            echo json_encode([]);
        }
	}

	public function updateProfile(){
        if ($this->input->post()){
            $postData = $this->input->post();
            $filename = '';
           	$main_folder = 'assets/profile_img';
            if (!empty($_FILES)) {
               		if(!is_dir($main_folder.'/'.$_SESSION['user_id'])){
						mkdir($main_folder.'/'.$_SESSION['user_id'], 0777, true);
					}
					// Define new $_FILES array - $_FILES['file']
					$_FILES['file']['name'] = $_FILES['files']['name'];
					$_FILES['file']['type'] = $_FILES['files']['type'];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
					$_FILES['file']['error'] = $_FILES['files']['error'];
					$_FILES['file']['size'] = $_FILES['files']['size'];
					// Set preference
					$config['upload_path'] = $main_folder.'/'.$_SESSION['user_id']; 
					$config['allowed_types']        = 'gif|jpg|png';
					$config['max_size'] = '5000'; // max_size in kb
					$config['file_name'] = $_FILES['files']['name'];
					
					//Load upload library

					$this->load->library('upload',$config); 
					// File upload
					$this->upload->initialize($config);
					if($this->upload->do_upload('file')){
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
					}
            }            
            // check the auth
            $update = $this->Auth_model->updateProfile($postData,$filename);
            echo json_encode($update);
        }
    }
}
