<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//twilio
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;
use Twilio\Security\RequestValidator;


class Conversation extends CI_Controller {
	public $_api_context;
    function __construct(){
        parent::__construct();
		$this->load->config('twilio', TRUE);
		$authPermissionID = [];
		if ($this->session->userdata('user_group')!=2) {
			$authPermissionID = $this->Permission_model->authPermissionID($this->session->userdata('user_id'));
		}
		$this->authentication=$authPermissionID;
    }

	public function index(){
		$ses_id = $_SESSION['user_id'];
		$user_group = $_SESSION['user_group'];
		$template = "";
		if (isset($_GET['msg_template'])) {
			$template = $this->Template_model->fetch_template($_GET['msg_template']);
		}

		$data = array(
			'page_description' 	=> 'Conversation ',
			'page' 			   	=> 'Conversation',
			'page_keyword' 		=> 'Conversation ',
			'page_title' 		=> 'Conversation ',
			'page_head' 		=> 'Conversation',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'template'			=> $this->Conversation_model->get_template($ses_id,$user_group)
		);
		$this->load->view('admin/conversation_view', $data);
	}

	public function send_message(){
    	$this->load->library('twilio');

    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		// print_r($postData);
			$group = $postData['group'];
			//Message Body
			$body = '';
			//Main Folder for file in message
			$main_folder = 'assets/MMS_Images';
			//Message contain
			$message = $postData['message'];
			//Message ID for Quote
			$quote = $postData['quote'];
			$filename = "";
			$extension = "";
			$status = 0;
			$sid = "";
			$token= "";
			$twil_number = "";
			$number ="";
			$MMS_SEND="";
			$total_fee = 0;
			$data= "";
			$total_credit = 0;
			$credits = $this->Payments_model->total_credit();
			$total_credit = $credits['total_credit'];

			$twil = $this->Admin_model->getConfigs();
			if (!empty($twil['twillio_auth_token'])&&!empty($twil['twillio_account_sid'])&&!empty($twil['twilio_account_number'])) {
				$sid = $twil['twillio_account_sid'];
				$token = $twil['twillio_auth_token'];
				$twil_number = $twil['twilio_account_number'];
				$twilio = new Client($sid,$token);

				//MMS Media URL
				if(isset($_FILES)&&!empty($_FILES)){
					$data = array();
					if(!is_dir(''.$main_folder)){
						mkdir(''.$main_folder);
					}
					// Count total files
					$countfiles = count($_FILES['files']['name']);
					// looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){
							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];
							// Set preference
							$config['upload_path'] = ''.$main_folder; 
							$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|pdf|mp4a|mp4|mpga|oga|3ga|3gpp|ac3|wav|weba|awb|amr|mpeg|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = $_FILES['files']['name'][$i];
							
							//Load upload library

							$this->load->library('upload',$config); 
							// File upload
							$this->upload->initialize($config);

							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];
								$path = pathinfo(''.$main_folder.'/'.$filename);
								$extension = $path['extension'];
								// Initialize array
								$data['filenames'][] =$filename;
								$mediaUrl = base_url().$main_folder.'/'.$filename;
								$medianame = $filename;
							}
						}
					}

				}

				//forward message
				if (isset($postData['forward_message'])) {
					$id = $postData['id'];
					$forward = $this->Admin_model->conversation_thread($id);
					if (!empty($forward['filename'])) {
						$filename = $forward['filename'];
					}
				}

				//Msg Template
				if (isset($postData['file'])) {
					$obj_file = json_decode($postData['file']);
					for($i=0;$i<count($obj_file);$i++){
						$filename = $obj_file[$i]->name;
						$mediaUrl = $obj_file[$i]->path;
					}
				}
				$this->db->select('name')->from('users')->where('id='.$this->session->userdata('user_id'));
				foreach ($this->db->get()->result_array() as $row){
					$name = $row['name'];
				}


				$this->db->select('*');
				$group=='single'? $this->db->from('contact')->where('id=', $postData['contact'])
				:$this->db->from('contacts_group a')->join('contact b','a.gid = b.group_id','left')->where('gid=', $postData['contact']);
				foreach ($this->db->get()->result() as $contact) {
					//Quote
					if (!empty($postData['quote'])) {
						$quote = $this->Admin_model->conversation_thread($quote);
						$quote_message = $quote['message'];
						$quote_file = $quote['filename'];
					 	$body = ucwords($name).", Quoted Message: \n".$quote_message."\n";

						if (!empty($quote_file)) {
							$mediaUrl = base_url().$main_folder.'/'.$quote['from_id'].'/'.$quote_file;
							try{
								$MMS_SEND = $twilio->messages
								->create("+".$contact->number, // to
									array(
										"body" => "",
										"from" => "	+".$twil_number,
										//Testing Media Url 
										// "mediaUrl"=> array("https://demo.twilio.com/owl.png")

										//If you test localhost please use https
										"mediaUrl" => array($mediaUrl)
									)
								);
							}catch(Exception $e){
								$error_code = $e->getCode();
							}
						}
						if (!empty($quote_message)) {
							try{
								$MMS_SEND = $this->twilio->sms($twil_number,'+'.$contact->number,$quote_message);
								// $MMS_SEND = $twilio->messages->create("+".$contact->number, // to
								// 	array(
								// 		"body" => $quote_message,
								// 		"from" => "	+".$twil_number,
								// 	)
								// );
							}catch(Exception $e){
								$error_code = $e->getCode();
							}
						}
					}

					if (!empty($filename)) {
						$total_fee = $total_fee+0.04;
					}
					if (!empty($message)) {
						$total_fee = $total_fee+0.15;
					}

					if ($total_credit>$total_fee||$this->session->userdata('user_group')==1) {
						$data = $this->Conversation_model->save_message($postData,$filename,$contact->id,$contact->group_id,$quote,$extension,$status);
						if (!empty($filename)) {
							// if ($fee>$pay&&$fee!="") {
							try{
								// $MMS_SEND = $this->twilio->mms($twil_number,$contact->number,$message,$mediaUrl);
								$MMS_SEND = $twilio->messages
									->create("+".$contact->number, // to
										array(
											"body" => "",
											"from" => "	+".$twil_number,
											//Testing Media Url 
											"mediaUrl"=> array("https://demo.twilio.com/owl.png")

											//If you test localhost please use https
											// "mediaUrl" => array($mediaUrl)
										)
									);
							}catch(Exception $e){
									$error_code = $e->getCode();
							}
						}

						if(!empty($message)){
							try{
								$MMS_SEND = $this->twilio->sms($twil_number,'+'.$contact->number,$message);
							}catch(Exception $e){
								$error_code = $e->getCode();
							    // echo $e->getCode() . ' : ' . $e->getMessage()."<br>";
							}
						}

						if($MMS_SEND->IsError){
							$status = "";
							$error_code =$MMS_SEND->ResponseXml->RestException->Code[0];
							if ($error_code==21211) {
								$status = 3;
							}else if ($error_code==21212||$error_code = 204204||$error_code=20003) {
								$status = 2;
							}
							$this->Admin_model->update_sms_status($data,$status);
						}else if($this->session->userdata('user_group')!=1){
							$this->Payments_model->update_balance_credit($total_fee);
						}
					}else{
						$data = 2;
					}

				}
				if ($data) {
					echo json_encode(true);
				}else if($data==2){
					echo json_encode(2);
				}
			}else{
				echo json_encode(false);
			}
    	}
    }
    public function fetch_template(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$id = $postData['id'];
			$data = $this->Conversation_model->fetch_template($id);
			//get file

			$file = array();
			$data_array = json_decode(json_encode($data), True);
			$id = $data_array[0]['id'];
			$subject = $data_array[0]['subject'];
			$filename = $data_array[0]['filename'];
			$created_by = $data_array[0]['created_by'];

			$main_folder = 'assets/msg_template/'.$created_by.'/';
			//check folder if exist
			if(is_dir(''.$main_folder)){
				//open folder
				if ($open = opendir($main_folder)) {
					// Read files
					if($filename != '' && $filename != '.' && $filename != '..'){
			          	// File path
			          	$file_path = $main_folder.$filename;			          	// Check its not folder
			          	if(!is_dir($file_path)){
			             	$size = filesize($file_path);
			             	$file = array('name'=>$filename,'size'=>$size,'path'=>$file_path);
			             	array_push($data, $file);

			          	}
			        }
				}
			}else{
				mkdir(''.$main_folder);
			}
			echo json_encode($data);
		}
	}
	public function listContacts(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();

    		if ($postData['contact']=='single') {
		        $valid_columns = array(
	            	0=>'a.firstName',
		            1=>'a.lastName',
		            2=>'d.group_name',
		            3=>'a.email',
		            4=>'b.name',
		            5=>'a.number'
		        );
		    }else{
		        $valid_columns = array(
		            0=>'b.name',
		            1=>'a.group_name',
		            2=>'a.tags',
		        );
		    }
			$data = $this->Conversation_model->listContacts($postData,$valid_columns);
			echo json_encode($data);
    	}

    }
    public function show_conversation(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
			$data = $this->Conversation_model->show_conversation($postData);
			echo json_encode($data);
    	}
    }
    public function conversation_thread(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		echo json_encode($this->Conversation_model->conversation_thread($postData['id']));
    		
    	}
    }
    public function update_message(){
    	if ($this->input->post()&&$this->session->userdata('user_group')!=1) {
    		$postData = $this->input->post();
    		echo json_encode($this->Conversation_model->update_message($postData['id']));
    	}
    }
    public function show_new_message(){
    	$limit = $this->input->post("limit");
    	$data = $this->Conversation_model->show_new_message($limit);
		echo json_encode($data);
    }
	
}
	