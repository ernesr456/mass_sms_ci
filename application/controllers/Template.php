<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Denver');
		//$authPermissionID = $this->Permission_model->authPermissionID($this->session->userdata('user_id'));
		//$this->setData('authPermissionID', $authPermissionID);
	}
		//Message Template Page
	public function index(){
		// $this->render('admin/message_template_view.php', 'Message Template', array(
		// 	'page_description' => "Message Template",
		// 	'page_keyword' => "Message Template",
		// 	'page' => "message_template",
		// ));

		$data = array(
			'page_description' 	=> 'Message Template ',
			'page' 			   	=> 'Message Template',
			'page_keyword' 		=> 'Message Template ',
			'page_title' 		=> 'Message Template ',
			'page_head' 		=> 'Message Template',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
		);
		$this->load->view('admin/message_template/message_template_view', $data);
	}

	public function show_template(){
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $type = "";
        if($this->input->post("type")){
            $type = $this->input->post("type");
        }
        $search = $search['value'];
        $col = 0;
        $dir = "";
        $data= array();
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc")
        {
            $dir = "desc";
        }
        $valid_columns = array(
            0=>'subject',
            1=>'message',
            2=>'filename',
        );
        if(!isset($valid_columns[$col]))
        {
            $order = null;
        }
        else
        {
            $order = $valid_columns[$col];
        }
        if($order !=null)
        {
            $this->db->order_by($order, $dir);
        }
 
        $this->db->limit($length,$start);
        $sql = $this->Template_model->show_template($valid_columns,$search);
        foreach ($sql as $rows) {
        	$file = '';
        	$button = '';
        	if ($rows->filename!="") {
        		$file= '<a href="javascript:void(0)" data-id='.$rows->id.' id="fetch_template" data-target="#"><button type="button" class="btn btn-xs btn-outline-primary waves-effect">Click to view</button></a>';
        	}else{
        		$file = '';
        	}
        	
        	if($type=="conversation"){
        	    $button = '<button type="button" id="select_template" data-id='.$rows->id.' class="btn btn-xs btn-outline-primary">Select</button>';
        	}else{
        	    $button = 
        	    '<a href="javascript:void(0)" id="edit_template" data-target="#updateTemplate" data-toggle="modal" class="btn icon-btn btn-sm btn-outline-warning" data-message="'.$rows->message.'" data-filename="'.$rows->filename.'" data-subject="'.$rows->subject.'" data-id='.$rows->id.' title="Edit Template">
							<span class="far fa-edit"></span>
				</a>
				<a href="javascript:void(0)" class="btn icon-btn btn-sm btn-outline-danger" id="delete_template" data-id='.$rows->id.' title="Delete Template">
							<span class="fas fa-trash"></span>
				</a>';
        	}
        	
            $data[]= array(
                // '<label class="custom-control custom-checkbox px-2 m-0" style="text-align: center;">
                // <input type="checkbox" class="custom-control-input" name="selected_contacts" id="selected_contacts" value='.$rows->id.'><span class="custom-control-label"></span>
                // </label>',
                $rows->id,
                $rows->subject,
                $rows->message,
                $file,
                $button
            );
        }

        $total_users = $this->Template_model->total_template($search,$valid_columns);
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $total_users,
            "recordsFiltered" => $total_users,
            "data" => $data
        );
        echo json_encode($output);
        exit();
	}

	public function save_template(){
		$filename = "";
		$extension = "";
		$postData = "";
		if ($this->input->post()) {
			$postData = $this->input->post();
			
		}
		if (!empty($_FILES)) {
			$data="";
			$main_folder = 'assets/msg_template';

			if(!is_dir(''.$main_folder.'/'.$_SESSION['user_id'])){
				mkdir(''.$main_folder.'/'.$_SESSION['user_id']);
			}
			// Count total files
			$countfiles = count($_FILES['files']['name']);
			//looping all files

			for($i=0;$i<$countfiles;$i++){
				if(!empty($_FILES['files']['name'][$i])){
					// Define new $_FILES array - $_FILES['file']

					$_FILES['file']['name'] = $_FILES['files']['name'][$i];
					$_FILES['file']['type'] = $_FILES['files']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['files']['error'][$i];
					$_FILES['file']['size'] = $_FILES['files']['size'][$i];
					// Set preference
					$config['upload_path'] = ''.$main_folder.'/'.$_SESSION['user_id']; 
					$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|mp4a|mpga|oga|3ga|pdf|3gpp|ac3|pdf|wav|weba|awb|amr|mpeg|mp4|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
					$config['max_size'] = '5000'; // max_size in kb
					$config['file_name'] = $_FILES['files']['name'][$i];
		
					//Load upload library
					$this->load->library('upload',$config); 
					
					// File upload
					if($this->upload->do_upload('file')){
						// Get data about the file
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
						$path = pathinfo(''.$main_folder.'/'.$_SESSION['user_id'].'/'.$filename);
						$extension = $path['extension'];
						// Initialize array
					}
				}
			}
		
		}
		if (!empty($_FILES)||!empty($postData['message'])) {
			$save_template = $this->Template_model->save_template($postData,$filename,$extension);
			echo json_encode($save_template);
		}else{
			echo json_encode(2);
		}
	}
    public function update_template(){
    	$id = "";
    	$filename = "";
    	$postData = "";
    	$extension = "";
    	$created_by = "";
    	$path = "";
    	$main_folder = 'assets/msg_template';
    	$postData = $this->input->post();
    	$id = $postData['id'];

    	$template_file = $this->db->select('filename')->from('message_template')->where('id=',$id)->get()->row_array();
    	$old_file = $template_file['filename'];

    	if (isset($_FILES)&&!empty($_FILES)) {
	    	if (!empty($old_file)) {
	    		unlink($main_folder.'/'.$name->created_by.'/'.$old_file);
    		}

			$ses_id = $this->session->userdata('user_id');
    		$data="";

			if(!is_dir(''.$main_folder.'/'.$template_file['created_by'])){
				mkdir(''.$main_folder.'/'.$template_file['created_by']);
			}


			// Count total files
			$countfiles = count($_FILES['files']['name']);
			//looping all files
			for($i=0;$i<$countfiles;$i++){
				if(!empty($_FILES['files']['name'][$i])){
					// Define new $_FILES array - $_FILES['file']

					$_FILES['file']['name'] = $_FILES['files']['name'][$i];
					$_FILES['file']['type'] = $_FILES['files']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['files']['error'][$i];
					$_FILES['file']['size'] = $_FILES['files']['size'][$i];
					// Set preference
					$config['upload_path'] = ''.$main_folder.'/'.$created_by; 
					$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|mp4a|mpga|oga|3ga|pdf|3gpp|ac3|pdf|wav|weba|awb|amr|mpeg|mp4|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
					$config['max_size'] = '5000'; // max_size in kb
					$config['file_name'] = $_FILES['files']['name'][$i];
		
					//Load upload library
					$this->load->library('upload',$config); 
					
					// File upload
					if($this->upload->do_upload('file')){
						// Get data about the file
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
						$path = pathinfo(''.$main_folder.'/'.$template_file['created_by'].'/'.$filename);
						$extension = $path['extension'];
					}
				}
			}
    	}
    	(empty($filename))? $filename = $template_file['filename'] : '';
    	$update = $this->Template_model->update_template($postData,$filename,$extension);
    	echo json_encode($update);
    }
    public function delete_template(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$main_folder = 'assets/msg_template';
    		$data = $postData['data'];
    		$delete= "";
    		for ($i=0; $i <count($data) ; $i++) { 
				$query = $this->db->query('SELECT * from message_template where id = '.$data[$i]);
				foreach ($query->result() as $name) {
					if ($name->filename!="") {
						$filename = $name->filename;
						$path = $main_folder.'/'.$name->created_by.'/'.$filename;
						unlink($path);
					}
					$delete = $this->Template_model->delete_template($data[$i]);
				}
    		}
    		echo json_encode($delete);
    	}
    }
	public function fetch_template(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$id = $postData['id'];
			$data = $this->Template_model->fetch_template($id);
			//get file

			$file = array();
			$data_array = json_decode(json_encode($data), True);
			$id = $data_array[0]['id'];
			$subject = $data_array[0]['subject'];
			$filename = $data_array[0]['filename'];
			$created_by = $data_array[0]['created_by'];

			$main_folder = 'assets/msg_template/'.$created_by.'/';
			//check folder if exist
			if(is_dir(''.$main_folder)){
				//open folder
				if ($open = opendir($main_folder)) {
					// Read files
					if($filename != '' && $filename != '.' && $filename != '..'){
			          	// File path
			          	$file_path = $main_folder.$filename;			          	// Check its not folder
			          	if(!is_dir($file_path)){
			             	$size = filesize($file_path);
			             	$file = array('name'=>$filename,'size'=>$size,'path'=>$file_path);
			             	array_push($data, $file);

			          	}
			        }
				}
			}else{
				mkdir(''.$main_folder);
			}
			echo json_encode($data);
		}
	}
}
