<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//twilio
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;
use Twilio\Security\RequestValidator;

class Campaign extends CI_Controller {

	function __construct(){
        parent::__construct();
        date_default_timezone_set('America/Denver');
    }
    
    public function index(){
        $data = array(
            'page_description'  => "Campaigns",
            'page_head'         => 'Campaigns',
            'page_keyword'      => "Campaigns",
            'page'              => "Campaigns",
            'page_title'        => 'Campaigns',
            'authPermissionID'  => $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
            'project'           => $this->Campaign_model->show_campaign($id='',$user='',$status='',$timezone='',$type=''),
            'group_contact'     => $this->Admin_model->getContactGroups(),
            'users'             => $this->Admin_model->getUsers($postData="")
        );
        $this->load->view('admin/project/campaign_view', $data);
    }

    public function add_campaign(){
        $title = (isset($_GET['id']))?'Update Project':'Add Project';
        
        // $group = $this->Admin_model->getContactGroups();
        // $this->setData('group', $group);

        $data = array(
			'page_description'  => $title,
			'page_head' 		=> $title,
			'page_keyword' 		=> $title,
			'page' 				=> $title,
			'page_title' 		=> $title,
			'group'			    => $this->Admin_model->getContactGroups()
        );
        if (isset($_GET['id'])) {
            $data['project']  = $this->Project_model->getProject($_GET['id']);
            $data['campaign'] = $this->Campaign_model->getSingleCampaign($_GET['id']);
        }
        $this->load->view('admin/project/add_project_view', $data);

    }
    public function insert_campaign(){
        $filename = "";
        $type = "";
        $id = "";

        if (!empty($_FILES)) {
            $data="";
            $main_folder = 'assets/project_file';

            if(!is_dir(''.$main_folder.'/'.$_SESSION['user_id'])){
                mkdir(''.$main_folder.'/'.$_SESSION['user_id']);
            }
            // Count total files
            $countfiles = count($_FILES['files']['name']);
            //looping all files

            for($i=0;$i<$countfiles;$i++){
                if(!empty($_FILES['files']['name'][$i])){
                    // Define new $_FILES array - $_FILES['file']

                    $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                    // Set preference
                    $config['upload_path'] = ''.$main_folder.'/'.$_SESSION['user_id']; 
                    $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|mp4a|mpga|oga|3ga|pdf|3gpp|ac3|pdf|wav|weba|awb|amr|mpeg|mp4|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
                    $config['max_size'] = '5000'; // max_size in kb
                    $config['file_name'] = $_FILES['files']['name'][$i];
        
                    //Load upload library
                    $this->load->library('upload',$config); 
                    
                    // File upload
                    if($this->upload->do_upload('file')){
                        // Get data about the file
                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];
                        $path = pathinfo(''.$main_folder.'/'.$_SESSION['user_id'].'/'.$filename);
                        $extension = $path['extension'];
                        // Initialize array
                    }
                }
            }
        }

        if ($this->input->post()) {
            $postData = $this->input->post();
            $data = $this->Campaign_model->project_submit($postData,$filename,$type,$id);
            echo json_encode($data);
        }

    }
    public function get_group(){
        $data = $this->Project_model->getContactGroups();
        echo json_encode($data);
    }
    public function show_campaign(){
        if ($this->input->post()) {
            $postData = $this->input->post();
            $id = $postData['campaign'];
            $user = $postData['user'];
            $status = $postData['status'];
            $type = "";
            $timezone = $postData['timezone'];
            $main_folder= "";
            $filename = "";
            $data = $this->Campaign_model->show_campaign($id,$user,$status,$timezone,$type);
            if ($postData['type']=='edit') {

                for ($i=0; $i <count($data) ; $i++) {
                    error_reporting(0);
                    if ($data[$i]->filename!="") {
                       $filename = $data[$i]->filename;
                       $main_folder = 'assets/project_file/'.$data[$i]->created_by.'/';
                       if(is_dir(''.$main_folder)){
                            //open folder
                            if ($open = opendir($main_folder)) {
                                // Read files
                                $file_path = $main_folder.$filename;                        // Check its not folder
                                if(!is_dir($file_path)){
                                    $size = filesize($file_path);
                                    $file = array('name'=>$filename,'size'=>$size,'path'=>$file_path);
                                    array_push($data, $file);
                                }
                            }
                        }else{
                            mkdir(''.$main_folder);
                        }

                    }
                }
            }
            echo json_encode($data);
        }
    }
    public function get_single_campaign(){
        $data = $this->Campaign_model->get_single_campaign($this->input->post('campaign'));
        $result = "";
        if ($data['filename']!="") {
           $filename = $data['filename'];
           $main_folder = 'assets/project_file/'.$data['created_by'].'/';
           if(is_dir(''.$main_folder)){
                //open folder
                if ($open = opendir($main_folder)) {
                    // Read files
                    $file_path = $main_folder.$filename;                        // Check its not folder
                    if(!is_dir($file_path)){
                        $size = filesize($file_path);
                        $file = array('name'=>$filename,'size'=>$size,'path'=>$file_path);
                        $result = $data + $file;
                    }
                }
            }else{
                mkdir(''.$main_folder);
            }

        }else{
            $result = $data;
        }
        echo json_encode($result);
    }
    public function update_status(){
        if ($this->input->post()) {
            $postData = $this->input->post();
            $data = $this->Campaign_model->update_status($postData);
            echo json_encode($data);
        }
    }
    public function delete_campaign(){
        if ($this->input->post()) {
            $postData = $this->input->post();
            $data = $this->Campaign_model->delete_campaign($postData);
            echo json_encode($data);
        }
    }
    public function send_text(){
        if ($this->input->post()) {
            $this->load->library('twilio');
            $postData = $this->input->post();
            $mediaUrl="";
            $campaign  = $this->Campaign_model->get_single_campaign($postData['id']);
            $message= $campaign['message'];
            $filename = $campaign['filename'];
            $created_id = $campaign['created_by'];
            $timezone = $postData['timezone'];
            $contacts = $this->Campaign_model->get_single_number($postData['contact_id']);
            $number = $contacts['number'];
            $group_id = $contacts['group_id'];
            $extension = "";
            $quote = 0;
            $sms_status = "";
            $MMS_SEND= "";
            $main_folder = 'assets/MMS_Images';
            $error_code = "0";
            $medianame = "";
            $total_fee=0;
            $status = 0;
            $credit = $this->Payments_model->total_credit();
            $total_credit = $credit['total_credit'];


            $twil = $this->Admin_model->getConfigs();
            if (!empty($twil['twillio_auth_token'])&&!empty($twil['twillio_account_sid'])&&!empty($twil['twilio_account_number'])) {
                //Twilio
                $sid = $twil['twillio_account_sid'];
                $token = $twil['twillio_auth_token'];
                $twil_number = $twil['twilio_account_number'];
                $twilio = new Client($sid, $token);

                if (!empty($filename)) {
                    // $url = base_url().'assets/project_file/'.$created_id.'/'.$filename;
                    $url = 'https://demo.twilio.com/owl.png';
                    $extension = pathinfo($url, PATHINFO_EXTENSION);
                    $filename = basename($url);
                    $image_data = file_get_contents($url);
                    $new_image_path = "assets/MMS_Images/". rand() . "." . $extension;
                    $file_put = file_put_contents($new_image_path, $image_data);
                    $filename =  basename($new_image_path);
                    $mediaUrl = base_url()."".$new_image_path;
                    $medianame = $filename;
                }

                if (!empty($filename)) {
                    $total_fee = $total_fee+0.04;
                }
                if (!empty($message)) {
                    $total_fee = $total_fee+0.15;
                }

                if ($total_credit>$total_fee) {
                    $data = $this->Campaign_model->send_message($message,$timezone,$medianame,$postData['contact_id'],$quote,$extension,$group_id,$status);
                    if (!empty($medianame)) {
                        // if ($fee>$pay&&$fee!="") {
                            try{
                                // $MMS_SEND = $this->twilio->sms($twil_number, $contact->number,$message);
                                $MMS_SEND = $twilio->messages
                                ->create("+".$number, // to
                                    array(
                                        "body" => "",
                                        "from" => " +".$twil_number,
                                        //Testing Media Url 
                                        "mediaUrl"=> array("https://demo.twilio.com/owl.png")

                                        //If you test localhost please use https
                                        // "mediaUrl" => array($mediaUrl)
                                    )
                                );
                            }catch(Exception $e){
                                $error_code = $e->getCode();
                            }
                        // }
                    }

                    if(!empty($message)){
                        $MMS_SEND = $this->twilio->sms($twil_number,'+'.$number,$message);
                    }

                    if($MMS_SEND->IsError){
                        $status = "";
                        $error_code =$MMS_SEND->ResponseXml->RestException->Code[0];
                    }else{
                        $this->Payments_model->update_balance_credit($total_fee);
                        echo json_encode(true);
                    }
                }
            }else{
                echo json_encode(false);
            }
        }
    }
    public function get_contacts(){
        if ($this->input->post()) {
            $postData = $this->input->post();
            echo json_encode($this->Campaign_model->get_contacts($postData['id']));
        }
    }
}
	