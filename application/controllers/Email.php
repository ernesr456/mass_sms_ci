<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Email extends CI_Controller {

	function __construct()
	{
        parent::__construct();
        date_default_timezone_set('America/Denver');
	}
    public function compose(){
		if($this->session->userdata('user_id')){
			$msg_id = $this->uri->segment(3);
			$data = array(
				'page_description' 	=> 'Compose Message',
				'page' 			   	=> 'compose',
				'page_keyword' 		=> 'Compose Message',
				'page_title' 		=> 'Compose Message',
				'page_head' 		=> 'Compose Message',
				'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
				'user_info'			=> $this->Email_model->get_user_info(),
				'contact_emails'	=> $this->Email_model->get_contact_emails(),
				'msg_data'			=> (isset($msg_id))?$this->Email_model->get_specific_mail($msg_id):'',
			);
			$this->load->view('admin/email/compose_msg', $data);
		}else{
			redirect('login');
		}
	}
	public function sent(){
		if($this->session->userdata('user_id')){
			$data = array(
				'page_description' 	=> 'Sent Messages',
				'page' 			   	=> 'sent',
				'page_keyword' 		=> 'Sent Messages',
				'page_title' 		=> 'Sent Messages',
				'page_head' 		=> 'Sent Messages',
				'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
				'base_url'			=> base_url('email/sent'),
				'total_rows'		=> $this->Email_model->get_emails(0,0,1,1),
				'per_page'			=> 20,
				'uri_segment'		=> 3,
			);
			$this->pagination->initialize($data);
			$page 					= isset($_GET['per_page']) ? $_GET['per_page'] : '';
			$data["links"] 			= $this->pagination->create_links();
			$data["emails"] 		= $this->Email_model->get_emails($data["per_page"], $page, 2, 1);
			$this->load->view('admin/email/view_messages', $data);
		}else{
			redirect('login');
		}
	}
	public function drafts(){
		if($this->session->userdata('user_id')){
			$data = array(
				'page_description' 	=> 'Draft Messages',
				'page' 			   	=> 'draft',
				'page_keyword' 		=> 'Draft Messages',
				'page_title' 		=> 'Draft Messages',
				'page_head' 		=> 'Draft Messages',
				'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
				'base_url'			=> base_url('email/drafts'),
				'total_rows'		=> $this->Email_model->get_emails(0,0,1,2),
				'per_page'			=> 20,
				'uri_segment'		=> 3,
			);
			$this->pagination->initialize($data);
			$page 					= isset($_GET['per_page']) ? $_GET['per_page'] : '';
			$data["links"] 			= $this->pagination->create_links();
			$data["emails"] 		= $this->Email_model->get_emails($data["per_page"], $page, 2, 2);
			$this->load->view('admin/email/view_messages', $data);
		}else{
			redirect('login');
		}
	}
	public function trash(){
		if($this->session->userdata('user_id')){
			$data = array(
				'page_description' 	=> 'Trash Messages',
				'page' 			   	=> 'trash',
				'page_keyword' 		=> 'Trash Messages',
				'page_title' 		=> 'Trash Messages',
				'page_head' 		=> 'Trash Messages',
				'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
				'base_url'			=> base_url('email/trash'),
				'total_rows'		=> $this->Email_model->get_emails(0,0,1,3),
				'per_page'			=> 20,
				'uri_segment'		=> 3,
			);
			$this->pagination->initialize($data);
			$page 					= isset($_GET['per_page']) ? $_GET['per_page'] : '';
			$data["links"] 			= $this->pagination->create_links();
			$data["emails"] 		= $this->Email_model->get_emails($data["per_page"], $page, 2, 3);
			$this->load->view('admin/email/view_messages', $data);
		}else{
			redirect('login');
		}
	}
	public function send_mail_message(){
		if($this->session->userdata('user_id')){
			echo $this->Email_model->send_mail_message() ? 1 : 0;
		}else{ echo 0; }
	}

	public function save_mail_as_draft(){
		if($this->session->userdata('user_id')){
			echo $this->Email_model->save_mail_as_draft() ? 1 : 0;
		}else{ echo 0; }
	}

	public function save_all_as_draft(){
		if($this->session->userdata('user_id')){
			$msg_id = explode(',',$this->input->post('msg_id'));
			echo $this->Email_model->change_mail_status($msg_id, 2) ? 1 : 0;
		}else{ echo 0; }
	}

	public function move_to_trash(){
		if($this->session->userdata('user_id')){
			$msg_id = explode(',',$this->input->post('msg_id'));
			echo $this->Email_model->change_mail_status($msg_id, 3) ? 1 : 0;
		}else{ echo 0; }
	}

	public function delete_permanently(){
		if($this->session->userdata('user_id')){
			$msg_id = explode(',',$this->input->post('msg_id'));
			echo $this->Email_model->delete_msg_permanently($msg_id) ? 1 : 0;
		}else{ echo 0; }
	}

	public function update_mail_sidebox(){
		if($this->session->userdata('user_id')){
			$data['page'] = $this->input->post('page');
			$this->load->view('admin/email/mail_sidebox', $data, false);
		}else{ echo 0; }
	}

	public function save_image(){
		if($this->input->post()){
			$target_path = "./assets/admin/img/email/";
			$email_img = $_POST['email_img'];
			$imgName = 'email_img_'.uniqid().".jpg"; 
			$data 	 = explode(',', $email_img);
			$decoded = base64_decode($data[1]);
			file_put_contents($target_path.$imgName,$decoded); 
			echo $imgName;

		}
	}
}
	