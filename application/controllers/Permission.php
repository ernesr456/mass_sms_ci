<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Denver');
		//$authPermissionID = $this->Permission_model->authPermissionID($this->session->userdata('user_id'));
		//$this->setData('authPermissionID', $authPermissionID);
	}

	public function permission(){
		$data = array(
			'page_description' 	=> 'Permission ',
			'page' 			   	=> 'Permission',
			'page_keyword' 		=> 'Permission ',
			'page_title' 		=> 'Permission ',
			'page_head' 		=> 'Permission',
			'users'				=> $this->Admin_model->getUsers($postData=[]),
			// 'project'           => $this->Campaign_model->show_campaign($id='',$user='',$status='',$timezone=''),
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'usersPermission'	=> $this->Permission_model->getPermissions()

		);
		$this->load->view('admin/permission/permission', $data);
	}
	public function show_user(){
		$postData = "";
		if ($this->input->post()) {
			$postData = $this->input->post();
		}
		echo json_encode($this->Admin_model->getUsers($postData));
	}
	public function update_permission(){
		if ($this->input->post()) {
			echo json_encode($this->Permission_model->update_permission($this->input->post()));
		}
	}
	public function add_permission(){

		$data = array(
			'page_description' 	=> 'Create Permissions ',
			'page' 			   	=> 'Create Permissions',
			'page_keyword' 		=> 'Create Permissions ',
			'page_title' 		=> 'Create Permissions ',
			'page_head' 		=> 'Create Permissions',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'usersPermission'	=> $this->Permission_model->getPermissions()
		);
		$this->load->view('admin/permission/add_permission', $data);
	}
	public function list_permission(){
		// $usersPermission = $this->Permission_model->getPermissions();
		// $this->setData('usersPermission', $usersPermission);
		// $this->render('admin/permission/list_permission', 'Permissions List', array(
		// 	'page_description' => "Permissions List",
		// 	'page_keyword' => "Permissions List",
		// 	'page' => "list_permission",
		// ));

		$data = array(
			'page_description' 	=> 'Permissions List',
			'page' 			   	=> 'Permissions List',
			'page_keyword' 		=> 'Permissions List ',
			'page_title' 		=> 'Permissions List ',
			'page_head' 		=> 'Permissions List',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'usersPermission'	=>  $this->Permission_model->getPermissions()
		);
		$this->load->view('admin/permission/list_permission', $data);
	}
	public function savePermission(){
		print_r($this->input->post());
		echo $this->Permission_model->savePermission();
	}
	public function deletePermission(){
		echo $this->Permission_model->deletePermission();
	}
	public function userDeletePermission(){
		echo $this->Permission_model->userDeletePermission();
	}
	public function userPermission(){
		echo $this->Permission_model->userPermission();
	}
	public function groupPermission(){
		echo $this->Permission_model->groupPermission();
	}
	public function getPermissionID(){
		$permission="";
		$permissionData =  $this->Permission_model->getPermissionID($this->input->post('id'));
		foreach ($permissionData as $s) {
			$permission = json_decode($s['permission']);
		}
		echo json_encode($permission);
	}
}
