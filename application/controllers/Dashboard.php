<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Denver');
	}

	public function index(){
		if($this->session->userdata('is_logged_in')){
			$campaign = $this->Campaign_model->show_campaign($id="",$user="",$status="",$timezone="",$type="");
			$role = "";
			if ($this->session->userdata('user_group')==1) {
				$role = "*";
			}else if($this->session->userdata('user_group')==1){
				$role = "volunteer";
			}else{
				$role = "volunteer";
			}

			$creditBal = $this->Admin_model->getClientCreditBalance($this->session->userdata('user_id'));
			$data = array(
				'page_description' 	=> 'Dashboard page',
				'page' 			   	=> 'dashboard',
				'page_keyword' 		=> 'Dashboard page',
				'page_title' 		=> 'Dashboard page',
				'page_head' 		=> 'Dashboard',
				'credit'			=> $creditBal,
				'campaign'			=> $campaign,
				'authPermissionID'  => $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
				'total_users' 		=> $this->Dashboard_model->totalUser($role),
				'total_messages' 	=> $this->Dashboard_model->totalMessage(),
				'total_clients' 	=> $this->Dashboard_model->totalClients(),
				'total_campaigns'	=> $this->Dashboard_model->totalcampaigns()
			);
			// $this->load->view('admin/dashboard_view', $data);
		}else{
			redirect('login');
		}
		$this->load->view('admin/dashboard_view', $data);
	}
    public function sms_status(){
    	if ($this->input->post()) {
    		$data = $this->Dashboard_model->sms_status($this->input->post('campaign'),$this->session->userdata('user_id'),$this->session->userdata('user_group'));
    		echo json_encode($data);

    	}
    }
}
	