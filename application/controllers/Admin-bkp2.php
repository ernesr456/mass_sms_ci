<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//twilio
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;
use Twilio\Security\RequestValidator;

use PayPal\Api\ItemList;   
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
 

class Admin extends CI_Controller {
	public $_api_context;
    function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Denver');
		$this->load->config('twilio', TRUE);
		$authPermissionID = [];
		if ($this->session->userdata('user_group')!=2) {
			$authPermissionID = $this->Permission_model->authPermissionID($this->session->userdata('user_id'));
		}
		$this->authentication=$authPermissionID;
    }

	public function index(){
		if($this->session->userdata('user_id')){
			$campaign = $this->Campaign_model->show_campaign($id="",$user="",$status="",$timezone="");
			$role = "";
			if ($this->session->userdata('user_group')==1) {
				$role = "*";
			}else if($this->session->userdata('user_group')==1){
				$role = "volunteer";
			}else{
				$role = "volunteer";
			}
			$creditBal = $this->Admin_model->getClientCreditBalance($this->session->userdata('user_id'));
			$data = array(
				'page_description' 	=> 'Dashboard page',
				'page' 			   	=> 'dashboard',
				'page_keyword' 		=> 'Dashboard page',
				'page_title' 		=> 'Dashboard page',
				'page_head' 		=> 'Dashboard',
				'credit'			=> $creditBal,
				'campaign'			=> $campaign,
				'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
				'total_users' 		=> $this->totalUser($role),
				'total_messages' 	=> $this->totalMessage(),
				'total_clients' 	=> $this->totalClients(),
				'total_campaigns'	=> $this->totalcampaigns()
			);
			$this->load->view('admin/dashboard_view', $data);
		}else{
			redirect('login');
		}
	}

	//Conversation Page
	public function conversation(){
		$ses_id = $_SESSION['user_id'];
		$user_group = $_SESSION['user_group'];
		$template = "";
		if (isset($_GET['msg_template'])) {
			$template = $this->Admin_model->fetch_template($_GET['msg_template']);
		}

		$data = array(
			'page_description' 	=> 'Conversation ',
			'page' 			   	=> 'Conversation',
			'page_keyword' 		=> 'Conversation ',
			'page_title' 		=> 'Conversation ',
			'page_head' 		=> 'Conversation',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'contacts'			=> $this->Admin_model->getContacts(),
			'template'			=> $this->Admin_model->get_template($ses_id,$user_group)
		);
		$this->load->view('admin/conversation_view', $data);
	}

	//Message Template Page
	public function template(){
		// $this->render('admin/message_template_view.php', 'Message Template', array(
		// 	'page_description' => "Message Template",
		// 	'page_keyword' => "Message Template",
		// 	'page' => "message_template",
		// ));

		$data = array(
			'page_description' 	=> 'Message Template ',
			'page' 			   	=> 'Message Template',
			'page_keyword' 		=> 'Message Template ',
			'page_title' 		=> 'Message Template ',
			'page_head' 		=> 'Message Template',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
		);
		$this->load->view('admin/message_template/message_template_view', $data);
	}

	public function respond(){
		$this->render('admin/respond', 'Respond', array(
			'page_description' => "Respond",
			'page_keyword' => "Respond",
			'page' => "respond",
		));
	}

	// campaigns
	public function campaigns()
	{
		$data = array(
			'page_description'  => "Campaigns",
			'page_head' 		=> 'Campaigns',
			'page_keyword' 		=> "Campaigns",
			'page' 				=> "campaigns",
			'page_title' 		=> 'Campaigns page',
		);
		$this->load->view('admin/campaigns_view', $data);
	}

	// user profiles
	public function profile(){
		$this->load->library('user_agent');
		$user = $this->Admin_model->getUser($postData=[]);
		// $this->setData('user', $user);

		// $this->render('admin/profile_view', 'Profile', array(
		// 	'page_description' => "Profile",
		// 	'page_keyword' => "Profile",
		// 	'page' => "profile",
		// ));

		$profile = "Profile";

		$data = array(
			'page_description'  => $profile,
			'page_head' 		=> $profile,
			'page_keyword' 		=> $profile,
			'page' 				=> $profile,
			'page_title' 		=> $profile,
			'user'				=> $user,
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
		);
		$this->load->view('admin/profile/profile_view', $data);
	}
		// users
	public function users(){
		$user = "";
		if($_SESSION['user_group']==1){
			$user = "Users";
		}else{
			$user = "Volunteers";
		}

		$data = array(
			'page_description'  => $user,
			'page_head' 		=> $user,
			'page_keyword' 		=> $user,
			'page' 				=> "users",
			'page_title' 		=> $user,
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
		);
		$this->load->view('admin/user/users_view', $data);
	}

	// manage contacts
	public function contacts(){
		$data = array(
			'page_description' 	=> 'Contacts_g',
			'page' 			   	=> 'contacts',
			'page_keyword' 		=> 'Contacts',
			'page_title' 		=> 'Contacts',
			'page_head' 		=> 'Contacts Page',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'contacts'			=> $contacts = $this->Admin_model->getContacts(),
			'groups'			=> $this->Admin_model->getContactGroups(),
			'users'				=> $this->Admin_model->getUsers($postData=[])
		);
		$this->load->view('admin/contacts/contacts_view', $data);
	}

	public function manage_group(){

		if (isset($_GET['id'])){
			$single = $this->Admin_model->getsingleContactGroups($_GET['id']);
			if ($single){
				$this->setData('single', $single);
			}
		}

		if (isset($_GET['did'])){
			$single = $this->Admin_model->deleteContactGroups($_GET['did']);
			redirect('/admin/manage_group');
		}

		$data = array(
			'page_description' 	=> 'Contacts Group',
			'page' 			   	=> 'Contacts Group',
			'page_keyword' 		=> 'Contacts Group',
			'page_title' 		=> 'Contacts Group',
			'page_head' 		=> 'Contacts Group',
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'groups'			=> $this->Admin_model->getContactGroups()
		);
		$this->load->view('admin/contacts/contacts_group_view', $data);
	}
	// // permissions
	public function permissions(){
		$role = $_GET['role'];
		$type = $_GET['type'];
		// $this->setData('users', $users);
		// $this->setData('role', $role);
		// $this->setData('usersPermission', $usersPermission);

		// $this->render('admin/permission_view', $role.' Permissions', array(
		// 	'page_description' => "Permissions",
		// 	'page_keyword' => "Permissions",
		// 	'page' => "permissions_".$type,
		// ));

		$data = array(
			'page_description' 	=> 'Permissions',
			'page' 			   	=> 'Permissions',
			'page_keyword' 		=> 'Permissions',
			'page_title' 		=> 'Permissions',
			'page_head' 		=> 'Permissions Page',
			'users' 			=> $this->Admin_model->getUsersType($type),
			'role' 				=> $role,
			'authPermissionID'	=> $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
			'usersPermission' 	=> $this->Permission_model->getPermissions(),
			'contacts'			=> $this->Admin_model->getContacts(),
		);
		$this->load->view('admin/permission_view', $data);
	}
	public function show_image(){
		$user = $this->Admin_model->getUser($postData[]);
		echo json_encode($user);
	}
	

	public function show_user(){
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $search = $search['value'];
        $role = "";
        if ($this->input->post("role")) {
        	$role = $this->input->post("role");
        }
        $col = 0;
        $dir = "";
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if (isset($_GET['role'])) {
        	echo $_GET['role'];
        }	
        if($dir != "asc" && $dir != "desc"){
            $dir = "desc";
        }
        $valid_columns = array(
            0=>'a.name',
            1=>'a.username',
            2=>'a.email',
            3=>'b.name',
        );

        if(!isset($valid_columns[$col])){
            $order = null;
        }
        else{
            $order = $valid_columns[$col];
        }
        if($order !=null){
            $this->db->order_by($order, $dir);
        }
       
        $groupContacts = $this->Admin_model->show_users($search,$valid_columns,$length,$start,$role);
        $data = array();
        foreach($groupContacts->result() as $rows){
        	$switcher = '';
        	$stat = "";
        	if ($rows->status==0) {
        		$switcher='checked';
        		$stat = 0;
        	}else if($rows->status==2||$rows->status==3||$rows->status==1){
        		$switcher = 'unchecked';
        		$stat = 1;
        	}

            $data[]= array(
                $rows->id,
                ucwords($rows->name),
                $rows->username,
                $rows->user_group,
                $rows->created_by,
                '<label class="switcher switcher-success">
                    <input id="user_stat" data-id='.$rows->id.' data-stat='.$stat.' type="checkbox" class="switcher-input" '.$switcher.'>
                    <input type="hidden" id="stat" value ="" />
                    <span class="switcher-indicator">
                        <span class="switcher-yes">
                            <span class="ion ion-md-checkmark"></span>
                        </span>
                        <span class="switcher-no">
                            <span class="ion ion-md-close"></span>
                        </span>
                    </span>
                </label>',
                '<a href="javascript:void(0)" data-id="'.$rows->id.'" data-stat="'.$stat.'" class="btn icon-btn btn-sm btn-outline-danger" id="update_user" title="Delete">
					<span class="fas fa-trash"></span>
				</a>
				<a href="javascript:void(0)" data-id="'.$rows->id.'" data-target="#update_user_form" data-toggle="modal" class="btn icon-btn btn-sm btn-outline-warning" id="edit_user" title="Delete">
					<span class="fas fa-pencil-alt"></span>
				</a>'
            );     
        }

        $totalGroup = $this->Admin_model->total_users($search,$valid_columns,$role);
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalGroup,
            "recordsFiltered" => $totalGroup,
            "data" => $data
        );
        echo json_encode($output);
        exit();
	}

    public function update_user(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->update_user($postData);
    		echo json_encode($data);
    	}
    }

	public function updateUserStatus(){

		if($this->input->post()){
			$update =  $this->Admin_model->updateUserStatus();
			echo json_encode($update); 
		}
	}
	public function getUsers(){
		if($this->input->post()){
			$postData = $this->input->post();
			$update =  $this->Admin_model->getUsers($postData);
			echo json_encode($update); 
		}
	}

    public function totalUser($role){
    	$query = "";
    	if ($_SESSION['user_group']==1) {
    		$where= "";

    		$this->db->select("COUNT(*) as num");
    		$this->db->from('users u');
    		$this->db->join('users_groups', 'u.user_group = users_groups.id');
    		if($role!="*") {
    			$this->db->where('users_groups.role_slug =',$role);
    		}

    	}else{
    		$query = $this->db->select("COUNT(*) as num")->from('users u')->join('users_groups', 'u.user_group = users_groups.id')->where('u.created_by =',$this->session->userdata('user_id'));
    	}
        $result = $this->db->count_all_results();
        if(isset($result)) return $result;
        return 0;
    }

    public function delete_user(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		echo json_encode($this->Admin_model->delete_user($postData));
    	}
    }
    public function status_user(){
    	
    }

    public function totalcampaigns(){
    	$query = "";
    	$this->db->select("COUNT(*) as num");
    	$query = $this->db->like('start_at', date('d-m-Y'))->get("campaign");
        $result = $query->row();
        return (isset($result))?$result->num:0;
    }

    public function totalClients(){
    	$query = "";
    	$query = $this->db->select("COUNT(*) as num")->get("contacts");
        $result = $query->row();
        return (isset($result))?$result->num:0;
    }

    public function totalMessage(){
    	$query = "";
    	$query = $this->db->select("COUNT(*) as num")->like('sent_time', date('Y-m-d'))->get("conversation");
        $result = $query->row();
        return (isset($result))?$result->num:0;
    }

	// add user
	public function add_user(){
		if($_SESSION['user_group']==1){
			$pagetitle = "Add User";
			$page_description = "Add User";
			$page_keyword = "Add User";
		}else{
			$pagetitle = "Add Volunteer";
			$page_description = "Add Volunteer";
			$page_keyword = "Add Volunteer";
		}

		if (isset($_GET['id'])){
			$single = $this->Admin_model->getsingleUser($_GET['id']);
			$updated = "";
			$addUser = "";
			if ($single){
				$pagetitle = "Update User";
				$page_description = "Update User";
				$page_keyword = "Update User";
				if ($this->input->post()){
					$postdata = $this->input->post();
					$updatecontact = $this->Admin_model->updateUserbyAdmin($_GET['id'], $postdata);
					if ($updatecontact){
						$single = $this->Admin_model->getsingleUser($_GET['id']);
						//$this->setData('updated', 1); // set data if success
						$updated = 1;
					}
					else {
						//$this->setData('updated', false); // set data if not added
						$updated = false;
					}
				}
				//$this->setData('single', $single);
			}
		}
		else{
			if ($this->input->post()){
				$postData = $this->input->post();
				$addUser = $this->Admin_model->addUsers($postData);
				//$this->setData('added', $addUser);
			}

		}
		
		// get all groups
		$groups = $this->Admin_model->getUserGroups();
		//$this->setData('groups', $groups); // set groups data
		// $this->render('admin/add_user_view', $pagetitle, array(
		// 	'page_description' => $page_description,
		// 	'page_keyword' => $page_keyword,
		// 	'page' => "users",
		// ));

		if (isset($_GET['id'])){
			$data = array(
				'page_description' 	=> $page_description,
				'page' 			   	=> 'Users',
				'page_keyword' 		=> $page_keyword,
				'page_title' 		=> $pagetitle,
				'page_head' 		=> 'Users',
				'added'				=> ($addUser) ? $addUser : '',
				'updated'			=> ($updated) ? $updated : '',
				'single'			=> ($single) ? $single : '',
				'groups'			=> $this->Admin_model->getUserGroups()
			);	
		}else{
			$data = array(
				'page_description' 	=> $page_description,
				'page' 			   	=> 'Users',
				'page_keyword' 		=> $page_keyword,
				'page_title' 		=> $pagetitle,
				'page_head' 		=> 'Users',
				'groups'			=> $this->Admin_model->getUserGroups()
			);	
		}
		
		$this->load->view('admin/add_user_view', $data);
	}

    public function updateProfile(){
        if ($this->input->post()){
            $postData = $this->input->post();
            $filename = '';
           	$main_folder = 'assets/profile_img';
            if (!empty($_FILES)) {
               		if(!is_dir($main_folder.'/'.$_SESSION['user_id'])){
						mkdir($main_folder.'/'.$_SESSION['user_id'], 0777, true);
					}
					// Define new $_FILES array - $_FILES['file']
					$_FILES['file']['name'] = $_FILES['files']['name'];
					$_FILES['file']['type'] = $_FILES['files']['type'];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
					$_FILES['file']['error'] = $_FILES['files']['error'];
					$_FILES['file']['size'] = $_FILES['files']['size'];
					// Set preference
					$config['upload_path'] = $main_folder.'/'.$_SESSION['user_id']; 
					$config['allowed_types']        = 'gif|jpg|png';
					$config['max_size'] = '5000'; // max_size in kb
					$config['file_name'] = $_FILES['files']['name'];
					
					//Load upload library

					$this->load->library('upload',$config); 
					// File upload
					$this->upload->initialize($config);
					if($this->upload->do_upload('file')){
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
					}
            }            
            // check the auth
            $update = $this->Admin_model->updateUser($postData,$filename);
            echo json_encode($update);
        }
    }

	public function getContacts(){
		$contacts = $this->Admin_model->getContacts();
		echo json_encode($contacts);
	}

	public function create_contact(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$data = $this->Admin_model->create_contact($postData);
			echo json_encode($data);
		}
	}

	public function update_contact(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$data = $this->Admin_model->update_contact($postData);
			echo json_encode($data);
		}
	}

	public function add_contact(){
		$pagetitle = "Add Contact";
		$page_description = "Add Contact";
		$page_keyword = "Add Contact";
		$added="";
		$updated="";
		if (isset($_GET['id'])){
			$single = $this->Admin_model->getsingleContact($_GET['id']);
			if ($single){
				$pagetitle = "Update Contact";
				$page_description = "Update Contact";
				$page_keyword = "Update Contact";
				if ($this->input->post()){
					$postdata = $this->input->post();
					$updatecontact = $this->Admin_model->updateContact($_GET['id'], $postdata);
					if ($updatecontact){
						$single = $this->Admin_model->getsingleContact($_GET['id']);
						//$this->setData('updated', true); // set data if success
						$updated = true;
					}
					else {
						$this->setData('updated', false); // set data if not added
						$updated = false;
					}
				}
				$this->setData('single', $single);
			}
		}
		else {
			if ($this->input->post()){
				$postdata = $this->input->post();
				$ses_id = $_SESSION['user_id'];

				$addcontact = $this->Admin_model->addContact($postdata);
				if ($addcontact === true){
					//$this->setData('added', true); // set data if success
					$added = true;
				}
				elseif ($addcontact === 1){
					//$this->setData('added', ''); // set data if number exists
					$added = 'exists';
				}
				else {
					//$this->setData('added', false); // set data if not added
					$added = false;
				}
			}
		}
		$data = array(
			'page_description' 	=> $page_description,
			'page' 			   	=> 'Add Contact',
			'page_keyword' 		=> $page_keyword,
			'page_title' 		=> $pagetitle,
			'page_head' 		=> 'Add Contact',
			'added'				=> $added,
			'updated'			=> $updated,
			'groups'			=> $this->Admin_model->getContactGroups()
		);
		$this->load->view('admin/add_contact_view', $data);

	}

	public function getsingleContact(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$data = $this->Admin_model->getsingleContact($postData['id']);
			echo json_encode($data);
		}
	}

	public function email_template(){
		$this->render('admin/contacts/contact_email_template', 'Email Template', array(
			'page_description' => "Email Template",
			'page_keyword' => "Email Template",
			'page' => "email_template",
		));
	}

	public function create_email_template(){

		$pagetitle = "Create Email Template";
		$page_description = "Create Email Template";
		$page_keyword = "Create Email Template";

		if (isset($_GET['id'])){
			$single = $this->Admin_model->getsingleContact($_GET['id']);
			if ($single){
				$pagetitle = "Update Email Template";
				$page_description = "Update Email Template";
				$page_keyword = "Update Email Template";
				if ($this->input->post()){
					$postdata = $this->input->post();
					$updatecontact = $this->Admin_model->updateContact($_GET['id'], $postdata);
					if ($updatecontact){
						$single = $this->Admin_model->getsingleContact($_GET['id']);
						$this->setData('updated', true); // set data if success
					}
					else {
						$this->setData('updated', false); // set data if not added
					}
				}
				$this->setData('single', $single);
			}
		}
		else {
			if ($this->input->post()){
				$postdata = $this->input->post();
				$addcontact = $this->Admin_model->addContact($postdata);
				echo $addcontact;
				if ($addcontact === true){
					$this->setData('added', true); // set data if success
				}
				elseif ($addcontact === 1){
					$this->setData('added', 'exists'); // set data if number exists
				}
				else {
					$this->setData('added', false); // set data if not added
				}
			}
		}

		// get all groups
		$groups = $this->Admin_model->getContactGroups();
		$this->setData('groups', $groups); // set groups data
		$this->render('admin/contacts/add_contact_email_template', $pagetitle, array(
			'page_description' => $page_description,
			'page_keyword' => $page_keyword,
			'page' => "create_email_template",
		));
	}

	public function show_email_templates(){
		$this->db->query("SET sql_mode= ''");
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc")
        {
            $dir = "desc";
        }
        $valid_columns = array(
            0=>'subject',
            1=>'message',
            2=>'filename',
            3=>'file_type',
        );
        if(!isset($valid_columns[$col]))
        {
            $order = null;
        }
        else
        {
            $order = $valid_columns[$col];
        }
        if($order !=null)
        {
            $this->db->order_by($order, $dir);
        }
        
        if(!empty($search))
        {
            $x=0;
            foreach($valid_columns as $sterm)
            {
                if($x==0)
                {
                    $this->db->like("mt.".$sterm,$search);
                }
                else
                {
                    $this->db->or_like("mt.".$sterm,$search);
                }
                $x++;
            }                 
        }
        $this->db->limit($length,$start);
        $sql ="";
        if ($_SESSION['user_group']==1) {
	        $this->db->select('*');
			$this->db->from('message_template mt');
			// $this->db->join('template_file tf', 'mt.id = tf.parent_id', 'left');
			$this->db->group_by("id");
        }else{
	        $this->db->select('*');
			$this->db->from('message_template mt');
			// $this->db->join('template_file tf', 'mt.id = tf.parent_id', 'left');			
			$this->db->where('mt.created_by ='.$_SESSION['user_id']);
			$this->db->group_by("id");
        }

        $users = $this->db->get();
        $data = array();
        $cnt = 1;
        foreach($users->result() as $rows)
        {
        	$switcher="";
        	if ($rows->status=="1") {
        		$switcher="unchecked";
        	}else{
        		$switcher="checked";
        	}

            $data[]= array(
                '<label class="custom-control custom-checkbox px-2 m-0" style="text-align: center;">
                <input type="checkbox" class="custom-control-input" name="selected_contacts" id="selected_contacts" value='.$rows->id.'><span class="custom-control-label"></span>
                </label>',
            	$cnt++,
                $rows->subject,
                '<a href="javascript:void(0)" data-id='.$rows->id.' id="fetch_template">'.$rows->file_name.'</a>',
                '<a href='.base_url().'admin/conversation?msg_template='.$rows->id.' target="_blank" class="btn icon-btn btn-sm btn-outline-info" title="Send Message"><span class="fas fa-envelope"></span></a>
                <a href='.base_url().'admin/add_template?id='.$rows->id.' class="btn icon-btn btn-sm btn-outline-warning" data-id='.$rows->id.' title="Edit Template">
							<span class="far fa-edit"></span>
				</a>
				<a href="javascript:void(0)" class="btn icon-btn btn-sm btn-outline-danger" id="delete_template" data-id='.$rows->id.' title="Delete Template">
							<span class="fas fa-trash"></span>
				</a>
				'
            );     
        }
        $total_users = $this->totalTemplate();
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $total_users,
            "recordsFiltered" => $total_users,
            "data" => $data
        );
        echo json_encode($output);
        exit();
	}

	public function addcontact_group()
	{
		$postdata = $this->input->post();
		$addgroup = $this->Admin_model->addContactGroup($postdata['group_name']);
		if ($addgroup === true){
			redirect('/admin/manage_group?added=true');
		}
		elseif ($addgroup === 1){
			redirect('/admin/manage_group?added=exists');
		}
		else {
			redirect('/admin/manage_group?added=false');
		}
	}

	public function updatecontact_group()
	{
		$postdata = $this->input->post();
		$addgroup = $this->Admin_model->updateContactGroup($postdata['id'], $postdata['group_name']);
		if ($addgroup === true){
			redirect('/admin/manage_group?updated=true');
		}
		elseif ($addgroup === 1){
			redirect('/admin/manage_group?updated=exists');
		}
		else {
			redirect('/admin/manage_group?updated=false');
		}
	}
	
	public function add_credits(){
		if($this->session->userdata('user_id')&&$this->session->userdata('role_slug')=='administrator'){
			$data = array(
				'page_description' 	=> 'Add Credits',
				'page' 			   	=> 'add_credits',
				'page_keyword' 		=> 'Add Credits',
				'page_title' 		=> 'Add Credits',
				'page_head' 		=> 'Add Credits',
			);
			if(isset($_GET['credit_id'])){
				$data['credit'] = $this->Admin_model->get_single_credit($_GET['credit_id']);
			}
			$this->load->view('admin/credits/add_credits_view', $data);
		}else{
			redirect(base_url());
		}
	}

	public function add_update_credits(){
		if($this->session->userdata('user_id')&&$this->session->userdata('role_slug')=='administrator'){
			if($this->input->post()){
				echo $this->Admin_model->add_update_credits() ? 1 : 0;
			}else{ echo 0; }
		}else{ echo 0; }
	}

	public function delete_credit($credit_id=''){
		if($this->session->userdata('user_id')&&$this->session->userdata('role_slug')=='administrator'){
			echo $this->Admin_model->delete_credit($credit_id) ? 1 : 0;
		}else{ echo 0; }
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('/login');
	}

	    /* Get payment settings using ajax */
    public function get_payment_settings(){
        $p_settings = $this->Admin_model->get_site_settings();
        echo $p_settings ? json_encode($p_settings) : 0;
    }

	public function showChat(){
		if ($this->input->post()){
			$postData = $this->input->post();
			$id = $postData['id'];
			$title = $postData['title'];
			$ses_id = $_SESSION['user_id'];
			$user_group = $_SESSION['user_group'];
			$filter = $postData['filter'];

						//Twilio Credentials
			$sid = "";
			$token="";
			$from="";
			$data = '';
			$credentials = $this->db->query('SELECT * FROM site_setting');
			foreach ($credentials->result() as $cred) {
				$token = $cred->twillio_auth_token;
				$sid = $cred->twillio_account_sid;
				$from = $cred->twilio_account_number;
			}
			$twilio = new Client($sid, $token);

			$this->db->select('*')->from('conversation')->where('to_id = '.$id.' AND sms_sid!="" AND status = 0 AND sms_status=""');
			$chat = $this->db->get();
			foreach($chat->result() as $c){
				$messages = $twilio->messages
                   ->read(array(
                              "Sms_sid" => $c->sms_sid
                          ),
                          1
                   );

				foreach ($messages as $record) {
					$this->db->set('sms_status', $record->status);
					$this->db->where('sms_sid', ''.$c->sms_sid);
					$this->db->update('conversation');
				}
			}

			$data = $this->Admin_model->showChat($ses_id,$id,$title,$user_group,$filter);
			echo json_encode($data);
		}
	}

	public function show_template(){
		$draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $type = "";
        if($this->input->post("type")){
            $type = $this->input->post("type");
        }
        $search = $search['value'];
        $col = 0;
        $dir = "";
        $data= array();
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc")
        {
            $dir = "desc";
        }
        $valid_columns = array(
            0=>'subject',
            1=>'message',
            2=>'filename',
        );
        if(!isset($valid_columns[$col]))
        {
            $order = null;
        }
        else
        {
            $order = $valid_columns[$col];
        }
        if($order !=null)
        {
            $this->db->order_by($order, $dir);
        }
 
        $this->db->limit($length,$start);
        $sql = $this->Admin_model->show_template($valid_columns,$search);
        foreach ($sql as $rows) {
        	$file = '';
        	$button = '';
        	if ($rows->filename!="") {
        		$file= '<a href="javascript:void(0)" data-id='.$rows->id.' id="fetch_template" data-target="#"><button type="button" class="btn btn-xs btn-outline-primary waves-effect">Click to view</button></a>';
        	}else{
        		$file = '';
        	}
        	
        	if($type=="conversation"){
        	    $button = '<button type="button" id="select_template" data-id='.$rows->id.' class="btn btn-xs btn-outline-primary">Select</button>';
        	}else{
        	    $button = 
        	    '<a href="javascript:void(0)" id="edit_template" data-target="#addTemplate" data-toggle="modal" class="btn icon-btn btn-sm btn-outline-warning" data-message="'.$rows->message.'" data-filename="'.$rows->filename.'" data-subject="'.$rows->subject.'" data-id='.$rows->id.' title="Edit Template">
							<span class="far fa-edit"></span>
				</a>
				<a href="javascript:void(0)" class="btn icon-btn btn-sm btn-outline-danger" id="delete_template" data-id='.$rows->id.' title="Delete Template">
							<span class="fas fa-trash"></span>
				</a>';
        	}
        	
            $data[]= array(
                // '<label class="custom-control custom-checkbox px-2 m-0" style="text-align: center;">
                // <input type="checkbox" class="custom-control-input" name="selected_contacts" id="selected_contacts" value='.$rows->id.'><span class="custom-control-label"></span>
                // </label>',
                $rows->id,
                $rows->subject,
                $rows->message,
                $file,
                $button
            );
        }

        $total_users = $this->Admin_model->total_template($search,$valid_columns);
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $total_users,
            "recordsFiltered" => $total_users,
            "data" => $data
        );
        echo json_encode($output);
        exit();
	}
    // public function totalTemplate(){
    // 	$query = "";
    // 	if ($_SESSION['user_group']==1) {
    // 		$query = $this->db->select("COUNT(*) as num")->get("message_template");
    // 	}else{
    // 		$query = $this->db->select("COUNT(*) as num")->get_where("message_template" , array('created_by' => $_SESSION['user_id']));
    // 	}
    //     $result = $query->row();
    //     if(isset($result)) return $result->num;
    //     return 0;
    // }
    
    public function add_template(){
    	if (isset($_GET['id'])) {
    		$pagetitle = 'Update Template';
    		$page_description = 'Update Template';
    		$page_keyword = 'Update Template';
    		$page = 'Update Template';
    		$page_head = 'Update Template';
    	}else{
    		$pagetitle = 'Add Template';
    		$page_description = 'Add Template';
    		$page_keyword = 'Add Template';
    		$page = 'Add Template';
    		$page_head = 'Add Template';
    	}

		$data = array(
			'page_description' 	=> $page_description,
			'page' 			   	=> $page,
			'page_keyword' 		=> $page_keyword	,
			'page_title' 		=> $pagetitle,
			'page_head' 		=> $page_head,
			'contacts'			=> $contacts = $this->Admin_model->getContacts()
		);
		$this->load->view('admin/message_template/add_template_view', $data);
    }
	public function save_template(){
		$filename = "";
		$extension = "";
		$postData = "";
		if ($this->input->post()) {
			$postData = $this->input->post();
			
		}
		if (!empty($_FILES)) {
			$data="";
			$main_folder = 'assets/msg_template';

			if(!is_dir(''.$main_folder.'/'.$_SESSION['user_id'])){
				mkdir(''.$main_folder.'/'.$_SESSION['user_id']);
			}
			// Count total files
			$countfiles = count($_FILES['files']['name']);
			//looping all files

			for($i=0;$i<$countfiles;$i++){
				if(!empty($_FILES['files']['name'][$i])){
					// Define new $_FILES array - $_FILES['file']

					$_FILES['file']['name'] = $_FILES['files']['name'][$i];
					$_FILES['file']['type'] = $_FILES['files']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['files']['error'][$i];
					$_FILES['file']['size'] = $_FILES['files']['size'][$i];
					// Set preference
					$config['upload_path'] = ''.$main_folder.'/'.$_SESSION['user_id']; 
					$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|mp4a|mpga|oga|3ga|pdf|3gpp|ac3|pdf|wav|weba|awb|amr|mpeg|mp4|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
					$config['max_size'] = '5000'; // max_size in kb
					$config['file_name'] = $_FILES['files']['name'][$i];
		
					//Load upload library
					$this->load->library('upload',$config); 
					
					// File upload
					if($this->upload->do_upload('file')){
						// Get data about the file
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
						$path = pathinfo(''.$main_folder.'/'.$_SESSION['user_id'].'/'.$filename);
						$extension = $path['extension'];
						// Initialize array
					}
				}
			}
		
		}

		if (!empty($_FILES)||!empty($postData['message'])) {
			$save_template = $this->Admin_model->save_template($postData,$filename,$extension);
			echo json_encode($save_template);
		}else{
			echo json_encode(2);
		}
		
	}
	public function fetch_template(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$id = $postData['id'];
			$data = $this->Admin_model->fetch_template($id);
			//get file

			$file = array();
			$data_array = json_decode(json_encode($data), True);
			$id = $data_array[0]['id'];
			$subject = $data_array[0]['subject'];
			$filename = $data_array[0]['filename'];
			$created_by = $data_array[0]['created_by'];

			$main_folder = 'assets/msg_template/'.$created_by.'/';
			//check folder if exist
			if(is_dir(''.$main_folder)){
				//open folder
				if ($open = opendir($main_folder)) {
					// Read files
					if($filename != '' && $filename != '.' && $filename != '..'){
			          	// File path
			          	$file_path = $main_folder.$filename;			          	// Check its not folder
			          	if(!is_dir($file_path)){
			             	$size = filesize($file_path);
			             	$file = array('name'=>$filename,'size'=>$size,'path'=>$file_path);
			             	array_push($data, $file);

			          	}
			        }
				}
			}else{
				mkdir(''.$main_folder);
			}
			echo json_encode($data);
		}
	}

    public function fetch_message(){
    		$ses_id = $this->session->userdata('user_id');
    		$user_group = $this->session->userdata('user_group');
    		$fetch = $this->Admin_model->fetch_message($ses_id,$user_group);
    		echo json_encode($fetch);
    }

    public function fetch_file(){
	    	if ($this->input->post()) {
	    		$postData = $this->input->post();
	    		$id = $postData['id'];
	    	}
    		$ses_id = $this->session->userdata('user_id');
    		$user_group = $this->session->userdata('user_group');
    		$fetch = $this->Admin_model->fetch_file($id);
    		echo json_encode($fetch);
    }
    public function update_template(){
    	$id = "";
    	$filename = "";
    	$postData = "";
    	$extension = "";
    	$created_by = "";
    	$path = "";
    	$main_folder = 'assets/msg_template';
    	$postData = $this->input->post();
    	$id = $postData['id'];

		$query = $this->db->query('SELECT * from message_template where id = '.$id);
			foreach ($query->result() as $name) {
			if ($name->filename!="") {
				$filename = $name->filename;
				$path = $main_folder.'/'.$name->created_by.'/'.$filename;
			}
			$created_by = $name->created_by;
		}
    	if (isset($_FILES)&&!empty($_FILES)) {
    		unlink($path);
			$ses_id = $this->session->userdata('user_id');
    		$data="";

			if(!is_dir(''.$main_folder.'/'.$_SESSION['user_id'])){
				mkdir(''.$main_folder.'/'.$_SESSION['user_id']);
			}


			// Count total files
			$countfiles = count($_FILES['files']['name']);
			//looping all files
			for($i=0;$i<$countfiles;$i++){
				if(!empty($_FILES['files']['name'][$i])){
					// Define new $_FILES array - $_FILES['file']

					$_FILES['file']['name'] = $_FILES['files']['name'][$i];
					$_FILES['file']['type'] = $_FILES['files']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['files']['error'][$i];
					$_FILES['file']['size'] = $_FILES['files']['size'][$i];
					// Set preference
					$config['upload_path'] = ''.$main_folder.'/'.$created_by; 
					$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|mp4a|mpga|oga|3ga|pdf|3gpp|ac3|pdf|wav|weba|awb|amr|mpeg|mp4|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
					$config['max_size'] = '5000'; // max_size in kb
					$config['file_name'] = $_FILES['files']['name'][$i];
		
					//Load upload library
					$this->load->library('upload',$config); 
					
					// File upload
					if($this->upload->do_upload('file')){
						// Get data about the file
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
						$path = pathinfo(''.$main_folder.'/'.$_SESSION['user_id'].'/'.$filename);
						$extension = $path['extension'];
					}
				}
			}
    	}
    	$update = $this->Admin_model->update_template($postData,$filename,$extension);
    	echo json_encode($update);
    }
    public function delete_temp(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$main_folder = 'assets/msg_template';
    		$data = $postData['data'];
    		$delete= "";
    		for ($i=0; $i <count($data) ; $i++) { 
				$query = $this->db->query('SELECT * from message_template where id = '.$data[$i]);
				foreach ($query->result() as $name) {
					if ($name->filename!="") {
						$filename = $name->filename;
						$path = $main_folder.'/'.$name->created_by.'/'.$filename;
						unlink($path);
					}
					$delete = $this->Admin_model->delete_temp($data[$i]);
				}
    		}
    		echo json_encode($delete);
    	}
    }

	public function showThread(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$data = $this->Admin_model->showThread($postData);
			echo json_encode($data);
		}
	}
	public function showContact(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$title = $postData['title'];
			$ses_id = $_SESSION['user_id'];
			$user_group = $_SESSION['user_group'];
			$value = "";
			if (isset($postData['value'])) {
				$value = $postData['value'];
			}else{
				$value = "";
			}

			$data = $this->Admin_model->showContacts($title,$ses_id,$user_group,$value);
			echo json_encode($data);
		}
	}
	public function showUnread(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$title = $postData['title'];
			$ses_id = $_SESSION['user_id'];
			$user_group = $_SESSION['user_group'];

			$value = "";
			if (isset($postData['value'])) {
				$value = $postData['value'];
			}else{
				$value = "";
			}
			$data = $this->Admin_model->showUnread($title,$ses_id,$user_group,$value);
			echo json_encode($data);
		}
	}

	public function update_conversation(){
		return true;
	}
	
	public function mms_sender(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			$ses_id = $_SESSION['user_id'];
			$group_id = $_SESSION['user_group'];
			$qoute = $postData['qoute'];
			$message = $postData['message'];
			$main_folder = 'assets/MMS_Images';
			$medianame = '';
			$mediaUrl='';
			$MMS_SEND = '';
			$extension = '';
			$name = '';
			$group_id= "";
			$sms_sid="";
			$error_code = "";
			$sms_status = "";
			$pay = "";
			$filename = "";
			//Text Mess=age = 1credit MMS = 3credit
			$fee="";

			$count_msg = strlen($message);
			
			//get credit
			$this->db->select('*');
			$this->db->from('account_credits ac');
			if ($this->session->userdata('user_group')==2) {
				$this->db->where('user_id ='.$this->session->userdata('user_id'));
			}else if($this->session->userdata('user_group')==3){
				echo "a";
				$this->db->where('user_id = (SELECT created_by from users where id='.$this->session->userdata('user_id').')');
			}

			$credit = $this->db->get();

			foreach($credit->result() as $credit){
				$fee = $credit->credit_balance;
			}

			$user = $this->db->query('SELECT * FROM users where id ='.$_SESSION['user_id']);
			foreach ($user->result() as $users) {
				$name = $users->name;
			}

			//Twilio Credentials
			$sid = "";
			$token="";
			$from="";
			$data = '';
			$credentials = $this->db->query('SELECT * FROM site_setting');
			foreach ($credentials->result() as $cred) {
				$token = $cred->twillio_auth_token;
				$sid = $cred->twillio_account_sid;
				$from = $cred->twilio_account_number;
			}

			$twilio = new Client($sid, $token);
			//get contact information
			if ($postData['title']=='single') {
				$query = $this->db->query('SELECT *,(SELECT name from users where cstatus!=2 AND id = (SELECT created_by from contacts where id = '.$postData['id'].')) as names,(SELECT timezone from users where id =contacts.created_by) as timezone from contacts where cstatus!=2 AND cstatus!=0 AND id = '.$postData['id'].'');
				$group_id = 0;

			}else{
				$group_id = $postData['id'];
				$query = $this->db->query('SELECT id,number,group_id from contacts where cstatus!=2 OR cstatus!=0 AND group_id ='.$postData['id'].'');
			}
			foreach ($query->result() as $contact) {
				$number = preg_replace('/[^A-Za-z0-9\-]/', '', $contact->number);
				$status = 0;

				if (isset($postData['qoute'])&&$postData['qoute']!="") {
					$query = $this->db->query('SELECT * FROM conversation where id='.$postData['qoute']);
					foreach ($query->result() as $key) {
						$mediaUrl = '';

						$message_qoute = $key->message;
						$filename = $key->filename;
						if ($message_qoute!="") {
							$body = ucwords($name).", replied to your: \n".$message."\n";
						}
						if (isset($filename)) {
							$mediaUrl = base_url().$main_folder.'/'.$key->from_id.'/'.$key->filename;
						}
						$count1 = strlen($message);
						if ($body!="") {
							$pay= $count1 * 0.006666666666666666666666;
						}
						if ($key->filename!="") {
							$pay = $pay + 3;
						}

						if ($key->filename!="") {
							if ($fee>$pay) {
								
								$MMS_SEND = $twilio->messages->create("+".$number, // to
									array(
										"body" => $body,
										"from" => "	+".$from,
										//Testing if local
										// "mediaUrl"=> array("https://demo.twilio.com/owl.png")
										//Live
										"mediaUrl" => array("".$mediaUrl)
									)
								);
							}else{
								return 2;
							}
						}else if($message_qoute!=""){
							if ($fee>$pay) {
								$MMS_SEND = $twilio->messages->create("+".$number, // to
									array(
										"body" => $body,
										"from" => "	+".$from,
									)
								);
							}else{
								echo json_encode(2);
							}
						}
						if ($MMS_SEND) {
							$this->db->set('credit_balance', 'credit_balance-'.$pay, FALSE);
						}
					}
				}

				//Msg Template
				if (isset($postData['file'])) {
					$pay= $count_msg * 0.006666666666666666666666;
					$obj_file = json_decode($postData['file']);
					$count = count($obj_file);
					for($i=0;$i<$count;$i++){
						if(!is_dir(''.$main_folder.'/'.$contact->id)){
							mkdir(''.$main_folder.'/'.$contact->id);
						}
						if (isset($obj_file[$i]->path)) {
							$path = $obj_file[$i]->path;
							$extension = pathinfo($path, PATHINFO_EXTENSION);

							// $url = 'https://demo.twilio.com/owl.png';
							$url = $path;
							$filename = basename($url);
							$image_data = file_get_contents($url);
							$new_image_path = "assets/MMS_Images/".$contact->id."/" . rand() . "." . $extension;
							$file_put = file_put_contents($new_image_path, $image_data);
							$filename =  basename($new_image_path);
							$mediaUrl = base_url()."".$new_image_path;
							$medianame = $filename;
							if ($mediaUrl!="") {
								$pay = $pay + 3;
							}
						}
					}
					$body = "Send From: ".ucwords($name)."\n".$message;
				}

				//forward message
				if (isset($postData['forward_message'])) {
					$pay= $count_msg * 0.006666666666666666666666;
					$qoute = 0;
					$body = 
					$query = $this->db->query('SELECT * FROM conversation where id='.$postData['chat_id']);
					foreach ($query->result() as $convo) {

						if ($convo->filename!="") {
							if(!is_dir(''.$main_folder.'/'.$postData['id'])){
								mkdir(''.$main_folder.'/'.$postData['id']);
							}
							$path = base_url().''.$main_folder.'/'.$postData['contact_id'].'/'.$convo->filename;
							// $url = 'https://demo.twilio.com/owl.png';
							$url = $path;
							$extension = pathinfo($url, PATHINFO_EXTENSION);
							$filename = basename($url);
							$image_data = file_get_contents($url);
							$new_image_path = "assets/MMS_Images/".$postData['id']."/" . rand() . "." . $extension;
							$file_put = file_put_contents($new_image_path, $image_data);
							$filename =  basename($new_image_path);
							$mediaUrl = base_url()."".$new_image_path;
							$medianame = $filename;

							$body = "Send From: ".ucwords($name)."\n".$message;
							if ($mediaUrl!="") {
								$pay = $pay + 3;
							}
						}
					}
				}
				//MMS Media URL
				if(isset($_FILES)&&!empty($_FILES)){
					$pay= $count_msg * 0.006666666666666666666666;
					$data = array();
					if(!is_dir(''.$main_folder.'/'.$contact->id)){
						mkdir(''.$main_folder.'/'.$contact->id);
					}
					// Count total files
					$countfiles = count($_FILES['files']['name']);
					// looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){
							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];
							// Set preference
							$config['upload_path'] = ''.$main_folder.'/'.$contact->id; 
							$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|pdf|mp4a|mp4|mpga|oga|3ga|3gpp|ac3|wav|weba|awb|amr|mpeg|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = $_FILES['files']['name'][$i];
							
							//Load upload library

							$this->load->library('upload',$config); 
							// File upload
							$this->upload->initialize($config);

							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];
								$path = pathinfo(''.$main_folder.'/'.$contact->id.'/'.$filename);
								$extension = $path['extension'];
								// Initialize array
								$data['filenames'][] =$filename;
								$mediaUrl = base_url().$main_folder.'/'.$contact->id.'/'.$filename;
								$medianame = $filename;
								$pay = $pay+3;
							}
						}
					}
					$body = "Send From: ".ucwords($name)."\n".$message;
				}

				if (!empty($filename)) {
					if ($fee>$pay&&$fee!="") {
						$MMS_SEND = $twilio->messages
						->create("+".$number, // to
							array(
								"body" => "Send From: ".ucwords($name)."\n".$message,
								"from" => "	+".$from,
								//Testing Media Url 
								// "mediaUrl"=> array("https://demo.twilio.com/owl.png")

								//If you test localhost please use https
								"mediaUrl" => array("".$mediaUrl)
							)
						);
					}
				}else if(!empty($message)){
					
					if ($fee>$pay&&$fee!="") {
						try{
							$MMS_SEND = $twilio->messages->create("+".$number, // to
								array(
									"body" => $body,
									"from" => "	+".$from,
								)
							);
						}catch(Exception $e){
							$error_code = $e->getCode();
						    // echo $e->getCode() . ' : ' . $e->getMessage()."<br>";
						}
					}
				}
				if ($fee>$pay&&$fee!="") {
					$data = $this->Admin_model->mmsSender($postData['message'],$contact->timezone,$medianame,$contact->id,$ses_id,$sms_sid,$qoute,$extension,$group_id,$sms_status,$status);
					if ($data) {
						echo json_encode(true);
					}
				}else{
					echo json_encode(false);
				}

				if ($MMS_SEND) {
					$this->db->set('sms_sid', $MMS_SEND->sid);
					$this->db->where('id', $data);
					$this->db->update('conversation'); 

					$this->db->set('credit_balance', 'credit_balance-'.$pay, FALSE);
					if ($this->session->userdata('user_group')==2||$this->session->userdata('user_group')==1){
						$this->db->where('user_id ='.$this->session->userdata('user_id'));
					}else if($this->session->userdata('user_group')==3){
						$this->db->where('user_id = (SELECT created_by from users where id='.$this->session->userdata('user_id').')');
					}
					$this->db->update('account_credits');
				}else{
					if ($error_code==21211) {
						$this->db->set('status','3', FALSE);
						$this->db->where('id', $data);
						$this->db->update('conversation'); 
					}else if ($error_code==21212||$error_code = 204204||$error_code=20003) {
						$this->db->set('status','2', FALSE);
						$this->db->where('id', $data);
						$this->db->update('conversation'); 
					}
				}
			}
		}
	}

	public function settings(){
		if($this->session->userdata('user_id')&&$this->session->userdata('user_group')==1){
			$data = array(
				'page_description' 	=> 'API Settings',
				'page' 			   	=> 'site_settings',
				'page_keyword' 		=> 'API Settings',
				'page_title' 		=> 'API Settings',
				'page_head' 		=> 'API Settings',
				'settings'			=> $this->Admin_model->get_site_settings()
			);
			$this->load->view('admin/settings/site_settings', $data);
		}else{
			redirect(base_url());
		}
	}

	
    public function save_site_settings(){
		if($this->session->userdata('user_id')&&$this->session->userdata('role_slug')=='administrator'){
			echo $this->Admin_model->save_site_settings() ? 1 : 0;
		}else{ echo 0; }
	}
	
    public function all_count_message(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->all_count_message($postData);
    		echo json_encode($data);

    	}
    }

    public function sms_status(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$ses_id = $_SESSION['user_id'];
    		$group_id = $_SESSION['user_group'];
    		$data = $this->Admin_model->sms_status($postData,$ses_id,$group_id);
    		echo json_encode($data);

    	}
    }
    public function get_percent_reply(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$ses_id = $_SESSION['user_id'];
    		$data = $this->Admin_model->get_percent_reply($postData,$ses_id);
    		echo json_encode($data);

    	}
    }
    public function get_reply_status(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$ses_id = $_SESSION['user_id'];
    		$data = $this->Admin_model->get_reply_status($postData,$ses_id);
    		echo json_encode($data);
    	}
    }

    public function deactivate_user(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->deactivate_user($postData);
    		echo json_encode($data);
    	}
    }
    public function delete_contact(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->delete_contact($postData);
    		echo json_encode($data);
    	}
	}
	public function restore_contact(){
		if($this->session->userdata('user_id')&&$this->session->userdata('user_group')==1){
			if($this->input->post()) {
				echo $this->Admin_model->restore_contact($this->input->post('id')) ? 1 : 0;
			}
		}else{ echo 0; }
    }
    public function delete_single_contact(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->delete_single_contact($postData);
    		echo json_encode($data);
    	}
    }

    public function activate_contact(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->activate_contact($postData);
    		echo json_encode($data);
    	}
    }
    public function do_not_send(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->do_not_send($postData);
    		echo json_encode($data);
    	}
    }

    public function contact_msg_stat(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->contact_msg_stat($postData);
    		echo json_encode($data);
    	}
    }
    public function add_tags(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->add_tags($postData);
    		echo json_encode($data);
    	}
    }
    public function transaction($uid=''){
    	if ($this->session->userdata('user_group')!=1) {
    		$uid = $this->session->userdata('user_id');
    	} 
    	$name = "Transaction";
    	if ($this->session->userdata('user_group')!=1||$uid!='') {
    		$query = $this->db->query("SELECT name from users where id =".$uid);
			foreach ($query->result() as $row)
			{
				$name = ucwords($row->name);
			}
    	}

		$data = array(
			'page_description' 	=> 'Transactions Page',
			'page' 			   	=> 'transaction',
			'page_keyword' 		=> 'Transactions Page',
			'page_title' 		=> 'Transactions Page',
			'page_head' 		=> 'Transactions',
			'total_payment'		=> $this->Admin_model->total_payment($uid),
			'transaction'		=> $this->Admin_model->get_user_transactions($uid),
		);
		$this->load->view('admin/transaction_view', $data);
    }
    public function listContacts(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();

    		if ($postData['contact']=='single') {
		        $valid_columns = array(
		            0=>'b.name',
		            1=>'d.group_name',
		            2=>'a.email',
		            3=>'a.firstName',
		            4=>'a.lastName',
		            5=>'a.number'
		        );
		    }else{
		        $valid_columns = array(
		            0=>'b.name',
		            1=>'a.group_name',
		            2=>'a.tags',
		        );
		    }
			$data = $this->Admin_model->listContacts($postData,$valid_columns);
			echo json_encode($data);
    	}

    }
    public function show_conversation(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
			$data = $this->Admin_model->show_conversation($postData);
			echo json_encode($data);
    	}
    }

    public function show_new_message(){
    	$limit = $this->input->post("limit");
    	$data = $this->Admin_model->show_new_message($limit);
		echo json_encode($data);
    }
    public function update_message(){
    	if ($this->input->post()&&$this->session->userdata('user_group')!=1) {
    		$postData = $this->input->post();
    		echo json_encode($this->Admin_model->update_message($postData['id']));
    	}
    }
    
    public function send_message(){
    	$this->load->library('twilio');

    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		// print_r($postData);
			$group = $postData['group'];
			//Message Body
			$body = '';
			//Main Folder for file in message
			$main_folder = 'assets/MMS_Images';
			//Message contain
			$message = $postData['message'];
			//Message ID for Quote
			$quote = $postData['quote'];
			$filename = "";
			$extension = "";
			$status = 0;
			$sid = "";
			$token= "";
			$twil_number = "";
			$number ="";
			$MMS_SEND="";
			$total_fee = 0;
			$data= "";
			$total_credit = 0;
			$credits = $this->Payments_model->total_credit();
			$total_credit = $credits['total_credit'];
			$twil = $this->Admin_model->getConfigs();
			if (!empty($twil['twillio_auth_token'])&&!empty($twil['twillio_account_sid'])&&!empty($twil['twilio_account_number'])) {
				//Twilio
				$sid = $twil['twillio_account_sid'];
				$token = $twil['twillio_auth_token'];
				$twil_number = $twil['twilio_account_number'];
				$twilio = new Client($sid, $token);

				//MMS Media URL
				if(isset($_FILES)&&!empty($_FILES)){
					$data = array();
					if(!is_dir(''.$main_folder)){
						mkdir(''.$main_folder);
					}
					// Count total files
					$countfiles = count($_FILES['files']['name']);
					// looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){
							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];
							// Set preference
							$config['upload_path'] = ''.$main_folder; 
							$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp|au|pdf|mp4a|mp4|mpga|oga|3ga|3gpp|ac3|wav|weba|awb|amr|mpeg|qt|webm|3gp|3g2|3gpp-tt|h261|h263|h264|vcf|csv|rtf|rtx|ics';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = $_FILES['files']['name'][$i];
							
							//Load upload library

							$this->load->library('upload',$config); 
							// File upload
							$this->upload->initialize($config);

							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];
								$path = pathinfo(''.$main_folder.'/'.$filename);
								$extension = $path['extension'];
								// Initialize array
								$data['filenames'][] =$filename;
								$mediaUrl = base_url().$main_folder.'/'.$filename;
								$medianame = $filename;
							}
						}
					}

				}

				//forward message
				if (isset($postData['forward_message'])) {
					$id = $postData['id'];
					$forward = $this->Admin_model->conversation_thread($id);
					if (!empty($forward['filename'])) {
						$filename = $forward['filename'];
					}
				}

				//Msg Template
				if (isset($postData['file'])) {
					$obj_file = json_decode($postData['file']);
					for($i=0;$i<count($obj_file);$i++){
						$filename = $obj_file[$i]->name;
						$mediaUrl = $obj_file[$i]->path;
					}
				}
				$this->db->select('name')->from('users')->where('id='.$this->session->userdata('user_id'));
				foreach ($this->db->get()->result_array() as $row){
					$name = $row['name'];
				}


				$this->db->select('*');
				$group=='single'? $this->db->from('contact')->where('id=', $postData['contact'])
				:$this->db->from('contacts_group a')->join('contact b','a.gid = b.group_id','left')->where('gid=', $postData['contact']);
				foreach ($this->db->get()->result() as $contact) {
					$body = "Send From: ".ucwords($name)."\n".$message;
					//Quote
					if (!empty($postData['quote'])) {
						$quote = $this->Admin_model->conversation_thread($quote);
						$quote_message = $quote['message'];
						$quote_file = $quote['filename'];
					 	$body = ucwords($name).", Quoted Message: \n".$quote_message."\n";

						if (!empty($quote_file)) {
							$mediaUrl = base_url().$main_folder.'/'.$quote['from_id'].'/'.$quote_file;
							try{
								$MMS_SEND = $this->twilio->sms($twil_number, $contact->number,$quote_message);
								// $MMS_SEND = $twilio->messages
								// ->create("+".$contact->number, // to
								// 	array(
								// 		"body" => $quote_message,
								// 		"from" => "	+".$twil_number,
								// 		//Testing Media Url 
								// 		// "mediaUrl"=> array("https://demo.twilio.com/owl.png")

								// 		//If you test localhost please use https
								// 		"mediaUrl" => array($mediaUrl)
								// 	)
								// );
							}catch(Exception $e){
								$error_code = $e->getCode();
							}
						}else if (!empty($quote_message)) {
							try{
								// $MMS_SEND = $twilio->messages->create("+".$contact->number, // to
								// 	array(
								// 		"body" => $quote_message,
								// 		"from" => "	+".$twil_number,
								// 	)
								// );
							}catch(Exception $e){
								$error_code = $e->getCode();
							}
						}
					}
					if (!empty($filename)) {
						$total_fee = $total_fee+0.04;
					}
					if (!empty($message)) {
						$total_fee = $total_fee+0.15;
					}

					if ($total_credit>$total_fee||$this->session->userdata('user_group')==1) {
						$data = $this->Admin_model->save_message($postData,$filename,$contact->id,$contact->group_id,$quote,$extension,$status);
						if (!empty($filename)) {
							// if ($fee>$pay&&$fee!="") {
								try{
									// $MMS_SEND = $this->twilio->sms($twil_number, $contact->number,$message);
									$MMS_SEND = $twilio->messages
									->create("+".$contact->number, // to
										array(
											"body" => "",
											"from" => "	+".$twil_number,
											//Testing Media Url 
											// "mediaUrl"=> array("https://demo.twilio.com/owl.png")

											//If you test localhost please use https
											"mediaUrl" => array($mediaUrl)
										)
									);
								}catch(Exception $e){
									$error_code = $e->getCode();
								}
							// }
						}

						if(!empty($message)){
							try{
								$MMS_SEND = $this->twilio->sms($twil_number,'+'.$contact->number,$message);
								// $MMS_SEND = $twilio->messages->create("+".$contact->number, // to
								// 	array(
								// 		"body" => $message,
								// 		"from" => "	+".$twil_number,
								// 	)
								// );
							}catch(Exception $e){
								$error_code = $e->getCode();
							    // echo $e->getCode() . ' : ' . $e->getMessage()."<br>";
							}
						}

						if($MMS_SEND->IsError){
							$status = "";
							$error_code =$MMS_SEND->ResponseXml->RestException->Code[0];
							if ($error_code==21211) {
								$status = 3;
							}else if ($error_code==21212||$error_code = 204204||$error_code=20003) {
								$status = 2;
							}
							$this->Admin_model->update_sms_status($data,$status);
						}else if($this->session->userdata('user_group')!=1){
							$this->Payments_model->update_balance_credit($total_fee);
						}
					}else{
						$data = 2;
					}

				}
				if ($data) {
					echo json_encode($data);
				}else if($data==2){
					echo json_encode(2);
				}
			}else{
				echo json_encode(false);
			}
    	}
    }

    public function show_groupContacts(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc")
        {
            $dir = "desc";
        }
        $valid_columns = array(
            0=>'group_name',
            1=>'username',
        );

        if(!isset($valid_columns[$col]))
        {
            $order = null;
        }
        else
        {
            $order = $valid_columns[$col];
        }
        if($order !=null)
        {
            $this->db->order_by($order, $dir);
        }
        
        if(!empty($search))
        {
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }

        $groupContacts = $this->Admin_model->show_groupContacts($search,$valid_columns,$length,$start);
        $data = array();
        foreach($groupContacts->result() as $rows)
        {
        	$switcher = '';
        	if ($rows->status==1) {
        		$switcher='checked';
        	}else{
        		$switcher = 'unchecked';
        	}

        	if ($this->session->userdata('user_group')!=3) {
	            $data[]= array(
	                $rows->gid,
	                $rows->group_name,
	                $rows->username,
	            	'<label class="switcher switcher-success">
	                    <input id="groupContacts_status" data-id='.$rows->gid.' data-stat='.$rows->status.' type="checkbox" class="switcher-input" '.$switcher.'>
	                    <input type="hidden" id="stat" value ="" />
	                    <span class="switcher-indicator">
	                        <span class="switcher-yes">
	                            <span class="ion ion-md-checkmark"></span>
	                        </span>
	                        <span class="switcher-no">
	                            <span class="ion ion-md-close"></span>
	                        </span>
	                    </span>
	                </label>'
	                ,
	                '<a href="javascript:;" id="edit_groupContacts" data-name="'.$rows->group_name.'" data-id="'.$rows->gid.'" data-target="#addContactGroup" data-toggle="modal" class="btn icon-btn btn-sm btn-outline-warning" title="Edit">
								<span class="far fa-edit"></span>
					</a>
					<a href="javascript:void(0)" data-id="'.$rows->gid.'" class="btn icon-btn btn-sm btn-outline-danger" id="delete_groupContacts" title="Delete">
								<span class="fas fa-trash"></span>
					</a>',
	            );
	        }else{
	        	$data[]= array(
	                $rows->gid,
	                $rows->group_name,
	            	'<label class="switcher switcher-success">
	                    <input id="groupContacts_status" data-id='.$rows->gid.' data-stat='.$rows->status.' type="checkbox" class="switcher-input" '.$switcher.'>
	                    <input type="hidden" id="stat" value ="" />
	                    <span class="switcher-indicator">
	                        <span class="switcher-yes">
	                            <span class="ion ion-md-checkmark"></span>
	                        </span>
	                        <span class="switcher-no">
	                            <span class="ion ion-md-close"></span>
	                        </span>
	                    </span>
	                </label>'
	                ,
	                '<a href="javascript:;" id="edit_groupContacts" data-name="'.$rows->group_name.'" data-id="'.$rows->gid.'" data-target="#addContactGroup" data-toggle="modal" class="btn icon-btn btn-sm btn-outline-warning" title="Edit">
								<span class="far fa-edit"></span>
					</a>
					<a href="javascript:void(0)" data-id="'.$rows->gid.'" class="btn icon-btn btn-sm btn-outline-danger" id="delete_groupContacts" title="Delete">
								<span class="fas fa-trash"></span>
					</a>',
	            );	
	        }   
        }

        $totalGroup = $this->Admin_model->totalGroup($search,$valid_columns);
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalGroup,
            "recordsFiltered" => $totalGroup,
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }
    public function totalGroup()
    {
        $query = $this->db->select("COUNT(*) as num")->get("contacts_group");
        $result = $query->row();
        if(isset($result)) return $result->num;
        return 0;
    }

    public function delete_groupContacts(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$delete = $this->Admin_model->delete_groupContacts($postData);
    		echo json_encode($delete);
    	}
    }
    public function groupContacts_status(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$delete = $this->Admin_model->groupContacts_status($postData);
    		echo json_encode($delete);
    	}
    }
    public function add_contactGroup(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$add = $this->Admin_model->add_contactGroup($postData);
    		echo json_encode($add);
    	}
    }

    public function show_contacts(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $search = $search['value'];
        $group = $this->input->post("group");
        $users = $this->input->post("users");        
        $cstatus = $this->input->post("status");
        echo $users;
        $col = 0;
        $dir = "";
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc"){
            $dir = "desc";
        }
        $valid_columns = array(
            0=>'firstName',
            1=>'lastName',
            2=>'number',
            3=>'a.email',
            4=>'group_name',
            5=>'name'
        );

        if(!isset($valid_columns[$col])){
            $order = null;
        }
        else{
            $order = $valid_columns[$col];
        }
        if($order !=null){
            $this->db->order_by($order, $dir);
        }
        
        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $groupContacts = $this->Admin_model->show_contacts($search,$valid_columns,$length,$start,$group,$users,$cstatus);
        $data = array();
        foreach($groupContacts->result() as $rows){
        	$stat = "";
			$switcher = ($rows->cstatus==1)?
			'<a href="javascript:void(0)" class="badge badge-success">Active</a>':(($rows->cstatus==0)?
				'<a href="javascript:void(0)" class="badge badge-danger">Deleted</a>':((($rows->cstatus==2))?
					'<a href="javascript:void(0)" class="badge badge-danger">Deactivated</a>':'<a href="javascript:void(0)" class="badge badge-danger">Opt-Out</a>'));
			
			$dr_button = ($rows->cstatus)?
			'<a href="javascript:void(0)" data-id="'.$rows->id.'" class="btn icon-btn btn-sm btn-outline-danger" id="delete_contact" title="Delete"><span class="fas fa-trash"></span></a>':
			'<a href="javascript:void(0)" onclick="restoreContact('.$rows->id.')" class="btn icon-btn btn-sm btn-outline-primary" title="Restore"><span class="fas fa-redo"></span></a>';

        	if ($this->session->userdata('user_group')==1) {
        		$data[]= array(
	                $rows->id,
	                ucwords($rows->firstName),
	                ucwords($rows->lastName),
	                '+'.$rows->number,
	                $rows->email,
	                $rows->group_name,
	                ucwords($rows->user_name),
	            	$switcher,
	                '<a href="javascript:;" id="edit_contact" data-target="#updateContact" data-toggle="modal" data-lname="'.$rows->lastName.'" data-fname="'.$rows->firstName.'" data-id="'.$rows->id.'" class="btn icon-btn btn-sm btn-outline-warning" title="Edit"> <span class="far fa-edit"></span> </a>
					'.$dr_button,
	            );   
        	}else{
        		$data[]= array(
	                $rows->id,
	                ucwords($rows->firstName),
	                ucwords($rows->lastName),
	                '+'.$rows->number,
	                $rows->email,
	                $rows->group_name,
	            	$switcher,
	                '<a href="javascript:;" id="edit_contact" data-target="#updateContact" data-toggle="modal" data-lname="'.$rows->lastName.'" data-fname="'.$rows->firstName.'" data-id="'.$rows->id.'" class="btn icon-btn btn-sm btn-outline-warning" title="Edit"><span class="far fa-edit"></span></a>
					<a href="javascript:void(0)" data-id="'.$rows->id.'" class="btn icon-btn btn-sm btn-outline-danger" id="delete_contact" title="Delete"><span class="fas fa-trash"></span></a>',
	            );   
        	}
        }

        $totalGroup = $this->Admin_model->total_contacts($search,$valid_columns,$group,$users,$cstatus);
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalGroup,
            "recordsFiltered" => $totalGroup,
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    public function submitContact(){
    	if ($this->input->post()) {
    		echo $this->Admin_model->submitContact();
    	}
    }

    public function contact_status(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		$data = $this->Admin_model->contact_status($postData);
    		echo json_encode($data);
    	}
    }

    public function conversation_thread(){
    	if ($this->input->post()) {
    		$postData = $this->input->post();
    		echo json_encode($this->Admin_model->conversation_thread($postData['id']));
    		
    	}
    }
    public function myCredit(){
    	echo json_encode($this->Admin_model->myCredit());
    }
	
	public function importContacts(){
		//import contacts
        $this->load->library('CSVReader');
        $result="";
        if (!empty($_FILES)) {
        	for($i=0;$i<count($_FILES['files']['tmp_name']);$i++){
	        	$result =   $this->csvreader->parse_file($_FILES['files']['tmp_name'][$i]);
	        }
        }
      	$firstName = ""; $lastName = ""; $phone = ""; $email = ""; 
		if($result){
			foreach ($result as $key => $value) {
				$array_keys = array_change_key_case($value,CASE_LOWER);
				foreach ($array_keys as $k => $v) {
					if ((strpos($k, 'first') !== false || strpos($k, 'first name') !== false || strpos($k, 'firstname') !== false ||  strpos($k, 'fname') !== false || strpos($k, 'fullname') !== false || strpos($k, 'firstn') !== false)&&$v!=='') {
						$firstName = $v;
					} else if ((strpos($k, 'last') !== false || strpos($k, 'last name') !== false ||  strpos($k, 'lastname') !== false ||  strpos($k, 'lname') !== false || strpos($k, 'lastn') !== false)&&$v!=='') {
						$lastName  = $v;
					}else if ((strpos($k, 'phone') !== false || strpos($k, 'number') !== false ||  strpos($k, 'phone number') !== false ||  strpos($k, 'phonenumber') !== false || strpos($k, 'mobile number') !== false || strpos($k, 'mobilenumber') !== false || strpos($k, 'cellphone') !== false || strpos($k, 'cell phone') !== false)&&$v!=='') {
						$phone  = $v;
					} else if ((strpos($k, 'email') !== false || strpos($k, 'email address') !== false ||  strpos($k, 'emailaddress') !== false)&&$v!=='') {
						$email  = $v;
					}else{ /* Do Nothing */}
				}
				if($firstName || $lastName || $phone || $email){
					$p = preg_replace('#[^\w()/.%\-&]#',"", $phone);
					$is_exist = $this->Admin_model->check_contact_exist('1'.$p, $email, 0);
					if(!$is_exist){
						$cont_list = array(
							'firstName' => ucwords(strtolower($firstName)),
							'lastName' 	=> ucwords(strtolower($lastName)),
							'number' 	=> '+1'.$p,
							'email'		=> $email,
							'cstatus'	=> 1,
							'group_id'	=> $this->input->post('group_id'),
							'created_by'=> $this->session->userdata('user_id')
						);
						$res = $this->db->insert('contact', $cont_list) ? true : false;
					}
					$firstName = ""; $lastName = ""; $phone = ""; $email = ""; 
				}
			}
			echo isset($res) ? 1 : 2;
		}else{ echo 0; }
    }
}