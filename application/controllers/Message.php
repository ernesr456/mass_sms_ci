<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Denver');
	}
	public function get_reply(){
		$this->db->query("SET sql_mode= ''");
        
		if (isset($_REQUEST)&&$_REQUEST!="") {
			if (isset($_POST['SmsStatus'])&&$_POST['SmsStatus']=='received') {
				$from = preg_replace('/\D+/', '', $_POST['From']);
				$to = $_POST['To'];
				$body = $_POST['Body'];
				$sid = $_POST['MessageSid'];
				$qoute = 0;
				$type = 0;
				$extension = 'text';
				$sms_status = $_POST['SmsStatus'];
				$postData['title'] = "single";
				$sms_reply_status= "";
				$filename = "";
				$group_id = 0;
				$status = 'received';
			    
	    		$this->db->select('a.id as userid,b.id as contactid,b.group _id')->from('users a')->join('contact b','a.id = b.created_by','left')->where('b.number='.$from);
        	    $data = $this->db->get()->result_array();
            	foreach ($data as $row){
                    $data = $this->Admin_model->getReply($body,$filename,$row['userid'],$row['contactid'],$sid,$qoute,$extension,$row['group_id'],$sms_status,$status);
            		$this->Admin_model->opt_out($row['contactid']);
                }
				echo json_encode($data);
			
				
			}
		}
	}
}
?>
