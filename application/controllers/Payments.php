<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {
	function __construct()
	{
        parent::__construct();
        date_default_timezone_set('America/Denver');
	}
	public function pricing(){
		$data = array(
			'page_description'  => $this->session->userdata('role_slug') === 'administrator' ? 'Manage Package Pricing' : 'Package Pricing' ,
			'page_head' 		=> $this->session->userdata('role_slug') === 'administrator' ? 'Manage Package Pricing' : 'Package Pricing',
			'page_keyword' 		=> $this->session->userdata('role_slug') === 'administrator' ? 'Manage Package Pricing' : 'Package Pricing',
			'page' 				=> $this->session->userdata('role_slug') === 'administrator' ? 'Manage Package Pricing' : 'Package Pricing',
			'page_title' 		=> $this->session->userdata('role_slug') === 'administrator' ? 'Manage Package Pricing' : 'Package Pricing',
            'authPermissionID'  => $this->Permission_model->authPermissionID($this->session->userdata('user_id')),
		);
        $this->load->view('admin/pricing/plan', $data);
	}
	public function show_pricing(){
		$data = $this->Payments_model->show_pricing();
		// for ($i=0; $i <count($data) ; $i++) { 
		// 	echo $data[$i]['name'];
		// }
		echo json_encode($data);
	}
	public function submit_pricing(){
		echo json_encode($this->Payments_model->submit_pricing());
	}
	public function get_pricing($id= ''){
		echo json_encode($this->Payments_model->get_pricing($id));
	}
    public function getPricing(){
        if ($this->input->post()) {
         $postData = $this->input->post();
         echo json_encode($this->Payments_model->get_pricing($postData['id']));
        }
    }
	public function purhcase_pricing(){
		if ($this->input->post()) {
			$postData = $this->input->post();
			echo json_encode($this->Payments_model->purhcase_pricing($postData));
		}
	}

    public function show_transaction(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc"){
            $dir = "desc";
        }
        $valid_columns = array(
            2=>'price',
            3=>'name',
            4=>'created_time'
        );

        if(!isset($valid_columns[$col])){
            $order = null;
        }
        else{
            $order = $valid_columns[$col];
        }
        if($order !=null){
            $this->db->order_by($order, $dir);
        }
        
        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $transaction = $this->Payments_model->get_transaction($search,$valid_columns,$length,$start);
        $data = array();
        foreach($transaction->result() as $rows){
            $data[]= array(
                $rows->name,
                $rows->date,
                $rows->price,
            );     
        }

        $totalGroup = $this->Payments_model->total_transaction($search,$valid_columns);
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalGroup,
            "recordsFiltered" => $totalGroup,
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }
    public function my_transaction(){
        echo json_encode($this->Payments_model->my_transaction());
    }
    public function get_package(){
    	echo json_encode($this->Payments_model->get_package());
    }
    public function client_permission(){
    	echo $this->Payments_model->client_permission();
    }
    public function update_pricing(){
        if ($this->input->post()&&$this->session->userdata('user_group')==1) {
            $postData = $this->input->post();
            echo json_encode($this->Payments_model->update_pricing($postData));
        }
    }
        /* | Credits Section */
    public function credits(){
        if($this->session->userdata('user_id')){
            $data = array(
                'page_description'  => 'Credits List',
                'page'              => 'credits',
                'page_keyword'      => 'Credits List',
                'page_title'        => 'Credits List',
                'page_head'         => 'Credits List',
                'credits'           => $this->Admin_model->getCredits(),
                'authPermissionID'  => $this->Permission_model->authPermissionID($this->session->userdata('user_id')),

            );
            $this->load->view('admin/credits/credits_view', $data);
        }else{
            redirect(base_url());
        }
    }
    public function submit_credit(){
        if ($this->input->post()&&$this->session->userdata('user_group')==1) {
            $postData = $this->input->post('data');
            $type = $this->input->post('type');
            echo json_encode($this->Payments_model->submit_credit($postData,$type));
        }
    }
    public function show_credit(){
        $data       = '';
        $option_btn = '';
        $buy_btn    = '';
        $credits = $this->Payments_model->show_credit();
        foreach($credits as $c){
            if($this->session->userdata('user_group')==1){
                $option_btn = 
                '<div class="card-body pull-right" style="padding: 10px 10px 0px 0px;text-align: right;">
                    <a href="javascript:void(0)" class="btn icon-btn btn-sm btn-outline-danger" data-id="'.$c['id'].'" id="delete_credit" title="Delete">
                        <span class="fas fa-trash"></span>
                    </a>&nbsp;
                    <a data-target="#planModal" data-toggle="modal" href="javascript:void(0)" data-id="'.$c['id'].'" class="btn icon-btn btn-sm btn-outline-warning" id="edit_plan" title="Edit">
                        <span class="fas fa-pencil-alt"></span>
                    </a>
                </div>';
            }
            if($this->session->userdata('user_group')!=1){
                $buy_btn = '<button type="button" class="btn btn-info"><i class="fas fa-shopping-cart"></i> Buy</button>';
            }
            $data .= 
            '<div class="col-md" style="margin-bottom: 0.75rem !important">
                <div class="card">'.$option_btn.'
                    <div class="card-body pricing-table">
                        <p class="card-text text-center">
                            <span class="pricing-currency">$</span> <span class="pricing-amount">'.$c['credit_price'].'</span>
                        </p>
                        <p class="pricing-title text-center"><span>'.$c['credit_name'].'</span></p>
                    </div>
                    <div class="card-body text-center">
                        <p>Credit: '.$c['credit_amount'].'</p> 
                        <a href="javascript:;" id="creidt_btn" onclick="payWithPaypal('.$c['id'].')">
                        '.$buy_btn.'
                    </a>
                    </div>
                </div>
            </div>';
        }
        echo $credits ? $data : 0;
    }
    public function update_credit(){
        if ($this->input->post()&&$this->session->userdata('user_group')==1) {
            $postData = $this->input->post();
            echo json_encode($this->Payments_model->update_credit($postData));
        }
    }
    public function purchase_credits(){
        if($this->session->userdata('user_id')){
            echo $this->Payments_model->purchase_credits() ? 1 : 0;
        }else{ echo 0; }
    }

    public function get_single_credit($credit_id=''){
        if($this->session->userdata('user_id')){
            $credits = $this->Payments_model->get_single_credit($credit_id);
            echo $credits ? json_encode($credits) : 0;
        }else{ echo 0; }
    }

    /* Get payment settings using ajax */
    public function get_payment_settings(){
        $p_settings = $this->Payments_model->get_site_settings();
        echo $p_settings ? json_encode($p_settings) : 0;
    }
    public function show_credit_transaction(){
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search= $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";
        if(!empty($order))
        {
            foreach($order as $o)
            {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc"){
            $dir = "desc";
        }
        $valid_columns = array(
            0=>'credit_name',
            1=>'credit_amount',
            2=>'credit_price',
            3=>'created_time'
        );

        if(!isset($valid_columns[$col])){
            $order = null;
        }
        else{
            $order = $valid_columns[$col];
        }
        if($order !=null){
            $this->db->order_by($order, $dir);
        }
        
        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
                $con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $transaction = $this->Payments_model->get_credit_transaction($search,$valid_columns,$length,$start);
        $data = array();
        foreach($transaction->result() as $rows){
            $data[]= array(
                $rows->credit_name,
                '$'.$rows->credit_price,
                $rows->credit_amount,
                $rows->date
            );     
        }

        $totalGroup = $this->Payments_model->total_credit_transaction($search,$valid_columns);
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalGroup,
            "recordsFiltered" => $totalGroup,
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }
    public function total_credit(){
        echo json_encode($this->Payments_model->total_credit());
    }
    public function balance_credit(){
        $this->Payments_model->balance_credit();
    }
}
?>
