<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends CI_Controller {

	function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Denver');
	}

	public function add_campaign(){
		$titleCampagin= (isset($_GET['id']))?'Update Campaign':'Add Campaign';
		$data = array(
			'page_description'  => $titleCampagin,
			'page_head' 		=> $titleCampagin,
			'page_keyword' 		=> $titleCampagin,
			'page' 				=> "add_campaign",
			'page_title' 		=> $titleCampagin,
			'users'				=> $this->Campaign_model->getUsers()
		);
		if(isset($_GET['id'])){
			$data['campaign'] = $this->Campaign_model->getSingleCampaign($_GET['id']);
		}
		$this->load->view('admin/campaign/add_campaign', $data);
	}

	public function deleteCampaign(){
		if (isset($_GET['id'])){
			$single = $this->Campaign_model->deleteCampaign($_GET['id']);
				redirect(base_url('admin/campaigns'));
		}
	}

	public function saveCampaign(){
		echo $this->Campaign_model->saveCampaign();
	}
	
	public function getContact(){
		$contact = $this->Campaign_model->getContact();  
		echo json_encode($contact);
	}
}
	