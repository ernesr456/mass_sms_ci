<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Email_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_user_info(){
		$user_id = $this->session->userdata('user_id');
		$this->db->select('email, name')->from('users');
		$this->db->where('id', $user_id);
		return $this->db->get()->row_array();
	}

	public function get_contact_emails(){
		$user_id = $this->session->userdata('user_id');
		$this->db->select('id,email,cstatus,created_by')->from('contact');
		$this->db->where('cstatus', 1);
		$this->db->where('created_by', $user_id);
		$this->db->where('email !=', '');
		return $this->db->get()->result_array();
	}

	public function get_some_emails($id){
		$this->db->select('id,email,cstatus,created_by')->from('contact');
		$this->db->where_in('id', $id);
		return $this->db->get()->result_array();
	}

	public function get_specific_mail($id){
		$this->db->select('*')->from('email');
		$this->db->where_in('id', $id);
		return $this->db->get()->row_array();
	}

	public function send_mail_message(){
		$msg_id = $this->session->userdata('msg_id');
		$data = array(
			'user_id' 		=> $this->session->userdata('user_id'),
			'from_name' 	=> $this->input->post('from_name'),
			'send_to_type' 	=> $this->input->post('send_to_type'),
			'send_type' 	=> $this->input->post('send_type'),
			'send_to' 		=> json_encode($this->input->post('send_to')),
			'subject' 		=> $this->input->post('subject'),
			'message' 		=> $this->input->post('message'),
			'status' 		=> 1,
			'date_send' 	=> date('Y-m-d H:i:s'),
		);
		$emails  = ($data['send_to_type']=='all_emails') ? $this->get_contact_emails() : $this->get_some_emails($this->input->post('send_to'));
		if($data['send_type']=='individual_sending'){
			foreach($emails as $e){
				$res = $this->send_now($e['email'], $data['subject'], $data['message'], $data['from_name']);
			}
			if($res){
				return $this->save_sent_mail($data) ? true : false;
			}else{ return false; }
		}else{
			$email_list = '';
			foreach($emails as $e){
				$email_list .= $e['email'].',';
			}
			$res = $this->send_now($email_list, $data['subject'], $data['message'], $data['from_name']);
			if($res){
				return $this->save_sent_mail($data) ? true : false;
			}else{ return false; }
		}
	}
	
	public function send_now($email, $subject, $body, $from_name){
		$user_info = $this->get_user_info();
        $this->email->from($user_info['email'], $from_name); 
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($body);
        return ($this->email->send()) ? true : false;
	} 
	
	public function save_sent_mail($data){
		$msg_id = $this->input->post('msg_id');
		if($msg_id){
			$this->db->where('id', $msg_id);
			$res = $this->db->update('email', $data);
		}else{
			$res = $this->db->insert('email', $data);
		}
		return $res ? true : false;
	}

	public function count_mails($status){
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->where('status', $status);
		return $this->db->count_all_results('email');
	}

	public function save_mail_as_draft(){
		$msg_id = $this->input->post('msg_id');
		$data = array(
			'user_id' 		=> $this->session->userdata('user_id'),
			'from_name' 	=> $this->input->post('from_name'),
			'send_to_type' 	=> $this->input->post('send_to_type'),
			'send_type' 	=> $this->input->post('send_type'),
			'send_to' 		=> $this->input->post('send_to')?json_encode($this->input->post('send_to')):'',
			'subject' 		=> $this->input->post('subject'),
			'message' 		=> $this->input->post('message'),
			'status' 		=> 2,
			'date_send' 	=> date('Y-m-d H:i:s'),
		);
		if($msg_id){
			$this->db->where('id', $msg_id);
			$res = $this->db->update('email', $data);
		}else{
			$res = $this->db->insert('email', $data);
		}
		return $res ? true : false;
	}

	public function change_mail_status($id, $status){
		$data = array('status' => $status);
		$this->db->where_in('id', $id);
		return $this->db->update('email', $data) ? true : false;
	}

	public function delete_msg_permanently($id){
		$this->db->where_in('id', $id);
		return $this->db->delete('email') ? true : false;
	}

	public function get_emails2($status){
		$user_id = $this->session->userdata('user_id');
		$this->db->select('*')->from('email');
		$this->db->where('user_id', $user_id);
		$this->db->where('status', $status);
		$this->db->order_by('id', 'DESC');
		return $this->db->get()->result_array();
	}

	public function get_emails($limit, $start, $type, $status){
		$user_id = $this->session->userdata('user_id');
		$this->db->select('*')->from('email');
		$this->db->where('user_id', $user_id);
		$this->db->where('status', $status);
		$this->db->order_by('id', 'DESC');
		if($type==2){
			$this->db->limit($limit, $start);
		}
		return ($type==1) ? $this->db->get()->num_rows() : $this->db->get()->result_array();
    }

	public function relative_date($time_set) {
		$time  = strtotime($time_set);
		$today = strtotime(date('M j, Y'));
		$hrs = date("h:i A", $time);
		$reldays = ($time - $today)/86400;
	
		if ($reldays >= 0 && $reldays < 1) {
			return 'Today, '.$hrs;
		} else if ($reldays >= 1 && $reldays < 2) {
			return 'Tomorrow, '.$hrs;
		} else if ($reldays >= -1 && $reldays < 0) {
			return 'Yesterday, '.$hrs;
		}
			
		if (abs($reldays) < 7) {
			if ($reldays > 0) {
				$reldays = floor($reldays);
				return 'In ' . $reldays . ' day' . ($reldays != 1 ? 's' : '');
			} else {
				$reldays = abs(floor($reldays));
				return $reldays . ' day' . ($reldays != 1 ? 's' : '') . ' ago';
			}
		}
			
		if (abs($reldays) < 182) {
			return date('l, j F',$time ? $time : time());
		} else {
			return date('l, j F, Y',$time ? $time : time());
		}
	}
}
	
