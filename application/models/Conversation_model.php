<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Conversation_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
	}

	public function update_sms_status($data,$status){
		$this->db->set('status',$status, FALSE);
		$this->db->where('id', $data);
		$this->db->update('conversation'); 
    }
    public function save_message($postData,$filename,$contact_id,$contacts_group,$quote,$extension,$status){
		date_default_timezone_set($postData['timezone']);
		$date = new DateTime();
		$sent_time = $date->format('Y-m-d H:i:s');

        $data = array(
	        'message' 	=> $postData['message'],
	        'filename' 	=> $filename,
	        'from_id' 	=> $this->session->userdata('user_id'),
	        'to_id'		=> $contact_id,
	        'sent_time'	=> $sent_time,
	        'status'	=> $status,
	        'extension'	=> $extension,
	        'parent_id'	=> $quote,
	        'group_id'	=> $contacts_group
		);

		$data = $this->db->insert('conversation', $data);
		return $this->db->insert_id();
    }
	public function conversation_thread($id){
		return $this->db->select('*')->from('conversation a')->where('id',$id)->get()->row_array();
	}
	public function listContacts($postData,$valid_columns){
		$this->db->query("SET sql_mode= ''");
    	$filter = $postData['filter'];
    	$contact = $postData['contact'];
    	$search = $postData['search'];
        
    	if ($contact=='single') {
	    	$this->db->select('a.id,b.name as assign_name,a.number,a.cstatus,d.group_name,max(c.sent_time) as sent_time,CASE when c.sent_time IS NOT NULL THEN max(DATE_FORMAT(c.sent_time, "%m/%d/%Y %h:%i %p")) END as last_activity, a.email as email,c.status,a.id,a.firstName,a.lastName,a.group_id,a.created_by,count(CASE when c.status = 1 THEN c.status END) as count_status')
	    	->from('contact a')
	    	->join('users b','a.created_by = b.id','left')
	    	->join('conversation c','c.from_id = a.id AND c.to_id = b.id OR c.from_id = b.id AND c.to_id = a.id','left')
	    	->join('contacts_group d','d.gid = a.group_id','left');
			if (!empty($search)) {
				$array_search =  explode(" ", $search);
				foreach ($array_search as $as) {
					if (!empty($as)) {
						foreach($valid_columns as $sterm){
			            	$this->db->or_like($sterm,$as);
			            }
					}
				}
			}
			$this->db->where('a.id IS NOT NULL');

			if ($this->session->userdata('user_group')==2) {
				$this->db->where('b.created_by = '.$this->session->userdata('user_id').' OR a.created_by ='.$this->session->userdata('user_id'));
			}else if($this->session->userdata('user_group')==3){
				$this->db->where('a.created_by='.$this->session->userdata('user_id'));
			}

	    	$this->db->group_by('a.id');

			if ($filter!=""&&$filter!=2&&$filter!=1) {
				$this->db->having('count(CASE when c.status = 1 THEN c.status END) <1');
			}else if($filter==1){
				$this->db->having('count(CASE when c.status = 1 THEN c.status END) >0');
			}
	    	$this->db->order_by('count(CASE when c.status = 1 THEN c.status END) DESC, max(c.sent_time) DESC')->limit($postData['limit_contact']);
    	}else{
			$this->db->select('a.gid,b.name as assign_name,a.group_name,a.tags,a.status,count(c.id) as total_contacts,sum((SELECT COUNT( CASE WHEN d.status = 1 THEN d.status END ) from conversation d where d.from_id = c.id AND d.to_id = b.id OR d.from_id = b.id AND d.to_id = c.id)) as count_status,( SELECT MAX(DATE_FORMAT(d.sent_time, "%m/%d/%Y %h:%i %p")) FROM conversation d WHERE d.from_id = c.id AND d.to_id = b.id OR d.to_id = c.id AND d.from_id = b.id OR d.to_id = c.id) AS last_activity
			')
			->from('contacts_group a')
			->join('users b','a.created_by = b.id','left')
			->join('contact c','a.gid = c.group_id','left');

			if (!empty($postData['search'])) {

				foreach($valid_columns as $sterm)
	            {
	            	$this->db->or_like($sterm,$postData['search']);
	            }
			}
			// $this->db->where('a.gid IS NOT NULL AND a.group_name IS NOT NULL');

			if ($this->session->userdata('user_group')==2) {
				$this->db->where('b.created_by = '.$this->session->userdata('user_id').' OR a.created_by ='.$this->session->userdata('user_id'). ' AND a.gid IS NOT NULL AND a.group_name IS NOT NULL');
			}else if($this->session->userdata('user_group')==3){
				$this->db->where('a.created_by='.$this->session->userdata('user_id'));
			}

			$this->db->group_by('a.gid');

			if ($filter!=""&&$filter!=2&&$filter!=1) {
				$this->db->having('count_status <1');
			}else if($filter==1){
				$this->db->having('count_status >0');
			}

			$this->db->order_by('count_status DESC, last_activity DESC')->limit($postData['limit_contact']);
    	}
		return $this->db->get()->result();
    }
    public function show_conversation($postData){
		$this->db->query("SET sql_mode= ''");
    	$type = $postData['type'];
    	if ($type=='single') {
	    	$this->db->select('b.id as contact_id,a.id,a.filename,b.number,a.sms_status,a.parent_id,a.extension,a.message,a.from_id,a.to_id,a.group_id,DATE_FORMAT(sent_time, "%W, %M-%e-%Y %l:%i %p") as sent_time,DATE_FORMAT(sent_time, "%l:%i %p") as time,a.status,a.sms_sid,b.firstName, b.lastName')
	    	->from('conversation a')
	    	->join('contact b','a.from_id = b.id OR a.to_id = b.id','left')
	    	->join('users c','c.id = b.created_by','left')
	    	->where('b.id ='.$postData['contact']);

	    	if ($postData['filter']==1) {
	    		$this->db->where('a.from_id!='.$postData['contact']);
	    	}else if($postData['filter']==2){
	    		$this->db->where('a.from_id='.$postData['contact']);
	    	}

	    	$this->db->group_by('a.id')->order_by('a.sent_time','DESC');
    	}else{
    		$this->db->select('c.id as contact_id,a.id,a.filename,c.number,a.sms_status,a.parent_id,a.extension,a.message,a.from_id,a.to_id,a.group_id,DATE_FORMAT(sent_time, "%W, %M %d,%Y %l:%i %p") as sent_time,DATE_FORMAT(sent_time, "%l:%i %p") as time,a.status,a.sms_sid,c.firstName, c.lastName')
    		->from('conversation a')
    		->join('contacts_group b','a.group_id = b.gid','left')
    		->join('contact c','b.gid = c.group_id','left');
    		$this->db->where('b.gid ='.$postData['contact'].' and c.cstatus!=0')
    		->group_by('a.id')
    		->order_by('a.sent_time','DESC');

	    	if ($postData['filter']==1) {
	    		$this->db->where('a.from_id!=c.id AND a.sms_status IS NOT NULL');
	    	}else if($postData['filter']==2){
	    		$this->db->where('a.from_id=c.id');
	    	}
    	}
    	$this->db->limit($postData['limit']);
    	$query = $this->db->get();
    	return $query->result();
    }

    public function show_new_message($limit){
    	$this->db->limit($limit);
		$this->db->select('b.id as contact_id,a.id,a.filename,b.number,a.sms_status,a.parent_id,a.extension,a.message,a.from_id,a.to_id,a.group_id,DATE_FORMAT(sent_time, "%c %d %Y %l:%i %p") AS sent_time,a.status,a.sms_sid,b.firstName, b.lastName')
    	->from('conversation a')
    	->join('contact b','a.from_id = b.id OR a.to_id = b.id','left')
    	->join('users c','c.id = b.created_by','left')
    	->where('a.status=1');
    	if ($this->session->userdata('user_group')==2) {
    		$this->db->where('c.created_by='.$this->session->userdata('user_id'). ' OR b.created_by='.$this->session->userdata('user_id'));
    	}else if($this->session->userdata('user_group')==3){
    		$this->db->where('b.created_by='.$this->session->userdata('user_id'));
    	}
    	return $this->db->get()->result();
    }
    public function update_message($id){
		$this->db->set('status', 0);
		$this->db->where('id='.$id);
		$submit = $this->db->update('conversation');
		return $submit? true: false;
    }
	public function get_template($ses_id,$user_group){
		$this->db->query("SET sql_mode= ''");
		if ($user_group==1) {
	        $this->db->select('*');
			$this->db->from('message_template mt');
			// $this->db->join('template_file tf', 'mt.id = tf.parent_id', 'left');
			$this->db->group_by("id");
        }else{
	        $this->db->select('*');
			$this->db->from('message_template mt');
			// $this->db->join('template_file tf', 'mt.id = tf.parent_id', 'left');			
			$this->db->where('mt.created_by ='.$_SESSION['user_id']);
			$this->db->group_by("id");
        }
        $res = $this->db->get();
		return $res->result();
	}
}
