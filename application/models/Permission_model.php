<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	
	public function getPermissions()
	{
		$this->db->query("SET sql_mode= ''");
		$this->db->select('*,up.id as upid');
		$this->db->from('user_permission up');
		$this->db->join('users u', 'up.created_by = u.id', 'left');
		if ($this->session->userdata('user_group')!=1) {
			$this->db->where('up.created_by ='.$this->session->userdata('user_id'));
		}
  		return $this->db->get()->result_array();    
  	}
  	public function getPermissionID($id){
  		$this->db->query("SET sql_mode= ''");
		$this->db->select('*');
		$this->db->from('user_permission');
		if ($this->session->userdata('user_group')==2) {
			$this->db->where('created_by',$id);
		}
		return $this->db->get()->result_array();    
  	}
  	public function authPermissionID($id){
  		$this->db->query("SET sql_mode= ''");
		if($this->session->userdata('user_group')==3){
			$this->db->select('user_permission')->from('users')->where('id='.$id);
        	foreach($this->db->get()->result_array() as $row){
		    	return $row['user_permission'];
			}
		}else if ($this->session->userdata('user_group')==2) {
			//select max(b.created_time),a.permission from pricing_table a left join payments b on a.id = b.pricing_id where b.user_id = 1 AND b.created_time > DATE_SUB(NOW(), INTERVAL 30 DAY)

			$this->db->select('max(b.created_time),a.permission')->from('pricing_table a')->join('payments b','a.id=b.pricing_id','left')->where('b.user_id='.$id. ' AND b.created_time > DATE_SUB(NOW(), INTERVAL 30 DAY)');
			foreach($this->db->get()->result_array() as $row){
		    	return $row['permission'];
			}
		}else{
			// redirect(base_url('/admin'));
		}
  	}
	public function update_permission($postData){
		$this->db->query("SET sql_mode= ''");
        $permission = array();
        $data = $this->input->post('permission');
        for ($i=0; $i <count($data) ; $i++) { 
            $permission = array_merge($permission, array($data[$i]['value']=>true));
        }
		$pricing_table = array(
        	'user_permission' => json_encode($permission)
        );
        $this->db->where('id',$postData['id']);
        $res = $this->db->update('users', $pricing_table);
        return $res? true : false;
	}
	public function getUsers($postData){
		$this->db->query("SET sql_mode= ''");
		$permission = "";
		if (isset($postData['group_id'])) {
			if ($postData['group_id']==3) {
				$permission = "a.user_permission as permission";
			}else if($postData['group_id']==2){
				$permission = "d.permission as permission";
			}
		}

		$this->db->select('a.id,a.name,a.verify,a.user_permission,a.status as user_stat, a.username,a.email,b.id as group_id, '.$permission.' ,b.name as group_name');
		$this->db->from('users a');
		$this->db->join('users_groups b','b.id = a.user_group','left');
		if (isset($postData['group_id'])) {
			if ($postData['group_id']==2) {
				$this->db->join('payments c','a.id=c.user_id','left')->join('pricing_table d','d.id=c.pricing_id','left');
			}
		}
		if ($this->session->userdata('user_group')==2) {
			$this->db->where('a.created_by = '.$this->session->userdata('user_id').' OR a.id ='.$this->session->userdata('user_id'));
		}
		if (isset($postData['id'])) {
			$this->db->where('a.id='.$postData['id']);
		}
		return $this->db->get()->result_array();
	}
}
