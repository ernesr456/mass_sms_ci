<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	public function show_profile($id){
		$this->db->select('username,image')->from('users')->where('id=',$id);
		return $this->db->get()->row_array();
	}
	
}
