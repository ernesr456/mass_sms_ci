<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	public function saveCampaign(){
		$assign_to = $this->input->post('assign_to');

		($this->input->post('assign_to')==0) ? $assign_to = $this->session->userdata('user_id') : '';
		$campaign = array(
            'campaign_title'  => $this->input->post('campaign_title'),
            'campaign_text'	  => $this->input->post('campaign_text'),
            'user_id'	  	  => $assign_to,
            'start_at'	  	  => $this->input->post('start_at'),
            'created_by'	  => $this->session->userdata('user_id'),
            'tags'	 		  => $this->input->post('tags'),
            'contacts'	 	  => $this->input->post('contacts'),
            'contacts_names'  => $this->input->post('contacts_names'),
            'auto_reply'	  => $this->input->post('auto_reply'),
            'auto_reply_text' => $this->input->post('auto_reply_text'),
        );

        if($this->input->post('id')!='0'){
            $this->db->where('id',$this->input->post('id'));
            $res = $this->db->update('campaign', $campaign);
        }else{
            $res = $this->db->insert('campaign', $campaign);
        }
        return $res; 
	}
	public function deleteCampaign($id){
		$this->db->where('id',$id);
		$res = $this->db->delete('campaign');
        return $res ? true : false;
	}
	public function getCampaign($uid,$group_id){
		$this->db->query("SET sql_mode= ''");
		$sql = '';
		$sql .= "SELECT *,a.id as campaign_id,b.username as b_username, c.username as c_created_by,d.email as contact_email, e.group_name,a.contacts,a.tags as tags FROM campaign a left join users b on a.user_id = b.id
				left join users c on a.created_by = c.id
				left join contacts d on a.contacts = d.id
				left join contacts_group e on a.contacts = e.gid ";

		if ($group_id!=1) {
			// $this->db->where('a.user_id='.$uid.' or a.created_by='.$uid);
			 $sql .= "WHERE c.user_group != 1 and a.user_id=".$uid." or a.created_by=".$uid;
		}
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function getSingleCampaign($id){
		$this->db->select('*,campaign.id as campaign_id');
		$this->db->from('campaign');
		$this->db->join('users', 'campaign.user_id = users.id');
		$this->db->where('campaign.id', $id);
		$res = $this->db->get();
		return $res->row();
	}
	public function getContact(){
		($this->input->post('id')==1)? $table = 'contacts_group' : $table = 'contacts';
			$query = $this->db->query("Select * from users where id=".$this->session->userdata('user_id'));
			$created_by = $query->row('created_by');
			$where = "created_by != 1 and created_by in (Select id from users where created_by =".$created_by.")";
			$this->db->select('*');
			$this->db->where($where);
			$this->db->from($table);
			return $this->db->get()->result_array(); 
	}
	public function getAllContact(){
		($this->input->post('id')==1)? $table = 'contacts_group' : $table = 'contacts' ;
			$this->db->select('*');
			$this->db->from($table);
			return $this->db->get()->result_array();    
	}
	public function getUsers(){
		$query = $this->db->query("Select * from users where id=".$this->session->userdata('user_id'));
		$created_by = $query->row('created_by');
		
		if($created_by !== $this->session->userdata('user_id')){
			$group = "user_group != 2 and";
		}else{
			$group = "id != ".$created_by." and";
		}
		$where = "created_by != 1 and ".$group." created_by in (Select id from users where created_by =".$created_by.")";
		$this->db->select('*');
		$this->db->where($where);
		$this->db->from('users');
		return $this->db->get()->result_array();    
	}
}
	
