<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
	}

	protected function generateSalt()
	{
		$salt = "xiORG17N6ayoEn6X3";
		return $salt;
	}

	protected function generateVerificationKey()
	{
		$length = 10;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function updateGroups($postData = null, $action = null)
	{
		if ($action == "add") {
			$error = 0;
			if (!isset($postData["name"]) || empty($postData["name"])) {
				$error = 2;
			} else {
				$name = $this->db->escape(strip_tags($postData["name"]));
			}
			if ($error == 2) {
				return $error;
			}
			$sql = "SELECT * FROM users_groups WHERE name = " . $name;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return 3;
			} else {
				$sql2 = "INSERT INTO users_groups (name) VALUES (" . $name . ")";
				$this->db->query($sql2);
				return TRUE;
			}
		}
		if ($action == "edit") {
			$error = 0;
			if (!isset($postData["name"]) || empty($postData["name"])) {
				$error = 2;
			} else {
				$name = $this->db->escape(strip_tags($postData["name"]));
			}
			if (!isset($postData["id"]) || empty($postData["id"])) {
				$error = 3;
			} else {
				$id = $this->db->escape(strip_tags($postData["id"]));
			}
			if ($error == 2) {
				return $error;
			}
			$sql = "SELECT * FROM users_groups WHERE name = " . $name;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return 4;
			} else {
				$sql2 = "UPDATE users_groups SET name = " . $name . " WHERE id = " . $id;
				$this->db->query($sql2);
				return TRUE;
			}
		}
		if ($action == "delete") {
			$admin_group = $this->db->escape(strip_tags((int)$postData["id"]));
			$sql = "SELECT * FROM users WHERE users_groups = " . $admin_group;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return FALSE;
			} else {
				$sql2 = "DELETE FROM users_groups WHERE id = " . $admin_group;
				$this->db->query($sql2);
				return TRUE;
			}
		}
	}
	public function getAdminGroups($additional = "")
	{
		if ($additional !== "") {
			$additional = "WHERE id = " . $this->db->escape($additional);
		}
		$sql = "SELECT * FROM users_groups " . $additional;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	public function getAdminInfo($adminid = null)
	{
		$sql = "SELECT * FROM users WHERE id = " . $this->db->escape(strip_tags((int)$adminid));
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return array();
		}
	}
	//User
	public function activate_user($postData){
		$query ="";
		for($i=0;$i<count($postData['data']);$i++){
			$sql = "UPDATE users set status= 0 WHERE id =".$postData['data'][$i];
			$query = $this->db->query($sql);
		}
		return $query ? true: false;
	}
	public function deactivate_user($postData){
		$query ="";
		for($i=0;$i<count($postData['data']);$i++){
			$sql = "UPDATE users set status= 1 WHERE id =".$postData['data'][$i];
			$query = $this->db->query($sql);
		}
		return $query ? true: false;
	}
	public function get_all_users($get){
		$role = $this->db->escape(strip_tags($get));
		$this->db->select('u.id,u.name,u.status as user_stat, u.username,u.email,ug.name as group_name,(SELECT name from users where id=u.created_by) as created_name');
		$this->db->from('users u');
		$this->db->join('users_groups ug','ug.id = u.user_group','left');
		if ($this->session->userdata('user_group')==2) {
			$this->db->where('u.created_by = '.$this->session->userdata('user_id').' OR u.id ='.$this->session->userdata('user_id'));
		}
		$this->db->where('ug.role_slug='.$role);

		$res = $this->db->get();
		return $res->result();
	}

	public function getUsers($postData){
		// print_r($postData);
		$this->db->select('a.id,a.name,a.verify,a.status as user_stat,a.username,a.email,b.id as group_id,a.user_permission,b.name as group_name');
		$this->db->from('users a');
		$this->db->join('users_groups b','b.id = a.user_group','left');
		if ($this->session->userdata('user_group')==2) {
			$this->db->where('a.created_by = '.$this->session->userdata('user_id'));
		}

		if (isset($postData['id'])) {
			$this->db->where('a.id='.$postData['id']);
		}

		return $this->db->get()->result_array();

	}


	public function getUsersType($type)
	{
		$query = $this->db->query("Select * from users where id=".	$this->session->userdata('user_id'));
		$created_by = $query->row('created_by');

        $uid=$this->session->userdata('user_id');

      	if ($this->session->userdata("role_slug") === 'administrator'){
  				$where = " a.user_group = ".$type;
		}else{
			$where = ' a.id !='.$uid.' and a.created_by='.$created_by.' and a.user_group='.$type;
		}		

		$sql = "SELECT a.id, a.username, ag.name as 'role', a.name as 'fullname', up.permission FROM users a 
                LEFT JOIN users_groups ag ON a.user_group = ag.id 
                LEFT JOIN user_permission up ON a.user_permission = up.id
                where".$where;
      	
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return array();
		}
	}

	public function updateAdmins($postData = null, $action = null)
	{
		if ($action == "add") {
			$error = 0;
			if (!isset($postData["username"]) || empty($postData["username"])) {
				$error = 2;
			} else {
				$username = $this->db->escape(strip_tags($postData["username"]));
			}
			if (!isset($postData["password"]) || empty($postData["password"])) {
				$error = 3;
			} else {
				$password = strip_tags($postData["password"]);
			}
			if (!isset($postData["password2"]) || empty($postData["password2"])) {
				$error = 4;
			} else {
				$password2 = strip_tags($postData["password2"]);
			}
			if (!isset($postData["email"]) || empty($postData["email"])) {
				$error = 5;
			} else {
				$email = $this->db->escape(strip_tags($postData["email"]));
			}
			if (!isset($postData["name"]) || empty($postData["name"])) {
				$error = 6;
			} else {
				$name = $this->db->escape(strip_tags($postData["name"]));
			}
			if (!isset($postData["user_group"]) || empty($postData["admin_group"])) {
				$error = 7;
			} else {
				$admin_group = $this->db->escape(strip_tags($postData["admin_group"]));
			}
			if (!isset($postData["address"]) || empty($postData["address"])) {
				$address = "''";
			} else {
				$address = $this->db->escape(strip_tags($postData["address"]));
			}
			if (!isset($postData["address2"]) || empty($postData["address2"])) {
				$address2 = "''";
			} else {
				$address2 = $this->db->escape(strip_tags($postData["address2"]));
			}
			if (!isset($postData["city"]) || empty($postData["city"])) {
				$city = "''";
			} else {
				$city = $this->db->escape(strip_tags($postData["city"]));
			}
			if (!isset($postData["state"]) || empty($postData["state"])) {
				$state = "''";
			} else {
				$state = $this->db->escape(strip_tags($postData["state"]));
			}
			if (!isset($postData["zip"]) || empty($postData["zip"])) {
				$zip = "''";
			} else {
				$zip = $this->db->escape(strip_tags($postData["zip"]));
			}
			$verification_key = $this->db->escape($this->generateVerificationKey());
			$salt = $this->generateSalt();
			if ($password !== $password2) {
				$error = 8;
			} else {
				$password = $this->db->escape(md5($salt . $password));
			}
			if ($error > 0) {
				return $error;
			}
			$now = $this->db->escape(time());
			$sql = "SELECT * FROM users WHERE username = " . $username;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return 9;
			} else {
				$sql2 = "INSERT INTO users (username,password,email,created_date,verification_key,admin_group,name,address,address2,city,state,zip) VALUES ($username, $password, $email, $now, $verification_key, $admin_group, $name, $address, $address2, $city, $state, $zip)";
				$this->db->query($sql2);
				return TRUE;
			}

		}
		if ($action == "edit") {
			$error = 0;
			if (!isset($postData["username"]) || empty($postData["username"])) {
				$username = "";
			} else {
				$username = $this->db->escape(strip_tags($postData["username"]));
			}
			if (!isset($postData["password"]) || empty($postData["password"])) {
				$pass = 0;
			} else {
				$pass = 1;
				$password = strip_tags($postData["password"]);
			}
			if (!isset($postData["password2"]) || empty($postData["password2"])) {
				$password2 = "";
			} else {
				$password2 = strip_tags($postData["password2"]);
			}
			if (!isset($postData["email"]) || empty($postData["email"])) {
				$error = 5;
			} else {
				$email = $this->db->escape(strip_tags($postData["email"]));
			}
			if (!isset($postData["name"]) || empty($postData["name"])) {
				$error = 6;
			} else {
				$name = $this->db->escape(strip_tags($postData["name"]));
			}
			if (!isset($postData["user_group"]) || empty($postData["admin_group"])) {
				$error = 7;
			} else {
				$admin_group = $this->db->escape(strip_tags($postData["admin_group"]));
			}
			if (!isset($postData["address"]) || empty($postData["address"])) {
				$address = "''";
			} else {
				$address = $this->db->escape(strip_tags($postData["address"]));
			}
			if (!isset($postData["address2"]) || empty($postData["address2"])) {
				$address2 = "''";
			} else {
				$address2 = $this->db->escape(strip_tags($postData["address2"]));
			}
			if (!isset($postData["city"]) || empty($postData["city"])) {
				$city = "''";
			} else {
				$city = $this->db->escape(strip_tags($postData["city"]));
			}
			if (!isset($postData["state"]) || empty($postData["state"])) {
				$state = "''";
			} else {
				$state = $this->db->escape(strip_tags($postData["state"]));
			}
			if (!isset($postData["zip"]) || empty($postData["zip"])) {
				$zip = "''";
			} else {
				$zip = $this->db->escape(strip_tags($postData["zip"]));
			}
			if ($error > 0) {
				return $error;
			}
			$sql = "SELECT * FROM users WHERE username = " . $username;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				if ($pass == 0) {
					$sql = "UPDATE users SET email = $email, name = $name, users_group = $admin_group, address = $address, address2 = $address2, city = $city, state = $state, zip = $zip WHERE id = " . $this->db->escape($query->row()->id);
					$this->db->query($sql);
					return TRUE;
				} else {
					if ($password !== $password2) {
						return 8;
					}
					$salt = $this->generateSalt();
					$password = $this->db->escape(md5($salt . $password));
					$sql = "UPDATE users SET email = $email, name = $name, users_groups = $admin_group, address = $address, address2 = $address2, city = $city, state = $state, zip = $zip, password = $password WHERE id = " . $this->db->escape($query->row()->id);
					$this->db->query($sql);
					return TRUE;
				}
			} else {
				return 9;
			}
		}
		if ($action == "delete") {
			$admin_id = $this->db->escape(strip_tags((int)$postData["id"]));
			if ((int)$postData["id"] == $this->session->userdata("admin_id")) {
				return FALSE;
			} else {
				$sql = "DELETE FROM users WHERE id = " . $admin_id;
				$this->db->query($sql);
				return TRUE;
			}

		}
	}
	//get verification_key true email
	public function get_key($postData){
		$email = $this->db->escape(strip_tags($postData["email"]));
		$sql = "SELECT verification_key from users where email = ".$email;
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			$sql = "SELECT verification_key from users where email =" .$email;
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}
	
	public function change_pass($data){

		$salt = $this->generateSalt();
		$password = $this->db->escape(strip_tags(md5($salt.$data[1]['value'])));
		$userid =$this->db->escape(strip_tags($data[0]['value']));
		$sql = "SELECT id from reset_password where user_id = ".$userid;
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			$sql = "UPDATE reset_password set password =".$password;
			$query = $this->db->query($sql);
			return $query;
		}else{
			$sql = "INSERT INTO reset_password (user_id,password) values ($userid,$password)";
			$query = $this->db->query($sql);
			return $query;
		}
	}

	public function confirm($id){
		$sql = "SELECT password FROM reset_password WHERE user_id ='$id'";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			$password = $this->db->escape($query->row()->password);
			$sql = "UPDATE users set password = ".$password."WHERE id ='$id'";
			$query = $this->db->query($sql);
			return $query;
		}else{
			return false;
		}
	}
	public function delete_request($id){
		$sql = "DELETE from reset_password where user_id = '$id'";
		$query = $this->db->query($sql);

	}
	public function verification($id){
		$sql = "UPDATE users set verify = '0' where id = '$id'";
		$query = $this->db->query($sql);
	}
	public function getUser(){
        $userid = $this->session->userdata("user_id");
        $sql = "SELECT id,name,username,email,image,user_group FROM users WHERE id = " . $userid;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0){
            return $query->row();
        }
        else {
            return false;
        }
    }

	public function addUsers($postData){
		$submit= '';
		$project_text = "";
		$group_id = 2;
		$get_username = '';
		$get_email = "";
		if (isset($postData['group_id'])) {
			$group_id = $postData['group_id'];
		}else if ($this->session->userdata('user_group')==2) {
			$group_id = 3;
		}

		if (isset($postData['project_text'])) {
			$project_text = $postData['project_text'];
		}

		$error = 0;
		$error1 = 0;
		$salt = $this->generateSalt();

		if ($error > 0) {
			return $error;
		}

		if (!empty($postData['id'])) {
			$this->db->where('id',$postData['id']);
			$query = $this->db->get('users');
			foreach ($query->result() as $row){
				$get_username=$row->username;
				$get_email = $row->email;
			}

			$this->db->where('username=',$postData['username']);
			$this->db->not_like('username', $get_username);
			$query = $this->db->get('users');

			$this->db->where('email=',$postData['email']);
			$this->db->not_like('email', $get_email);
			$query1 = $this->db->get('users');
			if ($query1->num_rows() > 0) {
				$error = 1;
			}if ($query->num_rows() > 0) {
				$error1 = 1;
			}
		}else{

			$this->db->where('username',$postData['username']);
			$query = $this->db->get('users');

			$this->db->where('email',$postData['email']);
			$query1 = $this->db->get('users');
			if ($query1->num_rows() > 0) {
				$error = 1;
			}if ($query->num_rows() > 0) {
				$error1 = 1;
			}
		}

		if ($error>0&&$error1>0) {
			return 4;
		}else if ($error>0) {
			return 6;
		}else if ($error1>0) {
			return 7;
		}

		if($error==0&&$error1==0){
			$created_by = '';
			if (isset($_SESSION['user_id'])) {
				$id = $_SESSION['user_id'];
			}
			if(isset($_SESSION['user_group'])&&$_SESSION['user_group']==1){
					// set admin group to client
				if (isset($postData["group_id"])) {
					$admin_group = $postData["group_id"];
				}
			}else if(isset($_SESSION['user_group'])&&$_SESSION['user_group']==2){
				$admin_group = 3;
			}else{
				$status = 1;
				$id = "0";
				$admin_group = 2;
			}

			if ($this->session->userdata('user_id')) {
				# code...
			}

			if (!empty($postData['password'])) {
				$user_data = array(
			        'username' 			=> $postData['username'],
			        'password' 			=> md5($salt . $postData['password']),
			        'email' 			=> $postData['email'],
			        'created_date'		=> time(),
			        'verification_key'	=> $this->generateVerificationKey(),
			        'user_group'		=> $group_id,
			        'name'				=> $postData['name'],
			        'created_by'		=> $id,
			        'status'			=> 0,
			        'verify'			=> 1,
			        'campaign_text'		=> $project_text
				);
			}else{
				$user_data = array(
			        'username' 			=> $postData['username'],
			        'email' 			=> $postData['email'],
			        'verification_key'	=> $this->generateVerificationKey(),
			        'name'				=> $postData['name'],
			        'campaign_text'		=> $project_text
				);
			}

			if ($this->session->userdata('user_id')) {

				if (!empty($postData['id'])) {
					$submit = $this->db->update('users', $user_data);
					$last_id = $this->db->insert_id();
				}else{
					$submit = $this->db->insert('users', $user_data);
					$last_id = $postData['id'];
					$this->db->where('user_id='.$id);
					$this->db->delete('project_assign_id');
				}
				if (!empty($postData['project_id'])) {
					$project_id = explode(",", $postData['project_id']);
					for ($i=0; $i <count($project_id) ; $i++) {
						$this->db->set('project_id', $id);
						$this->db->set('user_id', $last_id);
						$this->db->insert('project_assign_id');
					}
				}
			}else{
				$submit = $this->db->insert('users', $user_data);
			}
			return $submit ? true: false;
		}
	}

	public function updateUserbyAdmin($id, $postData){
		$error = 0;
		$error1 = 0;
		$passwordquery= "";
		$get_email = "";
		$get_username="";
		$group_query="";
		$username = strip_tags($postData["username"]);
		$email = strip_tags($postData["email"]);

		if (!empty($postData["password"]) && !empty($postData["password2"])) {
            $password = strip_tags($postData["password"]);
            $password2 = strip_tags($postData["password2"]);
            if (isset($_SESSION['user_id'])) {
            	$ses_id = $this->db->escape(strip_tags($_SESSION["user_id"]));
            	$salt = $this->generateSalt();
	            if ($password === $password2) {
	                $password = $this->db->escape(md5($salt . $password));
	                $passwordquery .= ", password = ".$password ." ";
	            } else {
	                $passwordquery = " ";
	            }
            }
        }

		$verification_key = $this->db->escape($this->generateVerificationKey());
		$salt = $this->generateSalt();

		$password = $this->db->escape(md5($salt . $postData['password']));
		$email = $this->db->escape(strip_tags($postData['email']));
		$username = $this->db->escape(strip_tags($postData['username']));
		$name = $this->db->escape(strip_tags($postData['name']));
		if ($error > 0) {
			return $error;
		}

		$sql = "SELECT * FROM users WHERE id = " . $id;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0 ) {
        	$get_username = $this->db->escape($query->row()->username);
        	$get_email = $this->db->escape($query->row()->email);
        }

		$now = $this->db->escape(time());
		$sql = "SELECT * FROM users WHERE username = " . $username. "AND NOT username =".$get_username;
		$query = $this->db->query($sql);
		$sql1 = "SELECT * FROM users WHERE email = " .$email. "AND NOT email =".$get_email;
		$query1 = $this->db->query($sql1);
		if ($query1->num_rows() > 0) {
			$error = 1;
		}if ($query->num_rows() > 0) {
			$error1 = 1;
		}

		if ($error>0&&$error1>0) {
			return 4;
		}else if ($error>0) {
			return 3;
		}else if ($error1>0) {
			return 2;
		}
		if($error==0&&$error1==0){
			if (isset($postData["group_id"])) {
				$group_query = ",user_group =".$postData["group_id"];
					$admin_group = $postData["group_id"];
			}
			if($_SESSION['user_group']==2){
				$group_query = ",user_group =3";
			}
	        $sql = "UPDATE users SET  username = $username".$group_query." ,email = $email, name = $name ".$passwordquery." WHERE id = " . $id;
			$update = $this->db->query($sql);
			return $update ? true : false;
		}

	}

	public function verify_email($id, $v_key){
		$sql = "SELECT * from users where id = '$id' AND Status !='0' AND verification_key = '$v_key'";
		$query=$this->db->query($sql);
		if ($query->num_rows()>0) {
			$sql = "UPDATE users SET status ='0'";
			$this->db->query($sql);
			return true;
		}else{
			return false;
		}
	}
	public function updateUserStatus(){
		$stat = $this->input->post('stat');
		$id = $this->input->post('id');
		$query = $this->db->query('SELECT status from users where id='.$id);
		foreach($query->result() as $status){
			if ($status->status==1) {
				$stat = 0;
			}else{
				$stat = 1;
			}
		}
		$sql = "UPDATE users SET status = ".$stat." WHERE id =".$id;
		$query = $this->db->query($sql);
		if ($query) {
			if ($stat==1) {
				return 0;
			}else{
				return 1;
			}
		}
	}
	

	public function adminLogin($postData){
		if (isset($postData["username"])&&isset($postData["password"])) {
			$result	  = "";
			$salt 	  = $this->generateSalt();
			$username = $this->db->escape(strip_tags($postData["username"]));
			$password = $this->db->escape(strip_tags(md5($salt . $postData["password"])));

			$sql 	  = "SELECT * FROM users WHERE username = " . $username . " AND password = " . $password;
			$query 	  = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				$q = $query->row();
				if ($q->verify == 1) { // Not verrified
					return 3;
				}else if($q->status==1){
					return 4;
				}else{
					$get_user = $this->db->query("SELECT * FROM users_groups WHERE id = ".$q->user_group);
					$result = "true";
				}
			} else {
				return false;
			}
		}
	 	$this->db->select('*')->from('user_permission');
        $this->db->where("id", $q->user_permission);
        $data = $this->db->get()->row_array();

		if ($result=="true") {
			$gq = $get_user->row();
			$this->session->set_userdata("username", $q->username);
			$this->session->set_userdata("verification_key", $q->verification_key);
			$this->session->set_userdata("user_id", $q->id);
			$this->session->set_userdata("user_group", $q->user_group);
			$this->session->set_userdata("role_slug", $gq->role_slug);
			$this->session->set_userdata("is_logged_in", 1);
			$this->session->set_userdata("permission", $data['permission']);
			$ip = $this->getUserIP();
			$time = $this->db->escape(strip_tags($postData['timezone']));
			$sql = "UPDATE users SET timezone=".$time.",last_signin = NOW(), ip = " . $this->db->escape($ip) . " WHERE id = " . $q->id;
			$this->db->query($sql);
			return TRUE;
		}
	}

	public function verifyUser()
	{
		if ($this->session->userdata("username") && $this->session->userdata("verification_key") && $this->session->userdata("admin_id") && $this->session->userdata("loggedin")) {
			$sql = "SELECT * FROM users WHERE id = " . $this->db->escape(strip_tags((int)$this->session->userdata("admin_id"))) . " AND verification_key = " . $this->db->escape(strip_tags($this->session->userdata("verification_key"))) . " AND username = " . $this->db->escape(strip_tags($this->session->userdata("username")));
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return TRUE;
			} else {
				$this->logout();
				redirect(base_url() . "login", 'auto');
			}
		} else {
			$this->logout();
			redirect(base_url() . "login", 'auto');
		}
	}

	// add new contact group
	public function addContactGroup($name)
	{
		$sql = "SELECT * FROM contacts_group WHERE group_name ='".$name."'";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return 1;
		}
		else {
			$data = array(
				'group_name' => $name,
				'created_by'=>	$this->session->userdata('user_id')
			);
			$res = $this->db->insert('contacts_group', $data);
			return $res ? true : false;
		}
	}

	public function delete_single_contact($postData){
		$sql = "UPDATE contacts set cstatus= 2 WHERE id =".$postData['id'];
		$query = $this->db->query($sql);
		return $query ? true: false;
	}

	//active selected contact
	public function activate_contact($postData){
		for($i=0;$i<count($postData['data']);$i++){
			$sql = "UPDATE contacts set cstatus= 1 WHERE id =".$postData['data'][$i];
			$query = $this->db->query($sql);
		}
		return $query ? true: false;
	}

	//do not send selected contact
	public function do_not_send($postData){
		for($i=0;$i<count($postData['data']);$i++){
			$sql = "UPDATE contacts set cstatus= 0 WHERE id =".$postData['data'][$i];
			$query = $this->db->query($sql);
		}
		return $query ? true: false;
	}

	public function contact_msg_stat($postData){
		$id = $postData['id'];
		$stat = $postData['stat'];
		$status = "";
		if ($stat==1) {
			$status = 0;
		}else{
			$status = 1;
		}
		$sql = "UPDATE contacts set msg_status =".$status." WHERE id =".$id;
		$query = $this->db->query($sql);
		return $query ? true: false;
	}
	//get all user group
	public function getUserGroups(){
		$this->db->select('*');
		$this->db->from('users_groups');
		$this->db->order_by('id', 'DESC');
		return $this->db->get()->result_array();
	}

	// get all contact group
	public function getContactGroups()
	{
		// select a.gid,a.group_name from contacts_group a left join users b on a.created_by = b.id

		$this->db->select('a.gid,a.group_name')->from('contacts_group a')->join('users b','a.created_by = b.id','left');
		if ($this->session->userdata('user_group')==2) {
			$this->db->where('b.created_by = '.$this->session->userdata('user_id').' OR a.created_by = '.$this->session->userdata('user_id'). ' AND a.status=1');
		}else if ($this->session->userdata('user_group')==3) {
			$this->db->where('a.created_by = '.$this->session->userdata('user_id').' AND a.status=1');
		}
		$this->db->order_by('a.group_name', 'ASC');
		return $this->db->get()->result_array();
	}

	// get single contact group
	public function getsingleContactGroups($id)
	{
		$this->db->select('*');
		$this->db->from('contacts_group');
		$this->db->where('gid', $id);
		$query = $this->db->get();
		return $query->row();
	}

	// update contact group
	public function updateContactGroup($id, $gname)
	{
		$sql = "SELECT * FROM contacts_group WHERE group_name ='".$gname."'";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return 1;
		}else {
			$this->db->set('group_name', "'".$gname."'", FALSE);
			$this->db->where('gid', $id);
			$res = $this->db->update('contacts_group');
			return $res ? true : false;
		}
	}

	// delete contact group
	public function deleteContactGroups($id)
	{
		$this->db->where('gid', $id);
		$res = $this->db->delete('contacts_group');
		return $res ? true : false;
	}

	public function getContacts(){
		$this->db->select('*')->from('contacts');
		return $this->db->get()->result_array();
	}
	
	public function create_contact($postData){
		$phone = preg_replace('/\D+/', '', $postData['number']);
		$email= $postData['email'];
		$res = $this->check_duplicate_contact($email, $phone,$this->session->userdata("user_id"));
		if ($res<1) {
			echo $res;
			$postData['number'] = preg_replace('/\D+/', '', $postData['number']);
			$postData['cstatus'] = !isset($postData['cstatus']) ? 0 : 1;
			$postData['email'] = $email;
			$postData['created_by'] = !$this->session->userdata("user_id") ? 1 : $this->session->userdata("user_id");
			$res = $this->db->insert('contacts', $postData);
			return $res ? true : false;
		}else {
			return $res;
		}

	}
	public function update_contact($postData){
		$error = "";
		$error1 = "";
		$phone = preg_replace('/\D+/', '', $postData['number']);
		$email= $postData['email'];
		$get_number = "";
		$get_email = "";
		$id = $postData['id'];
		$cstatus="";
		$name = $this->db->escape(strip_tags($postData['name']));
		$email = $this->db->escape(strip_tags($postData['email']));
		$sql= $this->db->query('SELECT number,email from contacts where id='.$id);
		$group_id = $postData['group_id'];

		foreach ($sql->result() as $contact) {
			$get_number = $this->db->escape(strip_tags($contact->number));
			$get_email = $this->db->escape(strip_tags($contact->email));
		}

		$query = $this->db->query('SELECT * FROM contacts WHERE cstatus!=2 AND number ='.$phone." AND number!=".$get_number." AND created_by =".$this->session->userdata("user_id"));
		$query1 = $this->db->query('SELECT * FROM contacts WHERE cstatus!=2 AND email ='.$email." AND email!=".$get_email ." AND created_by =".$this->session->userdata("user_id"));
		if ($query->num_rows()>0) {
			$error = 1;
		}else if($query1->num_rows()>0){
			$error1= 1;
		}
		if ($error!=""&&$error1!="") {
			return 4;
		}else if ($error!="") {
			return 3;
		}else if ($error1!="") {
			return 2;
		}

		if (isset($postData['cstatus'])) {
			$cstatus=1;
		}else{
			$cstatus=0;
		}

		if ($error==""&&$error1=="") {
			$name = $postData['name'];
			$email = $postData['email'];
			$data = array(
			        'name' => $name,
			        'email' => $email,
			        'number'=> $phone,
			        'group_id'=> $group_id,
			        'cstatus'=> $cstatus
			);
			$this->db->where('id', $id);
			$res = $this->db->update('contacts', $data);
			return $res ? true : false;
		}else {
			return false;
		}

	}

	// add new contact
	public function addContact($postData)
	{

		$phone = preg_replace('/\D+/', '', $postData['number']);
		$email= $postData['email'];
		$res = $this->check_duplicate_contact($email, $phone,$this->session->userdata("user_id"));
		if ($res==0) {
			$postData['number'] = preg_replace('/\D+/', '', $postData['number']);
			$postData['cstatus'] = !isset($postData['cstatus']) ? 0 : 1;
			$postData['email'] = $email;
			$postData['created_by'] = !$this->session->userdata("user_id") ? 1 : $this->session->userdata("user_id");
			$res = $this->db->insert('contacts', $postData);
			return $res ? true : false;
		}else {
			return $res;
		}
	}

	// get single contact group
	public function getsingleContact($id)
	{
		$this->db->select('*');
		$this->db->from('contact');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	//get single user group
	public function getsingleUser($id){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row();
	}


	// update contact
	public function updateContact($id, $postData)
	{
		$postData['number'] = preg_replace('/\D+/', '', $postData['number']);
		$postData['cstatus'] = !isset($postData['cstatus']) ? 0 : 1;
		$this->db->set($postData);
		$this->db->where('id', $id);
		$res = $this->db->update('contacts');
		return $res ? $postData : false;
	}

	// delete contact
	public function deleteContact($id)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('contacts');
		return $res ? true : false;
	}

    //get and return credit rows
    public function getCredits($id = ''){
        $this->db->select('*');
        $this->db->from('credits');
        if (!$this->session->userdata("role_slug") === 'administrator'){
            $this->db->where('credits.created_by', $this->session->userdata("user_id"));
        }

        if($id){
                $this->db->where('id',$id);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
               $this->db->order_by('credits.id', 'DESC');
                $query = $this->db->get();
                $result = $query->result_array();
            }
            return !empty($result)?$result:false;
    }

    // get single contact group
	/* | Credits Section */
	public function add_update_credits(){
		$post = $this->input->post();
		$data = array(
			'credit_name' 		=> $post['credit_name'],
			'credit_price' 		=> $post['credit_price'],
			'credit_amount' 	=> $post['credit_amount'],
			'credit_currency' 	=> $post['credit_currency'],
			'credit_desc' 		=> $post['credit_desc'],
			'credit_status' 	=> !isset($post['credit_status']) ? 0 : 1,
			'created_by' 		=> $this->session->userdata("user_id")
		);
		if($post['credit_id']){
			$this->db->where('id', $post['credit_id']);
			return $this->db->update('credits', $data) ? true : false;
		}else{
			return $this->db->insert('credits', $data) ? true : false;
		}
	}

	public function updateCredit($id, $postData){
		$postData['credit_status'] = !isset($postData['credit_status']) ? 0 : 1;
		$this->db->set($postData);
		$this->db->where('id', $id);
		$res = $this->db->update('credits');
		return $res ? $postData : false;
	}

    public function get_site_settings(){
        return $this->db->select('*')->from('site_setting')->get()->row_array();  
    }

	public function delete_credit($credit_id){
		$this->db->where('id', $credit_id);
		return $this->db->delete('credits') ? true : false;
	}
	/* | End Credits Section */

	public function update_credit_balance($credit_id){
		// Get My Credits
		$uid = $this->session->userdata("user_id");
		$credits = $this->db->select('*')->from('account_credits')->where('user_id', $uid)->get()->row_array();
		// Get Credit Details
		$c_details = $this->get_single_credit($credit_id);
		// Update Credits
		if($credits){
			$data = array('total_credits'=>($credits['total_credits']+$c_details['credit_amount']), 'credit_balance'=>($credits['credit_balance']+$c_details['credit_amount']));
			$this->db->where('user_id', $uid);
			return $this->db->update('account_credits', $data) ? true : false;
		}else{
			$data = array('user_id'=>$uid, 'total_credits'=>($credits['total_credits']+$c_details['credit_amount']), 'credit_balance'=>($credits['credit_balance']+$c_details['credit_amount']));
			return $this->db->insert('account_credits', $data) ? true : false;
		}
	}
	/* | Close Payments Section */

    //client's credit balance
    public function getClientCreditBalance($id)
	{
		$this->db->select('*, account_credits.total_credits as tCredits, account_credits.credit_balance as creditBal');
		$this->db->from('account_credits');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}
	//Conversation Page
	public function showThread($postData){
		$this->db->query("SET sql_mode= ''");
		$sql = "SELECT * FROM conversation con where con.id =".$postData['id'];
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function smsSender($type,$con_id,$message,$ses_id,$message_sid,$qoute){
		if ($qoute == "") {
			$qoute = 0;
		}

		$group_id;
		if ($type=='single') {
			$group_id = '0';
		}else{
			$group_id = '1';
		}

		$msg = $this->db->escape(strip_tags($message));
		if ($qoute!="") {
			$sql = "INSERT INTO conversation (message,from_id,to_id,group_id,sms_sid,msg_type,parent_id) VALUES ($msg,$ses_id,$con_id,$con_id,'$message_sid',0,$qoute)";
		}else{
			$sql = "INSERT INTO conversation (message,from_id,to_id,group_id,sms_sid,msg_type,parent_id) VALUES ($msg,$ses_id,$con_id,$con_id,'$message_sid',0,$qoute)";
		}
		$query = $this->db->query($sql);
		error_reporting(0);
		return $query;
	}
	public function add_tags($postData){
		$id = $postData['id'];
		$value = $this->db->escape(strip_tags($postData['value']));
		$sql = "UPDATE contacts set tags = $value where id =".$id;
		$query = $this->db->query($sql);
		return $query;
	}

	public function all_count_message($postData){
		$this->db->query("SET sql_mode= ''");
		$campaign = $postData['campaign'];
		$this->db->select("monthname(a.sent_time) as month,count(*) as total_text, sum(case a.status when 2 then 1 else 0 end) as undelivered,sum(case a.sms_status when 'received' then 1 else 0 end) as received")
		->from('conversation a')
		->join('contacts b','a.from_id = b.id OR a.to_id = b.id','left')
		->join('users c','c.id = b.created_by','left')
		->join('campaign d','c.id = d.user_id','left');

		$this->db->group_by('MONTH(a.sent_time) ASC');
		$query = $this->db->get();
		return $query->result();

		// select monthname(a.sent_time) as month,count(*) as total_text, sum(case a.status when 2 then 1 else 0 end) as undelivered,sum(case a.sms_status when 'delivered' then 1 else 0 end)+ sum(case a.sms_status when 'sent' then 1 else 0 end) as sent from conversation a left join contacts b on a.from_id = b.id OR a.to_id = b.id left join users c on c.id = b.created_by left join campaign d on c.id = d.user_id where a.status!=3 group by MONTH(a.sent_time) ASC



	}

	public function sms_status($postData,$ses_id,$group_id){
		$where="";
		$campaign = $postData['campaign'];


		$sql = "SELECT 
		round(sum(case con.sms_status when 'sent' then 1 else 0 end)/ (sum(case con.sms_status when 'sent' then 1 else 0 end)+sum(case con.sms_status when 'undelivered' then 1 else 0 end))*100, 2) as sent, 
		round(sum(case con.sms_status when 'undelivered' then 1 else 0 end)/ (sum(case con.sms_status when 'sent' then 1 else 0 end)+sum(case con.sms_status when 'undelivered' then 1 else 0 end))*100, 2) as undelivered 
		FROM conversation con LEFT JOIN contacts c ON con.from_id = c.id OR con.to_id = c.id LEFT JOIN users u ON u.id = c.created_by AND u.created_by = 2 LEFT JOIN campaign cam ON u.id = cam.user_id WHERE con.from_id = ".$ses_id." OR con.to_id = ".$ses_id." OR con.from_id = u.id OR con.to_id = u.id";
		$query = $this->db->query($sql);
		return $query->result();

	}
	public function get_percent_reply($postData,$ses_id){
		$where="";
		$campaign = $postData['campaign'];
		if ($campaign>0) {
			$where = "AND cam.id =".$campaign;
		}

		$sql = "SELECT round(count(con.to_id) / (SELECT count(con.to_id) as total FROM conversation con LEFT JOIN contacts c ON con.from_id = c.id OR con.to_id = c.id LEFT JOIN users u ON u.id = c.created_by LEFT JOIN campaign cam ON u.id = cam.user_id WHERE cam.created_by = ".$ses_id." AND con.to_id != u.id)* 100, 2) as all_reply FROM conversation con LEFT JOIN contacts c ON con.from_id = c.id OR con.to_id = c.id LEFT JOIN users u ON u.id = c.created_by LEFT JOIN campaign cam ON u.id = cam.user_id WHERE cam.created_by = ".$ses_id." AND con.to_id = u.id ".$where;
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_reply_status($postData,$ses_id){
		$where="";
		$campaign = $postData['campaign'];
		if ($campaign>0) {
			$where = "AND cam.id =".$campaign;
		}

		$sql = "SELECT con.message,cont.id,cont.name,con.sent_time,con.sms_reply_status from conversation con left join contacts cont on con.from_id = cont.id left join users u on u.id = cont.created_by left join campaign cam on cam.user_id = u.id where con.sms_status = 'received' AND cam.created_by = ".$ses_id." ".$where. " ORDER BY sent_time DESC LIMIT 3";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function fetch_message($ses_id,$user_group){
		$sql = "";
		if ($user_group==1) {
			$sql = "select * from message_template";
		}else{
			$sql = "select * from message_template where created_by = ".$ses_id;
		}
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function fetch_file($id){
		$sql = "SELECT * FROM message_template where id = ".$id;
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function update_file_template($postData,$filename,$ses_id,$parent_id,$extension){
		$file = $this->db->escape(strip_tags($filename));
		$type = $this->db->escape(strip_tags($extension));
		$id = $postData['id'];

		$sql = "UPDATE template_file SET parent_id = " . $parent_id . ",file_name=".$file.", file_type = ".$type." WHERE parent_id = ".$id;
		$query = $this->db->query($sql);
		return $query;
	}

	public function logout()
	{
		$this->session->unset_userdata("username");
		$this->session->unset_userdata("verification_key");
		$this->session->unset_userdata("user_group");
		$this->session->unset_userdata("user_id");
		$this->session->unset_userdata("role_slug");
		$this->session->unset_userdata("loggedin");
		return TRUE;
	}

	
	public function getConfigs(){
        $this->db->select('*')->from('site_setting');
        $query =  $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return 0;
        }
    }
    
    public function save_site_settings(){
    	$data = array(
            'send_grid_email' 		=> $this->input->post('send_grid_email'),
            'send_grid_key' 		=> $this->input->post('send_grid_api_key'),
            'twillio_account_sid'   => $this->input->post('twillio_account_sid'),
            'twillio_auth_token' 	=> $this->input->post('twillio_auth_token'),
            'twilio_account_number' => preg_replace('/[^A-Za-z0-9\-]/', '', $this->input->post('twilio_account_number')),
            'mailchimp_api_key' 	=> $this->input->post('mailchimp_api_key'),
            'paypal_sandbox' 		=> $this->input->post('paypal_sandbox'),
            'paypal_email' 			=> $this->input->post('paypal_email'),
            'paypal_client_id' 		=> $this->input->post('paypal_client_id'),
            'stripe_testmode' 		=> $this->input->post('stripe_testmode'),
            'stripe_test_skey' 		=> $this->input->post('stripe_test_skey'),
            'stripe_test_pubkey' 	=> $this->input->post('stripe_test_pubkey'),
            'stripe_live_skey' 		=> $this->input->post('stripe_live_skey'),
            'stripe_live_pubkey' 	=> $this->input->post('stripe_live_pubkey'),
        );
       	return $this->db->update('site_setting', $data) ? true : false;
	}
	
    public function transaction(){
    	$this->db->select('p.id as pid,u.username,p.payment_method,p.payment_state,p.total_payment, DATE_FORMAT(created_time, "%m-" "%d-" "%Y" " " "%I:" "%i " "%p") as date');
    	$this->db->from('payments p');
    	$this->db->join('users u', 'p.user_id = u.id', 'left');
    	$query = $this->db->get();
    	return $query->result();
    }
    public function total_transaction(){
    	$ac = '';
    	$u = '';
    	$
	    $this->db->select('
	    	round(sum(p.total_payment)/ 
	    	(count(con.id)/(SELECT count(*) FROM account_credits ac LEFT JOIN payments p ON ac.user_id = p.user_id '.$a.' )), 2) as total_payment,
	    	count(CASE when con.sms_status = "sent" THEN con.status END)/(SELECT count(*) from account_credits ac left join payments p on ac.user_id = p.user_id '.$ac.')+count(CASE when con.sms_status = "received" THEN con.status END)/(SELECT count(*) from account_credits ac left join payments p on ac.user_id = p.user_id) as total_message,ac.total_credits,ac.credit_balance');
		$this->db->from('account_credits ac');
		$this->db->join('payments p', 'p.user_id = ac.user_id '.$ac.'', 'left');
		$this->db->join('users u', ' u.id = p.user_id '.$u.'', 'left');
		$this->db->join('contacts c', 'u.id = c.created_by', 'left');
		$this->db->join('conversation con', 'con.from_id = u.id AND con.to_id = c.id OR con.from_id = c.id', 'left');
		$this->db->where('c.id IS NOT NULL AND con.id IS NOT null');
    		$query = $this->db->get();
    		return $query->result();
    }

    public function get_acc_credits($uid){
    	$user_group = $this->session->userdata('user_group');
    	$this->db->select('a.*, b.*')->from('account_credits a');
    	if($user_group==1){
    		$this->db->join('users b', 'a.user_id=b.id', 'left');
    		if($uid){ 
    			$this->db->where('c.user_id', $uid);
    			return $this->db->get()->row_array();
    		}
    		$data = $this->db->get()->result_array();

    		$this->db->select_sum('total_payment');
			$result = $this->db->get('payments')->row_array(); 
			return $result + $data;
    	}else{
    		$user_id = $this->session->userdata('user_id');
    		$this->db->where('user_id', $user_id);
    		return $this->db->get()->row_array();
    	}
    }

    public function get_user_transactions($uid){
    	$this->db->select('*')->from('payments a');
    	$this->db->join('users b', 'a.user_id=b.id', 'left');
    	if($uid){ 
    		$this->db->where('user_id', $uid); 
    	}
    	return $this->db->get()->result_array();
    }

    public function total_payment($uid){
    	// Total Payments
    	$this->db->select_sum('total_payment');
    	if($uid){ 
    		$this->db->where('user_id', $uid); 
    	}
		$payments = $this->db->get('payments')->row_array(); 

		$this->db->select_sum('total_credits');
    	if($uid){  	
    		$this->db->where('user_id', $uid); 
    	}
		$account_credits = $this->db->get('account_credits')->row_array(); 
		$this->db->select_sum('credit_balance');
    	if($uid){  	
    		$this->db->where('user_id', $uid); 
    	}
		$credit_balance = $this->db->get('account_credits')->row_array(); 

		return $payments + $account_credits + $credit_balance;
    }
    public function delete_groupContacts($postData){
    	$id = $postData['id'];
    	$this->db->set('status', '2', FALSE);
		$this->db->where('gid', $id);
		$data = $this->db->update('contacts_group');

		$this->db->set('group_id','0','FALSE')->where('group_id=',$id)->update('contact');
		return $data? true:false;

    }
    public function groupContacts_status($postData){
    	$id = $postData['id'];
    	$status = $postData['status'];
    	$val = '';
    	if ($status==1) {
    		$val = 0;
    	}else{
    		$val = 1;
    	}
    	$this->db->set('status', $val, FALSE);
		$this->db->where('gid', $id);
		$data = $this->db->update('contacts_group'); 
		return $data? $val:false;
    }
    public function add_contactGroup($postData){
    	$id = $postData['id'];
    	$name = $postData['name'];
    	$data_name = $postData['old_name'];
    	$query = '';

    	$this->db->select('*')->from('contacts_group')->where('created_by ='.$this->session->userdata('user_id'))->where('group_name', $name);
    	if (!empty($data_name)) {
    		$this->db->where('group_name !=', $data_name);
    	}
    	$query = $this->db->get();
	    if ($query->num_rows() > 0){
	        return 2;
	    }
	    else{
        	if ($id>0) {
				$this->db->set('group_name', $name);
				$this->db->where('gid', $id);
				$query = $this->db->update('contacts_group');
	    	}else{
	    		$data = array(
				        'group_name' => $name,
				        'created_by' => $this->session->userdata('user_id')
				);

				$query = $this->db->insert('contacts_group', $data);
	    	}
    		return $query ? true:false; 
	    }
  
    }

    public function getReply($body,$filename,$receive,$from,$sid,$quote,$extension,$group_id,$sms_status,$status){
		$date = new DateTime();
		$sent_time = $date->format('Y-m-d H:i:s');

        $data = array(
	        'message' 	=> $body,
	        'filename' 	=> $filename,
	        'from_id' 	=> $from,
	        'to_id'		=> $receive,
	        'sms_sid'	=> $sid,
	        'parent_id'	=> $quote,
	        'sent_time'	=> $sent_time,
	        'status'	=> $status,
	        'extension'	=> $extension,
	        'group_id'	=> $group_id,
	        'sms_status'=> $status
		);

		$data = $this->db->insert('conversation', $data);
		return $data? true:false;
    }
    public function opt_out($id){
		$this->db->set('status',3, FALSE);
		$this->db->where('id', $id);
		$this->db->update('contact'); 
    }

    public function update_SID($sid,$id){
		$this->db->set('sms_sid', $sid);
		$this->db->where('id', $id);
		$this->db->update('conversation');
    }
    public function update_credit($pay){
		$this->db->set('credit_balance', 'credit_balance-'.$pay, FALSE);
		if ($this->session->userdata('user_group')==2||$this->session->userdata('user_group')==1){
			$this->db->where('user_id ='.$this->session->userdata('user_id'));
		}else if($this->session->userdata('user_group')==3){
			$this->db->where('user_id = (SELECT created_by from users where id='.$this->session->userdata('user_id').')');
		}
		$this->db->update('account_credits');
    }
	public function myCredit(){
		$id = "";
		if ($this->session->userdata('user_group')==2) {
			$id = $this->session->userdata('user_id');
		}else if($this->session->userdata('user_group')==3){
			$data = $this->db->select('b.created_by')->from('users b')->where('b.id='.$this->session->userdata('user_id'))->get()->row_array();
			$id = $data['created_by'];
		}
		$this->db->select('sum(a.credit_balance) as credit_balance')->from('account_credits a');
		if ($id!="") {
			$this->db->where('user_id',$id);
		}
		return $this->db->get()->row_array();
	}
	public function show_group($search,$valid_columns,$length,$start,$user,$status){
        $this->db->limit($length,$start);
        $this->db->select('a.gid,a.group_name,a.status,a.tags,b.username')->from('contacts_group a')->join('users b','a.created_by = b.id', 'left');
        ($status=="")? '': $this->db->where('a.status='.$status);
        ($user=="")? '': $this->db->where('a.created_by='.$user);
        if ($this->session->userdata('user_group')!=1) {
        	$this->db->where('a.created_by='.$this->session->userdata('user_id'). ' AND a.status!=2');
        }
        // (empty($status))? " " : $this->db->where('status='.$status);

        if(!empty($search))
        {
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $this->db->order_by('a.gid','DESC');
        return $this->db->get();
	}
	public function totalGroup($search,$valid_columns,$user,$status){
		$query = $this->db->select("COUNT(*) as num")->from('contacts_group a')->join('users b','a.created_by = b.id', 'left');
	 	if ($this->session->userdata('group_name')!=1) {
        	$this->db->where('a.created_by='.$this->session->userdata('user_id'));
        }
        ($status=="")? '': $this->db->where('a.status='.$status);
        ($user=="")? '': $this->db->where('a.created_by='.$user);
        if(!empty($search))
        {
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $result = $this->db->get()->row();
        if(isset($result)) return $result->num;
        return 0;
	}

	public function show_contacts($search,$valid_columns,$length,$start,$group,$users,$cstatus){
		// select * from contact a left join contacts_group b on a.group_id = b.gid left join users c on a.created_by=c.id

        $this->db->limit($length,$start);
        $this->db->select('a.id,a.number,a.firstName,a.lastName,a.email,a.cstatus,a.tags,b.group_name,c.name user_name')
        ->from('contact a')
        ->join('contacts_group b','a.group_id = b.gid','left')
        ->join('users c','a.created_by=c.id', 'left');

        if ($this->session->userdata('user_group')==3) {
        	$this->db->where('a.created_by='.$this->session->userdata('user_id'));
        }else if ($this->session->userdata('user_group')==2) {
        	$this->db->where('c.created_by='.$this->session->userdata('user_id'). ' OR a.created_by='.$this->session->userdata('user_id'));
        }

        if ($this->session->userdata('user_group')!=1) {
        	$this->db->where('a.cstatus!=',0);
        }

        if (!empty($group)) {
        	$this->db->where('b.gid='.$group);
        }

        echo $users;
        if (!empty($users)) {

        	$this->db->where('c.id='.$users);
        }

        if (!empty($cstatus)) {
			$cs = ($cstatus=='5') ? 0 : $cstatus;
        	$this->db->where('a.cstatus='.$cs);
        }

        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        return $this->db->get();
	}
	public function total_contacts($search,$valid_columns,$group,$users,$cstatus){
		$query = $this->db->select("COUNT(*) as num")->from('contact a')->join('contacts_group b','a.group_id = b.gid','left')->join('users c','a.created_by = c.id', 'left');
	 	if ($this->session->userdata('user_group')==3) {
        	$this->db->where('a.created_by='.$this->session->userdata('user_id'));
        }else if ($this->session->userdata('user_group')==2) {
        	$this->db->where('c.created_by='.$this->session->userdata('user_id'). ' OR a.created_by='.$this->session->userdata('user_id'));
        }

        if (!empty($group)) {
        	$this->db->where('b.gid='.$group);
        }

        if (!empty($users)) {
        	$this->db->where('c.id='.$users);
        }

        if (!empty($cstatus)) {
        	$this->db->where('a.cstatus='.$cstatus);
        }

        if(!empty($search))
        {
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $result = $this->db->get()->row();
        if(isset($result)) return $result->num;
        return 0;
	}

	public function submitContact_bkp($postData){
		$data = "";
		$action = "";

		$phone = preg_replace('/\D+/', '', $postData['number']);
		$email= $postData['email'];
		$res = $this->check_duplicate_contact($email, $phone,$this->session->userdata("user_id"));
		if ($res<1) {

	        $data = array(
		        'firstName'	=> $postData['firstName'],
		        'lastName' 	=> $postData['lastName'],
		        'number' 	=> preg_replace('/\D+/', '', $postData['number']),
		        'email'		=> $postData['email'],
		        'group_id'	=> $postData['group_id'],
		        'cstatus'	=> !isset($postData['cstatus']) ? 2 : 1,
		        'created_by'=> $this->session->userdata('user_id')
			);

			$res = $this->db->insert('contact', $data);
			return $res ? true : false;
		}else {
			return $res;
		}
	}

	public function submitContact(){
		$phone 		= preg_replace('/\D+/', '', $this->input->post('number'));
		$old_phone 	= preg_replace('/\D+/', '', $this->input->post('old_number'));
		if($phone != $old_phone){
			if($this->check_contact_exist($phone, 0, 1)){
				return 2;
			}else{
				return $this->insert_update_contact($phone);
			}
		}else{
			if($this->input->post('email') != $this->input->post('old_email')){
				if($this->check_contact_exist(0, $this->input->post('email'), 1)){
					return 2;
				}else{
					return $this->insert_update_contact($phone);
				}
			}else{
				return $this->insert_update_contact($phone);
			}
		}
	}

	public function insert_update_contact($phone){
		$data = array(
			'firstName' => ucwords(strtolower($this->input->post('firstName'))),
			'lastName' 	=> ucwords(strtolower($this->input->post('lastName'))),
			'number' 	=> $phone,
			'email'		=> $this->input->post('email'),
			'group_id'	=> $this->input->post('group_id'),
			'cstatus'	=> $this->input->post('cstatus') ? 1 : 2,
			'created_by'=> $this->session->userdata('user_id')
		);
		if($this->input->post('id')){
			$this->db->where('id', $this->input->post('id'));
			return $this->db->update('contact', $data) ? 1 : 0;
		}else{
			return $this->db->insert('contact', $data) ? 1 : 0;
		}
	}

	//delete contact on select 
	public function delete_contact($postData){
		$query ="";
		for ($i=0; $i < count($postData['data']) ; $i++) { 
	        $data = array(
		        ($postData['type']=='contact')? 'cstatus': 'status'	=> 0
			);
			$this->db->where(($postData['type']=='contact')? 'id': 'gid', $postData['data'][$i]);
				$update = $this->db->update($postData['type'],$data);
		}
		return $data? true:false;
	}

	//Restore contact on select 
	// public function restore_contact($id){
	// 	$data['cstatus'] = 1;
	// 	$this->db->where('id', $id);
	// 	return $this->db->update('contact',$data) ? true : false;
	// }
	public function restore_contact($id,$type){
		// $type=($type=='contact')? 'cstatus':'status';
		// $data[$type];
		($type=='contact')? $data['cstatus']=1 : $data['status'] = 1;
		$this->db->where(($type=='contact')? 'id' : 'gid', $id);
		return $this->db->update($type,$data) ? true : false;
	}

	public function contact_status($postData){
		$data = "";

		for ($i=0; $i <count($postData['id']) ; $i++) {
	        $data = array(
		        ($postData['type']=='contact')? 'cstatus': 'status'		=> $postData['status'],
			);
			if ($postData['id'][$i]!="on") {
				$this->db->where(($postData['type']=='contact')? 'id': 'gid', $postData['id'][$i]);
				$update = $this->db->update($postData['type'],$data);
			}
		}
		return $update? true:false;
	}

	public function check_duplicate_contact($phone,$email){
		$error = "";
		$error1=  "";

		$this->db->where('created_by', $this->session->userdata('user_id'));
		$this->db->where('email',$email);
		$this->db->where('cstatus !=','2');

		if($this->db->count_all_results('contact')>0){
			$error = 1;
		}

		$this->db->or_where('number',$phone);
		$this->db->where('cstatus !=','2');
		$this->db->where('created_by', $this->session->userdata('user_id'));
      	if($this->db->count_all_results('contact')>0){
			$error1 = 1;
		}
		if ($error!=""&&$error1!="") {
			return 4;
		}else if ($error1!="") {
			return 3;
		}else if ($error!="") {
			return 2;
		}else{
			return 0;
		}
	}

	public function check_contact_exist($phone, $email, $type){
		if($email){
			$this->db->select('email')->from('contact');
			$this->db->where('email', $email);
			if($this->session->userdata('user_group')!=1){
				$this->db->where('cstatus !=', 0);
			}
			$this->db->where('created_by', $this->session->userdata('user_id'));
			$is_email = $this->db->get()->row_array();
			if($is_email){
				return true;
			}else{
				$is_number = $this->check_phone_exist($phone, $type);
				return $is_number ? true : false;
			}
		}else{
			$is_number = $this->check_phone_exist($phone, $type);
			return $is_number ? true : false;
		}
	}

	public function check_phone_exist($phone, $type){
		if($phone){
			$this->db->select('number')->from('contact');
			$this->db->where('number', $phone);
			if($this->session->userdata('user_group')!=1){
				$this->db->where('cstatus !=', 0);
			}
			$this->db->where('created_by', $this->session->userdata('user_id'));
			$is_number = $this->db->get()->row_array();
			return $is_number ? true : false;
		}
		return false;
	}

	// import contact group
	public function importContact($data)
	{
		$res = $this->db->insert_batch('contact', $data);
		return $res?true:false;
	}
	public function show_users($search,$valid_columns,$length,$start,$role){
		$this->db->limit($length,$start);
		$this->db->select('a.id,a.name,a.username,a.email,a.status,b.name as user_group,c.name as created_by')->from('users a')->join('users_groups b','a.user_group=b.id','left')->join('users c','a.created_by = c.id','left');
		if ($this->session->userdata('user_group')==2) {
			$this->db->where('a.created_by='.$this->session->userdata('user_id'));
		}else if ($this->session->userdata('user_group')==3) {
			$this->db->join('users d','c.id = d.created_by','left');
			$this->db->where('d.id='.$this->session->userdata('user_id'));
		}

		if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                    if ($this->session->userdata('user_group')==1) {
                    	$this->db->where('b.id='.$role);
                    }
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                	if ($this->session->userdata('user_group')==1) {
                    	$this->db->where('b.id='.$role);
                    }
                }
                $x++;
            }                 
        }else{
            if ($this->session->userdata('user_group')==1) {
            	$this->db->where('b.id='.$role);
            }
        }
		return $this->db->get();
	}
	public function total_users($search,$valid_columns,$role){
		$query = $this->db->select("COUNT(*) as num")->from('users a')->join('users_groups b','a.user_group=b.id','left')->join('users c','a.created_by = c.id','left');
		if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                    if ($this->session->userdata('user_group')==1) {
                    	$this->db->where('b.id='.$role);
                    }
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                	if ($this->session->userdata('user_group')==1) {
                    	$this->db->where('b.id='.$role);
                    }
                }
                $x++;
            }                 
        }else{
            if ($this->session->userdata('user_group')==1) {
            	$this->db->where('b.id='.$role);
            }
        }
		$result = $this->db->get()->row();
        if(isset($result)) return $result->num;
        return 0;
	}
	public function update_user($postData){
		$submit = $this->db->set('status',2)->where('id='.$postData['id'])->update('users');
		return $submit? true:false;
	}

	// MJ Codes ================================================================================================================================================
	public function get_user_id_name($id){
		return $this->db->select('id, name')->from('users')->where('id !=', $id)->order_by('name', 'ASC')->get()->result_array();
	}

	public function get_user_lists($table, $column_order, $limit, $offset, $search, $order, $role){
        $this->db->select('a.*, b.name as role_name, c.name as created_by_name')->from('users a');
		$this->db->join('users_groups b', 'b.id = a.user_group', 'left');
		$this->db->join('users c', 'c.id = a.created_by', 'left');
		$this->db->where('a.user_group', $role);
		if($this->input->post('created_by')){
			$this->db->where('a.created_by', $this->input->post('created_by'));
		}
		if($this->input->post('by_status')!='all_status'){
			$this->db->where('a.status', $this->input->post('by_status'));
		}
		$columns = $this->db->list_fields('users');

		/* If the user uses the search */  
		if($search){
			$this->db->group_start();
			foreach ($column_order as $item){
				$this->db->or_like($item, $search['value']);
			}
			$this->db->group_end();
		}

		/* If the user uses the order sort */
		if(isset($order)){
			$this->db->order_by($column_order[$order['0']['column']], $order['0']['dir']);
		}

        // Count results
        $temp 			= clone $this->db;
		$data['count'] 	= $temp->count_all_results();
        
        // Show number of results
		if($limit != -1){
			$this->db->limit($limit, $offset);
		}
        
        // Get the data
		$query = $this->db->get();
		$data['data'] = $query->result();

        // Count List
        $this->db->where('user_group', $role);
        $data['count_all'] = $this->db->count_all_results($table);
		return $data;
	}
	
	public function delete_user($id){
		$data['status'] = 2;
		$this->db->where('id', $id);
		return $this->db->update('users', $data) ? true : false;
	}

	public function get_this_user($id){
		return $this->db->select('a.*, b.project_id')->from('users a')->join('project_assign_id b', 'b.user_id = a.id', 'left')->where('a.id', $id)->get()->row_array();
	}

	public function update_project_id(){
		// Coming soon
	}

	public function template($name,$link,$verification_key,$email,$subject){
		$title 		= "Verify your Email Address";
		$message 	= 'Welcome to <strong><a href="'.base_url().'" rel="noopener" style="text-decoration: none; color: #3e6edf;" target="_blank" title="My Soapbox">My Soapbox</a></strong>';
		$message1 	= " Please click the button below to verify your email address. ";
		$button 	= "CONFIRM EMAIL";
		$message2 	= "If you did not sign up to My Soapbox, please ignore this email.";
        $from 		= 'noreply@getmysoapbox.com';
        $template ='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml"><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><meta content="width=device-width" name="viewport"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><title></title><style type="text/css">body{margin:0;padding:0}table,td,tr{vertical-align:top;border-collapse:collapse}*{line-height:inherit}a[x-apple-data-detectors=true]{color:inherit!important;text-decoration:none!important}</style><style id="media-query" type="text/css">@media (max-width:520px){.block-grid,.col{min-width:320px!important;max-width:100%!important;display:block!important}.block-grid{width:100%!important}.col{width:100%!important}.col>div{margin:0 auto}img.fullwidth,img.fullwidthOnMobile{max-width:100%!important}.no-stack .col{min-width:0!important;display:table-cell!important}.no-stack.two-up .col{width:50%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num8{width:66%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num3{width:25%!important}.no-stack .col.num6{width:50%!important}.no-stack .col.num9{width:75%!important}.video-block{max-width:none!important}.mobile_hide{min-height:0;max-height:0;max-width:0;display:none;overflow:hidden;font-size:0}.desktop_hide{display:block!important;max-height:none!important}}</style></head><body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #ffffff;"><table bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; width: 100%;" valign="top" width="100%"><tbody><tr style="vertical-align: top;" valign="top"><td style="word-break: break-word; vertical-align: top;" valign="top"><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;"><img align="center" alt="Image" border="0" class="center autowidth" src="'.base_url().'assets/Email_image/email.jpg" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 500px; display: block;" title="Image" width="500"/></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.8; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 25px;"><p style="font-size: 30px; line-height: 1.8; word-break: break-word; text-align: center; mso-line-height-alt: 54px; margin: 0;"><span style="font-size: 30px;"><strong>'.$title.'</strong></span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"></div><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 24px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 29px; margin: 0;"><span style="font-size: 24px;">'.$message.'</span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message1.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#3e6edf;border-radius:60px;-webkit-border-radius:60px;-moz-border-radius:60px;width:auto; width:auto;;border-top:1px solid #3e6edf;border-right:1px solid #3e6edf;border-bottom:1px solid #3e6edf;border-left:1px solid #3e6edf;padding-top:5px;padding-bottom:5px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><a href="'.$link.'" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #3e6edf; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; width: auto; width: auto; border-top: 1px solid #3e6edf; border-right: 1px solid #3e6edf; border-bottom: 1px solid #3e6edf; border-left: 1px solid #3e6edf; padding-top: 5px; padding-bottom: 5px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">'.$button.'</span></span></a></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:40px;padding-right:10px;padding-bottom:40px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message2.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div></td></tr></tbody></table></body></html>
        ';
		$this->email->from($from,'My Soapbox');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($template);
		return ($this->email->send()) ? true : false;
	}
	// Clsoe MJ Codes ================================================================================================================================================
}
