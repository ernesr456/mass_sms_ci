
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
    public function show_pricing(){
        $this->db->select('*')->from('pricing_table')->where('status!=2');
        $data = $this->db->get()->result_array();
        $i = 0;
        foreach ($data as $key => $value) {
            $array[$i]['id'] = $value['id'];
            $array[$i]['name'] = $value['name'];
            $array[$i]['pricing'] = $value['price'];
            $array[$i]['permission'] = json_decode($value['permission']);
            $i++;
        }
        return $array;
        // $array['name']= $data['name'];
        // $array['pricing']=$data['price'];
        // $array['permission'] = json_decode($data['permission']);
        // return $array;
    }
	public function submit_pricing(){
        $permission = array();
        $data = $this->input->post('permission');
        for ($i=0; $i <count($data) ; $i++) { 
            $permission = array_merge($permission, array($data[$i]['value']=>true));
        }
        
		$pricing_table = array(
            'name' 			=> $this->input->post('name'),
            'permission' 	=> json_encode($permission),
            "price"			=> $this->input->post('pricing')
        );
        if(!empty($this->input->post('id'))){
            $this->db->where('id',$this->input->post('id'));
            $res = $this->db->update('pricing_table', $pricing_table);
        }else{
            $res = $this->db->insert('pricing_table', $pricing_table);
        }
      	return $res ? true : false;
	}

    public function get_pricing($id){
        $this->db->select('*')->from('pricing_table')->where('id='.$id);
        return $this->db->get()->row_array();
    }
    public function purhcase_pricing($postData){
        $payments = array(
            'user_id'       => $this->session->userdata('user_id'),
            'pricing_id'    => $postData['pricing_id'],
            'payment_method'=> $postData['payment_method'],
            'total_payment' => $postData['total_payment'],
            'payer_email'   => $postData['payer_email'],
            'payer_status'  => $postData['payer_status'],
            'created_time'  => $postData['created_time']
        );
        $res = $this->db->insert('payments', $payments);
        return $res ? 1: 0;
    }
    public function get_transaction($search,$valid_columns,$length,$start){
        $this->db->select('b.price,b.name,DATE_FORMAT(a.created_time, "%c/%d/%Y") as date')->from('payments a')->join('pricing_table b','a.pricing_id = b.id','left');
        if ($this->session->userdata('user_group')!=1) {
            $this->db->where('user_id='.$this->session->userdata('user_id'));
        }

        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
                $con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        return $this->db->get();
    }
    public function my_transaction(){
        $this->db->select('a.total_payment,b.id,b.price,b.name,DATE_FORMAT(a.created_time, "%c/%d/%Y") as date')->from('payments a')->join('pricing_table b','a.pricing_id = b.id','left');
        if ($this->session->userdata('user_group')!=1) {
            $this->db->where('user_id='.$this->session->userdata('user_id'));
        }
        return $this->db->get()->result();
    }
    public function total_transaction($search,$valid_columns){
        $query = $this->db->select("COUNT(*) as num")->from('payments a')->join('pricing_table b','a.pricing_id = b.id','left');
        if ($this->session->userdata('user_group')!=1) {
            $this->db->where('user_id='.$this->session->userdata('user_id'));
        }

        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
                $con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $result = $this->db->get()->row();
        if(isset($result)) return $result->num;
        return 0;
    }
    public function get_package(){
        $this->db->select('max(a.created_time) as datetime, b.name')->from('payments a')->join('pricing_table b','a.pricing_id = b.id','left');
        if ($this->session->userdata('user_group')!=1) {
            $this->db->where('user_id='.$this->session->userdata('user_id'));
        }
        return $this->db->get()->result();
    }
    public function update_pricing($postData){
        $status = '';
        if ($postData['type']=='delete') {
            $status = 2;
        }
        $this->db->set('status', $status);
        $this->db->where('id=',$postData['id']);
        $submit = $this->db->update('pricing_table');
        return $submit? true:false;
    }
    public function submit_credit($postData,$type){
        $name = "";
        $price = "";
        $credit = "";
        $id = "";
        for($i=0;$i<count($postData);$i++){
            if ($postData[$i]['name']=='credit_name') {
                $name = $postData[$i]['value'];
            }else if($postData[$i]['name']=='currency-field'){
                $price = ltrim($postData[$i]['value'], '$');
            }else if($postData[$i]['name']=='credit'){
                $credit = $postData[$i]['value'];
            }else{
                $id = $postData[$i]['value'];
            }
        }

        if ($type=='insert') {
            $payments = array(
                'credit_name'       => $name,
                'credit_amount'    => $credit,
                'credit_price'=> $price,
                'credit_status' => 0
            );
            $res = $this->db->insert('credits', $payments);
        }else{
            $payments = array(
                'credit_name'   => $name,
                'credit_amount' => $credit,
                'credit_price'  => $price,
                'credit_status' => 0,
            );
            $this->db->where('id=',$id);
            $res = $this->db->update('credits', $payments);
        }
        return $res ? 1: 0;
    }
    
    public function show_credit(){
        $this->db->select('*')->from('credits')->where('credit_status', 0);
        return $this->db->get()->result_array();

    }
    public function update_credit($postData){
        $id = $postData['id'];
        $type = $postData['type'];
        $submit = "";
        if ($type=='delete') {
            $this->db->set('credit_status', 1);
            $this->db->where('id=',$id);
            $submit = $this->db->update('credits');
        }

        return $submit ? 1:0;
    }

        /* | Payments Section */
    public function purchase_credits(){
        $post = $this->input->post();
        $data = array(
            'txn_id'         => $post['txn_id'],
            'credit_id'      => $post['credit_id'],
            'payment_method' => $post['payment_method'],
            'total_payment'  => $post['total_payment'],
            'payer_email'    => $post['payer_email'],
            'payer_status'   => $post['payer_status'],
            'created_time'   => $post['created_time'],
            'user_id'        => $this->session->userdata("user_id")
        );
        $res = $this->db->insert('payments_credit', $data);
        if($res){
            return $this->update_credit_balance($post['total_credit']) ? true : false;
        }
        return false;
    }

    function update_credit_balance($total_credit){
        $submit = $this->db->set('total_credit','total_credit+'.$total_credit, FALSE)
        ->where('id=',$this->session->userdata('user_id'))
        ->update('users');
        return $submit ? true: false;
    }

    public function get_single_credit($credit_id){
        $this->db->select('*')->from('credits')->where('id', $credit_id);
        return $this->db->get()->row_array();
    }
    public function get_credit_transaction($search,$valid_columns,$length,$start){
        $this->db->select('b.credit_name,b.credit_amount,b.credit_price,DATE_FORMAT(a.created_time, "%c/%d/%Y") as date')->from('payments_credit a')->join('credits b','a.credit_id = b.id','left');
        if ($this->session->userdata('user_group')!=1) {
            $this->db->where('user_id='.$this->session->userdata('user_id'));
        }
        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
                $con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        return $this->db->get();
    }
    public function total_credit_transaction($search,$valid_columns){
        $query = $this->db->select("COUNT(*) as num")->from('payments_credit a')->join('credits b','a.credit_id = b.id','left');
        if ($this->session->userdata('user_group')!=1) {
            $this->db->where('user_id='.$this->session->userdata('user_id'));
        }

        if(!empty($search)){
            $x=0;
            foreach($valid_columns as $sterm)
            {
                $con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $result = $this->db->get()->row();
        if(isset($result)) return $result->num;
        return 0;
    }
    public function total_credit(){
        if ($this->session->userdata('user_group')==2) {
            $this->db->select_sum('total_credit')->from('users')->where('id='.$this->session->userdata('user_id'));
        }else if ($this->session->userdata('user_group')==3) {
            $this->db->select_sum('a.total_credit')->from('users a')->join('users b','a.created_by = b.id AND b.id='.$this->session->userdata('user_id'),'left');
        }else{
            $this->db->select_sum('total_credit')->from('users');
        }
        return $this->db->get()->row_array();
    }
    public function update_balance_credit($total_fee){
        
        $this->db->set('total_credit','total_credit-'.$total_fee, FALSE);
        $this->db->where('id=',$this->session->userdata('user_id'));
        $submit = $this->db->update('users');
    }
}
