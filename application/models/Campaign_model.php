<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Campaign_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	function project_submit($postData,$filename,$type,$id){
		$group_id = explode(",", $postData['group_id']);
		$assign_id = explode(",", $postData['user_id']);
		$name =  $postData['name'];
		$message = $postData['message'];
		$timezone = $postData['timezone'];
		$date = new DateTime("now", new DateTimeZone(''.$timezone) );
		$created_time = $date->format('Y-m-d H:i:s');
		$description = $postData['description'];
		$data = array(
	        'created_by' => $this->session->userdata('user_id'),
	        'proj_name' => $name,
	        'message' => $message,
	        'description'=> $description,
	        'created_time'=> $created_time,
	        'status'	=> 1,
	        'project_start'	=> $postData['start_at'],
	        'project_end'	=> $postData['end_at'],
	        'group_id'		=> $postData['group_id'],
	        'assign_id'		=> $postData['user_id'],
	        'groups_name'	=> $postData['group_text'],
	        'assign_name'	=> $postData['user_text'],
		);
		(!empty($filename))? $this->db->set('filename',$filename) : '';

		if (empty($postData['id'])) {
			$this->db->select('id')->where('proj_name='.$this->db->escape(strip_tags($name)))->from('project');
			$check_camp = $this->db->get();
			if ($check_camp->num_rows()>0) {
				return 2;
			}

			$this->db->insert('project', $data);

			$id = $this->db->insert_id();
			for ($i=0; $i <count($group_id) ; $i++) { 
				$this->db->set('project_id', $id);
				$this->db->set('group_id', $group_id[$i]);
				$this->db->insert('project_group_contact');
			}
			for ($i=0; $i <count($assign_id) ; $i++) { 
				$this->db->set('project_id', $id);
				$this->db->set('user_id', $assign_id[$i]);
				$this->db->insert('project_assign_id');
			}
		}else{
			$this->db->select('id')->where('proj_name='.$this->db->escape(strip_tags($name)).' AND proj_name !='.$this->db->escape(strip_tags($name)))->from('project');
			$check_camp = $this->db->get();
			if ($check_camp->num_rows()>0) {
				return 2;
			}

			$old_file = $this->db->select('filename')->where('id=',$postData['id'])->from('project')->get()->row_array();
			if (!isset($postData['filename'])) {
				if (!empty($old_file['filename'])) {
					$this->db->set('filename', $filename);
				}
			}
			// if (empty($filename)&&!isset(var)) {
			// 	if($old_file['filename']==$postData['filename']){

			// 	}
			// }

			$this->db->where('id', $postData['id']);
			$data = $this->db->update('project',$data);

			$this->db->where('project_id='.$postData['id']);
			$this->db->delete('project_assign_id');
			$this->db->where('project_id='.$postData['id']);
			$this->db->delete('project_group_contact');
			
			for ($i=0; $i <count($group_id) ; $i++) {
				$data = array(
			        'project_id' => $postData['id'],
			        'group_id'  => $group_id[$i]
				);
				$this->db->insert('project_group_contact',$data);
				

			}
			for ($i=0; $i <count($assign_id) ; $i++) { 
				$data = array(
			        'project_id' => $postData['id'],
			        'user_id'  => $assign_id[$i]
				);
				$this->db->insert('project_assign_id',$data);
			}
		}
		return $data ? true: false;

	}

	function show_campaign($id,$user,$status,$timezone,$type){

		$this->db->query("SET sql_mode= ''");
		$this->db->select("
		e.name,
		a.group_id,
		a.created_by,
		a.assign_id,
		a.id,
		c.group_name,FORMAT(count(c.gid),0) total_group,
		(SELECT FORMAT(count(*),0) from contact where group_id = c.gid) as total_contacts,
		a.proj_name,
		a.filename ,
		a.description,
		a.message ,
		e.name ,

		DATE_FORMAT(a.project_end, '%c/%d/%Y')as end_date,
		DATE_FORMAT(a.project_end, '%h:%i:%p') as end_time,
		DATE_FORMAT(a.project_start, '%c/%d/%Y')as start_date,
		DATE_FORMAT(a.project_start, '%h:%i:%p') as start_time,
		a.status,
		a.project_start,
		a.project_end, 
		count(CASE when g.sms_status = 'delivered' THEN g.status END)+count(CASE when g.sms_status = 'sent' THEN g.status END)+count(CASE when g.sms_status = 'received' THEN g.status END) as count_message,
		count(CASE when g.sms_status = 'delivered' THEN g.status END)+count(CASE when g.sms_status = 'sent' THEN g.status END) as total_sent,
		count(CASE when g.sms_status = 'undelivered' THEN g.status END) as total_undelivered,
		count(CASE when g.sms_status = 'received' THEN g.status END) as total_received")
		->from('project a')
		->join('project_group_contact b','a.id = b.project_id','left')
		->join('contacts_group c','b.group_id = c.gid','left')
		->join('contact d','c.gid = d.group_id','left')
		->join('users e','a.created_by = e.id','left')
		->join('users f','f.id = d.created_by','left')
		->join('conversation g','g.from_id = d.id OR g.to_id = f.id AND g.sent_time > a.project_start','left');

		if (!empty($status)) {
			date_default_timezone_set($timezone);
			$date = new DateTime();
			$sent_time = $date->format('Y-m-d H:i:s');
			if ($status==1) {
				$this->db->where('a.project_start > NOW()');
			}else if ($status==2) {
				$this->db->where('a.project_start < NOW() AND a.project_end > NOW()');
			}else if($status==3){
				$this->db->where('a.project_end < NOW() AND a.project_start < NOW()');
			}
		}
		// if ($this->session->userdata('user_group')==2) {
		// 	$this->db->where('a.status!=2 AND f.status!=3 ANd f.status!=2 AND a.created_by='.$this->session->userdata('user_id'));
		// }

		if (!empty($id)) {
			$this->db->where('a.id='.$id);
		}

		if (!empty($user)) {
			$this->db->where('a.created_by='.$user);
		}

		if ($this->session->userdata('user_group')==2) {
			$this->db->where('a.status!=2 AND a.created_by ='.$this->session->userdata('user_id').' OR e.created_by ='.$this->session->userdata('user_id'));
		}else if($this->session->userdata('user_group')==3){
			$this->db->where('a.status!=2 AND a.created_by ='.$this->session->userdata('user_id'));
		}


		$this->db->group_by('a.id');
		return $this->db->get()->result();
	}

	function update_status($postData){
		$id = $postData['id'];
		$status = $postData['status'];
		$val = "";
		if ($status==1) {
			$val = 0;
		}else if($status==0||$status==2){
			$val = 1;
		}

		$this->db->set('status', $val, FALSE);
		$this->db->where('id', $id);
		$data = $this->db->update('project');
		if ($data) {
			if ($status==1) {
				return 0;
			}else if($status==0||$status==2){
				return 1;
			}
		}
	}

	function fetch_project($id){
		$this->db->select('*')->where('status='.$id)->from('email');
		return $this->db->get()->result();
	}
	function count_message($id){
		$this->db->select('count(*) as count')->where('status='.$id)->from('email');
		return $this->db->get()->result();
	}
	function getProject($id){
		$this->db->select('*,DATE_FORMAT(created_time, "%c/%d/%Y")as date,DATE_FORMAT(created_time, "%h:%i:%p") as time');
		if ($this->session->userdata('user_group')==2) {
			$this->db->where('status!=2 AND created_by='.$this->session->userdata('user_id'));
		}
		if (!empty($id)) {
			$this->db->where('id='.$id);
		}
		$this->db->from('project');
		return $this->db->get()->row();
	}
	function getContactGroups(){
		$this->db->select('*');
		$this->db->from('contacts_group');
		return $this->db->get()->result_array();
	}
	function delete_campaign($postData){
		$id = $postData['id'];
		$this->db->set('status', 2, FALSE);
		$this->db->where('id', $id);
		$data = $this->db->update('project');

		return $data ? true: false;
	}
	function get_contacts($id){

		$this->db->query("SET sql_mode= ''");
		$this->db->select('a.id,c.id as contact_id,c.number,c.group_id,c.firstName,c.lastName,a.message,a.filename')
		->from('project a')
		->join('project_group_contact b','a.id=b.project_id','left')
		->join('contact c','b.group_id=c.group_id')
		->where('a.id='.$id. ' AND c.cstatus=1');
		return $this->db->get()->result_array();
	}

    public function send_message($message,$timezone,$filename,$contact_id,$quote,$extension,$contacts_group,$status){
		date_default_timezone_set($timezone);
		$date = new DateTime();
		$sent_time = $date->format('Y-m-d H:i:s');

        $data = array(
	        'message' 	=> $message,
	        'filename' 	=> $filename,
	        'from_id' 	=> $this->session->userdata('user_id'),
	        'to_id'		=> $contact_id,
	        'sent_time'	=> $sent_time,
	        'status'	=> $status,
	        'extension'	=> $extension,
	        'parent_id'	=> $quote,
	        'group_id'	=> $contacts_group
		);

		$data = $this->db->insert('conversation', $data);
		return $this->db->insert_id();
    }

    public function get_single_campaign($id){
    	$this->db->select('*')->from('project')->where('id='.$id);
    	return $this->db->get()->row_array();
    }
    public function get_single_number($id){
    	$this->db->select('*')->from('contact')->where('id='.$id);
    	return $this->db->get()->row_array();
    }

}
	
