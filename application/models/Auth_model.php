<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Auth_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	protected function generateSalt(){
		$salt = "xiORG17N6ayoEn6X3";
		return $salt;
	}

	protected function generateVerificationKey()
	{
		$length = 10;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function login($postData){
		$get_user="";
		if (isset($postData["username"])&&isset($postData["password"])) {
			$result	  = "";
			$salt 	  = $this->generateSalt();
			$username = $this->db->escape(strip_tags($postData["username"]));
			$password = $this->db->escape(strip_tags(md5($salt . $postData["password"])));

			$get_user = $this->db->select('a.username,a.verification_key,a.id,a.user_group,b.role_slug,a.verify,a.status')->from('users a')->join('users_groups b','a.user_group=b.id','left')->where('username ='.$username.' AND password = '.$password)->get()->row_array();
			if (isset($get_user['id'])) {
				if ($get_user['verify'] == 1) { // Not verrified
					return 3;
				}else if($get_user['status']==1&&$get_user['user_group']!=1){
					return 4;
				}else{
					$result = "true";
				}
			}else {
				return false;
			}
		}
		if ($result=="true") {
			$this->session->set_userdata("username", $get_user['username']);
			$this->session->set_userdata("verification_key", $get_user['verification_key']);
			$this->session->set_userdata("user_id", $get_user['id']);
			$this->session->set_userdata("user_group", $get_user['user_group']);
			$this->session->set_userdata("role_slug", $get_user['role_slug']);
			$this->session->set_userdata("is_logged_in", 1);
			$ip = $this->getUserIP();
			$time = $this->db->escape(strip_tags($postData['timezone']));
			$data = [
		        'timezone' => $time,
		        'ip'  => $ip
			];
			$this->db->set('last_signin', 'NOW()', FALSE);
			$this->db->where('id', $get_user['id'])->update('users',$data);
			// $sql = "UPDATE users SET timezone=".$time.",last_signin = NOW(), ip = " . $this->db->escape($ip) . " WHERE id = " . $q->id;
			return TRUE;
		}
	}

	public function getUserIP(){
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if (isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	public function get_user_info(){
		$user_id = $this->session->userdata('user_id');
		$this->db->select('email, name')->from('users');
		$this->db->where('id', $user_id);
		return $this->db->get()->row_array();
	}

	public function register($postData){
		$user_err = 0;
		$email_err = 0;
		$verification_key = $this->generateVerificationKey();
		$subject = "My Soapbox: Verify Email";
		$user_group=0;

		if ($this->session->userdata('user_id')==1) {
			$user_group = $postData['user_group'];
		}else if ($this->session->userdata('user_id')==2) {
			$user_group = 3;
		}else{
			$user_group = 2;
		}

		$this->db->where('username=',$postData['username']);
		$user_query = $this->db->get('users');

		$this->db->where('email=',$postData['email']);
		$email_query = $this->db->get('users');

		if ($user_query->num_rows() > 0) {
			$user_err = 1;
		}
		if ($email_query->num_rows() > 0) {
			$email_err = 1;
		}

		if ($user_err>0&&$email_err>0) {
			return 4;
		}else if ($user_err>0) {
			return 6;
		}else if ($email_err>0) {
			return 7;
		}

		$created_by = $this->session->userdata('is_logged_in')? $this->session->userdata('user_id') : '0';
		$project_text= "";
		$salt = $this->generateSalt();

		$user_data = array(
	        'username' 			=> $postData['username'],
	        'email' 			=> $postData['email'],
	        'password' 			=> md5($salt . $postData['password']),
	        'verification_key'	=> $verification_key,
	        'name'				=> $postData['name'],
	        'campaign_text'		=> $project_text,
	        'created_by'		=> $created_by,
	        'user_group'		=> $user_group
		);

		$submit = $this->db->insert('users', $user_data);
		$link = base_url()."auth/verification?id=".$this->db->insert_id()."&type=verification";
		$id = $this->db->insert_id();
		if ($id>0) {
			$template = $this->template($link,$verification_key,$postData['email'],$subject);

			if ($template==true) {
				return 1;
			}else{
				$this->delete_user($id);
				return 0;
			}
		}else{
			return false;
		}
	}

	public function delete_user($id){
		$this->db->where('id', $id);
		$this->db->delete('users');
	}

	public function template_user($name,$link,$verification_key,$email,$subject){
		$title 		= "Verify your Email Address";
		$message 	= 'Welcome to <strong><a href="'.base_url().'" rel="noopener" style="text-decoration: none; color: #3e6edf;" target="_blank" title="My Soapbox">My Soapbox</a></strong>';
		$message1 	= " Please click the button below to verify your email address. ";
		$button 	= "CONFIRM EMAIL";
		$message2 	= "If you did not sign up to My Soapbox, please ignore this email.";
        $from 		= 'noreply@getmysoapbox.com';
        $template ='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml"><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><meta content="width=device-width" name="viewport"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><title></title><style type="text/css">body{margin:0;padding:0}table,td,tr{vertical-align:top;border-collapse:collapse}*{line-height:inherit}a[x-apple-data-detectors=true]{color:inherit!important;text-decoration:none!important}</style><style id="media-query" type="text/css">@media (max-width:520px){.block-grid,.col{min-width:320px!important;max-width:100%!important;display:block!important}.block-grid{width:100%!important}.col{width:100%!important}.col>div{margin:0 auto}img.fullwidth,img.fullwidthOnMobile{max-width:100%!important}.no-stack .col{min-width:0!important;display:table-cell!important}.no-stack.two-up .col{width:50%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num8{width:66%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num3{width:25%!important}.no-stack .col.num6{width:50%!important}.no-stack .col.num9{width:75%!important}.video-block{max-width:none!important}.mobile_hide{min-height:0;max-height:0;max-width:0;display:none;overflow:hidden;font-size:0}.desktop_hide{display:block!important;max-height:none!important}}</style></head><body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #ffffff;"><table bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; width: 100%;" valign="top" width="100%"><tbody><tr style="vertical-align: top;" valign="top"><td style="word-break: break-word; vertical-align: top;" valign="top"><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;"><img align="center" alt="Image" border="0" class="center autowidth" src="'.base_url().'assets/Email_image/email.jpg" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 500px; display: block;" title="Image" width="500"/></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.8; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 25px;"><p style="font-size: 30px; line-height: 1.8; word-break: break-word; text-align: center; mso-line-height-alt: 54px; margin: 0;"><span style="font-size: 30px;"><strong>'.$title.'</strong></span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"></div><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 24px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 29px; margin: 0;"><span style="font-size: 24px;">'.$message.'</span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message1.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#3e6edf;border-radius:60px;-webkit-border-radius:60px;-moz-border-radius:60px;width:auto; width:auto;;border-top:1px solid #3e6edf;border-right:1px solid #3e6edf;border-bottom:1px solid #3e6edf;border-left:1px solid #3e6edf;padding-top:5px;padding-bottom:5px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><a href="'.$link.'" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #3e6edf; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; width: auto; width: auto; border-top: 1px solid #3e6edf; border-right: 1px solid #3e6edf; border-bottom: 1px solid #3e6edf; border-left: 1px solid #3e6edf; padding-top: 5px; padding-bottom: 5px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">'.$button.'</span></span></a></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:40px;padding-right:10px;padding-bottom:40px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message2.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div></td></tr></tbody></table></body></html>
        ';
		$this->email->from($from,'My Soapbox');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($template);
		return ($this->email->send()) ? true : false;
	}

	public function template($link,$verification_key,$email,$subject){

		$template="";
		$message="";
		$message1="";
		$button="";
		$name = "Get MySoapBox";
		$title = "";
		$message2="";
		$smtp="";
		$smtp_email ="";
		$from 		= 'noreply@getmysoapbox.com';

		if ($subject=="My Soapbox: Verify Email") {
			$title = "Verify your Email Address";
			$message = '
				Welcome to <strong><a href="https://massci.techonsolutions.us/" rel="noopener" style="text-decoration: none; color: #3e6edf;" target="_blank" title="My Soapbox">My Soapbox</a></strong>
			';

			$message1="
				Please click the button below to verify your email address.
			";
			$button = "CONFIRM EMAIL";
			$message2="If you did not sign up to My Soapbox,Please ignore this email";
		}else if($subject=="My Soapbox: Confirmation Password Changed"){
			$title = "Confirm to Change Password";
			$message.=
			'
			<p style="text-align: center;">You changed your password to confirm that was you!</p>
                <br>
                <p style="text-align: center;">Simply click on the button to a confirm</p>
			'
			;
			$button = "Confirm to set new password";
			$message2.='
                <p style="text-align: center;">If you didn&apos;t ask to change your password don&apos;t worry, your password is still safe and you can ignore this email.</p>

			';

		}
		else if($subject=="My Soapbox: Password Recovery"){
			$title = "Password Recovery";
			$message.=
			"
			<p>We received request to reset your password of your My Soapbox account, We're to help!</p>
                <br>
                <p>Simply click on the button to a new password</p>
			"
			;
			$button = "Set a New Password";
			$message2.='
                <p style="text-align: center;">If you didn&apos;t ask to change your password don&apos;t worry, your password is still safe and you can ignore this email.</p>

			';
		}

        $template .='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml"><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><meta content="width=device-width" name="viewport"/><meta content="IE=edge" http-equiv="X-UA-Compatible"/><title></title><style type="text/css">body{margin:0;padding:0}table,td,tr{vertical-align:top;border-collapse:collapse}*{line-height:inherit}a[x-apple-data-detectors=true]{color:inherit!important;text-decoration:none!important}</style><style id="media-query" type="text/css">@media (max-width:520px){.block-grid,.col{min-width:320px!important;max-width:100%!important;display:block!important}.block-grid{width:100%!important}.col{width:100%!important}.col>div{margin:0 auto}img.fullwidth,img.fullwidthOnMobile{max-width:100%!important}.no-stack .col{min-width:0!important;display:table-cell!important}.no-stack.two-up .col{width:50%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num8{width:66%!important}.no-stack .col.num4{width:33%!important}.no-stack .col.num3{width:25%!important}.no-stack .col.num6{width:50%!important}.no-stack .col.num9{width:75%!important}.video-block{max-width:none!important}.mobile_hide{min-height:0;max-height:0;max-width:0;display:none;overflow:hidden;font-size:0}.desktop_hide{display:block!important;max-height:none!important}}</style></head><body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #ffffff;"><table bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; width: 100%;" valign="top" width="100%"><tbody><tr style="vertical-align: top;" valign="top"><td style="word-break: break-word; vertical-align: top;" valign="top"><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;"><img align="center" alt="Image" border="0" class="center autowidth" src="'.base_url().'assets/Email_image/sms2.jpg" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 500px; display: block;" title="Image" width="500"/></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.8; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 25px;"><p style="font-size: 30px; line-height: 1.8; word-break: break-word; text-align: center; mso-line-height-alt: 54px; margin: 0;"><span style="font-size: 30px;"><strong>'.$title.'</strong></span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"></div><div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 24px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 29px; margin: 0;"><span style="font-size: 24px;">'.$message.'</span></p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message1.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;"><div style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#3e6edf;border-radius:60px;-webkit-border-radius:60px;-moz-border-radius:60px;width:auto; width:auto;;border-top:1px solid #3e6edf;border-right:1px solid #3e6edf;border-bottom:1px solid #3e6edf;border-left:1px solid #3e6edf;padding-top:5px;padding-bottom:5px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><a href="'.$link.'" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #3e6edf; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; width: auto; width: auto; border-top: 1px solid #3e6edf; border-right: 1px solid #3e6edf; border-bottom: 1px solid #3e6edf; border-left: 1px solid #3e6edf; padding-top: 5px; padding-bottom: 5px; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">'.$button.'</span></span></a></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffffff;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div style="color:#a4a4a4;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:40px;padding-right:10px;padding-bottom:40px;padding-left:10px;"><div style="font-size: 14px; line-height: 1.2; color: #a4a4a4; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 17px;"><p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message2.'</p></div></div></div></div></div></div></div></div><div style="background-color:#efefef;"><div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"><div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;"><div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;"><div style="width:100% !important;"><div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"><div></div></div></div></div></div></div></div></td></tr></tbody></table></body></html>
        ';

		$user_info = $this->get_user_info();
		$this->email->from($from,'My Soapbox');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($template);
		return ($this->email->send()) ? true : false;
	}

	public function add_update_this_user(){
		$id 			  = $this->input->post('user_id');
		$email 			  = $this->input->post('email');
		$old_email 		  = $this->input->post('old_email');
		$username 		  = $this->input->post('username');
		$old_username 	  = $this->input->post('old_username');
		$password 		  = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$salt 			  = $this->generateSalt();
		$is_email_exists  = $this->check_email_exist($email, $old_email);
		if(!$is_email_exists){
			$is_username_exists = $this->check_username_exist($username, $old_username);
			if(!$is_username_exists){
				if($password){
					if($password==$confirm_password){
						$new_pass = md5($salt . $password);
						$res_id = $this->add_update_user($new_pass, $id);
						return ($res_id) ? $this->response('200', 'The user was successfully saved.', $res_id) : $this->response('201', 'A problem occured Please login again.', '');
					}else{ 
						return $this->response('201', 'Password must be equal to Confirm Password.', '');
					}
				}else{
					$res_id = $this->add_update_user(0, $id);
					return ($res_id) ? $this->response('200', 'The user was successfully saved.', $res_id) : $this->response('201', 'A problem occured Please login again.', '');
				}
			}else{
				return $this->response('201', 'Username already exist.', '');
			}
		}else{
			return $this->response('201', 'Email already exist.', '');
		}
	}

	public function check_email_exist($email, $old_email){
		if($email!=$old_email){
			$res = $this->db->select('email')->from('users')->where('email', $email)->get()->row_array();
			return $res ? true : false;
		}
		return false;
	}

	public function check_username_exist($username, $old_username){
		if($username!=$old_username){
			$res = $this->db->select('username')->from('users')->where('username', $username)->get()->row_array();
			return $res ? true : false;
		}
		return false;
	}

	public function add_update_user($new_pass, $id){
		$image = "";
		if (!empty($_FILES)) {
       		if(!is_dir($main_folder.'/'.$_SESSION['user_id'])){
					mkdir($main_folder.'/'.$_SESSION['user_id'], 0777, true);
				}
				// Define new $_FILES array - $_FILES['file']
				$_FILES['file']['name'] = $_FILES['files']['name'];
				$_FILES['file']['type'] = $_FILES['files']['type'];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
				$_FILES['file']['error'] = $_FILES['files']['error'];
				$_FILES['file']['size'] = $_FILES['files']['size'];
				// Set preference
				$config['upload_path'] = $main_folder.'/'.$_SESSION['user_id']; 
				$config['allowed_types']        = 'gif|jpg|png';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = $_FILES['files']['name'];
				
				//Load upload library

				$this->load->library('upload',$config); 
				// File upload
				$this->upload->initialize($config);
				if($this->upload->do_upload('file')){
					$uploadData = $this->upload->data();
					$filename = $uploadData['file_name'];
				}
        }
		$data = array(
			'name' 		  => $this->input->post('name'),
			'email'  	  => $this->input->post('email'),
			'username'    => $this->input->post('username'),
			'user_group'  => $this->input->post('user_group'),
			'created_by'  => $this->input->post('created_by'),
			'status'  	  => ($this->input->post('cstatus')=='on') ? 0 : 1,
		);
		if($new_pass){ $data['password'] = $new_pass; }
		if($id){
			$data['created_by'] = $this->input->post('created_by');
			$this->db->where('id', $id);
			return $this->db->update('users', $data) ? $id : false;
		}else{
			$data['created_by'] = $this->session->userdata('user_id');
			return $this->db->insert('users', $data) ? $this->db->insert_id() : false;
		}
	}

	public function response($code, $msg, $data){
		$code = array('code' => $code, 'msg' => $msg, 'data' => $data);
		return $code;
	}

	public function get_this_user($id){
		return $this->db->select('a.*, b.project_id')->from('users a')->join('project_assign_id b', 'b.user_id = a.id', 'left')->where('a.id', $id)->get()->row_array();
	}

	public function change_selected_users_status($id, $status){
		$data['status'] = $status;
		$this->db->where_in('id', $id);
		return $this->db->update('users', $data) ? true : false;
	}
	
	public function update_user_status(){
		$id 	= $this->input->post('id');
		$status = $this->input->post('status');
		$data['status'] = ($status==1 || $status==2) ? 0 : 1;
		$this->db->where('id', $id);
		return $this->db->update('users', $data) ? true : false;
	}

	public function get_user_lists($table, $column_order, $limit, $offset, $search, $order, $role){
        $this->db->select('a.*, b.name as role_name, c.name as created_by_name')->from('users a');
		$this->db->join('users_groups b', 'b.id = a.user_group', 'left');
		$this->db->join('users c', 'c.id = a.created_by', 'left');
		$this->db->where('a.user_group', $role);
		if($this->input->post('created_by')){
			$this->db->where('a.created_by', $this->input->post('created_by'));
		}
		if($this->input->post('by_status')!='all_status'){
			$this->db->where('a.status', $this->input->post('by_status'));
		}
		$columns = $this->db->list_fields('users');


		/* If the user uses the search */  
		if($search){
			$this->db->group_start();
			foreach ($column_order as $item){
				$this->db->or_like($item, $search['value']);
			}
			$this->db->group_end();
		}

		/* If the user uses the order sort */
		if(isset($order)){
			$this->db->order_by($column_order[$order['0']['column']], $order['0']['dir']);
		}

        // Count results
        $temp 			= clone $this->db;
		$data['count'] 	= $temp->count_all_results();
        
        // Show number of results
		if($limit != -1){
			$this->db->limit($limit, $offset);
		}
        
        // Get the data
		$query = $this->db->get();
		$data['data'] = $query->result();

        // Count List
        $this->db->where('user_group', $role);
        $data['count_all'] = $this->db->count_all_results($table);
		return $data;
	}


	public function updateProfile($postData,$filename) {
		$username = $postData["username"];
        $email = $postData['email'];
        $name = $postData['name'];
        $passwordquery = "";
    	$err_username="";
    	$check_pass="";
    	$err_email="";
    	$files = $filename;
    	$get_username = '';
    	$get_email = '';

    	if (isset($postData['id'])) {
    		$id = $this->db->escape(strip_tags($postData["id"]));
    	}else{
    		$id = $this->session->userdata('user_id');
    	}


    	if (!empty($postData["password"]) && !empty($postData["password2"])&& !empty($postData['old_password'])) {
    		$salt = $this->generateSalt();
    		$old_password = $this->db->escape(strip_tags(md5($salt . $postData["password"])));
	    	$check_pass = $this->db->where('username=',$username)->where('password=',md5($salt . $postData["old_password"]))->get('users');;
    		($check_pass->num_rows()>0)? $check_pass = 1 : '';

    		if (empty($check_pass)) {
    			// echo md5($salt . $postData["old_password"]);
    			return $this->response('201', 'Old password is incorrect!', '');
    		}

    		
			$data = array(
		        'image'		=> $files,
		        'username' 	=> $username,
		        'email'		=> $email,
		        'name'		=> $name,
		        'password'	=> md5($salt . $postData['password']),
			);
        }else{
			$data = array(
		        'image' 	=> $files,
		        'username' 	=> $username,
		        'email'		=> $email,
		        'name'		=> $name,
			);
        }

        $get_info = $this->db->select('*')->from('users')->where('id='.$id)->get()->row_array();
        $get_username = $get_info['username'];
        $get_email = $get_info['email'];

    	$username = $this->db->select('*')->from('users')->where('username=',$username)->where('username!=',$get_username)->get();
    	($username->num_rows()>0)? $err_username = 1 : '';

	    $email = $this->db->where('email=',$email)->where('email!=',$get_email)->get('users');
	    ($email->num_rows()>0)? $err_email = 1 : '';


    	if (!empty($err_username)&&!empty($err_email)) {
			return $this->response('201', 'Username and Email already exist.', '');
		}else if (!empty($err_username)) {
			return $this->response('201', 'Username already exist.', '');
		}else if (!empty($err_email)) {
			return $this->response('201', 'Email already exist.', '');
		}else{
	        $update = $this->db->where('id='.$id)->update('users',$data);
	        return ($update) ? $this->response('200', 'Your profile was successfully saved.', $update) : $this->response('201', 'You can not update profile right now.', '');
	    }

	}

}
	
