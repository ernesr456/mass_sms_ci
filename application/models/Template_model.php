<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	
	public function show_template($valid_columns,$search){
    	$this->db->select('a.id,a.subject,a.message,a.filename,a.file_type,a.status,b.name as created_name,b.username')
    	->from('message_template a')
    	->join('users b','a.created_by = b.id','left');

    	$this->session->userdata('user_group')==2? $this->db->where('a.created_by='.$this->session->userdata('user_id').' OR b.created_by ='.$this->session->userdata('user_id')) : '';
        if(!empty($search))
        {
            $x=0;
            foreach($valid_columns as $sterm)
            {
                if($x==0)
                {
                    $this->db->like($sterm,$search);
                }
                else
                {
                    $this->db->or_like($sterm,$search);
                }
                $x++;
            }                 
        }
    	return $this->db->get()->result();
    }
    public function total_template($search,$valid_columns){
		$query = $this->db->select("COUNT(*) as num")->from('message_template a')->join('users b','a.created_by = b.id','left');
	 	if ($this->session->userdata('group_name')==1) {
        	$this->session->where('a.created_by='.$this->session->userdata('user_id'));
        }
        if(!empty($search))
        {
            $x=0;
            foreach($valid_columns as $sterm)
            {
            	$con = '';
                if($x==0){
                    $this->db->like($con.''.$sterm,$search);
                }
                else{
                    $this->db->or_like($con.''.$sterm,$search);
                }
                $x++;
            }                 
        }
        $result = $this->db->get()->row();
        if(isset($result)) return $result->num;
        return 0;
	}
 	public function save_template($postData,$filename,$extension){
        $data = array(
	        'subject' 		=> $postData['subject'],
	        'message' 		=> $postData['message'],
	        'created_by' 	=> $this->session->userdata('user_id'),
	        'filename'		=> $filename,
	        'file_type'		=> $extension,
	        'status'		=> 1,
		);

		$data = $this->db->insert('message_template', $data);
		return $data? true:false;
    }
	public function update_template($postData,$filename,$extension){
        $data = array(
	        'subject' 		=> $postData['subject'],
	        'message' 		=> $postData['message'],
	        'filename'		=> $filename,
		);
		$this->db->where('id', $postData['id']);
		$data = $this->db->update('message_template',$data);
		return $data? true:false;
	}
	public function delete_template($id){
		$sql = "DELETE FROM message_template where id = ".$id;
		$query = $this->db->query($sql);
		return $query;
	}
	public function fetch_template($id){
		$this->db->select('*')->from('message_template')->where('id='.$id);
		return $this->db->get()->result();
	}
}
