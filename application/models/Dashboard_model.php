<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	
	public function totalUser($role){
    	$query = "";
    	if ($_SESSION['user_group']==1) {
    		$where= "";

    		$this->db->select("COUNT(*) as num");
    		$this->db->from('users u');
    		$this->db->join('users_groups', 'u.user_group = users_groups.id');
    		if($role!="*") {
    			$this->db->where('users_groups.role_slug =',$role);
    		}

    	}else{
    		$query = $this->db->select("COUNT(*) as num")->from('users u')->join('users_groups', 'u.user_group = users_groups.id')->where('u.created_by =',$this->session->userdata('user_id'));
    	}
        $result = $this->db->count_all_results();
        if(isset($result)) return $result;
        return 0;
	}

	public function totalMessage(){
    	$query = "";
    	$query = $this->db->select("COUNT(*) as num")->like('sent_time', date('Y-m-d'))->get("conversation");
        $result = $query->row();
        return (isset($result))?$result->num:0;
    }
 	public function totalClients(){
    	$query = "";
    	$query = $this->db->select("COUNT(*) as num")->get("contacts");
        $result = $query->row();
        return (isset($result))?$result->num:0;
    }
	public function totalcampaigns(){
    	$query = "";
    	$this->db->select("COUNT(*) as num");
    	$query = $this->db->like('start_at', date('d-m-Y'))->get("campaign");
        $result = $query->row();
        return (isset($result))?$result->num:0;
    }
    public function sms_status($campaign,$user_id,$group_id){
        $this->db->select('
            monthname(e.sent_time) as month,

            round(sum(case when (e.status="2") then 1 else 0 end) / sum(case when (e.sms_status="") then 1 else 0 end),2)*100 as undelivered,
            round(sum(case when (e.status=" ") then 1 else 0 end) / sum(case when (e.sms_status="") then 1 else 0 end),2)*100 as sent,

            round((SELECT count(*) from contact where id = e.from_id AND e.sms_status="received") / 
            IF((SELECT count(*) from contact where id = e.to_id AND e.status=0)<1, 1, (SELECT count(*) from contact where id = e.to_id AND e.status=0))*100,2) as received
        ')
        ->from('project a')
        ->join('project_group_contact b','a.id=b.project_id','left')
        ->join('contacts_group c','b.group_id = c.gid','left')
        ->join('contact d','c.gid = d.group_id','left')
        ->join('conversation e','e.from_id = d.id OR e.to_id = d.id','left');

        (!empty($campaign))? $this->db->where('a.id=',$campaign): '';
        (!empty($user_id))? $this->db->where('e.id=',$campaign): '';
        $this->db->group_by('MONTH(e.sent_time) ASC');
        return $this->db->get()->result_array();

        // select * from project a left join project_group_contact b on e.id = b.project_id left join contacts_group c on b.group_id = c.gid left join contact d on c.gid = d.group_id left join conversation e on e.from_id = d.id OR e.to_id = d.id

        // (SELECT count(*) from contact where id = e.to_id AND e.status=0) as total_sent_contact,
        // (SELECT count(*) from contact where id = e.from_id AND e.sms_status="received") as total_received,
        // round(sum(case when (e.status="2" OR e.status="3") then 1 else 0 end)) as undelivered,
        // round(sum(case when (e.sms_status="") then 1 else 0 end)) as send,
        // round(sum(case when (e.sms_status="received") then 1 else 0 end)) as received,
    }
}
