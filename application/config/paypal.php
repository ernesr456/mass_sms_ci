<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/** set your paypal credential **/

$config['client_id'] = 'AZYTz5dAt7PDRm5cMOanate7H4m0ngqhWHjDfwTfiyOWMZxkTDIGOD2FvJo81H5D8xEeHA0IU3ZRgUzj';
$config['secret'] = 'EKgZVLCbjeB6I_feQgzAXYI2XhS2kSpXSuAJq0-TYCnZW1iyrEb1Gx-SeWUdXhr_W_1v1KmCX9WQ-JXB';

/**
 * SDK configuration
 */
/**
 * Available option 'sandbox' or 'live'
 */
$config['settings'] = array(

    'mode' => 'sandbox',
    /**
     * Specify the max request time in seconds
     */
    'http.ConnectionTimeOut' => 1000,
    /**
     * Whether want to log to a file
     */
    'log.LogEnabled' => true,
    /**
     * Specify the file that want to write on
     */
    'log.FileName' => 'application/logs/paypal.log',
    /**
     * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
     *
     * Logging is most verbose in the 'FINE' level and decreases as you
     * proceed towards ERROR
     */
    'log.LogLevel' => 'FINE'
);
