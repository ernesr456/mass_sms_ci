<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $base_url = load_class('Config')->config['base_url'];  ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>

<!-- Core stylesheets -->
<link rel="stylesheet" href="<?=$base_url;?>assets/admin/css/bootstrap-material.css">
<link rel="stylesheet" href="<?=$base_url;?>assets/admin/css/shreerang-material.css">
<link rel="stylesheet" href="<?=$base_url;?>assets/admin/css/uikit.css">
<style type="text/css">
    div{display:block}body{font-weight:300}.layout-full .page{max-width:none!important;height:100%;padding:0;margin:0!important;background-color:transparent}.page{position:relative;min-height:calc(100% - 44px);margin:0 auto;background:#f1f4f5}.page-content{padding:30px}.vertical-align-middle{vertical-align:middle}.vertical-align-middle{display:inline-block;max-width:100%;font-size:1rem}h1{font-size:10em;font-weight:400;text-shadow:rgba(0,0,0,.15) 0 0 1px}.animation-slide-top{-webkit-animation-name:slide-top;animation-name:slide-top}header p{margin-bottom:30px;font-size:30px;text-transform:uppercase}p{margin-top:0;margin-bottom:1rem}p{display:block;margin-block-start:1em;margin-block-end:1em;margin-inline-start:0;margin-inline-end:0}
</style>
</head>
<body>
	<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
      <div class="page-content vertical-align-middle">
        <header>
          <h1 class="animation-slide-top">403</h1>
          <p>Page Not Found !</p>
        </header>
        <p class="error-advise">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
        <a class="btn btn-primary btn-round" href="<?=$base_url; ?>admin">GO TO HOME PAGE</a>
        <footer class="page-copyright">
          <p>WEBSITE BY My Soapbox</p>
          <p>© 2020. All RIGHT RESERVED.</p>
          <div class="social">
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-twitter" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-facebook" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-dribbble" aria-hidden="true"></i>
        </a>
          </div>
        </footer>
      </div>
    </div>
</body>
</html>