
<!DOCTYPE html>

<html lang="en" class="material-style layout-fixed">

<head>
	<title><?php echo $page_title;?></title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="<?=$meta['page_description']?>" />
	<meta name="keywords" content="<?=$meta['page_keyword']?>">
	<meta name="author" content="Srthemesvilla" />
	<link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/admin/img/favicon.ico">

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

	<!-- Icon fonts -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/fontawesome.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/ionicons.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/linearicons.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/open-iconic.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/feather.css">

	<!-- Core stylesheets -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap-material.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/shreerang-material.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/uikit.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/sweetalert.min.css">

	<!-- Libs -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/perfect-scrollbar/perfect-scrollbar.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/plyr/plyr.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/flot/flot.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/morris/morris.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.css">
 	<link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/select2/select2.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/photoswipe/photoswipe.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/blueimp-gallery/gallery.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/blueimp-gallery/gallery-indicator.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/blueimp-gallery/gallery-video.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/dropzone/dropzone.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/flow-js/flow.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-maxlength/bootstrap-maxlength.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/tribute/dist/tribute.css">
    <link href="<?=base_url();?>assets/admin/libs/summernote/dist/summernote.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/admin/libs/summernote/dist/summernote-bs4.css" rel="stylesheet">
	<!-- Page -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/authentication.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/toastr/toastr.css">
	<link href="<?=base_url();?>assets/admin/libs/ladda/ladda.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/admin/libs/datatables/datatables.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/admin/css/custom.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=base_url();?>assets/admin/css/pages/chat.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/admin/css/pages/users.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/admin/css/pages/account.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/quill/typography.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/quill/editor.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/admin/css/pages/messages.css">
	<!-- Basic universal javascripts -->
	<script src="<?=base_url()?>assets/admin/js/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/toastr/toastr.js"></script>
	<link rel="stylesheet" href="<?=base_url()?>assets/build/css/intlTelInput.css">
	<?php echo $before_head;?>

	<input type="hidden" id="base_url" value="<?=base_url()?>">
</head>

<body>
<!-- [ Preloader ] Start -->
<div class="page-loader">
	<div class="bg-primary"></div>
</div>
