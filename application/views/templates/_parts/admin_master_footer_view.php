<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!-- Core scripts -->
<script src="<?=base_url()?>assets/admin/js/pages/jquery.timeago.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/pace.js"></script>
<script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
<script src="<?=base_url()?>assets/admin/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/admin/js/sidenav.js"></script>
<script src="<?=base_url()?>assets/admin/js/layout-helpers.js"></script>
<script src="<?=base_url()?>assets/admin/js/material-ripple.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="<?=base_url()?>assets/admin/libs/validate/validate.js"></script>
<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
<!-- <script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> -->

<!-- Libs -->
<script src="<?=base_url()?>assets/admin/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="<?=base_url()?>assets/admin/libs/flot/flot.js"></script>
<script src="<?=base_url()?>assets/admin/libs/flot/curvedLines.js"></script>
<script src="<?=base_url()?>assets/admin/libs/eve/eve.js"></script>
<script src="<?=base_url()?>assets/admin/libs/chart-am4/core.js"></script>
<script src="<?=base_url()?>assets/admin/libs/chart-am4/charts.js"></script>
<script src="<?=base_url()?>assets/admin/libs/chart-am4/animated.js"></script>
<script src="<?=base_url()?>assets/admin/libs/autosize/autosize.js"></script>
<script src="<?=base_url()?>assets/admin/libs/knob/knob.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-maxlength/bootstrap-maxlength.js"></script>
<script src="<?=base_url()?>assets/admin/libs/pwstrength-bootstrap/pwstrength-bootstrap.js"></script>

<script src="<?=base_url()?>assets/admin/libs/bootstrap-select/bootstrap-select.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?=base_url()?>assets/admin/libs/select2/select2.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/moment/moment.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?=base_url()?>assets/admin/libs/photoswipe/photoswipe.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-fullscreen.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-indicator.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-video.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-vimeo.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-youtube.js"></script>
<script src="<?=base_url()?>assets/admin/libs/dropzone/dropzone.js"></script>
<script src="<?=base_url()?>assets/admin/libs/flow-js/flow.js"></script>
<script src="<?=base_url();?>assets/admin/tribute/dist/tribute.js"></script>
<script src="<?=base_url()?>assets/admin/libs/summernote/dist/summernote.js"></script>
<script src="<?=base_url()?>assets/admin/libs/summernote/dist/summernote-bs4.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/pages_users_edit.js"></script>

<script src="<?=base_url()?>assets/admin/js/dropdown-hover.js"></script>

<script src="<?=base_url()?>assets/admin/libs/spin/spin.js"></script>
<script src="<?=base_url()?>assets/admin/libs/ladda/ladda.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/pages_chat.js"></script>
<script src="<?=base_url()?>assets/admin/js/demo.js"></script>

<script src="<?=base_url()?>assets/admin/js/pages/forms_selects.js"></script>

<script src="<?=base_url()?>assets/admin/libs/raphael/raphael.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/admin-general.js"></script>

<script src="<?=base_url()?>assets/admin/libs/vanilla-text-mask/vanilla-text-mask.js"></script>
<script src="<?=base_url()?>assets/admin/libs/vanilla-text-mask/text-mask-addons.js"></script>

<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/ui_lightbox.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/forms_extras.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/forms_input-groups.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/tables_datatables.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/pages_messages.js"></script>

<script src="<?=base_url()?>assets/build/js/intlTelInput.min.js"></script> 
<script src="<?=base_url()?>assets/build/js/intlTelInput-jquery.min.js"></script> 
<?php echo $before_body;?>
 <script>
    if (document.getElementById('phonenumber')) {
    var input = document.querySelector("#phonenumber");
    var phone = $("#phonenumber");
    $(document).ready(function(){
      var dial_code = $(".iti__country.iti__preferred.iti__active").data('dialCode');
      phone.val("+"+dial_code+" ");

      $(document).on('click', '.iti__country.iti__preferred' ,function(){
        var dial_code = $(this).data('dialCode');
        phone.val("+"+dial_code+" ");
      })
    })
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",   
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: base_url+"assets/build/js/utils.js",
    });
  }
  </script>
<script>


    $('.select2').select2({
        
        // data: ["Piano", "Flute", "Guitar", "Drums", "Photography"],
        tags: true,
        // maximumSelectionLength: 10,
        // tokenSeparators: [',', ' '],
        // placeholder: "Select or type keywords",


        minimumInputLength: 1,
        ajax: {
         url: "you url to data",
         dataType: 'json',
         quietMillis: 250,
         data: function (term, page) {
            return {
                q: term, // search term
           };
        },
        results: function (data, page) { 
        return { results: data.items };
        },
        cache: true
       }
    });
</script>
<script type="text/javascript">
    'use strict';
    (function(){
        $('.summernote').summernote({
            height: 200,
            minHeight: null, // set minimum height of editor
            maxHeight: null,// set maximum height of editor
            focus: false,
            toolbar: [
                ['tool', ['undo', 'redo']],
                ['font', ['bold', 'italic', 'underline']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['para', [ 'paragraph','ul', 'ol']],
                ['insert',['emoji', 'picture', 'link']],
                ['height', ['height']],
                ['table', ['table']],
                ['view', ['codeview']],
                
            ],
        });
        $('.summernote').summernote('normal');
    })();
</script>
<script type="text/javascript">
    // Quill does not support IE 10 and below so don't load it to prevent console errors
    if (document.getElementById('message-editor')) {
        if (typeof document.documentMode !== 'number' || document.documentMode > 10) {
            document.write('\x3Cscript src="<?=base_url()?>assets/admin/libs/quill/quill.js">\x3C/script>');
        }
    }

    $(function() {
        if (document.getElementById('project_list')) {
            new PerfectScrollbar(document.getElementById('project_list'));
        }
        if (document.getElementById('team-member1')) {
            new PerfectScrollbar(document.getElementById('team-member1'));
        }
        if (document.getElementById('body-thread')) {
            new PerfectScrollbar(document.getElementById('body-thread'));
        }
        if (document.getElementById('table_campaign')) {
            new PerfectScrollbar('#table_campaign');
        }
    });
</script>
</body>
</html>

