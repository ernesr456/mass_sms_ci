<div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-white">
	<!-- Brand demo (see assets/css/demo/demo.css) -->
	<div class="app-brand demo">
                    <span class="app-brand-logo demo">
<!--                        <img src="assets/img/logo.png" alt="Brand Logo" class="img-fluid">-->
                    </span>
		<a href="<?=base_url();?>admin" class="app-brand-text demo sidenav-text font-weight-normal ml-2"><?=$this->config->item('site_name')?></a>
		<a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
			<i class="ion ion-md-menu align-middle"></i>
		</a>
	</div>
	<div class="sidenav-divider mt-0"></div>

	<!-- Links -->
	<ul class="sidenav-inner py-1">

		<!-- Dashboards -->
		<li class="sidenav-item <?=($page=='dashboard')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modDashboard']==="false")? 'hidden' : ''?>>
			<a href="<?=base_url();?>admin" class="sidenav-link">
				<i class="sidenav-icon feather icon-home"></i>
				<div>Dashboard</div>
			</a>
		</li>
		<li class="sidenav-item <?=($page=='campaigns')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modCampaign']==="false")? 'hidden' : ''?>>
			<a href="<?=base_url();?>admin/campaigns" class="sidenav-link">
				<i class="sidenav-icon feather icon-share-2"></i>
				<div>Campaigns</div>
			</a>
		</li>
		<li class="sidenav-item <?=($page=='Project')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modCampaign']==="false")? 'hidden' : ''?>>
			<a href="<?=base_url();?>project/project" class="sidenav-link">
				<i class="sidenav-icon feather icon-clipboard"></i>
				<div>Project</div>
			</a>
		</li>
		<li class="sidenav-item <?=($page=='conversation' || $page=='message_template')?'open':'';?>" <?=(json_decode($authPermissionID,true)['modConversation']==="false")? 'hidden' : ''?>>
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon feather icon-lock"></i>
				<div>Conversation</div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='conversation')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modContacts']==="false")? 'hidden' : ''?>>
					<a href="<?=base_url();?>admin/conversation" class="sidenav-link">
						<div>Conversation</div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='message_template')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modGroup']==="false")? 'hidden' : ''?>>
					<a href="<?=base_url();?>admin/template" class="sidenav-link">
						<div>Templates</div>
					</a>
				</li>
			</ul>
		</li>
		<li class="sidenav-item <?=($page=='campaigns')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modCampaign']==="false")? 'hidden' : ''?>>
			<a href="<?=base_url();?>email/inbox/1" class="sidenav-link">
				<i class="sidenav-icon feather icon-mail"></i>
				<div>Email</div>
			</a>
		</li>
		<?php if ($this->session->userdata('role_slug')=='administrator'|| $this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
			<li class="sidenav-item <?=($page=='users')?'open':'';?>" <?=(json_decode($authPermissionID,true)['modUser']==="false")? 'hidden' : ''?>>
				<a href="javascript:" class="sidenav-link sidenav-toggle">
					<i class="sidenav-icon feather icon-user"></i>
					<div>Users</div>
				</a>
				<!-- <?php echo $this->session->userdata('role_slug');?> -->
				<ul class="sidenav-menu">
					<?php if ($this->session->userdata('role_slug')=='administrator'&&$this->session->userdata('is_logged_in')) { ?>
					<li class="sidenav-item <?=($this->input->server('QUERY_STRING')=='administrator')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modUser']==="false")? 'hidden' : ''?>>
						<a href="<?=base_url();?>admin/users?val=administrator" class="sidenav-link">
							<div>Administrators</div>
						</a>
					</li>
					<li class="sidenav-item <?=($this->input->server('QUERY_STRING')=='client')?'active':'';?>" <?=(json_decode($authPermissionID,true)['modGroup']==="false")? 'hidden' : ''?>>
						<a href="<?=base_url();?>admin/users?val=client " class="sidenav-link">
							<div>Clients</div>
						</a>
					</li>
					<?php } ?>
					
					<?php if ($this->session->userdata('role_slug')=='administrator'|| $this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
					<li class="sidenav-item <?=($this->input->server('QUERY_STRING')=='volunteers')?'active':'';?>">
						<a href="<?=base_url();?>admin/users?val=volunteers" class="sidenav-link">
							<div>Volunteers</div>
						</a>
					</li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>
		<pre>
			<?php print_r(json_decode($authPermissionID)); ?>
		</pre>
		<li class="sidenav-item <?=($page=='contacts' || $page=='contacts_group' || $page=='email_template' ||$page=='create_email_template')?'open':'';?>" <?=(json_decode($authPermissionID,true)['modContacts']==="false")? 'hidden' : ''?>>
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon feather icon-users"></i>
				<div>Contacts</div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='contacts')?'active':'';?>" <?=(json_decode($authPermissionID,true)['addContacts']===false)? 'hidden' : ''?>>
					<a href="<?=base_url();?>admin/contacts" class="sidenav-link">
						<div>Contacts</div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='contacts_group')?'active':'';?>" <?=(json_decode($authPermissionID,true)['addGroup']==="false")? 'hidden' : ''?>>
					<a href="<?=base_url();?>admin/manage_group" class="sidenav-link">
						<div>Manage Groups</div>
					</a>
				</li>
			</ul>
		</li>
		
		<?php if ($this->session->userdata('role_slug')=='administrator'||$this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
			<li class="sidenav-item <?=($page=='permissions'||$page=='create_permissions'||$page=='list_permission'||$page=='permissions_2'||$page=='permissions_3')?'open':'';?>" <?=(json_decode($authPermissionID,true)['modPermission']==="false")? 'hidden' : ''?>>
				<a href="javascript:" class="sidenav-link sidenav-toggle">
					<i class="sidenav-icon feather icon-lock"></i>
					<div>Permissions</div>
				</a>
				<ul class="sidenav-menu">
					<li class="sidenav-item <?=($page=='create_permissions'||$page=='list_permission')?'open':'';?>">
						<a href="javascript:" class="sidenav-link sidenav-toggle">
							<div>Permissions</div>
						</a>
						<ul class="sidenav-menu">
							<li class="sidenav-item <?=($page=='create_permissions')?'active':'';?>">
								<a href="<?=base_url();?>permission/add_permission" class="sidenav-link">
									<div>Create Permission</div>
								</a>
							</li>
							<li class="sidenav-item <?=($page=='list_permission')?'active':'';?>">
								<a href="<?=base_url();?>permission/list_permission" class="sidenav-link">
									<div>Permission Lists</div>
								</a>
							</li>
						</ul>
					</li>
					<?php if($this->session->userdata('role_slug')!='client') { ?>
						<li class="sidenav-item <?=($page=='permissions_2')?'active':'';?>">
							<a href="<?=base_url();?>admin/permissions?role=Client&type=2" class="sidenav-link">
								<div>Client Permissions</div>
							</a>
						</li>
					<?php } ?>

					<li class="sidenav-item <?=($page=='permissions_3')?'active':'';?>">
						<a href="<?=base_url();?>admin/permissions?role=Volunteers&type=3" class="sidenav-link">
							<div>Volunteers Permissions</div>
						</a>
					</li>
				</ul>
			</li>
		<?php } ?>
		<?php if ($this->session->userdata('role_slug')=='administrator'||$this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
		<li class="sidenav-item <?=($page=='credits'||$page=='site_settings'||$page=='Transaction')?'open':'';?>" <?=(json_decode($authPermissionID,true)['modSettings']==="false")? 'hidden' : ''?>>
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon feather icon-settings"></i>
				<div><?= $this->session->userdata['role_slug'] === 'administrator' ? 'Settings' : 'Credits'  ?></div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='credits')?'active':'';?>">
					<a href="<?=base_url();?>admin/credits" class="sidenav-link">
						<div><?= $this->session->userdata['role_slug'] === 'administrator' ? 'Manage Credits' : 'Buy Credits'  ?></div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='Transaction')?'active':'';?>">
					<a href="<?=base_url();?>admin/transaction" class="sidenav-link">
						<div><?= $this->session->userdata['role_slug'] === 'administrator' ? 'All Transaction' : 'Transaction'  ?></div>
					</a>
				</li>
<!-- 				<li class="sidenav-item <?=($page=='site_settings')?'active':'';?>" <?= $this->session->userdata['role_slug'] === 'administrator' ? '' : 'hidden'  ?>>
					<a href="<?=base_url();?>admin/transaction" class="sidenav-link">
						<div>Transaction</div>
					</a>
				</li> -->
				<li class="sidenav-item <?=($page=='site_settings')?'active':'';?>" <?= $this->session->userdata['role_slug'] === 'administrator' ? '' : 'hidden'  ?>>
					<a href="<?=base_url();?>admin/settings" class="sidenav-link">
						<div>Site Settings</div>
					</a>
				</li>			
			</ul>
		</li>
		<?php } ?>
	</ul>
</div>
