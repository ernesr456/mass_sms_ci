<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/_parts/admin_master_header_view');
if (isset($authpage)){
	echo $the_view_content;
}
else {
	?>
	<!-- [ Layout wrapper ] Start -->
	<div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php
			$this->load->view('templates/_parts/admin_master_sidebar_view');
			?>
			<!-- [ Layout sidenav ] End -->
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] Start -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout navbar ( Header ) ] End -->

				<!-- [ Layout content ] Start -->
				<div class="layout-content">

					<!-- [ content ] Start -->
					<div class="container-fluid flex-grow-1<?=!$hasBreadcrumbs ? ' p-0' : 'container-p-y'?>">
						<?php
						if ($hasBreadcrumbs){
							?>
							<h4 class="font-weight-bold py-3 mb-0"><?=$page_head?></h4>
							<div class="text-muted small mt-0 mb-4 d-block breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
									<li class="breadcrumb-item"><a href="#">Admin</a></li>
									<li class="breadcrumb-item active"><?=$page_head?></li>
								</ol>
							</div>
							<?php
						}
						?>

                        <?php
                        echo $the_view_content;
                        ?>
					</div>
					<!-- [ content ] End -->

					<!-- [ Layout footer ] Start -->
					<nav class="layout-footer footer bg-white">
						<div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
							<div class="pt-3">
								<span class="footer-text font-weight-semibold">&copy; <a href="/" class="footer-link" target="_blank"><?=$this->config->item('copyright')?></a></span>
							</div>
							<div>

							</div>
						</div>
					</nav>
					<!-- [ Layout footer ] End -->
				</div>
				<!-- [ Layout content ] Start -->
			</div>
			<!-- [ Layout container ] End -->
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->

	<?php
}

$this->load->view('templates/_parts/admin_master_footer_view');?>
