<!doctype html>
<html lang="zxx">

<!-- CSS header -->
<?php $this->load->view('frontend/common/css'); ?>

<body id="top">
    <!-- LOADER TEMPLATE -->
    <div id="page-loader">
        <div class="loader-icon fa fa-spin colored-border"></div>
    </div>
    <!-- /LOADER TEMPLATE -->
    <?php 
        $this->load->view('frontend/content/intro_hero');
        $this->load->view('frontend/content/navmenu');
        $this->load->view('frontend/content/about');
        $this->load->view('frontend/content/features');
        $this->load->view('frontend/content/why_choose');
        $this->load->view('frontend/content/fun_facts');
        $this->load->view('frontend/content/possibilities');
        $this->load->view('frontend/content/reviews');
        $this->load->view('frontend/content/newsletter');
        $this->load->view('frontend/content/contact');
        $this->load->view('frontend/content/footer');
    ?>
    <!-- JS Files -->
    <?php $this->load->view('frontend/common/js'); ?>

</body>
</html>