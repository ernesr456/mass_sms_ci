<!-- START WHY CHOOSE My Soapbox -->
<section id="why-choose">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="app-screen">
                    <img src="<?php echo site_url(); ?>assets/frontend/img/screen/3.png" alt="">
                </div>
            </div>
            <!-- END col-md-6-->
            <div class="col-md-5 col-md-offset-1">
                <div class="section-count">
                    <span>03</span>
                </div>
                <div class="section-text">
                    <h2 class="section-title">Why Choose Us?</h2>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </p>
                </div>
                <!-- END section-text-->
                <div class="why-choose-reason-blocks">
                    <div class="reason-block wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".1s">
                        <div class="reason-inner">
                            <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                            <p>
                                Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Tetro bespoke sustainable Kickstarter organic.
                            </p>
                        </div>
                        <!-- END reason-inner -->
                    </div>
                    <!-- END reason-block-->
                    <div class="reason-block wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".3s">
                        <div class="reason-inner active">
                            <h4>Neutra asymmetrical drinking vinegar direct.</h4>
                            <p>
                                Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Tetro bespoke sustainable Kickstarter organic.
                            </p>
                        </div>
                        <!-- END reason-inner -->
                    </div>
                    <!-- END reason-block-->
                    <div class="reason-block wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".5s">
                        <div class="reason-inner">
                            <h4>Your perfect landing page to present your app. Ignis re volorerovit</h4>
                            <p>
                                Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Tetro bespoke sustainable Kickstarter organic.
                            </p>
                        </div>
                        <!-- END reason-inner -->
                    </div>
                    <!-- END reason-block-->
                    <div class="reason-block wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".7s">
                        <div class="reason-inner">
                            <h4>Neutra asymmetrical drinking vinegar direct.</h4>
                            <p>
                                Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Tetro bespoke sustainable Kickstarter organic.
                            </p>
                        </div>
                        <!-- END reason-inner -->
                    </div>
                    <!-- END reason-block-->
                </div>
                <!-- END why-choose-blocks-->
            </div>
            <!-- END col-md-5 col-md-offset-1 -->
        </div>
        <!-- END row-->
    </div>
    <!-- END container-->
</section>
<!-- END WHY CHOOSE My Soapbox-->