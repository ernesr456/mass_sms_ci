<!--START FOORER-->
<footer id="naive-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-12 pull-right">
                <div class="social">
                    <ul class="list-inline">
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-google"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-pinterest-p"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END social-->
            </div>
            <!-- END col-sm-6 col-xs-12 pull-right -->
            <div class="col-sm-6 col-xs-12 copy-right">
                <p>
                    &copy;<?=date('Y');?> My Soapbox. All rights reserved. 
                </p>
            </div>
            <!-- END col-sm-6 col-xs-12 copy-right -->
        </div>
        <!-- END row-->
    </div>
<!-- END container-->
</footer>
<!--END FOORER-->

<!-- SCROLL TO TOP-->
<div class="scroll-top">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
</div>
<!-- END Scroll to Top -->