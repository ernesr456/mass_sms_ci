<!--START CONTACT-->
<section id="contact">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 no-padding">
                <div id="naive_map" style="width:100%; height:650px;"></div>
            </div>
            <!-- END col-sm-6 -->
            <div class="col-md-6 contact-wrap">
                <div class="row row-naive-contact">
                    <div class="col-sm-6">
                        <address class="address-block wow fadeInLeft" data-wow-duration="0.6s" data-wow-delay=".1s">
                        <div class="address-icon">
                            <i class="fa fa-phone-square"></i>
                        </div>
                        <p>
                            <span>Office</span> (514) 888-8888 <br>
                            <span>Home</span> (514) 888-9999
                        </p>
                    </address>
                        <!-- END address-block-->
                        <address class="address-block wow fadeInLeft" data-wow-duration="0.6s" data-wow-delay=".3s">
                        <div class="address-icon">
                            <i class="fa fa-envelope-square"></i>
                        </div>
                            <a href="mailto:support@email.com">support@email.com</a>
                        <p>
                            www.example.com
                        </p>
                    </address>
                        <!-- END address-block-->
                        <address class="address-block wow fadeInLeft" data-wow-duration="0.6s" data-wow-delay=".5s">
                        <div class="address-icon">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <p>
                            Invisible Street <br>
                            Unknown City, INV 34331
                        </p>
                    </address>
                        <!-- END address-block-->
                    </div>
                    <!-- END col-sm-6-->
                    <div class="col-sm-6">
                        <div class="naive-contact-form wow fadeInRight" data-wow-duration="0.6s" data-wow-delay=".5s">
                            <h4>Say Hello</h4>
                            <form class="contact-form clearfix" action="http://echotheme.com/naive-v2/parallax-background/contact/contact.php" name="contactform" method="POST" id="contactform">
                                <div class="contact-message"></div>
                                <input type="text" class="form-field" name="name" id="contactname" placeholder="Name">
                                <input type="text" class="form-field" name="email" id="contactemail" placeholder="Email">
                                <textarea rows="4" class="form-field" name="message" id="contactmessage" placeholder="Message"></textarea>
                                <button type="submit" value="Submit" name="submit" id="submit" class="submit"><i class="fa fa-paper-plane"></i></button>
                            </form>
                        </div>
                        <!-- END naive-contact-->
                    </div>
                    <!-- END col-sm-6-->
                </div>
                <!-- END row-->
            </div>
            <!-- END col-sm-6 -->
        </div>
        <!-- END row-->
    </div>
    <!-- END container-->
</section>
<!--END CONTACT-->