<!-- START ABOUT-->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="app-screen wow fadeInLeft" data-wow-duration="1s">
                    <img class="img-responsive" src="<?php echo site_url(); ?>assets/frontend/img/screen/1.png" alt="">
                </div>
            </div>
            <!-- END col-md-6-->
            <div class="col-md-6">
                <div class="section-count">
                    <span>01</span>
                </div>
                <div class="section-text about-text">
                    <h2 class="section-title">Cut Through the Noise with My Soapbox</h2>
                    <h4>Trusted Leader</h4>
                    <p>
                        My Soapbox is the trusted leader in peer to peer text messaging.
                    </p>
                    <p>
                        With security, compliance, analytics, and support, we empower organizations to have authentic conversations at scale.
                    </p>
                </div>
                <!-- END section-text-->
            </div>
            <!-- END col-md-6-->
        </div>
        <!-- END row-->
    </div>
    <!-- END container-->
</section>
<!-- END ABOUT-->