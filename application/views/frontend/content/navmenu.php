<!-- START NAVIGATION -->
<nav id="nav">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#naive-nav-collapse" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="#top" class="logo scrolling">
                <!-- <img src="img/logo.png" alt=""> -->
                My Soapbox
            </a>
        </div>
        <!-- END navbar-header-->

        <div class="collapse navbar-collapse" id="naive-nav-collapse">
            <ul class="main-menu pull-right">
                <li class="current"><a class="scrolling" href="#top">HOME</a></li>
                <li><a class="scrolling" href="#about">ABOUT</a></li>
                <li><a class="scrolling" href="#features">Features</a></li>
                <li><a class="scrolling" href="#reviews">Reviews</a></li>
                <li><a class="scrolling" href="#contact">CONTACT</a></li>
                <li><a class="scrolling" href="<?=base_url();?>login">Join Now</a></li>
            </ul>
        </div>
        <!-- END naive-nav-collapse-->
    </div>
    <!-- END container-->
    <div class="section-scroll-arrow">
        <i class="fa fa-arrow-down"></i>
    </div>
</nav>
<!-- END nav-->
<!-- END NAVIGATION -->