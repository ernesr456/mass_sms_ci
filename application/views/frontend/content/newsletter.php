<!-- START NEWSLETTER -->
<section id="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 text-right">
                <div class="section-text">
                    <h2 class="section-title">Join Our Newsletter:</h2>
                </div>
                <!-- END section-text-->
            </div>
            <!-- END col-md-4 col-sm-5 text-right-->
            <div class="col-md-5 col-sm-7">
                <form class="newsletter-subscribe">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" id="mc-email" placeholder="Enter your email address" class="form-control">
                        <label class="subscribe-message"></label>
                        <span class="input-group-btn">
                        <button type="submit" class="btn"><span><i class="fa fa-paper-plane"></i></span></button>
                        </span>
                    </div>
                </form>
            </div>
            <!-- END col-md-5 col-sm-7-->
        </div>
        <!-- END row-->
    </div>
    <!-- END container-->
</section>
<!-- END NEWSLETTER-->