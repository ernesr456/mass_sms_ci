<!-- START FEATURES-->
<section id="features">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 features-brief text-right">
                <div class="features-text">
                    <div class="section-count">
                        <span>02</span>
                    </div>
                    <div class="section-text">
                        <h2 class="section-title">My Soapbox Features</h2>
                        <p>
                            My Soapbox works because people read and respond to texts—and to communication that feels more human.
                        </p>
                        <a href="" class="btn btn-primary btn-lg">Talk to Sales</a>
                    </div>
                    <!-- END section-text-->
                    <div class="features-tab">

                        <div class="features-tab-list">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".1s">
                                    <a href="#features-tab1" data-toggle="tab">
                                        <i class="fa fa-codiepie"></i>
                                    </a>
                                </li>
                                <li class="wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".2s">
                                    <a href="#features-tab2" data-toggle="tab">
                                        <i class="fa fa-diamond "></i>
                                    </a>
                                </li>
                                <li class="wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".3s">
                                    <a href="#features-tab3" data-toggle="tab">
                                        <i class="fa fa-lightbulb-o"></i>
                                    </a>
                                </li>
                                <li class="wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".1s">
                                    <a href="#features-tab4" data-toggle="tab">
                                        <i class="fa fa-pie-chart"></i>
                                    </a>
                                </li>
                                <li class="wow zoomIn" data-wow-duration="0.7s" data-wow-delay=".2s">
                                    <a href="#features-tab5" data-toggle="tab">
                                        <i class="fa fa-copy"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END features-tab-list-->


                        <div class="tab-content features-tab-content">

                            <div role="tabpanel" class="tab-pane fade in active" id="features-tab1">
                                <h4>Simple and flexible</h4>
                                <p>
                                    Stay focused on your audience and goals. Straightforward features and functionality take the stress out of managing conversations.
                                </p>
                                <ul>
                                    <li> <i class="fa fa-check-circle"></i> Lorem ipsum dolor sit. Incidunt laborum beatae earum
                                    </li>
                                    <li> <i class="fa fa-check-circle"></i> Enim ad minim veniam, quis nostrud exercitation ullamco
                                    </li>
                                    <li><i class="fa fa-check-circle"></i> Laboris nisi ut aliquip ex ea commodo consequat
                                    </li>
                                </ul>
                            </div>
                            <!-- END features-tab1 -->

                            <div role="tabpanel" class="tab-pane fade" id="features-tab2">
                                <h4>A smarter solution</h4>
                                <p>
                                    Maximize the value of real connection. Two-way dialogue builds trust, encourages more interaction, and moves people to act.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis dolores cumque nobis laboriosam nam corporis quos sit et, aliquid odio.
                                </p>
                            </div>
                            <!-- END features-tab2-->

                            <div role="tabpanel" class="tab-pane fade" id="features-tab3">
                                <h4>Truly social</h4>
                                <p>
                                    One-to-one messaging is personal, and makes people more inclined to listen. Listening back leads to relationships that last.
                                </p>
                                <ul>
                                    <li> <i class="fa fa-check-circle"></i> Lorem ipsum dolor sit. Incidunt laborum beatae earum
                                    </li>
                                    <li><i class="fa fa-check-circle"></i> Enim ad minim veniam, quis nostrud exercitation ullamco
                                    </li>
                                </ul>
                            </div>
                            <!-- END features-tab3 -->

                            <div role="tabpanel" class="tab-pane fade" id="features-tab4">
                                <h4>Success at scale</h4>
                                <p>
                                    With Hustle, you don’t have to sacrifice reach for results. Engage one-on-one with as many people as you want, and watch what happens.
                                </p>
                                <ul>
                                    <li> <i class="fa fa-check-circle"></i> Lorem ipsum dolor sit. Incidunt laborum beatae earum
                                    </li>
                                    <li> <i class="fa fa-check-circle"></i> Enim ad minim veniam, quis nostrud exercitation ullamco
                                    </li>
                                    <li> <i class="fa fa-check-circle"></i> Laboris nisi ut aliquip ex ea commodo consequat
                                    </li>
                                </ul>
                            </div>
                            <!-- END features-tab4 -->

                            <div role="tabpanel" class="tab-pane fade" id="features-tab5">
                                <h4>Easy To customize</h4>
                                <p>
                                    Itatendam utentotatiat volupta expero ma volorum quiam vel es tioreped erae rendipsa nulluptur ressunt eatistis etur, eatisti aut militat. Ate reicipid quidestia vent.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At nemo vel veniam soluta quasi, rem accusantium officia debitis laudantium dolorum placeat, exercitationem iure magnam doloremque perferendis nulla maiores ullam sunt.
                                </p>
                            </div>
                            <!-- END features-tab5 -->
                        </div>
                        <!-- END tab-content-->
                    </div>
                    <!-- END features-tab-->
                </div>
                <!-- END features-text-->
            </div>
            <!-- END col-md-6 features-berif -->
            <div class="col-md-6 no-padding">
                <div class="feature-screen">
                    <img src="<?php echo site_url(); ?>assets/frontend/img/screen/f1.png" alt="">
                    <img src="<?php echo site_url(); ?>assets/frontend/img/screen/f2.png" alt="">
                    <img src="<?php echo site_url(); ?>assets/frontend/img/screen/f3.png" alt="">
                </div>
                <!-- END feature-screen-->
            </div>
            <!-- END col-md-6 no-padding-->
        </div>
        <!-- END row-->
    </div>
    <!-- END container-fluid-->
</section>
<!-- END FEATURES-->