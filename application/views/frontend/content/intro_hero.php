<!-- START Intro-Hero -->
<section id="intro-hero">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="hero-screen">
                    <img src="<?php echo site_url(); ?>assets/frontend/img/hero-screen.png" alt="">
                </div> <!-- END hero-screen-->
            </div> <!-- END col-md-3-->
            <div class="col-md-9">
                <div class="hero-content">
                    <h2 class="hero-title"> My <span>Soapbox</span></h2>
                    <p class="hero-subtitle">Awesome SMS Campaign App</p>
                    <div class="hero-btn">
                        <a href="<?=base_url();?>login" class="scrolling btn-active">Join Now</a>
                        <a href="#about" class="scrolling">Discover More</a>
                    </div>
                </div>
            </div> <!-- END col-md-9-->
        </div> <!-- END row-->
    </div> <!-- END conrtainer-->
</section>
<!-- END intro-hero -->