<!-- START FUN FACT-->
<section id="fun-fact">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="fun-fact-block wow zoomIn" data-wow-duration="1s" data-wow-delay=".1s">
                    <h4 data-from="0" data-to="14500" data-speed="3000">14500</h4>
                    <p>conversations</p>
                </div>
                <!-- END fun-fact-block-->
            </div>
            <!-- END col-md-2 -->
            <div class="col-md-2">
                <div class="fun-fact-block wow zoomIn" data-wow-duration="1s" data-wow-delay=".3s">
                    <h4 data-from="0" data-to="3532" data-speed="3000">3532</h4>
                    <p>Recipients</p>
                </div>
            </div>
            <!-- END col-md-2 -->
            <div class="col-md-4">
                <div class="fun-fact-screen wow zoomIn" data-wow-duration="1s" data-wow-delay=".5s">
                    <img src="<?php echo site_url(); ?>assets/frontend/img/screen/4.png" alt="">
                </div>
            </div>
            <!-- END col-md-4-->
            <div class="col-md-2">
                <div class="fun-fact-block wow zoomIn" data-wow-duration="1s" data-wow-delay=".7s">
                    <h4 data-from="0" data-to="3532" data-speed="3000">3532</h4>
                    <p>active recipients</p>
                </div>
            </div>
            <!-- END col-md-2 -->
            <div class="col-md-2">
                <div class="fun-fact-block wow zoomIn" data-wow-duration="1s" data-wow-delay=".9s">
                    <h4 data-from="0" data-to="835" data-speed="3000">835</h4>
                    <p>happy clients</p>
                </div>
            </div>
            <!-- END col-md-2 -->
        </div>
        <!-- END row-->
    </div>
    <!-- END container-->
</section>
<!-- END  FUN FACT-->