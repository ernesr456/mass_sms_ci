<!-- START REVIEWS -->
<section id="reviews">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-4 text-center">
                <div class="section-text">
                    <h2 class="section-title">What People Are Saying About My Soapbox</h2>
                </div>
                <!-- END section-text-->
            </div>
            <!-- END col-sm-12 -->
        </div>
        <!--  END row -->
        <div class="row">
            <div class="col-sm-4">
                <div class="reviewer-list">
                    <ul>
                        <li class="wow zoomIn" data-wow-duration="0.6s" data-wow-delay=".1s">
                            <a href="#review-1" data-review="review-1">
                                <figure class="reviewer-thumb">
                                    <img src="<?php echo site_url(); ?>assets/frontend/img/man/1.jpg" alt="">
                                </figure>
                                <span class="reviewer-info">
                                <span class="name">Alex Petrou</span>
                                <span>Client</span>
                                </span>
                            </a>
                        </li>
                        <li class="wow zoomIn" data-wow-duration="0.6s" data-wow-delay=".3s">
                            <a href="#review-2" data-review="review-2">
                                <figure class="reviewer-thumb">
                                    <img src="<?php echo site_url(); ?>assets/frontend/img/man/2.jpg" alt="">
                                </figure>
                                <span class="reviewer-info">
                                <span class="name">Gustavo Souza</span>
                                <span>Client</span>
                                </span>
                            </a>
                        </li>
                        <li class="active wow zoomIn" data-wow-duration="0.6s" data-wow-delay=".5s">
                            <a href="#review-3" data-review="review-3">
                                <figure class="reviewer-thumb">
                                    <img src="<?php echo site_url(); ?>assets/frontend/img/man/3.jpg" alt="">
                                </figure>
                                <span class="reviewer-info">
                                <span class="name">Ali Zaman</span>
                                <span>Client</span>
                                </span>
                            </a>
                        </li>
                        <li class="wow zoomIn" data-wow-duration="0.6s" data-wow-delay=".7s">
                            <a href="#review-4" data-review="review-4">
                                <figure class="reviewer-thumb">
                                    <img src="<?php echo site_url(); ?>assets/frontend/img/man/4.jpg" alt="">
                                </figure>
                                <span class="reviewer-info">
                                <span class="name">Nur jaman</span>
                                <span>Client</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--END reviewer-list-->
            </div>
            <!-- END col-sm-4-->

            <div class="col-sm-8 text-center">
                <div class="review-single" id="review-1">
                    <div class="rating">
                        <span class="rating-title">This is what I was looking for </span>
                        <span class="rating-stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        </span>
                    </div>
                    <!-- END rating-->
                    <div class="review-text">
                        <p>
                            In the aftermath of the attack, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <!-- END review-text-->
                    <div class="reviewer-meta">
                        <div class="reviewer-img">
                            <img src="<?php echo site_url(); ?>assets/frontend/img/man/1.jpg" alt="">
                        </div>
                        <!-- END reviewer-img-->
                        <div class="reviewer-name">
                            <h4>Alex Petrou</h4>
                            <p>Client</p>
                        </div>
                        <!-- END reviewer-name-->
                    </div>
                    <!-- END reviewer-meta-->
                </div>
                <!-- END review-single-->

                <div class="review-single" id="review-2">
                    <div class="rating">
                        <span class="rating-title">Great SMS Tool ever</span>
                        <span class="rating-stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        </span>
                    </div>
                    <!-- END rating-->
                    <div class="review-text">
                        <p>
                            In the aftermath of the attack, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <!-- END review-text-->
                    <div class="reviewer-meta">
                        <div class="reviewer-img">
                            <img src="<?php echo site_url(); ?>assets/frontend/img/man/2.jpg" alt="">
                        </div>
                        <!-- END reviewer-img-->
                        <div class="reviewer-name">
                            <h4>Gustavo Souza</h4>
                            <p>Client</p>
                        </div>
                        <!-- END reviewer-name-->
                    </div>
                    <!-- END reviewer-meta-->
                </div>
                <!-- END review-single-->



                <div class="review-single active" id="review-3">
                    <div class="rating">
                        <span class="rating-title">AWESOME! love it </span>
                        <span class="rating-stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        </span>
                    </div>
                    <!-- END rating-->
                    <div class="review-text">
                        <p>
                            In the aftermath of the attack, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <!-- END review-text-->
                    <div class="reviewer-meta">
                        <div class="reviewer-img">
                            <img src="<?php echo site_url(); ?>assets/frontend/img/man/3.jpg" alt="">
                        </div>
                        <!-- END reviewer-img-->
                        <div class="reviewer-name">
                            <h4>Ali Zaman</h4>
                            <p>Client</p>
                        </div>
                        <!-- END reviewer-name-->
                    </div>
                    <!-- END reviewer-meta-->
                </div>
                <!-- END review-single-->


                <div class="review-single" id="review-4">
                    <div class="rating">
                        <span class="rating-title">Nice design </span>
                        <span class="rating-stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        </span>
                    </div>
                    <!-- END rating-->
                    <div class="review-text">
                        <p>
                            In the aftermath of the attack, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <!-- END review-text-->
                    <div class="reviewer-meta">
                        <div class="reviewer-img">
                            <img src="<?php echo site_url(); ?>assets/frontend/img/man/4.jpg" alt="">
                        </div>
                        <!-- END reviewer-img-->
                        <div class="reviewer-name">
                            <h4>Nur jaman</h4>
                            <p>Client</p>
                        </div>
                        <!-- END reviewer-name-->
                    </div>
                    <!-- END reviewer-meta-->
                </div>
                <!-- END review-single-->
            </div>
            <!-- END col-sm-8 text-center -->
        </div>
        <!-- END row-->
    </div>
    <!-- END container-->
</section>
<!-- END REVIEWS -->