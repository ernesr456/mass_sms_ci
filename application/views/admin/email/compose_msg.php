<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/quill/typography.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/quill/editor.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/messages.css">
<?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modEmail'])? (json_decode($authPermissionID,true)['modEmail']===true? '':show_404()): show_404()) : ''?>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2 email-page">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start --> 
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <!-- [ content ] Start -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <div class="row">
                            <!-- Messages sidebox -->
                            <div class="messages-sidebox col mb-4" id="mail-sidebox">
                                <?php $this->load->view('admin/email/mail_sidebox'); ?>
                            </div>
                            <!-- / Messages sidebox -->

                            <div class="col">
                                <div class="card">
                                    <h4 class="card-header py-4"><i class="ion ion-md-create"></i> &nbsp; Compose Message</h4>
                                    <div class="card-body">
                                        <form action="javascript:;" id="send_message_form">
                                            <div class="row">
                                                <div class="col-md-6">  
                                                    <div class="form-group">
                                                        <label class="form-label">Send To:</label>
                                                        <select name="send_to_type" id="send_to_type" class="form-control">
                                                            <option value="all_emails" <?=($msg_data&&$msg_data['send_to_type']=='all_emails')?'selected':'';?>>All Emails from My Contacts</option>
                                                            <option value="specific_emails" <?=($msg_data&&$msg_data['send_to_type']=='specific_emails')?'selected':'';?>>Specific Emails Only</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Send Type:</label>
                                                        <select name="send_type" id="send_type" class="form-control">
                                                            <option value="group_send" <?=($msg_data&&$msg_data['send_to_type']=='group_send')?'selected':'';?>>Group Send</option>
                                                            <option value="individual_sending" <?=($msg_data&&$msg_data['send_to_type']=='individual_sending')?'selected':'';?>>Send Individually</option>
                                                        </select>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">From:</label>
                                                        <input type="text" class="form-control" id="from_name" name="from_name" value="<?=($msg_data)?$msg_data['from_name']:$user_info['name'];?>">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Sender Email:</label>
                                                        <input type="text" class="form-control" value="<?=$user_info['email'];?>" disabled>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group <?=($msg_data&&$msg_data['send_to_type']=='specific_emails')?'':'d-none';?>" id="select_emails_group">
                                                <label class="form-label">Select Emails:</label>
                                                <?php if($contact_emails){ ?>
                                                <?php $send_list = ($msg_data) ? json_decode($msg_data['send_to']) : []; ?>
                                                    <select class="select2-demo form-control" id="send_to" name="send_to[]" multiple style="width: 100%" required>
                                                        <?php foreach($contact_emails as $e){ ?>
                                                            <option value="<?=$e['id'];?>" <?=($send_list&&in_array($e['id'],$send_list))?'selected':'';?>><?=$e['email'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                <?php }else{ ?>
                                                    <input type="text" class="form-control" id="send_to" name="send_to" placeholder="No contact emails added" disabled>
                                                <?php } ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Subject:</label>
                                                <input type="text" class="form-control" id="subject" name="subject" value="<?=($msg_data)?$msg_data['subject']:'';?>">
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group mt-4">
                                                <div id="message-editor-toolbar">
                                                    <span class="ql-formats">
                                                        <select class="ql-font"></select>
                                                        <select class="ql-size"></select>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <button class="ql-bold"></button>
                                                        <button class="ql-italic"></button>
                                                        <button class="ql-underline"></button>
                                                        <button class="ql-strike"></button>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <select class="ql-color"></select>
                                                        <select class="ql-background"></select>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <button class="ql-script" value="sub"></button>
                                                        <button class="ql-script" value="super"></button>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <button class="ql-header" value="1"></button>
                                                        <button class="ql-header" value="2"></button>
                                                        <button class="ql-blockquote"></button>
                                                        <button class="ql-code-block"></button>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <button class="ql-list" value="ordered"></button>
                                                        <button class="ql-list" value="bullet"></button>
                                                        <button class="ql-indent" value="-1"></button>
                                                        <button class="ql-indent" value="+1"></button>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <button class="ql-direction" value="rtl"></button>
                                                        <select class="ql-align"></select>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <button class="ql-link"></button>
                                                        <button class="ql-image"></button>
                                                        <button class="ql-video"></button>
                                                    </span>
                                                    <span class="ql-formats">
                                                        <button class="ql-clean"></button>
                                                    </span>
                                                </div>
                                                <div id="message-editor" style="height: 400px"><?=($msg_data)?$msg_data['message']:'';?></div>
                                                <textarea id="message-editor-fallback" name="message" class="form-control d-none" style="height: 400px"></textarea>
                                            </div>
                                            <!-- Footer -->
                                            <div class="text-right mt-4">
                                                <input type="hidden" id="msg_id" name="msg_id" value="<?=($msg_data)?$msg_data['id']:'';?>">
                                                <?php if($contact_emails){ ?>
                                                    <button onclick="send_message()" type="button" class="btn btn-primary ml-2" id="send_msg_btn"><i class="ion ion-ios-paper-plane"></i>&nbsp; Send</button>
                                                <?php } ?>
                                                <button onclick="save_mail_as_draft()" type="button" class="btn btn-info ml-2" id="draft_msg_btn"><i class="ion ion-md-save"></i>&nbsp; Save to Drafts</button>
                                            </div>
                                            <!-- / Footer -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ content ] End -->
                    </div>
                <!-- [ Layout footer ] -->
                <?php $this->load->view('admin/common/footer'); ?>
            </div>
        </div>
    </div>
    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script>
    // Quill does not support IE 10 and below so don't load it to prevent console errors
    if (typeof document.documentMode !== 'number' || document.documentMode > 10) {
        document.write('\x3Cscript src="<?=base_url();?>assets/admin/libs/quill/quill.js">\x3C/script>');
    }
</script>
<script src="<?=base_url();?>assets/admin/js/pages/pages_messages.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_email.js"></script>
</body>
</html>
