<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/quill/typography.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/quill/editor.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/messages.css">
<?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modEmail'])? (json_decode($authPermissionID,true)['modEmail']===true? '':show_404()): show_404()) : ''?>
<style>
    .mtb-10{ margin: 10px 0; }
    .mt-7{ margin-top: 7px; }
</style>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2 email-page">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start --> 
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <!-- [ content ] Start -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <div class="row">
                            <!-- Messages sidebox -->
                            <div class="messages-sidebox col mb-4" id="mail-sidebox">
                                <?php $this->load->view('admin/email/mail_sidebox'); ?>
                            </div>
                            <!-- / Messages sidebox -->
                            <div class="col">

                                <!-- Messages list -->
                                <div class="card">
                                    <?php $icon = ($page=='compose')?'md-create':(($page=='sent')?'ios-mail':(($page=='draft')?'md-folder-open':(($page=='trash')?'md-trash':''))); ?>
                                    <!-- Header -->
                                    <h4 class="card-header media flex-wrap align-items-center py-4">
                                        <span class="media-body"><i class="ion ion-<?=$icon?>"></i> &nbsp; <?=$page_head?></span>
                                        <span class="clearfix"></span>
                                    </h4>
                                    <!-- / Header -->

                                    <!-- Controls -->
                                    <div class="media flex-wrap align-items-center py-1 px-2">
                                        <div class="media-body d-flex flex-wrap flex-basis-100 flex-basis-sm-auto">
                                            <button onclick="location.reload()" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Refresh">
                                                <i class="ion ion-md-refresh"></i>
                                            </button>
                                            <button onclick="selectAllMsg(1)" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" id="select-btn" title="Select/Deselect All">
                                                <i class="ion ion-md-checkbox-outline"></i>
                                            </button>
                                            <?php if($page=='sent' || $page=='trash'){ ?>
                                                <button onclick="saveAllAsDraft('<?=$page;?>')" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Move to drafts">
                                                    <i class="ion ion-md-folder-open"></i>
                                                </button>
                                            <?php } if($page=='sent' || $page=='draft'){ ?>
                                                <button onclick="moveToTrash('<?=$page;?>')" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Move to trash">
                                                    <i class="ion ion-md-trash"></i>
                                                </button>
                                            <?php } if($page=='trash'){ ?>
                                                <button onclick="deletePermanently('<?=$page;?>')" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Delete Permamently">
                                                    <i class="ion ion-md-close"></i>
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <hr class="border-light m-0">
                                    <!-- / Controls -->
                                    <form action="javascript:;" id="messageListForm">
                                        <ul class="list-group messages-list">
                                            <?php if($emails){ foreach($emails as $e){ ?>
                                                <li class="list-group-item px-4">
                                                    <div class="message-checkbox mr-1">
                                                        <label class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input msg_id" name="msg_id[]" id="msg<?=$e['id'];?>" value="<?=$e['id'];?>">
                                                            <span class="custom-control-label"></span>
                                                        </label>
                                                    </div>
                                                    <a href="<?=base_url();?>email/compose/<?=$e['id'];?>" class="message-sender flex-shrink-1 d-block text-dark"><?=($e['from_name'])?$e['from_name']:'No Name'; ?></a>
                                                    <a href="<?=base_url();?>email/compose/<?=$e['id'];?>" class="message-subject flex-shrink-1 d-block text-dark"><?=($e['subject'])?$e['subject']:'[No Subject]'; ?></a>
                                                    <div class="message-date text-muted"><?=$this->Email_model->relative_date($e['date_send']);?></div>
                                                </li>
                                            <?php } }else{ ?>
                                                <li class="list-group-item px-4">
                                                    <a href="javascript:;" class="message-sender flex-shrink-1 d-block text-dark">No emails found.</a>
                                                    <div class="message-date text-muted">Today</div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </form>
                                    <?php if($emails){ ?>
                                        <hr class="border-light m-0">
                                        <div class="media flex-wrap align-items-center py-1 px-2" id="pagination">
                                            <div class="text-muted mr-3 ml-auto mtb-10">Showing <?=count($emails)?> of <?=$total_rows;?></div>
                                            <?=$links;?> 
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- / Messages list -->
                            </div>
                        </div>
                    </div>
                    <!-- [ content ] End -->
                    </div>
                <!-- [ Layout footer ] -->
                <?php $this->load->view('admin/common/footer'); ?>
            </div>
        </div>
    </div>
    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script>
    // Quill does not support IE 10 and below so don't load it to prevent console errors
    if (typeof document.documentMode !== 'number' || document.documentMode > 10) {
        document.write('\x3Cscript src="<?=base_url();?>assets/admin/libs/quill/quill.js">\x3C/script>');
    }
</script>
<script src="<?=base_url();?>assets/admin/js/pages/pages_messages.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_email.js"></script>
</body>
</html>
