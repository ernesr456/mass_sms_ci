<a href="<?=base_url();?>email/compose" class="btn btn-primary btn-block mt-md-4">Compose</a>
<hr class="border-light my-4">
<!-- Mail boxes -->
<a href="<?=base_url();?>email/compose" class="d-flex justify-content-between align-items-center <?=($page=='compose')?'text-dark font-weight-bold':'text-muted';?> py-2">
    <div> <i class="ion ion-md-create"></i> &nbsp; Compose </div>
</a>
<a href="<?=base_url();?>email/sent" class="d-flex justify-content-between align-items-center  <?=($page=='sent')?'text-dark font-weight-bold':'text-muted';?> py-2">
    <div> <i class="ion ion-ios-mail"></i> &nbsp; Sent </div>
    <?php $cnt_sent = $this->Email_model->count_mails(1); ?>
    <?=($cnt_sent)?(($cnt_sent>99)?'<div class="badge badge-success">99+</div>':'<div class="badge badge-success">'.$cnt_sent.'</div>'):''; ?>
</a>
<a href="<?=base_url();?>email/drafts" class="d-flex justify-content-between align-items-center  <?=($page=='draft')?'text-dark font-weight-bold':'text-muted';?> py-2">
    <div> <i class="ion ion-md-folder-open"></i> &nbsp; Drafts </div>
    <?php $cnt_drafts = $this->Email_model->count_mails(2); ?>
    <?=($cnt_drafts)?(($cnt_drafts>99)?'<div class="badge badge-primary">99+</div>':'<div class="badge badge-primary">'.$cnt_drafts.'</div>'):''; ?>
</a>
<a href="<?=base_url();?>email/trash" class="d-flex justify-content-between align-items-center  <?=($page=='trash')?'text-dark font-weight-bold':'text-muted';?> py-2">
    <div> <i class="ion ion-md-trash"></i> &nbsp; Trash </div>
    <?php $cnt_delete = $this->Email_model->count_mails(3); ?>
    <?=($cnt_delete)?(($cnt_delete>99)?'<div class="badge badge-danger">99+</div>':'<div class="badge badge-danger">'.$cnt_delete.'</div>'):''; ?>
</a>