

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Forgot Password</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="my-5">
					<div class="form-group" id="email_content">
						<label class="form-label">Enter your email here</label>
						<input type="email" id="recover_email" name="email" class="form-control" autocomplete="none">
						<div class="clearfix"></div>
					</div>
					<div id="verify_content" style="display: none;">
						<div class="form-group">
							<label class="form-label d-flex justify-content-between align-items-end">
							<span>Password</span>
							</label>
							<input type="password" id="password" name="password" class="form-control">
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<label class="form-label d-flex justify-content-between align-items-end">
							<span>Confirm Password</span>
							</label>
							<input type="password" id="password2" name="password2" class="form-control">
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<label class="form-label">Verification Code</label>
							<input type="password" id="verify_key" name="verify_key" class="form-control" autocomplete="none">
							<br/>
							<button type="button" id="resend_code" class="btn btn-primary md-btn-flat">Resend email again</button>
							<div class="clearfix"></div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="get_key">Send</button>
				<button type="button" class="btn btn-primary" id="save_code" style="display: none;">Save</button>
			</div>
		</div>
	</div>
</div>

<!-- [ content ] Start -->
<div class="authentication-wrapper authentication-3">
	<div class="authentication-inner">

		<!-- [ Side container ] Start -->
		<!-- Do not display the container on extra small, small and medium screens -->
		<div class="d-none d-lg-flex col-lg-8 align-items-center ui-bg-cover ui-bg-overlay-container p-5" style="background-image: url('<?=base_url()?>assets/admin/img/bg/21.jpg');">
			<div class="ui-bg-overlay bg-dark opacity-50"></div>
			<!-- [ Text ] Start -->
			<div class="w-100 text-white px-5">
				<h1 class="display-2 font-weight-bolder mb-4">JOIN <br>With Us</h1>
				<div class="text-large font-weight-light">
					As your trusted communication partner.
				</div>
			</div>
			<!-- [ Text ] End -->
		</div>
		<!-- [ Side container ] End -->

		<!-- [ Form container ] Start -->
		<div class="d-flex col-lg-4 align-items-center bg-white p-5">
			<!-- Inner container -->
			<!-- Have to add `.d-flex` to control width via `.col-*` classes -->
			<div class="d-flex col-sm-7 col-md-5 col-lg-12 px-0 px-xl-4 mx-auto">
				<div class="w-100">

					<!-- [ Logo ] Start -->
					<div class="d-flex justify-content-center align-items-center">
						<div class="ui-w-60">
							<div class="w-100 position-relative">
								<img src="<?=base_url()?>assets/admin/img/logo-dark.png" alt="Brand Logo" class="img-fluid">
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<!-- [ Logo ] End -->

					<h4 class="text-center text-lighter font-weight-normal mt-5 mb-0">Login to Your Account</h4>
					<form class="my-5" id="login-form" autocomplete="off"  method="POST">

						<p>
							<?php
							if ($error == 1) {
								?>
								<script>
                                    toastr.error(
                                        'Please check username and password.',
                                        'Too Many Login Attempts!'
									);
								</script>
								<?php
							}
							if ($error == 2) {
								?>
								<script>
                                    toastr.error(
                                        'Please check username and password.',
                                        'Invalid Login Credentials.'
									);
								</script>
								<?php
							}
							if ($error == 4) {
								?>
								<script>
                                    toastr.warning(
                                        'Please check your email to verify.',
                                        'Your account doesn&#39;t verify.'
									);
								</script>
								<?php
							}
							?>
						</p>

						<div class="form-group">
							<label class="form-label">Username</label>
							<input type="text" name="username" id="username1" class="form-control" autocomplete="none">
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<label class="form-label d-flex justify-content-between align-items-end">
								<span>Password</span>
								<a href="/forgot" class="d-block small"data-toggle="modal" data-target="#exampleModal">Forgot password?</a>
							</label>
							<input type="password" name="password" id="password1"class="form-control">
							<div class="clearfix"></div>
						</div>
						<div class="d-flex justify-content-between align-items-center m-0">
							<label class="custom-control custom-checkbox m-0">
								<input type="checkbox" class="custom-control-input">
								<span class="custom-control-label">Remember me</span>
							</label>
						</div>
					</form>
					<div class="d-flex justify-content-between align-items-center m-0">
						<button type="submit" id="login" class="btn btn-block btn-primary">Sign In</button>
					</div>
					<br>
					<!-- [ Form ] End -->
					<div class="text-center text-muted">
						Already have an account?
						<a href="/auth/register">Sign Up</a>
					</div>
				</div>
			</div>
		</div>
		<!-- [ Form container ] End -->

	</div>
</div>
<!-- [ content ] End -->
