<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="<?=base_url()?>admin" class="btn btn-primary">Back</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="d-flex justify-content-end align-content-end">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="site_settings">
                                <form action="javascript:;" method="POST" id="settingsForm">
                                    <!-- Send Grid Settings -->
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="d-flex justify-content-between text-dark" data-toggle="collapse" aria-expanded="true" href="#sendgrid_settings"><b><i class="fa fa-cog"></i> Send Grid Settings</b><div class="collapse-icon"></div></a>
                                        </div>
                                        <div id="sendgrid_settings" class="collapse show" data-parent="#site_settings">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Send Grid API KEY</label>
                                                        <input type="text" name="send_grid_api_key" id="send_grid_api_key" value="<?=isset($settings) ? $settings['send_grid_key'] : ''?>" class="form-control" placeholder="Send Grid API KEY">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Twillio Settings -->
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="d-flex justify-content-between text-dark" data-toggle="collapse" aria-expanded="true" href="#tw_settings"><b><i class="fa fa-cog"></i> Twillio Settings</b><div class="collapse-icon"></div></a>
                                        </div>
                                        <div id="tw_settings" class="collapse show" data-parent="#site_settings">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label class="form-label">Twillio Account SID</label>
                                                        <input type="text" name="twillio_account_sid" id="twillio_account_sid" value="<?=isset($settings) ? $settings['twillio_account_sid'] : ''?>" class="form-control" placeholder="Twillion Account SID">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="form-label">Twillio Auth Token</label>
                                                        <input type="text" name="twillio_auth_token" id="twillio_auth_token" value="<?=isset($settings) ? $settings['twillio_auth_token'] : ''?>" class="form-control" placeholder="Twillio Auth Token">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="form-label">Twillio Account Number</label>
                                                        <input type="text" name="twilio_account_number" id="twilio_account_number" value="<?=isset($settings) ? $settings['twilio_account_number'] : ''?>" class="form-control" placeholder="Twillio Auth Token">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Mail Chimp Settings -->
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="d-flex justify-content-between text-dark" data-toggle="collapse" aria-expanded="true" href="#mc_settings"><b><i class="fa fa-cog"></i> Mail Chimp Settings</b><div class="collapse-icon"></div></a>
                                        </div>
                                        <div id="mc_settings" class="collapse show" data-parent="#site_settings">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Mail Chimp API Key</label>
                                                        <input type="text" name="mailchimp_api_key" id="mailchimp_api_key" value="<?=isset($settings) ? $settings['mailchimp_api_key'] : ''?>" class="form-control" placeholder="Mail Chimp API Key">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- PayPal Settings -->
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="d-flex justify-content-between text-dark" data-toggle="collapse" aria-expanded="true" href="#paypal_settings"><b><i class="fa fa-cog"></i> PayPal Settings</b><div class="collapse-icon"></div></a>
                                        </div>
                                        <div id="paypal_settings" class="collapse show" data-parent="#site_settings">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">PayPal Sanbox/Live</label>
                                                        <select name="paypal_sandbox" id="paypal_sandbox" class="custom-select">
                                                            <option value="1" <?=(isset($settings)) ? (($settings['paypal_sandbox']==1)?'selected':'') : ''?>>Sanbox</option>
                                                            <option value="0" <?=(isset($settings)) ? (($settings['paypal_sandbox']==0)?'selected':'') : ''?>>Live</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Paypal Email</label>
                                                        <input type="text" name="paypal_email" id="paypal_email" value="<?=isset($settings) ? $settings['paypal_email'] : ''?>" class="form-control" placeholder="Paypal Email">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Paypal Client ID</label>
                                                        <input type="text" name="paypal_client_id" id="paypal_client_id" value="<?=isset($settings) ? $settings['paypal_client_id'] : ''?>" class="form-control" placeholder="Paypal Client ID">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Stripe Settings -->
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="d-flex justify-content-between text-dark" data-toggle="collapse" aria-expanded="true" href="#stripe_settings"><b><i class="fa fa-cog"></i> Stripe Settings</b><div class="collapse-icon"></div></a>
                                        </div>
                                        <div id="stripe_settings" class="collapse show" data-parent="#site_settings">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Live/Test Mode</label>
                                                        <select name="stripe_testmode" id="stripe_testmode" class="custom-select">
                                                            <option value="1" <?=(isset($settings)) ? (($settings['stripe_testmode']==1)?'selected':'') : ''?>>Test Mode</option>
                                                            <option value="0" <?=(isset($settings)) ? (($settings['stripe_testmode']==0)?'selected':'') : ''?>>Live Mode</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Test Secret Key</label>
                                                        <input type="text" name="stripe_test_skey" id="stripe_test_skey" value="<?=isset($settings) ? $settings['stripe_test_skey'] : ''?>" class="form-control" placeholder="Test Secret Key">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Test Publishable Key</label>
                                                        <input type="text" name="stripe_test_pubkey" id="stripe_test_pubkey" value="<?=isset($settings) ? $settings['stripe_test_pubkey'] : ''?>" class="form-control" placeholder="Test Publishable Key">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Live Secret Key</label>
                                                        <input type="text" name="stripe_live_skey" id="stripe_live_skey" value="<?=isset($settings) ? $settings['stripe_live_skey'] : ''?>" class="form-control" placeholder="Live Secret Key">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label class="form-label">Live Publishable Key</label>
                                                        <input type="text" name="stripe_live_pubkey" id="stripe_live_pubkey" value="<?=isset($settings) ? $settings['stripe_live_pubkey'] : ''?>" class="form-control" placeholder="Live Publishable Key">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <button id="saveSettingsBtn" type="submit" class="btn btn-primary" onclick="saveSettings()"><i class="ion ion-md-checkmark text-white"></i> Save Changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- [ Content End ] -->
                </div>
                <!-- [ Layout footer ] -->
                <?php $this->load->view('admin/common/footer'); ?>
            </div>
        </div>
    </div>
    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/js/init/init_settings.js"></script>
</body>
</html>
