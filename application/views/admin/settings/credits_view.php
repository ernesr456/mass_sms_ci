<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#our_pricing">
                                        Our Pricing</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php if ($this->session->userdata('role_slug')=='administrator'&&$this->session->userdata('is_logged_in')) { ?>
                                        <div class="d-flex justify-content-end align-content-end">
                                            <a href="<?=base_url()?>admin/add_credits" class="btn btn-primary">Add Credits</a>

                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-datatable table-responsive">
                                        <table class="contact-group-table table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ID</th>
                                                    <th class="text-center">Package</th>
                                                    <th class="text-center">Credits</th>
                                                    <th class="text-center">Price</th>
                                                    <?=($this->session->userdata('role_slug')=='client')?'<th class="text-center">Payment</th>':'';?>
                                                    <?=($this->session->userdata('role_slug')=='administrator')?'<th class="text-center">Action</th>':'';?>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if(!empty($credits)){
                                                    foreach ($credits as $c){
                                                    ?>
                                                    <tr>
                                                        <td class="text-center"><?=$c['id']?></td>
                                                        <td class="text-center"><?=$c['credit_name']?></td>
                                                        <td class="text-center"><?=number_format($c['credit_amount'])?></td>
                                                        <td class="text-center"><?=number_format($c['credit_price'],2).' '.$c['credit_currency']?></td>
                                                        <?php if (($this->session->userdata('role_slug')=='client')) { ?>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Payment Option
                                                                </button>
                                                                <div class="dropdown-menu">
                                                                    <a onclick='window.open("<?php echo base_url().'admin/payment_with_paypal/'.$c['id']; ?>","mywindow","location=1,status=1,scrollbars=1,width=400,height=auto");' style="cursor:pointer;" class="dropdown-item">Paypal</a>
                                                                </div>
                                                            </div>
                                                        </td>   
                                                        <?php } ?>
                                                        <?php if ($this->session->userdata('role_slug')=='administrator'&&$this->session->userdata('is_logged_in')) { ?>
                                                        <td class="text-center">
                                                            <a href="<?=base_url()?>admin/add_credits?id=<?=$c['id']?>"class="btn icon-btn btn-sm btn-outline-primary">
                                                                <span class="far fa-edit"></span>
                                                            </a>
                                                            <a href="<?=base_url()?>admin/credits?did=<?=$c['id']?>" class="btn icon-btn btn-sm btn-outline-danger">
                                                                <span class="fas fa-trash-alt"></span>
                                                            </a>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Our Pricing Modal -->
                <div class="modal fade" id="our_pricing">
                    <div class="modal-dialog">
                        <form class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <img class="img-fluid" src="<?php base_url()?>/assets/Email_image/masssms.jpg">
                                </div>
                                <br>
                                <h3 class="modal-title text-center" style="text-align: center;">How Credit Works?
                                </h3>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-center">
                                        <h5>Sending Message</h5>
                                        <p>Send texts with 150 characters cost <b>1 credit</b></p>
                                    </div>
                                    <br>
                                    <div class="col-md-6 text-center">
                                        <h5>Sending Message with File</h5>
                                        <p>Sending Message with file cost <b>3 credit</b></p>
                                    </div>
                                </div>  
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- [ Layout footer ] -->
                <?php $this->load->view('admin/common/footer'); ?>
            </div>
        </div>
    </div>
    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
</body>
</html>

