<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <?php (isset(json_decode($authPermissionID,true)['modPermission']) ? (json_decode($authPermissionID,true)['modPermission']===true ? '' : show_404()) : show_404()) ?>

    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <h6 class="card-header"><?=$role?></h6>
                                    <div class="col-md-4 mt-3">
                                        <button type="button" class="group-permission btn btn-outline-info" id="<?=($role=='Client')?'2':'3'?>" onclick="showmodal()">Set Permission For All Group</button>
                                    </div>
                                    <div class="card-datatable table-responsive">
                                        <table class="advence-datatables table table-striped table-bordered">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Full Name</th>
                                                    <th>Username</th>
                                                    <th>Permissions</th>
                                                    <th>Role</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              <?php
                                              foreach ($users as $i=>$u) { ?>
                                                <tr class="odd gradeX">
                                                    <td><?=$u['fullname']?></td>
                                                    <td><?=$u['username']?></td>
                                                    <td class="permissionTD">
                                                        <?php $color;$id; 
                                                            //Dashboard Module
                                                            strstr($u['permission'],'modDashboard')?$color='green':$color='red'; 
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Dashboard</kbd>';
                                                            //Campaign Module
                                                            strstr($u['permission'],'modCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Campaign</kbd>';
                                                            strstr($u['permission'],'addCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Campaign</kbd>';
                                                            strstr($u['permission'],'updateCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Campaign</kbd>';
                                                            strstr($u['permission'],'deleteCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Campaign</kbd>';
                                                            //Conversation Module
                                                            strstr($u['permission'],'modConversation')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Conversation</kbd>';
                                                            strstr($u['permission'],'viewMessage')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">View Message</kbd>';
                                                            strstr($u['permission'],'sendMessage')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Send Message</kbd>';
                                                            strstr($u['permission'],'deleteMessage')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Message</kbd>';
                                                            //Contacts Module
                                                            strstr($u['permission'],'modContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Contact</kbd>';
                                                            strstr($u['permission'],'addContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Contact</kbd>';
                                                            strstr($u['permission'],'updateContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Contact</kbd>';
                                                            strstr($u['permission'],'deleteContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Contact</kbd>';
                                                            //Group Module
                                                            strstr($u['permission'],'modGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Group</kbd>';
                                                            strstr($u['permission'],'addGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Group</kbd>';
                                                            strstr($u['permission'],'updateGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Group</kbd>';
                                                            strstr($u['permission'],'deleteGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Group</kbd>';
                                                            //User Module
                                                            strstr($u['permission'],'modUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">User</kbd>';
                                                            strstr($u['permission'],'addUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add User</kbd>';
                                                            strstr($u['permission'],'updateUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update User</kbd>';
                                                            strstr($u['permission'],'deleteUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete User</kbd>';
                                                            //Permission Module   
                                                            strstr($u['permission'],'modPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Permission</kbd>';
                                                            strstr($u['permission'],'createPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Create Permission</kbd>';
                                                            strstr($u['permission'],'updatePermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Permission</kbd>';
                                                            strstr($u['permission'],'deletePermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Permission</kbd>';
                                                            strstr($u['permission'],'setGroupPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Group Permission</kbd>';   
                                                            strstr($u['permission'],'setClientPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Client Permission</kbd>';
                                                            strstr($u['permission'],'removeClientPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Remove Client Permission</kbd>';
                                                            strstr($u['permission'],'setVolunteerPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Volunteer Permission</kbd>';
                                                            strstr($u['permission'],'removeVolunteerPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Remove Volunteer Permission</kbd>';

                                                            //Template Module
                                                            strstr($u['permission'],'modTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Template</kbd>';
                                                            strstr($u['permission'],'addTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Template</kbd>';
                                                            strstr($u['permission'],'updateTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Template</kbd>';
                                                            strstr($u['permission'],'deleteTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Template</kbd>';

                                                            //Project Module
                                                            strstr($u['permission'],'modProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Project</kbd>';
                                                            strstr($u['permission'],'addProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Project</kbd>';
                                                            strstr($u['permission'],'updateProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Project</kbd>';
                                                            strstr($u['permission'],'deleteProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Project</kbd>';

                                                            //Email Module
                                                            strstr($u['permission'],'modEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Email</kbd>';
                                                            strstr($u['permission'],'sendEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Send Email</kbd>';
                                                            strstr($u['permission'],'addEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Email</kbd>';
                                                            strstr($u['permission'],'deleteEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Email</kbd>';
                                                        ?></td>
                                                        <td><?=$u['role']?></td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Options
                                                                </button>
                                                                <div class="dropdown-menu">
                                                                    <button class="dropdown-item" onclick="showmodal(<?=$u['id']?>)">Set Permissions</button>
                                                                    <button class="dropdown-item" onclick="userDeletePermission(<?=$u['id']?>)">Remove Permissions</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- Staustic card 3 Start -->
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="permissionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Set Permission</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" id="user_id" class="form-control">
                                <select class="select2-demo form-control" id="permission_name" style="width: 100%" data-select2-id="76" tabindex="-1" onchange="permission()" aria-hidden="true">
                                    <option  value="0"  selected>Select Option</option>
                                    <?php foreach ($usersPermission as $i=>$u){
                                        echo "<option value=".$u['upid'].">".$u['permission_name']."</option>";
                                    }?>
                                </select>
                                <div class="col-md-12 mt-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                          <!--  // <label><strong>Module Permission</strong></label> -->
                                        </div>
                                        <div class="col-md-12">
                                            <div class="permissionModal ml-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" onclick="userPermission()">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="tdModal" tabindex="-1" role="dialog" aria-labelledby="tdModal" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Full Permission Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="container-fluid permissionBody"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        <!-- [ Content End ] -->
    </div>
    <!-- [ Layout footer ] -->
    <?php $this->load->view('admin/common/footer'); ?>
</div>
</div>
</div>
<!-- Overlay -->
<div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/js/init/init_permission.js"></script>

</body>
</html>
