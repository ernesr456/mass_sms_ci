<?php if($this->session->userdata('user_group')==1): ?>
<!-- Add Contact -->
<div class="modal fade" id="modal_credit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" id="add_credit" class="add_credit" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather ion-ios-add-circle"></i> <span id="modal_title">Add New Credits</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="validation-form">
                        <div class="form-group">
                            <label class="form-label">Credit Name</label>
                            <input type="hidden" id="id" name="id" name="">
                            <input class="form-control" id="credit_name" type="text" name="credit_name" value="" placeholder="Enter credit name">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Price</label>
                            <input class="form-control" type="text" name="currency-field" id="currency-field" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" value="" data-type="currency" placeholder="Enter price here">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Credits</label>
                            <input class="form-control" type="number" id="total_credit" name="credit" value="" placeholder="Enter credit here">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="btn_credits" class="btn btn-primary">Save</button>
                    </div>
            </form>      
        </div>
    </div>
</div>
<?php endif; ?>

<!-- Add Contact -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" class="add_contact" method="POST" id="paymentModalForm">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-shopping-cart"></i> Checkout</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <b>Credit Name: </b>
                        </div>
                        <div class="form-group col-md-8">
                            <span id="package_name"></span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <b>Price: </b>
                        </div>
                        <div class="form-group col-md-8">
                            <input type="hidden" id="package_amount">
                            <input type="hidden" id="credit_id">
                            <span id="credit_price"></span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <b>Total Credit: </b>
                        </div>
                        <div class="form-group col-md-8">
                            <span id="no_of_credits"></span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div id="paypal-button"></div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </form>      
        </div>
    </div>
</div>