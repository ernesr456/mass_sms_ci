<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<?php $this->load->view('admin/common/css'); ?>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
                            <?php if($this->session->userdata('user_group')==1): ?>
                            <div class="col-xl-12 mb-3">
                                <div class="pull-right">
                                        <a data-target="#modal_credit" id="add_plan" data-toggle="modal" href="javascript:;" class="btn btn-info btn-sm"><i class="ion ion-ios-add-circle text-white"></i> Add Credits</a>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row" id="credit_card">
                            <!-- <div class="col-md" style="margin-bottom: 0.75rem !important">
                                <div class="card">
                                    <div class="">
                                        <div class="card-body pull-right" style="padding: 1rem 1.5rem 0rem 1.5rem;">
                                            <a href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-danger" id="delete_plan" title="Delete">
                                                <span class="fas fa-trash"></span>
                                            </a>&nbsp;
                                            <a data-target="#planModal" data-toggle="modal" href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-warning" id="edit_plan" title="Edit">
                                                <span class="fas fa-pencil-alt"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body pricing-table">
                                        <p class="card-text text-center">
                                            <span class="pricing-currency">$</span> <span class="pricing-amount">123.10</span>
                                        </p>
                                        <p class="pricing-title text-center"><span>Pricing 1</span></p>
                                    </div>
                                    <div class="card-body text-center">
                                        <a href="javascript:;" onclick="payWithPaypal(17)">
                                            <button type="button" class="btn btn-info"><i class="fas fa-shopping-cart"></i> Buy</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md" style="margin-bottom: 0.75rem !important">
                                <div class="card">
                                    <div class="">
                                        <div class="card-body pull-right" style="padding: 1rem 1.5rem 0rem 1.5rem;">
                                            <a href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-danger" id="delete_plan" title="Delete">
                                                <span class="fas fa-trash"></span>
                                            </a>&nbsp;
                                            <a data-target="#planModal" data-toggle="modal" href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-warning" id="edit_plan" title="Edit">
                                                <span class="fas fa-pencil-alt"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body pricing-table">
                                        <p class="card-text text-center">
                                            <span class="pricing-currency">$</span> <span class="pricing-amount">123.10</span>
                                        </p>
                                        <p class="pricing-title text-center"><span>Pricing 1</span></p>
                                    </div>
                                    <div class="card-body text-center">
                                        <a href="javascript:;" onclick="payWithPaypal(17)">
                                            <button type="button" class="btn btn-info">Get this Plan</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md" style="margin-bottom: 0.75rem !important">
                                <div class="card">
                                    <div class="">
                                        <div class="card-body pull-right" style="padding: 1rem 1.5rem 0rem 1.5rem;">
                                            <a href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-danger" id="delete_plan" title="Delete">
                                                <span class="fas fa-trash"></span>
                                            </a>&nbsp;
                                            <a data-target="#planModal" data-toggle="modal" href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-warning" id="edit_plan" title="Edit">
                                                <span class="fas fa-pencil-alt"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body pricing-table">
                                        <p class="card-text text-center">
                                            <span class="pricing-currency">$</span> <span class="pricing-amount">123.10</span>
                                        </p>
                                        <p class="pricing-title text-center"><span>Pricing 1</span></p>
                                    </div>
                                    <div class="card-body text-center">
                                        <a href="javascript:;" onclick="payWithPaypal(17)">
                                            <button type="button" class="btn btn-info">Get this Plan</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md" style="margin-bottom: 0.75rem !important">
                                <div class="card">
                                    <div class="">
                                        <div class="card-body pull-right" style="padding: 1rem 1.5rem 0rem 1.5rem;">
                                            <a href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-danger" id="delete_plan" title="Delete">
                                                <span class="fas fa-trash"></span>
                                            </a>&nbsp;
                                            <a data-target="#planModal" data-toggle="modal" href="javascript:void(0)" data-id="17" class="btn icon-btn btn-sm btn-outline-warning" id="edit_plan" title="Edit">
                                                <span class="fas fa-pencil-alt"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body pricing-table">
                                        <p class="card-text text-center">
                                            <span class="pricing-currency">$</span> <span class="pricing-amount">123.10</span>
                                        </p>
                                        <p class="pricing-title text-center"><span>Pricing 1</span></p>
                                    </div>
                                    <div class="card-body text-center">
                                        <a href="javascript:;" onclick="payWithPaypal(17)">
                                            <button type="button" class="btn btn-info">Get this Plan</button>
                                        </a>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="row">
                            <input type="hidden" id="credit_id" name="">
                            <div class="col-md">
                                <div class="card mb-4">
                                    <h6 class="card-header">LAST TRANSACTION</h6>
                                    <div class="card-datatable table-responsive">
                                        <table class="table table-striped table-bordered" id="view_transaction">
                                            <thead>
                                                <tr>
                                                    <th>Credit Name</th>
                                                    <th>Price</th>
                                                    <th>Credit</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Payment Modal -->
                    <?php $this->load->view('admin/credits/payment_modal'); ?>

                    <div class="modal fade" id="our_pricing">
                        <div class="modal-dialog">
                            <form class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <img style="max-width: 250px; max-height: 500px; " class="img-fluid" src="<?=base_url()?>assets/Email_image/masssms.jpg">
                                    </div>
                                    <br>
                                    <h3 class="modal-title text-center" style="text-align: center;">How Credit Works?
                                    </h3>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6 text-center">
                                            <h5>Sending Message</h5>
                                            <p>Send texts with 150 characters cost <b>1 credit</b></p>
                                        </div>
                                        <br>
                                        <div class="col-md-6 text-center">
                                            <h5>Sending Message with File</h5>
                                            <p>Sending Message with file cost <b>3 credit</b></p>
                                        </div>
                                    </div>  
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- [ Layout footer ] -->
                    <?php $this->load->view('admin/common/footer'); ?>
                </div>
            </div>
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->
    <!-- JS Files -->
    <?php $this->load->view('admin/common/js'); ?>
    <script src="<?=base_url()?>assets/admin/js/custom.js"></script>
    <script src="<?=base_url()?>assets\admin\js/init/init_credit.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
    <script src="<?=base_url()?>assets/admin/js/pages/tables_datatables.js"></script>
</body>
</html>

