<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
						<div class="row">
							<div class="col-sm-12 mb-3">
								<div class="row">
									<div class="col-sm-6">
										<a href="<?=base_url()?>admin/credits" class="btn btn-primary">Back</a>
									</div>
									<div class="col-sm-6">
										<div class="d-flex justify-content-end align-content-end">
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="card">
									<div class="card-body">
										<form id="add-credits-form" method="POST" onchange="validateCredits()" action="javascript:;">
											<div class="form-row">
												<div class="form-group col-md-6">
													<label class="form-label">Name</label>
													<input type="text" name="credit_name" value="<?=isset($credit) ? $credit['credit_name'] : ''?>" class="form-control" placeholder="Credit Name">
													<div class="clearfix"></div>
												</div>
												<div class="form-group col-md-6">
													<label class="form-label">Price</label>
													<input type="text" name="credit_price" id="credit_price" oninput="validateNumber(this);" value="<?=isset($credit) ? $credit['credit_price'] : ''?>" class="form-control number" placeholder="Number">
													<div class="clearfix"></div>
												</div>
											</div>

											<div class="form-row">
												<div class="form-group col-md-6">
													<label class="form-label">Total Credits</label>
													<input type="text" id="credit_amount" name="credit_amount" value="<?=isset($credit) ? $credit['credit_amount'] : ''?>" class="form-control number numOnly" placeholder="Number">
													<div class="clearfix"></div>
												</div>
												<div class="form-group col-md-6">
													<label class="form-label">Currency</label>
													<input type="text" readonly="true" name="credit_currency" value="<?=isset($credit) ? $credit['credit_currency'] : 'USD'?>" class="form-control" placeholder="Currency">
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-md-12">
													<label class="form-label">Description</label>
													<input type="text" name="credit_desc" value="<?=isset($credit) ? $credit['credit_desc'] : ''?>" class="form-control" placeholder="Description">
													<div class="clearfix"></div>
												</div>						
											</div>

											<div class="form-group">
												<label class="custom-control custom-checkbox m-0">
													<input type="checkbox" value="1" name="credit_status" class="custom-control-input" <?=(isset($credit)) ? (($credit['credit_status']==1) ? 'checked' : '') : 'checked'?>>
													<span class="custom-control-label">Credit Status</span>
												</label>
											</div>
											<div class="d-flex justify-content-end">
												<input type="hidden" value="<?=(isset($credit))?$credit['id']:''?>" name="credit_id">
												<button onclick="saveCredits()" id="saveCreditsBtn" <?=isset($single) ? '' : 'disabled'?> type="button" class="btn btn-primary"><?=isset($single) ? 'Update' : 'Save'?></button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
                    <!-- [ Content End ] -->
                </div>
                <!-- [ Layout footer ] -->
                <?php $this->load->view('admin/common/footer'); ?>
            </div>
        </div>
    </div>
    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets\admin\js/init/init_credit.js"></script>
</body>
</html>
