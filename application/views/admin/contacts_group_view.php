<<<<<<< HEAD
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
	<div class="col-sm-12">
		<form id="create-contact-group" action="#" method="POST" class="col-md mb-4 add_contactGroup">
			<div class="form-group">
				<div class="input-group">
					<input type="hidden" name="id" id="group_id" value="">
					<input type="text" class="form-control" name="group_name" data-name="" id="group_name" value="" placeholder="Group Name ...">
					<span class="input-group-append">
						<button <?=(json_decode($authPermissionID,true)['addGroup']==="false")? 'style="cursor: no-drop;" disabled' : ''?>
						class="btn btn-primary waves-effect" id="save_group" type="submit"><?=isset($single) ? 'Update group' : 'Save group'?></button>
					</span>
					<div class="clearfix"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-sm-12">
		<div class="card">
			<div class="card-datatable table-responsive">
				<table class="table table-bordered" id="view_contact_group">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th>Group Name</th>
                            <th>Created By</th>
                            <th width="5%">Status</th>
                            <th width="5%">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>        
=======
<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
	<!-- [ Layout wrapper ] Start -->
	<div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
				<?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
						<!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
						<!-- [ Content Start ] -->
						<div class="row">
							<div class="col-sm-12">
								<?php
								if (isset($_GET['added'])){
									if ($_GET['added'] === "true"){
										?>
										<div class="alert alert-dark-success alert-dismissible fade show">
											<button type="button" class="close" data-dismiss="alert">×</button>
											Group successfully added.
										</div>
										<?php
									}
									elseif ($_GET['added'] === 'exists'){
										?>
										<div class="alert alert-dark-danger alert-dismissible fade show">
											<button type="button" class="close" data-dismiss="alert">×</button>
											Group name already exits, please try another name.
										</div>
										<?php
									}
									else {
										?>
										<div class="alert alert-dark-danger alert-dismissible fade show">
											<button type="button" class="close" data-dismiss="alert">×</button>
											Cannot add group now.
										</div>
										<?php
									}
								}
								if (isset($_GET['updated'])){
									if ($_GET['updated'] === "true"){
										?>
										<div class="alert alert-dark-success alert-dismissible fade show">
											<button type="button" class="close" data-dismiss="alert">×</button>
											Group successfully updated.
										</div>
										<?php
									}
									elseif ($_GET['updated'] === 'exists'){
										?>
										<div class="alert alert-dark-danger alert-dismissible fade show">
											<button type="button" class="close" data-dismiss="alert">×</button>
											Group name already exits, please try another name.
										</div>
										<?php
									}
									else {
										?>
										<div class="alert alert-dark-danger alert-dismissible fade show">
											<button type="button" class="close" data-dismiss="alert">×</button>
											Cannot update group now.
										</div>
										<?php
									}
								}
								?>
								<form id="create-contact-group" action="/admin/<?=isset($single) ? 'updatecontact_group' : 'addcontact_group'?>" method="POST" class="col-md mb-4">
									<div class="form-group">
										<div class="input-group">
											<?php
											if (isset($single)){
												?>
												<input type="hidden" name="id" value="<?=$single->gid?>">
												<?php
											}
											?>
											<input type="text" class="form-control" name="group_name" value="<?=isset($single) ? $single->group_name : ''?>" placeholder="Group Name ...">
											<span class="input-group-append">
								              <?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'addGroup')): ?>
												<button class="btn btn-primary waves-effect" type="submit"><?=isset($single) ? 'Update group' : 'Save group'?></button>
												<?php endif ?>
											</span>
											<div class="clearfix"></div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-sm-12">
								<div class="card">
									<div class="card-datatable table-responsive">
										<table class="contact-group-table table table-striped table-bordered">
											<thead>
												<tr>
													<th>Group ID</th>
													<th>Group Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
												foreach ($groups as $g){
													?>
													<tr>
														<td><?=$g['gid']?></td>
														<td><?=$g['group_name']?></td>
														<td>
															<a href="<?=base_url()?>/admin/manage_group?id=<?=$g["gid"]?>" class="btn icon-btn btn-sm btn-outline-primary">
																<span class="far fa-edit"></span>
															</a>
															<a href="<?=base_url()?>admin/manage_group?did=<?=$g["gid"]?>" class="btn icon-btn btn-sm btn-outline-danger">
																<span class="fas fa-trash-alt"></span>
															</a>
														</td>
													</tr>
													<?php
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- [ Content End ] -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
>>>>>>> 15ed708ebd941b08a983d7094391ac8cc04800e1
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
<<<<<<< HEAD
</div>
=======
	<!-- [ Layout wrapper] End -->
	<!-- JS Files -->
	<?php $this->load->view('admin/common/js'); ?>
</body>
</html>
>>>>>>> 15ed708ebd941b08a983d7094391ac8cc04800e1
