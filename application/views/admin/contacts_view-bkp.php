<div class="row">
	<!-- Display status message -->
	<div class="col-sm-12">
		<div class="row">
			<div class="col-xs-12" id="import_response"></div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card">
			<h6 class="card-header">My Contacts Lists
				<div class="pull-right">
					<div class="btn-group all_action_btn" id="all_action_btn" style="display:none;">
						<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Options
						</button>
						<div class="dropdown-menu text-center">
							<a href="javascript:;" id="do_not_send" style="cursor:pointer;" class="dropdown-item">DO NOT SEND LIST</a>
							<a href="javascript:;" id="delete_contact" style="cursor:pointer;" class="dropdown-item">DELETE CONTACT</a>
							<a href="javascript:;" id="activate_contact" style="cursor:pointer;" class="dropdown-item">ACTIVATE CONTACT</a>
						</div>
					</div>&nbsp;
					<a class="btn btn-primary btn-sm"><i class="ion ion-ios-cloud-upload text-white"></i> Import Contacts</a>&nbsp;
					<a href="<?base_url()?>'admin/add_contact" class="btn btn-primary btn-sm"><i class="ion ion-ios-add-circle text-white"></i> Add Contact</a>
				</div>
			</h6>
			
			<div class="card-datatable table-responsive">
				<table class="datatables-demo table table-striped table-bordered">
					<thead>
						<tr>
							<th><input name="select_all" id="select-all" type="checkbox" /></th>
							<th>Name</th>
							<th>Number</th>
							<th>Email</th>
							<th>Group</th>
							<th>Created By</th>
							<th>Status</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($contacts as $c){ ?>
							<tr class="odd gradeX">
								<td><input type="checkbox" id="selected_contact" value="<?=$c['id'];?>" /></td>
								<td><?=$c['name'];?></td>
								<td><?=$c['number'];?></td>
								<td><?=$c['email'];?></td>
								<td><?=$c['group_id'];?></td>
								<td><?=$c['created_by'];?></td>
								<td><?=$c['msg_status'];?></td>
								<td class="text-center">
									<div class="btn-group">
										<button type="button" class="btn btn-info btn-sm dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Options </button>
										<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start">
											<button class="dropdown-item">Delete Permissions</button>
										</div>
									</div>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('admin/common/contact_modal');?>
