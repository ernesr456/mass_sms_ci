<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<style type="text/css">
    .iti--allow-dropdown{
        width: 100% !important;
    }

    #phonenumber{
        padding-left: 52px !important;
        .iti--allow-dropdown {
            width: 100% !important;
        }
    }
</style>
<?php $this->load->view('admin/common/css'); ?>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="javascript:;" onclick="window.history.back()" class="btn btn-primary">Back</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="d-flex justify-content-end align-content-end">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form class="<?=isset($single) ? 'update_contact' : 'add_contact'?>" id="add-contact-form" method="POST">
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label class="form-label">Name</label>
                                                    <input type="hidden" name="id" value="<?php if(isset($_GET['id'])){echo$_GET['id'];} ?>">
                                                    <input type="text" name="name" value="<?=isset($single) ? $single->name : ''?>" class="form-control" placeholder="Name">
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="form-label">Group</label>
                                                    <select class="custom-select" name="group_id">
                                                        <option value="">Select group</option>
                                                        <?php
                                                        foreach ($groups as $i=>$g){
                                                            ?>
                                                            <option value="<?=$g['gid']?>" <?=(isset($single) && $single->group_id === $g['gid']) ? 'selected' : ''?>><?=$g['group_name']?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label class="form-label">Number</label>
                                                    <div class="input-group">
                                                        <input class="form-control" id="phonenumber" name="number" type="tel" value="<?=isset($single) ? $single->number : ''?>">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label class="form-label">Email</label>
                                                    <input type="email" name="email" value="<?=isset($single) ? $single->email : ''?>" class="form-control" placeholder="Email">
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                                    <input type="checkbox" name="cstatus" class="custom-control-input" <?=isset($single) ? $single->cstatus === "1" ? 'checked' : '' : 'checked'?>>
                                                    <span class="custom-control-label">Status</span>
                                                </label>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <button type="submit" id="create_contact" class="btn btn-primary"><?=isset($single) ? 'Update' : 'Save'?></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $this->load->view('admin/contacts/contact_modal');?>
                        <!-- [ Content End ] -->
                    </div>
                    <!-- [ Layout footer ] -->
                    <?php $this->load->view('admin/common/footer'); ?>
                </div>
            </div>
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->
    <!-- JS Files -->
    <?php $this->load->view('admin/common/js'); ?>
</body>
</html>
