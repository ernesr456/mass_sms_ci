<div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-white">
	<!-- Brand demo (see assets/css/demo/demo.css) -->
	<div class="app-brand demo">
                    <span class="app-brand-logo demo">
<!--                        <img src="assets/img/logo.png" alt="Brand Logo" class="img-fluid">-->
                    </span>
		<a href="<?=base_url();?>admin" class="app-brand-text demo sidenav-text font-weight-normal ml-2"><?=$this->config->item('site_name')?></a>
		<a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
			<i class="ion ion-md-menu align-middle"></i>
		</a>
	</div>

	<div class="sidenav-divider mt-0"></div>
	<!-- Links -->
	<ul class="sidenav-inner py-1">
		<!-- Dashboards -->
		<li class="sidenav-item <?=($page=='dashboard')?'active':'';?>"
			<?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modDashboard'])? : 'hidden' ) : '' ?>>
			<a href="<?=base_url();?>admin" class="sidenav-link">
				<i class="sidenav-icon feather icon-home"></i>
				<div>Dashboard</div>
			</a>
		</li>
		<li class="sidenav-item <?=($page=='Campaigns')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modCampaign'])? (json_decode($authPermissionID,true)['modCampaign']===true? '':'hidden'): 'hidden') : ''?>>
			<a href="<?=base_url();?>campaign/campaign" class="sidenav-link">
				<i class="sidenav-icon feather icon-share-2"></i>
				<div>Campaigns</div>
			</a>
		</li>
		<li class="sidenav-item <?=($page=='Conversation' || $page=='Message Template')?'open':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modConversation'])? (json_decode($authPermissionID,true)['modConversation']===true? '':'hidden'): 'hidden') : ''?>>
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon feather icon-message-circle"></i>
				<div>Conversation</div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='Conversation')?'active':'';?>">
					<a href="<?=base_url();?>admin/conversation" class="sidenav-link">
						<div>Conversation</div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='Message Template')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modTemplate'])? (json_decode($authPermissionID,true)['modTemplate']===true? '':'hidden'): 'hidden') : ''?>>
					<a href="<?=base_url();?>admin/template" class="sidenav-link">
						<div>Templates</div>
					</a>
				</li>
			</ul>
		</li>
		<li class="sidenav-item <?=($page=='email')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modEmail'])? (json_decode($authPermissionID,true)['modEmail']===true? '':'hidden'): 'hidden') : ''?>>
			<a href="<?=base_url();?>email" class="sidenav-link">
				<i class="sidenav-icon feather icon-mail"></i>
				<div>Email</div>
			</a>
		</li>
			<li class="sidenav-item <?=($page=='users')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modUser'])? : 'hidden' ) : '' ?>>
				<a href="<?=base_url();?>admin/users" class="sidenav-link">
					<i class="sidenav-icon feather icon-users"></i>
					<div><?= ($this->session->userdata('user_group') == 1) ? "Users" : "Volunteers"; ?></div>
				</a>
			</li>
			<!-- <li class="sidenav-item <?=($page=='users')?'open':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modUser'])? (json_decode($authPermissionID,true)['modUser']===true? '':'hidden'): 'hidden') : ''?>>
				<a href="javascript:" class="sidenav-link sidenav-toggle">
					<i class="sidenav-icon feather icon-users"></i>
					<div>Users</div>
				</a>
				<ul class="sidenav-menu">
					<?php if ($this->session->userdata('role_slug')=='administrator'&&$this->session->userdata('is_logged_in')) { ?>
					<li class="sidenav-item <?=($this->input->server('QUERY_STRING')=='administrator')?'active':'';?>">
						<a href="<?=base_url();?>admin/users/administrator" class="sidenav-link">
							<div>Administrators</div>
						</a>
					</li>
					<li class="sidenav-item <?=($this->input->server('QUERY_STRING')=='client')?'active':'';?>">
						<a href="<?=base_url();?>admin/users/client " class="sidenav-link">
							<div>Clients</div>
						</a>
					</li>
					<?php } ?>
					
					<?php if ($this->session->userdata('role_slug')=='administrator'|| $this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
					<li class="sidenav-item <?=($this->input->server('QUERY_STRING')=='volunteers')?'active':'';?>">
						<a href="<?=base_url();?>admin/users/volunteers" class="sidenav-link">
							<div>Volunteers</div>
						</a>
					</li>
					<?php } ?>
				</ul>
			</li> -->
		<li class="sidenav-item <?=($page=='contacts' || $page=='Contacts Group' || $page=='email_template' ||$page=='create_email_template')?'open active':'';?>"
			<?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modContacts'])||isset(json_decode($authPermissionID,true)['modGroup']))? '' : 'hidden' : '' ?>>
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon feather icon-phone"></i>
				<div>Contacts</div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='contacts')?'active':'';?>">
					<a href="<?=base_url();?>admin/contacts" class="sidenav-link">
						<div>Contacts</div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='Contacts Group')?'active':'';?>">
					<a href="<?=base_url();?>admin/manage_group" class="sidenav-link">
						<div>Manage Groups</div>
					</a>
				</li>
			</ul>
		</li>
		
		<?php if ($this->session->userdata('role_slug')=='administrator'||$this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
			<li class="sidenav-item <?=($page=='Permission')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modPermission'])? (json_decode($authPermissionID,true)['modPermission']===true? '':'hidden'): 'hidden') : ''?>>
				<a href="<?=base_url();?>permission/permission" class="sidenav-link">
					<i class="sidenav-icon feather icon-lock"></i>
					<div>Permission</div>
				</a>
			</li>

<!-- 			<li class="sidenav-item <?=($page=='permissions'||$page=='create_permissions'||$page=='list_permission'||$page=='permissions_2'||$page=='permissions_3')?'open':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modPermission'])? (json_decode($authPermissionID,true)['modPermission']===true? '':'hidden'): 'hidden') : ''?>>
				<a href="javascript:" class="sidenav-link sidenav-toggle">
					<i class="sidenav-icon feather icon-lock"></i>
					<div>Permissions</div>
				</a>
				<ul class="sidenav-menu">
					<li class="sidenav-item <?=($page=='permissions_3')?'active':'';?>">
						<a href="<?=base_url();?>permission/permission" class="sidenav-link">
							<div>Permissions</div>
						</a>
					</li>
					<li class="sidenav-item <?=($page=='create_permissions'||$page=='list_permission')?'open':'';?>">
						<a href="javascript:" class="sidenav-link sidenav-toggle">
							<div>Permissions</div>
						</a>
						<ul class="sidenav-menu">
							<li class="sidenav-item <?=($page=='create_permissions')?'active':'';?>">
								<a href="<?=base_url();?>permission/add_permission" class="sidenav-link">
									<div>Create Permission</div>
								</a>
							</li>
							<li class="sidenav-item <?=($page=='list_permission')?'active':'';?>">
								<a href="<?=base_url();?>permission/list_permission" class="sidenav-link">
									<div>Permission Lists</div>
								</a>
							</li>
						</ul>
					</li>

					<li class="sidenav-item <?=($page=='permissions_3')?'active':'';?>">
						<a href="<?=base_url();?>admin/permissions?role=Volunteers&type=3" class="sidenav-link">
							<div>Volunteers Permissions</div>
						</a>
					</li>
				</ul>
			</li> -->
		<?php } ?>
		<?php if ($this->session->userdata('role_slug')=='administrator'||$this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
		<!-- <li class="sidenav-item <?=($page=='Package Pricing'||$page=='Manage Package Pricing'||$page=='transaction')?'open':'';?>">
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon fas fa-money-check"></i>
				<div><?= $this->session->userdata('role_slug') === 'administrator' ? 'Payments' : 'Payments'  ?></div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='Manage Package Pricing'||$page=='Package Pricing')?'active':'';?>">
					<a href="<?=base_url();?>payments/pricing" class="sidenav-link">
						<div><?= $this->session->userdata('role_slug') === 'administrator' ? 'Manage Plan' : 'Pricing'  ?></div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='transaction')?'active':'';?>">
					<a href="<?=base_url();?>admin/transaction" class="sidenav-link">
						<div><?= $this->session->userdata('role_slug') === 'administrator' ? 'All Transaction' : 'My Transaction'  ?></div>
					</a>
				</li>			
			</ul>
		</li>
		 -->
		<?php } ?>
		<?php if ($this->session->userdata('user_group')!=3) { ?>
			<li class="sidenav-item <?=($page=='credits'||$page=='Package Pricing'||$page=='site_settings'||$page=='Manage Package Pricing')?'open':'';?>">
				<a href="javascript:" class="sidenav-link sidenav-toggle">
					<i class="sidenav-icon feather icon-settings"></i>
					<div><?= $this->session->userdata('role_slug') === 'administrator' ? 'Settings' : 'Settings'  ?></div>
				</a>
				<ul class="sidenav-menu">
					<li class="sidenav-item <?=($page=='Package Pricing'||$page=='Manage Package Pricing'||$page=='transaction')?'active':'';?>">
						<a href="<?=base_url();?>payments/pricing" class="sidenav-link">
							<i class="sidenav-icon fas fa-box-open"></i>
							<div>Package</div>
						</a>
					</li>

					<li class="sidenav-item <?=($page=='credits')?'active':'';?>">
						<a href="<?=base_url();?>payments/credits" class="sidenav-link">
							<i class="sidenav-icon fas fa-credit-card"></i>
							<div>Credits</div>
						</a>
					</li>	
					<li class="sidenav-item <?=($page=='site_settings')?'active':'';?>" <?= $this->session->userdata('role_slug') === 'administrator' ? '' : 'hidden'  ?>>
						<a href="<?=base_url();?>admin/settings" class="sidenav-link">
							<i class="sidenav-icon ion ion-md-settings"></i>
							<div>API Settings</div>
						</a>
					</li>		
				</ul>
			</li>
		<?php } ?>
	</ul>
</div>
