<div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-white">
	<!-- Brand demo (see assets/css/demo/demo.css) -->
	<div class="app-brand demo">
		<span class="app-brand-logo demo">
		</span>
		<a href="<?=base_url();?>admin" class="app-brand-text demo sidenav-text font-weight-normal ml-2"><?=$this->config->item('site_name')?></a>
		<a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
			<i class="ion ion-md-menu align-middle"></i>
		</a>
	</div>

	<div class="sidenav-divider mt-0"></div>
	<!-- Links -->
	<ul class="sidenav-inner py-1">
		<!-- Dashboards -->
		<li class="sidenav-item <?=($page=='dashboard')?'active':'';?>"
			<?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modDashboard'])? : 'hidden' ) : '' ?>>
			<a href="<?=base_url();?>dashboard" class="sidenav-link">
				<i class="sidenav-icon feather icon-home"></i>
				<div>Dashboard</div>
			</a>
		</li>
		<li class="sidenav-item <?=($page=='Campaigns')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modCampaign'])? (json_decode($authPermissionID,true)['modCampaign']===true? '':'hidden'): 'hidden') : ''?> id="step2">
			<a href="<?=base_url();?>campaign" class="sidenav-link">
				<i class="sidenav-icon feather icon-share-2"></i>
				<div>Campaigns</div>
			</a>
		</li>
		<li class="sidenav-item <?=($page=='Conversation' || $page=='Message Template')?'open':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modConversation'])||isset(json_decode($authPermissionID,true)['modTemplate'])? (json_decode($authPermissionID,true)['modConversation']===true||json_decode($authPermissionID,true)['modTemplate']===true? '':'hidden'): 'hidden') : ''?> id="step3">
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon feather icon-message-circle"></i>
				<div>Conversation</div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='Conversation')?'active':'';?>">
					<a href="<?=base_url();?>conversation" class="sidenav-link">
						<div>Conversation</div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='Message Template')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modTemplate'])? (json_decode($authPermissionID,true)['modTemplate']===true? '':'hidden'): 'hidden') : ''?>>
					<a href="<?=base_url();?>template" class="sidenav-link">
						<div>Templates</div>
					</a>
				</li>
			</ul>
		</li>
		<li class="sidenav-item <?=($page=='compose'||$page=='sent'||$page=='drafts'||$page=='trash')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modEmail'])? (json_decode($authPermissionID,true)['modEmail']===true? '':'hidden'): 'hidden') : ''?>id="step4">
			<a href="<?=base_url();?>email/compose" class="sidenav-link">
				<i class="sidenav-icon feather icon-mail"></i>
				<div>Email</div>
			</a>
		</li>
		<!-- users -->
		<li class="sidenav-item <?=($page=='users')?'open active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modUser'])? : 'hidden' ) : '' ?> id="step5">
			<a href="javascript:" class="sidenav-link sidenav-toggle">
			<i class="sidenav-icon feather icon-users"></i>
				<div><?= ($this->session->userdata('user_group') == 1) ? "Users" : "Volunteers"; ?></div>
			</a>
			<ul class="sidenav-menu">

				<li class="sidenav-item <?=(isset($_GET['role'])&&$_GET['role']==1)?'active':'';?>" <?=($this->session->userdata('user_group')==1)? '' : 'hidden'?>>
					<a href="<?=base_url();?>admin/users?role=1" class="sidenav-link">
						<div>Administrator</div>
					</a>
				</li>
				<li class="sidenav-item <?=(isset($_GET['role'])&&$_GET['role']==2)?'active':'';?>" <?=($this->session->userdata('user_group')==1)? '' : 'hidden'?>>
					<a href="<?=base_url();?>admin/users?role=2" class="sidenav-link">
						<div>Clients</div>
					</a>
				</li>
				<li class="sidenav-item <?=(isset($_GET['role'])&&$_GET['role']==3)?'active':'';?>">
					<a href="<?=base_url();?>admin/users?role=3" class="sidenav-link">
						<div>Volunteers</div>
					</a>
				</li>
			</ul>
		</li>
		<!-- users -->

		<li class="sidenav-item <?=($page=='contacts' || $page=='Contacts Group' || $page=='email_template' ||$page=='create_email_template')?'open active':'';?>"
			<?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modContacts'])||isset(json_decode($authPermissionID,true)['modGroup']))? '' : 'hidden' : '' ?> id="step6">
			<a href="javascript:" class="sidenav-link sidenav-toggle">
				<i class="sidenav-icon feather icon-phone"></i>
				<div>Contacts</div>
			</a>
			<ul class="sidenav-menu">
				<li class="sidenav-item <?=($page=='Contacts Group')?'active':'';?>">
					<a href="<?=base_url();?>admin/manage_group" class="sidenav-link">
						<div>Manage Groups</div>
					</a>
				</li>
				<li class="sidenav-item <?=($page=='contacts')?'active':'';?>">
					<a href="<?=base_url();?>admin/contacts" class="sidenav-link">
						<div>Contacts</div>
					</a>
				</li>
			</ul>
		</li>
		
		<?php if ($this->session->userdata('role_slug')=='administrator'||$this->session->userdata('role_slug')=='client'&&$this->session->userdata('is_logged_in')) { ?>
			<li class="sidenav-item <?=($page=='Permission')?'active':'';?>" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modPermission'])? (json_decode($authPermissionID,true)['modPermission']===true? '':'hidden'): 'hidden') : ''?>>
				<a href="<?=base_url();?>permission/permission" class="sidenav-link">
					<i class="sidenav-icon feather icon-lock"></i>
					<div>Permission</div>
				</a>
			</li>
		<?php } ?>
		<?php if ($this->session->userdata('user_group')!=3) { ?>
			<li class="sidenav-item <?=($page=='credits'||$page=='Package Pricing'||$page=='site_settings'||$page=='Manage Package Pricing')?'open':'';?>">
				<a href="javascript:" class="sidenav-link sidenav-toggle">
					<i class="sidenav-icon feather icon-settings"></i>
					<div><?= $this->session->userdata('role_slug') === 'administrator' ? 'Settings' : 'Settings'  ?></div>
				</a>
				<ul class="sidenav-menu">
					<li class="sidenav-item <?=($page=='Package Pricing'||$page=='Manage Package Pricing'||$page=='transaction')?'active':'';?>">
						<a href="<?=base_url();?>payments/pricing" class="sidenav-link">
							<i class="sidenav-icon fas fa-box-open"></i>
							<div>Package</div>
						</a>
					</li>

					<li class="sidenav-item <?=($page=='credits')?'active':'';?>">
						<a href="<?=base_url();?>payments/credits" class="sidenav-link">
							<i class="sidenav-icon fas fa-credit-card"></i>
							<div>Credits</div>
						</a>
					</li>	
					<li class="sidenav-item <?=($page=='site_settings')?'active':'';?>" <?= $this->session->userdata('role_slug') === 'administrator' ? '' : 'hidden'  ?>>
						<a href="<?=base_url();?>admin/settings" class="sidenav-link">
							<i class="sidenav-icon ion ion-md-settings"></i>
							<div>API Settings</div>
						</a>
					</li>		
				</ul>
			</li>
		<?php } ?>
	</ul>
</div>
