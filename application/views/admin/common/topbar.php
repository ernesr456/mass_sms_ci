<nav class="layout-navbar navbar navbar-expand-lg align-items-lg-center bg-white container-p-x" id="layout-navbar">
    <!-- Brand demo (see assets/css/demo/demo.css) -->
    <a href="index-2.html" class="navbar-brand app-brand demo d-lg-none py-0 mr-4">
        <span class="app-brand-logo demo">
            <img src="<?=base_url();?>assets/admin/img/logo-dark.png" alt="Brand Logo" class="img-fluid">
        </span>
        <span class="app-brand-text demo font-weight-normal ml-2"><?=$this->config->item('site_name')?></span>
    </a>

    <!-- Sidenav toggle (see assets/css/demo/demo.css) -->
    <div class="layout-sidenav-toggle navbar-nav d-lg-none align-items-lg-center mr-auto">
        <a class="nav-item nav-link px-0 mr-lg-4" href="javascript:">
            <i class="ion ion-md-menu text-large align-middle"></i>
        </a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#layout-navbar-collapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse" id="layout-navbar-collapse">
        <!-- Divider -->
        <hr class="d-lg-none w-100 my-2">

        <div class="navbar-nav align-items-lg-center ml-auto">
            <div class="demo-navbar-messages nav-item dropdown mr-lg-3">
                <a class="nav-link dropdown-toggle hide-arrow" href="#" data-toggle="dropdown">
                    <i class="feather icon-mail navbar-icon align-middle"></i>
                    <span class="badge badge-success badge-dot indicator"></span>
                    <span class="d-lg-none align-middle">&nbsp; Messages</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="bg-info text-center text-white font-weight-bold p-3" id="new_message">
                        
                    </div>
                    <div class="list-group list-group-flush overflow-auto" id="list_message" style="max-height: 250px !important;">

                    </div>

                    <a href="<?php echo base_url() ?>admin/conversation" class="d-block text-center text-light small p-2 my-1">Show all messages</a>
                </div>
            </div>
            <div class="demo-navbar-user nav-item dropdown">
                <div class="nav-link" href="#">
                    <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle" id="topbar_profile_img">
                        <span class="px-1 mr-lg-2 ml-2 ml-lg-0" id="topbar_credit"></span>
                    </span>
                </div>
            </div>
            <!-- Divider -->
            <div class="nav-item d-none d-lg-block text-big font-weight-light line-height-1 opacity-25 mr-3 ml-1">|
            </div>
            <div class="demo-navbar-user nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                    <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle">
                        <!-- <img src="assets/img/avatars/1.png" alt class="d-block ui-w-30 rounded-circle"> -->
                        <span class="px-1 mr-lg-2 ml-2 ml-lg-0" id="profile_name"></span>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/profile" class="dropdown-item">
                        <i class="feather icon-user text-muted"></i>
                        My profile
                    </a>

                    <a href="/admin/logout" class="dropdown-item">
                        <i class="feather icon-power text-danger"></i>
                        Log Out
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- [ Layout navbar ( Header ) ] End -->
