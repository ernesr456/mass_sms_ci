<head>
    <title><?=$page_title;?> - My Soapbox</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Bhumlu Bootstrap admin template made using Bootstrap 4, it has tons of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="Bhumlu, bootstrap admin template, bootstrap admin panel, bootstrap 4 admin template, admin template">
    <meta name="author" content="Srthemesvilla" />
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">

            <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/fontawesome.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/ionicons.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/linearicons.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/open-iconic.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/feather.css">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap-material.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/shreerang-material.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/uikit.css">

    <!-- Libs -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-sweetalert/bootstrap-sweetalert.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/dropzone/dropzone.css">
    
    <!-- Page -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/select2/select2.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/datatables/datatables.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/dropzone/dropzone.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-maxlength/bootstrap-maxlength.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/ladda/ladda.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/growl/growl.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/toastr/toastr.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/timepicker/timepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/loading.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/build/css/intlTelInput.css">

    <input type="hidden" id="base_url" value="<?=base_url()?>">
	<link rel="stylesheet" href="<?=base_url()?>assets/build/css/intlTelInput.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/custom.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/responsive.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/introjs.css">
	<input type="hidden" id="base_url" value="<?=base_url()?>">
    <style type="text/css">
        table.dataTable.select tbody tr,
        table.dataTable thead th:first-child {
          cursor: pointer !important;
        }
    </style>
    <style type="text/css">
        .pricing-title {
            font-size: 20px;
            text-transform: uppercase;
            letter-spacing: 2px;
        }
        .pricing-table{
            font-size: 48px;
            font-family: Roboto,sans-serif;
        }
        .pricing-period{
            font-size: 16px !important;
        }
        .pricing-currency {
            display: inline-block;
            margin-top: 10px;
            margin-right: -10px;
            font-size: 20px;
            vertical-align: top;
        }
        #timer {
          font-family: Arial, sans-serif;
          font-size: 20px;
          color: #999;
          letter-spacing: -1px;
        }
        #timer span {
          font-size: 60px;
          color: #333;
          margin: 0 3px 0 15px;
        }
        #timer span:first-child {
          margin-left: 0;
        }
        .material-style #toast-container>.toast-info {
            background-color: #1E9FF2 !important;
        }
    </style>
</head>

<!-- Page Loader -->
<div class="page-loader">
    <div class="bg-primary"></div>
</div>

<?php ($this->session->userdata('is_logged_in')? '' : redirect('/login')) ?>