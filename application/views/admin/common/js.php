   <!-- Core scripts -->
<script src="<?=base_url()?>assets/admin/js/pace.js"></script>
<script src="<?=base_url()?>assets/admin/js/jquery-3.2.1.min.js"></script>
<script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
<script src="<?=base_url()?>assets/admin/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/admin/js/sidenav.js"></script>
<script src="<?=base_url()?>assets/admin/js/layout-helpers.js"></script>
<script src="<?=base_url()?>assets/admin/js/material-ripple.js"></script>

<!-- Libs -->
<script src="<?=base_url()?>assets/admin/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootbox/bootbox.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-sweetalert/bootstrap-sweetalert.js"></script>
<script src="<?=base_url()?>assets/admin/libs/validate/validate.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/ui_modals.js"></script>
<script src="<?=base_url()?>assets/admin/libs/spin/spin.js"></script>
<script src="<?=base_url()?>assets/admin/libs/ladda/ladda.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/misc_ladda.js"></script>
<script src="<?=base_url()?>assets/admin/libs/growl/growl.js"></script>
<script src="<?=base_url()?>assets/admin/libs/toastr/toastr.js"></script>
<script src="<?=base_url()?>assets/admin/libs/moment/moment.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/timepicker/timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-select/bootstrap-select.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?=base_url()?>assets/admin/libs/select2/select2.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?=base_url()?>assets/admin/js/loading.min.js"></script> 
<script src="<?=base_url()?>assets/admin/js/pages/jquery.timeago.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_general.js"></script>
<script src="<?=base_url()?>assets/admin/js/demo.js"></script>
<script src="<?=base_url()?>assets/admin/intro.js"></script>
<script>
	$(function() {
    	new PerfectScrollbar(document.getElementById('list_message'));
	});
</script>
