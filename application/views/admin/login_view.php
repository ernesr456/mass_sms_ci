<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">

<!-- CSS Header -->
<head>
	<title><?=$page_title;?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="<?=$page_description?>" />
	<meta name="keywords" content="<?=$page_keyword;?>">
	<meta name="author" content="Srthemesvilla" />
	<link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/admin/img/favicon.ico">

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

	<!-- Icon fonts -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/fontawesome.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/ionicons.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/linearicons.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/open-iconic.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/feather.css">

	<!-- Core stylesheets -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap-material.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/shreerang-material.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/uikit.css">

	<!-- Libs -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-maxlength/bootstrap-maxlength.css">
	<!-- Page -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/authentication.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/toastr/toastr.css">
	<link href="<?=base_url();?>assets/admin/libs/ladda/ladda.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/admin/css/custom.css" rel="stylesheet">
	<!-- Basic universal javascripts -->
	<script src="<?=base_url()?>assets/admin/js/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/toastr/toastr.js"></script>
	<input type="hidden" id="base_url" value="<?=base_url()?>">
</head>

<!-- Page Loader -->
<div class="page-loader">
	<div class="bg-primary"></div>
</div>

<body>
	<div class="authentication-wrapper authentication-3">
		<div class="authentication-inner">

			<!-- [ Side container ] Start -->
			<!-- Do not display the container on extra small, small and medium screens -->
			<div class="d-none d-lg-flex col-lg-8 align-items-center ui-bg-cover ui-bg-overlay-container p-5" style="background-image: url('<?=base_url()?>assets/admin/img/bg/21.jpg');">
				<div class="ui-bg-overlay bg-dark opacity-50"></div>
				<!-- [ Text ] Start -->
				<div class="w-100 text-white px-5">
					<h1 class="display-2 font-weight-bolder mb-4">JOIN <br>With Us</h1>
					<div class="text-large font-weight-light">
						As your trusted communication partner.
					</div>
				</div>
				<!-- [ Text ] End -->
			</div>
			<!-- [ Side container ] End -->

			<!-- [ Form container ] Start -->
			<div class="d-flex col-lg-4 align-items-center bg-white p-5">
				<!-- Inner container -->
				<!-- Have to add `.d-flex` to control width via `.col-*` classes -->
				<div class="d-flex col-sm-7 col-md-5 col-lg-12 px-0 px-xl-4 mx-auto">
					<div class="w-100">

						<!-- [ Logo ] Start -->
						<div class="d-flex justify-content-center align-items-center">
							<div class="ui-w-60">
								<div class="w-100 position-relative">
									<img src="<?=base_url()?>assets/admin/img/logo-dark.png" alt="Brand Logo" class="img-fluid">
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<!-- [ Logo ] End -->

						<h4 class="text-center text-lighter font-weight-normal mt-5 mb-0">Login to Your Account</h4>
						<form class="my-5 login_form" id="register-form" autocomplete="off" method="POST">
							<div class="form-group">
							<label class="form-label">Username</label>
							<input type="text" name="username" id="username1" class="form-control" autocomplete="none">
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<label class="form-label d-flex justify-content-between align-items-end">
								<span>Password</span>
								<a href="/forgot" class="d-block small"data-toggle="modal" data-target="#exampleModal">Forgot password?</a>
							</label>
							<input type="password" name="password" id="password1"class="form-control">
							<div class="clearfix"></div>
						</div>
						<div class="d-flex justify-content-between align-items-center m-0">
							<label class="custom-control custom-checkbox m-0">
								<input type="checkbox" class="custom-control-input">
								<span class="custom-control-label">Remember me</span>
							</label>
						</div>
						<br>
							<div class="d-flex justify-content-between align-items-center m-0">
								<button type="submit" id="create_account" class="btn btn-block btn-primary">Sign In</button>
							</div>
						</form>
<!-- 						<form class="my-5" id="login-form" autocomplete="off" method="POST">
							<div class="form-group">
								<label class="form-label">Username</label>
								<input type="text" name="username" id="username1" class="form-control" autocomplete="none">
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="form-label d-flex justify-content-between align-items-end">
									<span>Password</span>
									<a href="/forgot" class="d-block small"data-toggle="modal" data-target="#exampleModal">Forgot password?</a>
								</label>
								<input type="password" name="password" id="password1"class="form-control">
								<div class="clearfix"></div>
							</div>
							<div class="d-flex justify-content-between align-items-center m-0">
								<label class="custom-control custom-checkbox m-0">
									<input type="checkbox" class="custom-control-input">
									<span class="custom-control-label">Remember me</span>
								</label>
							</div>
							<br>
							<div class="d-flex justify-content-between align-items-center m-0">
								<button type="submit" class="btn btn-block btn-primary">Sign In</button>
							</div>
						</form> -->
						<br>
						<!-- [ Form ] End -->
						<div class="text-center text-muted">
							Already have an account?
							<a href="/auth/register">Sign Up</a>
						</div>
					</div>
				</div>
			</div>
			<!-- [ Form container ] End -->

		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Forgot Password</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="my-5">
						<div class="form-group" id="email_content">
							<label class="form-label">Enter your email here</label>
							<input type="email" id="recover_email" name="email" class="form-control" autocomplete="none">
							<div class="clearfix"></div>
						</div>
						<div id="verify_content" style="display: none;">
							<div class="form-group">
								<label class="form-label d-flex justify-content-between align-items-end">
								<span>Password</span>
								</label>
								<input type="password" id="password" name="password" class="form-control">
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="form-label d-flex justify-content-between align-items-end">
								<span>Confirm Password</span>
								</label>
								<input type="password" id="password2" name="password2" class="form-control">
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<label class="form-label">Verification Code</label>
								<input type="password" id="verify_key" name="verify_key" class="form-control" autocomplete="none">
								<br/>
								<button type="button" id="resend_code" class="btn btn-primary md-btn-flat">Resend email again</button>
								<div class="clearfix"></div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" onclick="forgotPassword(this)" class="btn btn-primary" id="get_key">Send</button>
					<button type="button" onclick="forgotPassword(this)" class="btn btn-primary" id="save_code" style="display: none;">Save</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- JS Files -->
	<?php //$this->load->view('admin/common/js'); ?>
	<script src="<?=base_url()?>assets/admin/js/pace.js"></script>
	<script src="<?=base_url()?>assets/admin/js/jquery-3.2.1.min.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/toastr/toastr.js"></script>
	<script src="<?=base_url()?>assets/admin/js/pages/jquery.timeago.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
	<script src="<?=base_url()?>assets/admin/js/bootstrap.js"></script>
	<script src="<?=base_url()?>assets/admin/js/sidenav.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/spin/spin.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/ladda/ladda.js"></script>
	<script src="<?=base_url()?>assets/admin/js/init/init_login.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/validate/validate.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="<?=base_url()?>assets/admin/js/layout-helpers.js"></script>
	<script src="<?=base_url()?>assets/admin/js/material-ripple.js"></script>
	<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
</body>
</html>