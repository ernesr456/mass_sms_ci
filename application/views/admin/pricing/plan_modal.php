<?php if($this->session->userdata('user_group')==1): ?>
<!-- Add Contact -->
<div class="modal fade" id="planModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" id="add_package" class="add_pricing" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather ion-ios-add-circle"></i> <span id="modal_title">Add New Package Pricing</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="validation-form">
                        <div class="form-group">
                            <label class="form-label">Pricing Name</label>
                            <input type="hidden" class="form-control" id="id">
                            <input type="text" class="form-control" name="pricing_name" id="pricing_name" placeholder="Pricing Name">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Price</label>

                                <input class="form-control" type="text" name="currency-field" id="currency-field" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" value="" data-type="currency" placeholder="Enter price here">
                            <!-- <input type="text" id="text-mask-number" name="price" class="form-control pricing" placeholder="$"> -->
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="dashboard" name="permission[]" value="modDashboard" class="custom-control-input" checked>
                                            <span class="custom-control-label">Dashboard</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="campaign" name="permission[]" value="modCampaign" class="custom-control-input" checked>
                                            <span class="custom-control-label">Campaign</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="conversation" name="permission[]" value="modConversation" class="custom-control-input" checked>
                                            <span class="custom-control-label">Conversation</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="template" name="permission[]" value="modTemplate" class="custom-control-input" checked>
                                            <span class="custom-control-label">Message Template</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="email" name="permission[]" value="modEmail" class="custom-control-input" checked>
                                            <span class="custom-control-label">Email</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="contacts" name="permission[]" value="modContacts" class="custom-control-input" checked>
                                            <span class="custom-control-label">Contacts</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="group_contacts" name="permission[]" value="modGroup" class="custom-control-input" checked>
                                            <span class="custom-control-label">Group Contacts</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="user" name="permission[]" value="modUser"  class="custom-control-input" checked>
                                            <span class="custom-control-label">Volunteer</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 custom-input">
                                        <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                            <input type="checkbox" id="volunteer_permission" name="permission[]" value="modPermission" class="custom-control-input" checked>
                                            <span class="custom-control-label">Volunteer Permission</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="btn_pricing" class="btn btn-primary">Save</button>
                    </div>
            </form>      
        </div>
    </div>
</div>
<?php endif; ?>

<!-- Add Contact -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" class="add_contact" method="POST" id="paymentModalForm">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-shopping-cart"></i> Checkout</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <b>Package Name: </b>
                        </div>
                        <div class="form-group col-md-8">
                            <span id="package_name"></span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <b>Price: </b>
                        </div>
                        <div class="form-group col-md-8">
                            <input type="hidden" id="package_amount">
                            <input type="hidden" id="credit_id">
                            <span id="credit_price"></span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div id="paypal-button"></div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </form>      
        </div>
    </div>
</div>