<!-- Add User -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="register" id="register-form" autocomplete="off" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-user-plus"></i> <span id="modal_titles">Add New <?= ($this->session->userdata('user_group') == 1) ? "User" : "Volunteer"; ?></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                    <div class="form-group col-md-12">
                            <label class="form-label">Full Name</label>
                            <input type="hidden" name="user_id">
                            <input type="hidden" name="created_by">
                            <input style ="text-transform: capitalize;" type="text" name="name" class="form-control" autocomplete="none">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Username</label>
                            <input type="text" name="username" class="form-control charDashOnly" autocomplete="none">
                            <input type="hidden" name="old_username">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Email</label>
                            <input type="email" name="email" class="form-control" autocomplete="none">
                            <input type="hidden" name="old_email">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>Password</span></label>
                            <input type="password" name="password" class="form-control">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>Confirm Password</span></label>
                            <input type="password" name="confirm_password" class="form-control">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>User Role</span></label>
                            <select class="custom-select" name="user_group">
                                <option value="1" <?=(isset($_GET['role'])&&$_GET['role']==1)?'selected':'disabled';?>>Administrator</option>
                                <option value="2" <?=(isset($_GET['role'])&&$_GET['role']==2)?'selected':'disabled';?>>Client</option>
                                <option value="3" <?=(isset($_GET['role'])&&$_GET['role']==3)?'selected':'disabled';?>>Volunteer</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group custom-input">
                                <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                    <input type="checkbox" name="cstatus" class="custom-control-input" checked>
                                    <span class="custom-control-label">Status</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" onclick="addUser()" id="addUserButton" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>

<div class="modal fade" id="update_user_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="update_user" id="update-form" autocomplete="off" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-edit"></i> <span id="modal_titles">Edit <?= ($this->session->userdata('user_group') == 1) ? "User" : "Volunteer"; ?></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Full Name</label>
                            <input id="id" type="hidden" name="user_id">
                            <input id="update_created_by" type="hidden" name="created_by">
                            <input style ="text-transform: capitalize;" id="update_name" type="text" name="name" class="form-control" autocomplete="none">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Username</label>
                            <input type="text" id="update_username" name="username" class="form-control charDashOnly" autocomplete="none">
                            <input type="hidden" id="update_old_username" name="old_username">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Email</label>
                            <input type="email" id="update_email" name="email" class="form-control" autocomplete="none">
                            <input type="hidden" id="update_old_email" name="old_email">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>Password</span></label>
                            <input type="password" id="password" name="password" class="form-control">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>Confirm Password</span></label>
                            <input type="password" name="confirm_password" class="form-control">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>User Role</span></label>
                            <select class="custom-select" id="update_group_id" name="user_group">
                                <option value="">Select User Type</option>
                                <option value="1">Administrator</option>
                                <option value="2">Client</option>
                                <option value="3">Volunteer</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group custom-input">
                                <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                    <input type="checkbox" name="cstatus" class="custom-control-input" checked>
                                    <span class="custom-control-label">Status</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="updateBtn" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>