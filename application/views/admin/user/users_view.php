<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modUser'])? (json_decode($authPermissionID,true)['modUser']===true? '':show_404()): show_404()) : ''?>
    <?= ($_GET['role']!=3&&$this->session->userdata('user_group')!=1)? show_404() : ''?>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->

                        <div class="card">
                            <h6 class="card-header"> <?=(isset($_GET['role']))?(($_GET['role']==1)?'Administrators':(($_GET['role']==2)?'Clients':(($_GET['role']==3)?'Volunteers':'Users'))):'Users';?>
                            <a data-target="#addUser" data-toggle="modal" href="javascript:;" class="btn btn-info btn-sm pull-right"><i class="ion ion-ios-add-circle text-white"></i> Add <?= ($this->session->userdata('user_group') == 1) ? "User" : "Volunteer"; ?></a></h6>
                            <!-- Controls -->
                            <div class="media flex-wrap align-items-center py-1 px-2">
                                <div class="media-body d-flex flex-wrap flex-basis-100 flex-basis-sm-auto">
                                    <button onclick="reloadUsers()" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Refresh">
                                        <i class="ion ion-md-refresh"></i>
                                    </button>
                                    <button onclick="activateSelectedUsers()" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Activate">
                                        <i class="ion ion-md-checkmark"></i>
                                    </button>
                                    <button onclick="deactivateSelectedUsers()" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Deactivate">
                                        <i class="ion ion-md-close"></i>
                                    </button>
                                    <button onclick="deleteSelectedUsers()" type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Delete">
                                        <i class="ion ion-md-trash"></i>
                                    </button>
                                    <span class="filter-text">Filter Options: </span>
                                    <select onchange="reloadUsers()" class="custom-select filter-input" name="created_by" id="created_by">
                                        <option value="">Created by All Users</option>
                                        <option value="<?=$this->session->userdata('user_id'); ?>">Created by Me</option>
                                        <optgroup label="Created By:">
                                            <?php foreach($user_list as $u){ ?>
                                                <option value="<?=$u['id'];?>"><?=$u['name'];?></option>
                                            <?php } ?>
                                        </optgroup>
                                    </select>
                                    <select onchange="reloadUsers()" class="custom-select filter-input" name="by_status" id="by_status">
                                        <option value="all_status" selected>All Status</option>
                                        <option value="0">Active</option>
                                        <option value="1">Deactivated</option>
                                        <option value="2">Deleted</option>
                                    </select>
                                </div>
                            </div>
                            <hr class="border-light m-0">
                            <div class="card-datatable">
                                <div class="tbl-responsive">
                                    <table class="datatables-demo table table-striped table-bordered" id="view_users">
                                        <thead>
                                            <tr>
                                                <th width="5%">
                                                    <div class="message-checkbox mr-1">
                                                        <label class="custom-control custom-checkbox">
                                                            <input onclick="selectAllUsers(1)" id="select-btn" type="checkbox" class="custom-control-input usersID" name="usersID[]">
                                                            <span class="custom-control-label"></span>
                                                        </label>
                                                    </div>
                                                </th>
                                                <th>Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Created By</th>
                                                <th>Status</th>
                                                <th class="center" width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <input type="hidden" id="user_role" value="<?=isset($_GET['role'])?$_GET['role']:0;?>">
                                </div>
                            </div>
                        </div>
                        <!-- [ Content End ] -->
                    </div>
                    <?php $this->load->view('admin/user/users_modal');?>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>

<script src="<?=base_url()?>assets/admin/libs/select2/select2.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-select/bootstrap-select.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/pages_chat.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_user.js"></script>
<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
</body>
</html>
