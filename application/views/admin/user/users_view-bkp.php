<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modUser'])? (json_decode($authPermissionID,true)['modUser']===true? '':show_404()): show_404()) : ''?>

<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
                                <div class="col-md">
                                    <div class="card text-center">
                                        <div class="col-md-12">
                                            <br>
                                            <div class="pull-right">
                                                <a data-target="#addUser" data-toggle="modal" href="javascript:;" class="btn btn-info btn-sm"><i class="ion ion-ios-add-circle text-white"></i> Add <?= ($this->session->userdata('user_group') == 1) ? "User" : "Volunteer"; ?></a>
                                                <!-- <a onclick="addContactModal()" href="<?=base_url()?>admin/add_contact" class="btn btn-info btn-sm"><i class="ion ion-ios-add-circle text-white"></i> Add Contact</a> -->
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card-header">
                                                <ul class="nav nav-tabs card-header-tabs nav-responsive-md">
                                                    <?php if($this->session->userdata('user_group')==1): ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link active show" id="select_role" data-toggle="tab" data-id="1" href="#navs-wc-home">Administrators</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link show" id="select_role" data-toggle="tab" data-id="2" href="#navs-wc-profile">Clients</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link show" id="select_role" data-toggle="tab" data-id="3" href="#navs-wc-profile">Volunteers</a>
                                                    </li>
                                                    <?php endif; ?>
                                                    <?php if($this->session->userdata('user_group')!=1): ?>
                                                    <li class="nav-item">
                                                        <a class="nav-link show" data-toggle="tab" href="#navs-wc-profile">Volunteers</a>
                                                    </li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tab-content">
                                            <div class="card-datatable table-responsive">
                                                <table class="datatables-demo table table-striped table-bordered" id="view_user">
                                                    <thead>
                                                        <tr>
                                                            <th width="5%">
                                                            <input name="select_all" value="1" id="select-all" type="checkbox" />
                                                            </th>
                                                            <th>Name</th>
                                                            <th>Username</th>
                                                            <th>Role</th>
                                                            <th>Created By</th>
                                                            <th width="5%">Status</th>
                                                            <th class="center" width="5%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $this->load->view('admin/user/users_modal');?>
                                <!-- Staustic card 3 Start -->
                            </div>
                        </div>
                        <!-- [ Content End ] -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>

<script src="<?=base_url()?>assets/admin/libs/select2/select2.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-select/bootstrap-select.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/pages_chat.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_user.js"></script>
<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
</body>
</html>
