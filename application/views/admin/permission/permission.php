<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>

<head>
    <style type="text/css">
        .list-group-item.activechat{
            background-color: #716aca;
            color: #ffff !important;
        }
        .material-style .chat-contacts .list-group-item.activechat .chat-status{
            color: #ffff !important;
        }
        .material-style .chat-contacts .list-group-item.activechat .badge-outline-success{
            color: #ffff !important;
        }
        .chat-contacts .list-group-item.activechat {
            font-weight: bold;
        }
        .container-p-y:not([class^="pb-"]):not([class*=" pb-"]) {
            padding: 0rem !important;
        }
        .material-style .chat-contacts .list-group-item.activechat .badge-outline-success{
            color: #ffff !important;
        }
    </style>
    <style type="text/css">
        #overlay{   
        position: fixed;
        top: 0;
        z-index: 100;
        width: 100%;
        height:100%;
        display: none;
        background: rgba(0,0,0,0.6);
        }
        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;  
        }
        .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #2e93e6 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }
        @keyframes sp-anime {
            100% { 
                transform: rotate(360deg); 
            }
        }
        .is-hide{
            display:none;
        }

        .material-style .dropzone{
            border: none !important;
        }
        .dropzone{
            padding: 0 0 0 0 !important;
        }
        .dz-image-preview{
            margin: 15px !important;
        }
        .ui-chat img{
            margin: 0px !important;
        }
        .mb-3{
            margin-bottom: 0 !important;
        }
        .note.needsclick{
            display: none;
        }
        .btn:not(:disabled):not(.disabled),#button_msg{
            max-height: 50px !important;
        }
        .card-body {
            padding: 0.9rem !important;
        }
        .dz-image-preview {
            margin: 5px !important;
        }
        a[itemprop="contentUrl"] img{
            width:auto !important;
            max-height: 150px;
            max-width: 200px;
        }
    </style>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/chat.css">
</head>
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modConversation'])? (json_decode($authPermissionID,true)['modConversation']===true? '':show_404()): show_404()) : ''?>
    <!-- Modal template -->
    <div class="modal fade" id="show_all_contact_modal">
        <div class="modal-dialog modal-lg">
            <form class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">All Contact
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">
                    <table>
                        <thead>
                            
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
  <div class="modal fade" id="show_templates">
    <div class="modal-dialog modal-lg">
        <form class="modal-content">
             <div class="modal-header">
                <h5 class="modal-title">Choose a template
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body">
               <table class="datatables-demo table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="15%">Name</th>
                            <th width="78%">Content</th>
                            <th width="5%">File</th>
                            <th style="text-align: center; width: 1%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($template as $t){
                        ?>
                        <tr>
                            <td><?=$t->subject?></td>
                            <td><?=$t->message?></td>
                            <td class="center" width="5%">
                                <?php if($t->filename!=""){ ?>
                                    <button type="button" id="fetch_template" data-id='<?=$t->id?>' class="btn btn-xs btn-outline-primary">Click to view</button>
                                <?php } ?>
                            </td>
                            <td class="center" width="5%">
                                <div class="form-row">
                                    <button type="button" id="select_template" data-id='<?=$t->id?>' class="btn btn-xs btn-outline-primary">Select</button>
                                </div>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <!-- [ Layout content ] Start -->
                <div class="layout-content">
                    <!-- [ content ] Start -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <div class="row" style="height: 100% !important">
                            <div class="chat-wrapper col-md-9">
                                <!-- Make card full height of `.chat-wrapper` -->
                                <div class="card flex-grow-1 position-relative overflow-hidden">
                                    <!-- Make row full height of `.card` -->
                                    <div class="row no-gutters h-100">
                                        <div class="chat-sidebox col">
                                            <!-- Chat contacts header -->
                                            <div class="flex-grow-0 px-4">
                                              <div class="media align-items-center">
<!--                                                 <div class="media-body">
                                                      <input type="hidden" id="id" placeholder="Search...">
                                                      <div class="clearfix"></div>
                                                </div> -->
                                                <input type="hidden" id="id" placeholder="Search...">
                                                <input type="hidden" id="group_id" placeholder="Search...">
                                                <div class="media-body">
<!--                                                   <button type="button" data-toggle="modal" data-target="#addUser_modal" class="btn btn-sm btn-info my-3 col text-center">
                                                  <span class="fas fa-plus"></span>&nbsp;&nbsp;Add New User</button> -->
                                                </div>
                                              </div>
                                              <div class="media align-items-center">
                                                  <div class="media-body">
                                                      <input type="text" class="form-control chat-search my-3" placeholder="Search..." style="margin-top: 0rem !important;">
                                                      <div class="clearfix"></div>
                                                  </div>
                                              </div>
                                              <div class="d-flex justify-content-between">
                                                  <div class="btn-group mb-2">
                                                      <button id="msg_filter" data-value="2" class="btn btn-default btn-xs dropdown-toggle waves-effect btn-cp" type="button" data-toggle="dropdown" aria-expanded="false">Filter by status</button>
                                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 25px; left: 0px;">
                                                          <a class="dropdown-item" href="javascript:void(0)" id="message_filter" data-name="All" data-value="2">All</a>
                                                          <a class="dropdown-item" href="javascript:void(0)" id="message_filter" data-name="Unread" data-value="1">Active</a>
                                                          <a class="dropdown-item" href="javascript:void(0)" id="message_filter" data-name="Read" data-value="0">Deactive</a>
                                                      </div>
                                                  </div>
                                                  <div class="btn-group mb-2">
                                                      <button class="btn btn-default btn-xs dropdown-toggle waves-effect btn-cp" type="button" data-toggle="dropdown" aria-expanded="false">Filter by type</button>
                                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 25px; left: 0px;">
                                                          <a class="dropdown-item" href="javascript:void(0)">All</a>
                                                          <a class="dropdown-item" href="javascript:void(0)">Administrator</a>
                                                          <a class="dropdown-item" href="javascript:void(0)">Client</a>
                                                          <a class="dropdown-item" href="javascript:void(0)">Volunteer</a>
                                                      </div>
                                                  </div>
                                              </div>
                                            </div>
                                            <!-- / Chat contacts header -->
                                            <!-- Wrap `.chat-scroll` to properly position scroll area. Remove this wtapper if you don't need scroll -->
                                            <div class="flex-grow-1 position-relative">
                                                <div class="chat-contacts list-group chat-scroll">
                                                  <!-- Online -->
<!--                                                   <a href="javascript:void(0)" class="list-group-item list-group-item-action online activechat">
                                                    <div class="media-body ml-3">
                                                        Leon Wilson
                                                        <div class="chat-status small">
                                                            <span class="badge badge-dot"></span>&nbsp; Activate</div>
                                                    </div>
                                                    <label class="custom-control custom-checkbox m-0">
                                                        <input type="checkbox" class="custom-control-input">
                                                        <span class="custom-control-label"></span>
                                                    </label>
                                                  </a> -->
                                                </div>
                                                <!-- / .chat-contacts -->
                                            </div>

                                        </div>
                                        <div class="d-flex col flex-column">
                                            <!-- Chat header -->
                                            <div class="flex-grow-0 py-3 pr-4 pl-lg-4">

                                                <div class="media align-items-center">
                                                    <a href="javascript:void(0)" class="chat-sidebox-toggler d-lg-none d-block text-muted text-large px-4 mr-2">
                                                        <i class="ion ion-md-more"></i>
                                                    </a>

                                                    <div class="position-relative" id="user_badge">
<!--                                                         <span class="badge badge-dot badge-success indicator"></span>
                                                        <div class="clearfix"></div> -->
                                                    </div>
                                                    <div class="media-body pl-3">
                                                        <strong id="contact_name"></strong>
                                                        <div class="text-muted small" id="user_status">
                                                            <!-- <span id="user_status"></span> -->
                                                        </div>
                                                    </div>
                                                    <div>
                                                      <button type="button" class="btn btn-danger btn-round icon-btn mr-1" id="update_user" data-status="2" title="Delete" style="">
                                                          <i class="fas fa-trash"></i>
                                                      </button>
                                                        <div class="btn-group" id="btn-top">
                                                            <button type="button" class="btn btn-info btn-round icon-btn mr-1" id="show_all_contact" data-toggle="modal" data-target="#show_all_contact_modal" title="Contact List" style="display: none;">
                                                                <i class="ion-ios-list"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-default btn-round icon-btn" data-toggle="dropdown" aria-expanded="false" title="Message Filter">
                                                                <i class="ion ion-ios-more"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: top, left; top: 25px; left: -135px;">
                                                                <a href="javascript:" class="dropdown-item" id="update_user" data-status="0">&nbsp; Activate</a>
                                                                <a href="javascript:" class="dropdown-item" id="update_user" data-status="1">&nbsp; Deactivate</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <hr class="flex-grow-0 border-light m-0">
                                            <!-- / Chat header -->

                                            <!-- Wrap `.chat-scroll` to properly position scroll area. Remove this wtapper if you don't need scroll -->
                                            <div class="flex-grow-1 position-relative">

                                                <!-- Remove `.chat-scroll` and add `.flex-grow-1` if you don't need scroll -->
                                                <div class="chat-messages ui-chat chat-scroll p-4">
                                                  <h2>Permission</h2>
                                                    <div class="card mb-3">
                                                        <!-- Add `.with-elements` to the parent `.card-header` element -->
                                                        <div class="card-header with-elements">
                                                          <h5 class="m-0 mr-2">Dashboard</h5>
                                                            <div class="card-header-elements ml-md-auto">
                                                               <div class="card-title-elements">
                                                                  <label class="switcher switcher-success">
                                                                    <input type="checkbox" class="switcher-input" name="permission[]" value="modDashboard" id="modDashboard" checked="">
                                                                    <span class="switcher-indicator">
                                                                        <span class="switcher-yes">
                                                                            <span class="ion ion-md-checkmark"></span>
                                                                        </span>
                                                                        <span class="switcher-no">
                                                                            <span class="ion ion-md-close"></span>
                                                                        </span>
                                                                    </span>
                                                                </label>
                                                              </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-body" style="padding-left: 1.5rem !important">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="card mb-3">
                                                        <!-- Add `.with-elements` to the parent `.card-header` element -->
                                                        <div class="card-header with-elements">
                                                          <h5 class="m-0 mr-2">Campaign</h5>
                                                            <div class="card-header-elements ml-md-auto">
                                                               <div class="card-title-elements">
                                                                  <label class="switcher switcher-success">
                                                                    <input type="checkbox" class="switcher-input" name="permission[]" value="modCampaign" id="modCampaign" checked="">
                                                                    <span class="switcher-indicator">
                                                                        <span class="switcher-yes">
                                                                            <span class="ion ion-md-checkmark"></span>
                                                                        </span>
                                                                        <span class="switcher-no">
                                                                            <span class="ion ion-md-close"></span>
                                                                        </span>
                                                                    </span>
                                                                </label>
                                                              </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-body" style="padding-left: 1.5rem !important">
                                                          <label class="switcher switcher-success">
                                                            <input type="checkbox" class="switcher-input" name="permission[]" value="addCampaign"  id="addCampaign" checked="">
                                                            <span class="switcher-indicator">
                                                                <span class="switcher-yes">
                                                                    <span class="ion ion-md-checkmark"></span>
                                                                </span>
                                                                <span class="switcher-no">
                                                                    <span class="ion ion-md-close"></span>
                                                                </span>
                                                            </span>
                                                            <span class="switcher-label">Create Campaign</span>
                                                          </label>
                                                          <label class="switcher switcher-success">
                                                            <input type="checkbox" class="switcher-input" name="permission[]" value="updateCampaign"  id="updateCampaign" checked="">
                                                            <span class="switcher-indicator">
                                                                <span class="switcher-yes">
                                                                    <span class="ion ion-md-checkmark"></span>
                                                                </span>
                                                                <span class="switcher-no">
                                                                    <span class="ion ion-md-close"></span>
                                                                </span>
                                                            </span>
                                                            <span class="switcher-label">Update Campaign</span>
                                                          </label>
                                                          <label class="switcher switcher-success">
                                                            <input type="checkbox" class="switcher-input" name="permission[]" value="deleteCampaign" id="deleteCampaign" checked="">
                                                            <span class="switcher-indicator">
                                                                <span class="switcher-yes">
                                                                    <span class="ion ion-md-checkmark"></span>
                                                                </span>
                                                                <span class="switcher-no">
                                                                    <span class="ion ion-md-close"></span>
                                                                </span>
                                                            </span>
                                                            <span class="switcher-label">Delete Campaign</span>
                                                          </label>
                                                          <label class="switcher switcher-success">
                                                            <input type="checkbox" class="switcher-input" name="permission[]" value="sendCampaign"  id="sendCampaign" checked="">
                                                            <span class="switcher-indicator">
                                                                <span class="switcher-yes">
                                                                    <span class="ion ion-md-checkmark"></span>
                                                                </span>
                                                                <span class="switcher-no">
                                                                    <span class="ion ion-md-close"></span>
                                                                </span>
                                                            </span>
                                                            <span class="switcher-label">Send Text</span>
                                                          </label>
                                                        </div>
                                                    </div>
                                                  <br>
                                                  <div class="card mb-3">
                                                      <!-- Add `.with-elements` to the parent `.card-header` element -->
                                                      <div class="card-header with-elements">
                                                      <h5 class="m-0 mr-2">Conversation</h5>
                                                        <div class="card-header-elements ml-md-auto">
                                                           <div class="card-title-elements">
                                                              <label class="switcher switcher-success">
                                                                <input type="checkbox" class="switcher-input" name="permission[]" value="modConversation" id="modConversation" checked="">
                                                                <span class="switcher-indicator">
                                                                    <span class="switcher-yes">
                                                                        <span class="ion ion-md-checkmark"></span>
                                                                    </span>
                                                                    <span class="switcher-no">
                                                                        <span class="ion ion-md-close"></span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                          </div>
                                                        </div>
                                                    </div>
                                                      <div class="card-body" style="padding-left: 1.5rem !important">
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="addConversation"  id="addConversation" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Create Conversation</span>
                                                        </label>
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="sendMessage" id="sendMessage" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Send Text</span>
                                                        </label>
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input"name="permission[]" value="updateConversation" id="updateConversation" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Update Contacts</span>
                                                        </label>
                                                      </div>
                                                    </div>
                                                    <br>
                                                    <div class="card mb-3">
                                                      <!-- Add `.with-elements` to the parent `.card-header` element -->
                                                      <div class="card-header with-elements">
                                                        <h5 class="m-0 mr-2">Template</h5>
                                                        <div class="card-header-elements ml-md-auto">
                                                           <div class="card-title-elements">
                                                              <label class="switcher switcher-success">
                                                                <input type="checkbox" class="switcher-input" name="permission[]" value="modTemplate" id="modTemplate" checked="">
                                                                <span class="switcher-indicator">
                                                                    <span class="switcher-yes">
                                                                        <span class="ion ion-md-checkmark"></span>
                                                                    </span>
                                                                    <span class="switcher-no">
                                                                        <span class="ion ion-md-close"></span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                          </div>
                                                        </div>
                                                    </div>
                                                      <div class="card-body" style="padding-left: 1.5rem !important">
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="addTemplate" id="addTemplate" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Create Template</span>
                                                        </label>
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="updateTemplate" id="updateTemplate" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Update Template</span>
                                                        </label>
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="deleteTemplate" id="deleteTemplate" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Delete Template</span>
                                                        </label>
                                                      </div>
                                                    </div>
                                                    <br>
                                                    <div class="card mb-3">
                                                      <!-- Add `.with-elements` to the parent `.card-header` element -->
                                                      <div class="card-header with-elements">
                                                      <h5 class="m-0 mr-2">Volunteer</h5>
                                                        <div class="card-header-elements ml-md-auto">
                                                           <div class="card-title-elements">
                                                              <label class="switcher switcher-success">
                                                                <input type="checkbox" class="switcher-input" name="permission[]" value="modUser" id="modUser" checked="">
                                                                <span class="switcher-indicator">
                                                                    <span class="switcher-yes">
                                                                        <span class="ion ion-md-checkmark"></span>
                                                                    </span>
                                                                    <span class="switcher-no">
                                                                        <span class="ion ion-md-close"></span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                          </div>
                                                        </div>
                                                    </div>
                                                      <div class="card-body" style="padding-left: 1.5rem !important">
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="addUser" id="addUser" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Create Volunteer</span>
                                                        </label>
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="updateUser" id="updateUser" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Update Volunteer</span>
                                                        </label>
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="deleteUser" id="deleteUser" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Delete Volunteer</span>
                                                        </label>
                                                        <label class="switcher switcher-success">
                                                          <input type="checkbox" class="switcher-input" name="permission[]" value="updatePermission" id="updatePermission" checked="">
                                                          <span class="switcher-indicator">
                                                              <span class="switcher-yes">
                                                                  <span class="ion ion-md-checkmark"></span>
                                                              </span>
                                                              <span class="switcher-no">
                                                                  <span class="ion ion-md-close"></span>
                                                              </span>
                                                          </span>
                                                          <span class="switcher-label">Update Permission</span>
                                                        </label>
                                                      </div>
                                                    </div>
                                                    <br>
                                                    <div class="card mb-3">
                                                      <!-- Add `.with-elements` to the parent `.card-header` element -->
                                                      <div class="card-header with-elements">
                                                          <h5 class="m-0 mr-2">Contacts</h5>
                                                            <div class="card-header-elements ml-md-auto">
                                                               <div class="card-title-elements">
                                                                  <label class="switcher switcher-success">
                                                                    <input type="checkbox" class="switcher-input" name="permission[]" value="modContact" id="modContact" checked="">
                                                                    <span class="switcher-indicator">
                                                                        <span class="switcher-yes">
                                                                            <span class="ion ion-md-checkmark"></span>
                                                                        </span>
                                                                        <span class="switcher-no">
                                                                            <span class="ion ion-md-close"></span>
                                                                        </span>
                                                                    </span>
                                                                </label>
                                                              </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-body" style="padding-left: 1.5rem !important">
                                                            <label class="switcher switcher-success">
                                                              <input type="checkbox" class="switcher-input" name="permission[]" value="addGroup" id="addGroup" checked="">
                                                              <span class="switcher-indicator">
                                                                  <span class="switcher-yes">
                                                                      <span class="ion ion-md-checkmark"></span>
                                                                  </span>
                                                                  <span class="switcher-no">
                                                                      <span class="ion ion-md-close"></span>
                                                                  </span>
                                                              </span>
                                                              <span class="switcher-label">Create Contact</span>
                                                            </label>
                                                            <label class="switcher switcher-success">
                                                              <input type="checkbox" class="switcher-input" name="permission[]" value="addContact" id="addContact" checked="">
                                                              <span class="switcher-indicator">
                                                                  <span class="switcher-yes">
                                                                      <span class="ion ion-md-checkmark"></span>
                                                                  </span>
                                                                  <span class="switcher-no">
                                                                      <span class="ion ion-md-close"></span>
                                                                  </span>
                                                              </span>
                                                              <span class="switcher-label">Update Group</span>
                                                            </label>
                                                            <label class="switcher switcher-success">
                                                              <input type="checkbox" class="switcher-input" name="permission[]" value="deleteGroup" id="deleteGroup" checked="">
                                                              <span class="switcher-indicator">
                                                                  <span class="switcher-yes">
                                                                      <span class="ion ion-md-checkmark"></span>
                                                                  </span>
                                                                  <span class="switcher-no">
                                                                      <span class="ion ion-md-close"></span>
                                                                  </span>
                                                              </span>
                                                              <span class="switcher-label">Delete Group</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="card mb-3">
                                                      <!-- Add `.with-elements` to the parent `.card-header` element -->
                                                      <div class="card-header with-elements">
                                                          <h5 class="m-0 mr-2">Group Contacts</h5>
                                                            <div class="card-header-elements ml-md-auto">
                                                               <div class="card-title-elements">
                                                                  <label class="switcher switcher-success">
                                                                    <input type="checkbox" class="switcher-input" name="permission[]" value="modGroup" id="modGroup" checked="">
                                                                    <span class="switcher-indicator">
                                                                        <span class="switcher-yes">
                                                                            <span class="ion ion-md-checkmark"></span>
                                                                        </span>
                                                                        <span class="switcher-no">
                                                                            <span class="ion ion-md-close"></span>
                                                                        </span>
                                                                    </span>
                                                                </label>
                                                              </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-body" style="padding-left: 1.5rem !important">
                                                            <label class="switcher switcher-success">
                                                              <input type="checkbox" class="switcher-input" name="permission[]" value="addGroup" id="addGroup" checked="">
                                                              <span class="switcher-indicator">
                                                                  <span class="switcher-yes">
                                                                      <span class="ion ion-md-checkmark"></span>
                                                                  </span>
                                                                  <span class="switcher-no">
                                                                      <span class="ion ion-md-close"></span>
                                                                  </span>
                                                              </span>
                                                              <span class="switcher-label">Create Group</span>
                                                            </label>
                                                            <label class="switcher switcher-success">
                                                              <input type="checkbox" class="switcher-input" name="permission[]" value="updateGroup" id="updateGroup" checked="">
                                                              <span class="switcher-indicator">
                                                                  <span class="switcher-yes">
                                                                      <span class="ion ion-md-checkmark"></span>
                                                                  </span>
                                                                  <span class="switcher-no">
                                                                      <span class="ion ion-md-close"></span>
                                                                  </span>
                                                              </span>
                                                              <span class="switcher-label">Update Group</span>
                                                            </label>
                                                            <label class="switcher switcher-success">
                                                              <input type="checkbox" class="switcher-input" name="permission[]" value="deleteGroup" id="deleteGroup" checked="">
                                                              <span class="switcher-indicator">
                                                                  <span class="switcher-yes">
                                                                      <span class="ion ion-md-checkmark"></span>
                                                                  </span>
                                                                  <span class="switcher-no">
                                                                      <span class="ion ion-md-close"></span>
                                                                  </span>
                                                              </span>
                                                              <span class="switcher-label">Delete Group</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- / .chat-messages -->
                                            </div>
                                            <form class="send_message_conversation" method="post">
                                        </form>
                                        </div>
                                    </div>
                                    <!-- / .row -->
                                </div>
                                <!-- / .card -->

                            </div>
                            <!-- / .chat-wrapper -->
                            <div class="col-md-3">
                                <div class="d-flex justify-content-between align-items-center m-0 py-2 pr-2">
                                </div>
                                <div class="col-md-12">
                                    <h4>Campaign Assign</h4>
                                    <hr class="my-2">
                                    <div class="mb-4">
                                        <strong id="assign_texter" style="text-transform: capitalize;">Campaign: Test</strong>
                                    </div>
                                    <h4>User Information</h4>
                                    <hr class="my-2">
                                    <p id="user-name" style="text-transform: capitalize;"></p>
                                    <p id="user-username"></p>
                                    <p id="user-email"></p>
                                    <p id="user-status"></p>
                                    <p id="user-group"></p>
                                    <p id="last-activity"></p>
                                    <br>
                                    <h4>Campaign</h4>
                                    <hr class="my-2">
                                    <select class="project_id form-control" name="project" multiple style="width: 100%">
                                      <?php
                                          for($i=0;$i<count($project);$i++){
                                              if ($project[$i]->name!="") {
                                                  echo '<option value='.$project[$i]->id.'>'.$project[$i]->name.'</option>';
                                              }
                                              // print_r($project[$i]);
                                          }
                                      ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php $this->load->view('admin/permission/permission_modal');?>
                    </div>
                    <!-- [ content ] End -->
                    <?php $this->load->view('admin/common/footer'); ?>

                </div>
                <!-- [ Layout content ] Start -->
            </div>
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->
<!-- JS Files -->
<!-- Core scripts -->
<?php $this->load->view('admin/common/js'); ?>

<!-- <script src="assets/js/analytics.js"></script> -->
<script src="<?=base_url()?>assets/admin/libs/select2/select2.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-select/bootstrap-select.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/pages_chat.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_permission.js"></script>
<script src="<?=base_url()?>assets/admin/js/custom.js"></script>

</body>
</html>
