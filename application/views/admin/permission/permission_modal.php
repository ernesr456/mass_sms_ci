<!-- Add Contact -->
<!-- <div class="modal fade" id="addUser_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="register" id="register-form" autocomplete="off" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-user-plus"></i> <span id="modal_titles">Add New User</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Full Name</label>
                            <input id="id" type="hidden">
                            <input style ="text-transform: capitalize;" id="name" type="text" name="name" class="form-control" autocomplete="none">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Username</label>
                            <input type="text" id="username" name="username" class="form-control" autocomplete="none">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Email</label>
                            <input type="email" id="email" name="email" class="form-control" autocomplete="none">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>Password</span></label>
                            <input type="password" id="password" name="password" class="form-control">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label"><span>Confirm Password</span></label>
                            <input type="password" name="password2" class="form-control">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Project Assign</label>
                            <select class="project_id form-control" name="project" multiple style="width: 100%">
                                <?php
                                    for($i=0;$i<count($project);$i++){
                                        if ($project[$i]->name!="") {
                                            echo '<option value='.$project[$i]->id.'>'.$project[$i]->name.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group custom-input">
                                <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                    <input type="checkbox" name="cstatus" class="custom-control-input" checked>
                                    <span class="custom-control-label">Status</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="create_account" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div> -->