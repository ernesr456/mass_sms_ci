<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
     <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modPermission'])? (json_decode($authPermissionID,true)['modPermission']===true? '':show_404()): show_404()) : ''?>

	<!-- [ Layout wrapper ] Start -->
	<div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
				<?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
						<!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
						<!-- [ Content Start ] -->
						
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<h6 class="card-header">Permission Lists</h6>
									<div class="card-datatable table-responsive">
										<table class="advence-datatables table table-striped table-bordered">
											<thead>
												<tr class="text-center">
													<th>Permission Name</th>
													<th>Permission</th>
													<th>Created By</th>
													<th>Date Created</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody> 
												<?php foreach ($usersPermission as $i=>$u){ ?>
												<tr class="odd gradeX text-center">
													<td id="<?=$u['upid']?>"><?=$u['permission_name']?></td>
													<td class="permissionTD">
														<?php $color;$id; 
														//Dashboard Module
														strstr($u['permission'],'modDashboard')?$color='green':$color='red'; 
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Dashboard</kbd>';
                                                            //Campaign Module
                                                            strstr($u['permission'],'modCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Campaign</kbd>';
                                                            strstr($u['permission'],'addCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Campaign</kbd>';
                                                            strstr($u['permission'],'updateCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Campaign</kbd>';
                                                            strstr($u['permission'],'deleteCampaign')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Campaign</kbd>';
                                                            //Conversation Module
                                                            strstr($u['permission'],'modConversation')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Conversation</kbd>';
                                                            strstr($u['permission'],'viewMessage')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">View Message</kbd>';
                                                            strstr($u['permission'],'sendMessage')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Send Message</kbd>';
                                                            strstr($u['permission'],'deleteMessage')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Message</kbd>';
                                                            //Contacts Module
                                                            strstr($u['permission'],'modContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Contact</kbd>';
                                                            strstr($u['permission'],'addContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Contact</kbd>';
                                                            strstr($u['permission'],'updateContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Contact</kbd>';
                                                            strstr($u['permission'],'deleteContacts')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Contact</kbd>';
                                                            //Group Module
                                                            strstr($u['permission'],'modGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Group</kbd>';
                                                            strstr($u['permission'],'addGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Group</kbd>';
                                                            strstr($u['permission'],'updateGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Group</kbd>';
                                                            strstr($u['permission'],'deleteGroup')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Group</kbd>';
                                                            //User Module
                                                            strstr($u['permission'],'modUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">User</kbd>';
                                                            strstr($u['permission'],'addUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add User</kbd>';
                                                            strstr($u['permission'],'updateUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update User</kbd>';
                                                            strstr($u['permission'],'deleteUser')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete User</kbd>';
                                                            //Permission Module   
                                                            strstr($u['permission'],'modPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Permission</kbd>';
                                                            strstr($u['permission'],'createPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Create Permission</kbd>';
                                                            strstr($u['permission'],'updatePermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Permission</kbd>';
                                                            strstr($u['permission'],'deletePermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Permission</kbd>';
                                                            strstr($u['permission'],'setGroupPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Group Permission</kbd>';   
                                                            strstr($u['permission'],'setClientPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Client Permission</kbd>';
                                                            strstr($u['permission'],'removeClientPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Remove Client Permission</kbd>';
                                                            strstr($u['permission'],'setVolunteerPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Volunteer Permission</kbd>';
                                                            strstr($u['permission'],'removeVolunteerPermission')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Remove Volunteer Permission</kbd>';

                                                            //Template Module
                                                            strstr($u['permission'],'modTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Template</kbd>';
                                                            strstr($u['permission'],'addTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Template</kbd>';
                                                            strstr($u['permission'],'updateTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Template</kbd>';
                                                            strstr($u['permission'],'deleteTemplate')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Template</kbd>';

                                                            //Project Module
                                                            strstr($u['permission'],'modProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Project</kbd>';
                                                            strstr($u['permission'],'addProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Project</kbd>';
                                                            strstr($u['permission'],'updateProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Project</kbd>';
                                                            strstr($u['permission'],'deleteProject')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Project</kbd>';

                                                            //Email Module
                                                            strstr($u['permission'],'modEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Email</kbd>';
                                                            strstr($u['permission'],'sendEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Send Email</kbd>';
                                                            strstr($u['permission'],'addEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Email</kbd>';
                                                            strstr($u['permission'],'deleteEmail')?$color='green':$color='red';
                                                            echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Email</kbd>';	
														?>  
													</td>
													<td><?=ucwords($u['name'])?></td>
													<td><?=$u['created_at']?></td>
													<td class="text-center">
														<div class="btn-group">
															<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Options
															</button>
															<div class="dropdown-menu">
		                                                        <?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'updatePermission')): ?>
															   <button class="editButton dropdown-item">Edit Permissions</button>
										                       <?php endif ?>
										                      	<?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'deletePermission')): ?>
															   <button class="dropdown-item" onclick="deletePermission(<?=$u['upid']?>)">Delete Permissions</button>
   										                       <?php endif ?>
														   </div>
													   </div>
												   </td>
											   </tr>
											   <?php } ?>
										   </tbody>
									   </table>
								   </div>
							   </div>
						   </div>
					   </div>
		   <div class="modal fade" id="tdModal" tabindex="-1" role="dialog" aria-labelledby="tdModal" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
					  	<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">Full Permission Detail</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  		<span aria-hidden="true">&times;</span>
						  	</button>
					  	</div>
				  		<div class="modal-body">
							<div class="container-fluid permissionBody"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="updatePermssionModal" tabindex="-1" role="dialog" aria-labelledby="updatePermssionModal" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Update Permission</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
					  </button>
				  </div>
				  <div class="col-md-12 mt-4 text-left" style="font-size: 11px;">
					<div class="form-group row ml-2">
						<label class="col-form-label text-sm-right">Permission Name</label>
						<div class="col-sm-8">
							<input type="text" id="permissionName" class="form-control" placeholder="Permission">
							<input type="hidden" id="idPermission" class="form-control">
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="nav-tabs-left mb-4">
						<ul class="nav nav-tabs">
							<li class="nav-item">
								<a class="nav-link show active" data-toggle="tab" href="#navs-left-dashboard">Dashboard</a>
							</li>
							<li class="nav-item">
								<a class="nav-link show" data-toggle="tab" href="#navs-left-campaign">Campaigns</a>
							</li>
							<li class="nav-item">
								<a class="nav-link show" data-toggle="tab" href="#navs-left-project">Project</a>
							</li>
							<li class="nav-item">
								<a class="nav-link show" data-toggle="tab" href="#navs-left-conversation">Conversation</a>
							</li>
							<li class="nav-item">
								<a class="nav-link show" data-toggle="tab" href="#navs-left-email">Email</a>
							</li>
							<li class="nav-item">
								<a class="nav-link show" data-toggle="tab" href="#navs-left-contacts">Contacts</a>
							</li>
							<li class="nav-item">
								<a class="nav-link show" data-toggle="tab" href="#navs-left-user">User</a>
							</li>
							<li class="nav-item">
								<a class="nav-link show" data-toggle="tab" href="#navs-left-permission">Permission</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade active show" id="navs-left-dashboard">
								<div class="card-body">
									<div class="col-md-12 text-left">
										<form class="was-validated">
										   <div class="custom-control custom-checkbox mb-2">
											 <input type="checkbox" class="custom-control-input" name="permission[]" id="modDashboard" value="modDashboard" required checked>
											<label class="custom-control-label" for="modDashboard"><h5>Dashboard</h5></label>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane fade " id="navs-left-campaign">
							<div class="card-body">
								<div class="col-md-12 text-left">
									<form class="was-validated">
									  <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="modCampaign" id="modCampaign" required checked>
                                        <label class="custom-control-label" for="modCampaign"><h5>Campaign</h5></label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="addCampaign" id="addCampaign" required checked>
                                        <label class="custom-control-label" for="addCampaign">Add Campaign</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="updateCampaign" id="updateCampaign" required checked>
                                        <label class="custom-control-label" for="updateCampaign">Update Campaign</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteCampaign" id="deleteCampaign" required checked>
                                        <label class="custom-control-label
                                        " for="deleteCampaign">Delete Campaign</label>
                                    </div>
								</form>
							</div>
						</div>
					</div>
					<div class="tab-pane fade " id="navs-left-project">
						<div class="card-body">
							<div class="col-md-12 text-left">
						 	<form class="was-validated">
                                   <div class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" name="permission[]" value="modProject" id="modProject" required checked>
                                    <label class="custom-control-label" for="modProject"><h5>Project</h5></label>
                                </div>
                                <div class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" name="permission[]" value="addProject" id="addProject" required checked>
                                    <label class="custom-control-label" for="addProject">Add Project</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" name="permission[]" value="updateProject" id="updateProject" required checked>
                                    <label class="custom-control-label" for="updateProject">Update Project</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteProject" id="deleteProject" required checked>
                                    <label class="custom-control-label
                                    " for="deleteProject">Delete Project</label>
                                </div>
                            </form>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="navs-left-conversation">
					   <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-6 text-left">
                                                        <form class="was-validated">
                                                           <div class="custom-control custom-checkbox mb-2">
                                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="modConversation" id="modConversation" required checked>
                                                            <label class="custom-control-label" for="modConversation"><h5>Conversation</h5></label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox mb-2">
                                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="viewMessage" id="viewMessage" required checked>
                                                            <label class="custom-control-label" for="viewMessage">View Message</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox mb-2">
                                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="sendMessage" id="sendMessage" required checked>
                                                            <label class="custom-control-label" for="sendMessage">Send Message</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox mb-2">
                                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteMessage" id="deleteMessage" required checked>
                                                            <label class="custom-control-label" for="deleteMessage">Delete Message</label>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-6 col-xs-12 text-left">
                                                    <form class="was-validated">
                                                       <div class="custom-control custom-checkbox mb-2">
                                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="modTemplate" id="modTemplate" required checked>
                                                        <label class="custom-control-label" for="modTemplate"><h5>Template</h5></label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox mb-2">
                                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="addTemplate" id="addTemplate" required checked>
                                                        <label class="custom-control-label" for="addTemplate">Add Template</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox mb-2">
                                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="updateTemplate" id="updateTemplate" required checked>
                                                        <label class="custom-control-label" for="updateTemplate">Update Template</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox mb-2">
                                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteTemplate" id="deleteTemplate" required checked>
                                                        <label class="custom-control-label" for="deleteTemplate">Delete Template</label>
                                                    </div>
                                                </form>
                                            </div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade " id="navs-left-email">
			<div class="card-body">
                                    <div class="col-md-12 text-left">
                                        <form class="was-validated">
                                           <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="modEmail" id="modEmail" required checked>
                                            <label class="custom-control-label" for="modEmail"><h5>Email</h5></label>
                                        </div>
                                        <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="addEmail" id="addEmail" required checked>
                                            <label class="custom-control-label" for="addEmail">Add Email</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="sendEmail" id="sendEmail" required checked>
                                            <label class="custom-control-label" for="sendEmail">Send Email</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteEmail" id="deleteEmail" required checked>
                                            <label class="custom-control-label
                                            " for="deleteEmail">Delete Email</label>
                                        </div>
                                    </form>
                                </div>
		</div>
	</div>
	<div class="tab-pane fade" id="navs-left-contacts">
		<div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <form class="was-validated">
                                           <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="modContacts" id="modContacts" required checked>
                                            <label class="custom-control-label" for="modContacts"><h5>Contacts</h5></label>
                                        </div>
                                        <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="addContacts" id="addContacts" required checked>
                                            <label class="custom-control-label" for="addContacts">Add Contact</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="updateContacts" id="updateContacts" required checked>
                                            <label class="custom-control-label" for="updateContacts">Update Contact</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mb-2">
                                            <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteContacts" id="deleteContacts" required checked>
                                            <label class="custom-control-label" for="deleteContacts">Delete Contact</label>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6 col-xs-12 text-left">
                                    <form class="was-validated">
                                       <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="modGroup" id="modGroup" required checked>
                                        <label class="custom-control-label" for="modGroup"><h5>Module Group</h5></label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="addGroup" id="addGroup" required checked>
                                        <label class="custom-control-label" for="addGroup">Add Group</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="updateGroup" id="updateGroup" required checked>
                                        <label class="custom-control-label" for="updateGroup">Update Group</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-2">
                                        <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteGroup" id="deleteGroup" required checked>
                                        <label class="custom-control-label" for="deleteGroup">Delete Group</label>
                                    </div>
                                </form>
                            </div>
	</div>
</div>
</div>
<div class="tab-pane fade " id="navs-left-user">
	<div class="card-body">
	 <div class="col-md-12">
		 <form class="was-validated">
                           <div class="custom-control custom-checkbox mb-2">
                            <input type="checkbox" class="custom-control-input" name="permission[]" value="modUser" id="modUser" required checked>
                            <label class="custom-control-label" for="modUser"><h5>User</h5></label>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <input type="checkbox" class="custom-control-input" name="permission[]" value="addUser" id="addUser" required checked>
                            <label class="custom-control-label" for="addUser">Add User</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <input type="checkbox" class="custom-control-input" name="permission[]" value="updateUser" id="updateUser" required checked>
                            <label class="custom-control-label" for="updateUser">Update User</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <input type="checkbox" class="custom-control-input" name="permission[]" value="deleteUser" id="deleteUser" required checked>
                            <label class="custom-control-label" for="deleteUser">Delete User</label>
                        </div>
                    </form>
</div>
</div>
</div>
<div class="tab-pane fade" id="navs-left-permission">
	<div class="card-body">
		<div class="col-md-12">
			<form class="was-validated">
                       <div class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" name="permission[]" value="modPermission" id="modPermission" required checked>
                        <label class="custom-control-label" for="modPermission"><h5>Permission</h5></label>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12 text-left">
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="createPermission" id="createPermission" required checked>
                                <label class="custom-control-label" for="createPermission">Create Permission</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="updatePermission" id="updatePermission" required checked>
                                <label class="custom-control-label" for="updatePermission">Update Permission</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="deletePermission" id="deletePermission" required checked>
                                <label class="custom-control-label" for="deletePermission">Delete Permission</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="setGroupPermission" id="setGroupPermission" required checked>
                                <label class="custom-control-label" for="setGroupPermission">Set Permission To Group</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12  text-left">
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="setClientPermission" id="setClientPermission" required checked>
                                <label class="custom-control-label" for="setClientPermission">Set Permission To Client</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="removeClientPermission" id="removeClientPermission" required checked>
                                <label class="custom-control-label" for="removeClientPermission">Remove Permission To Client</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="setVolunteerPermission" id="setVolunteerPermission" required checked>
                                <label class="custom-control-label" for="setVolunteerPermission">Set Permission To Volunteer</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" name="permission[]" value="removeVolunteerPermission" id="removeVolunteerPermission" required checked>
                                <label class="custom-control-label" for="removeVolunteerPermission">Remove Permission To Volunteer</label>
                            </div>
                        </div>
                    </div>
		</form>
	</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal-footer">
  	<?php if(strstr($this->session->userdata('permission'),'updatePermission')): ?>
	<button type="button" class="btn btn-primary" onclick="save()">Save changes</button>
	<?php endif ?>
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

</div>

<!-- [ Content End ] -->
</div>
<!-- [ Layout footer ] -->
<?php $this->load->view('admin/common/footer'); ?>
</div>
</div>
</div>
<!-- Overlay -->
<div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/js/init/init_permission.js"></script>
</body>
</html>
