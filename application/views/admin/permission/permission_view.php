<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- 2nd row Start -->
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<h6 class="card-header"><?=$role?></h6>
			<div class="col-md-4 mt-3">
				<button type="button" class="group-permission btn btn-outline-info" id="<?=($role=='Client')?'2':'3'?>" onclick="showmodal()">Set Permission For All Group</button>
			</div>
			<div class="card-datatable table-responsive">
				<table class="advence-datatables table table-striped table-bordered">
					<thead>
					<tr class="text-center">
						<th>Full Name</th>
						<th>Username</th>
						<th>Permissions</th>
						<th>Role</th>
						<th>Actions</th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach ($users as $i=>$u){	?>

						<tr class="odd gradeX">
							<td><?=$u['fullname']?></td>
							<td><?=$u['username']?></td>
							<td style="width:250px; word-break: break-all;white-space: normal;">
								<?php if(json_decode($u['permission'],true)['modDashboard']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">Dashboard</kbd>';
								}if(json_decode($u['permission'],true)['modCampaign']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">Campaign</kbd>';
								}if(json_decode($u['permission'],true)['modConversation']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">Conversation</kbd><br>';
								}if(json_decode($u['permission'],true)['viewMessage']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">View Message</kbd>';
								}if(json_decode($u['permission'],true)['sendMessage']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">Send Message</kbd>';
								}if(json_decode($u['permission'],true)['deleteMessage']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">Delete Message</kbd><br>';
								}if(json_decode($u['permission'],true)['modUser']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">User</kbd>';
								}if(json_decode($u['permission'],true)['modPermission']==='true'){
									echo '<kbd class="ml-1" style="background-color:green;font-size:9px;">Permission</kbd>';
								}
								?></td>
							<td><?=$u['role']?></td>
							<td class="text-center">
								<div class="btn-group">
									<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Options
									</button>
									<div class="dropdown-menu">
										<button class="dropdown-item" onclick="showmodal(<?=$u['id']?>)"<?=($role==='Client')?(json_decode($authPermissionID,true)['setClientPermission']==="false")? 'style="cursor: no-drop;" disabled' : '':(json_decode($authPermissionID,true)['setVolunteerPermission']==="false")? 'style="cursor: no-drop;" disabled' : ''?>>Set Permissions</button>
										<button class="dropdown-item" onclick="userDeletePermission(<?=$u['id']?>)"
											<?=($role==='Client')?(json_decode($authPermissionID,true)['removeClientPermission']==="false")? 'style="cursor: no-drop;" disabled' : '':(json_decode($authPermissionID,true)['removeVolunteerPermission']==="false")? 'style="cursor: no-drop;" disabled' : ''?>
										>Remove Permissions</button>
									</div>
								</div>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- Staustic card 3 Start -->
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="permissionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Set Permission</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="user_id" class="form-control">
				<select class="select2-demo form-control" id="permission_name" style="width: 100%" data-select2-id="76" tabindex="-1" onchange="permission()" aria-hidden="true">
					<option value="0"  selected>Select Option</option>
					<?php foreach ($usersPermission as $i=>$u){
						echo "<option value=".$u['id'].">".$u['permission_name']."</option>";
					}?>
				</select>
				<div class="col-md-12 mt-3">
					<div class="row">
						<div class="col-md-12">
							<label><strong>Module Permission</strong></label>
						</div>
						<div class="col-md-12">
							<label class="mt-2">Dasboard</label>
							<div class="dashboard ml-3"></div>
						</div>
						<div class="col-md-12">
							<label class="mt-2">Campaign</label>
							<div class="campaign ml-3"></div>
						</div>
						<div class="col-md-12">
							<label class="mt-2">Conversation</label>
							<div class="conversation ml-3"></div>
						</div>
						<div class="col-md-12">
							<label class="mt-2">User</label>
							<div class="user ml-3"></div>
						</div>
						<div class="col-md-12">
							<label class="mt-2">Permission</label>
							<div class="permission ml-3"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="userPermission()">Save changes</button>
			</div>
		</div>
	</div>
</div>
