<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modDashboard'])? '' : redirect('profile') ) : '' ?>
    

<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <?php if ($this->session->userdata['role_slug'] === 'client'){ ?>
                            <div class="row">
                                <div class="col-xl-12 col-md-12">
                                    <h1 class="display-4">Good afternoon, <span style="font-weight: bolder;"><strong></strong></h1>
                                    <dd class="text-big mt-2">Here what's going on with your campaign in <strong>My Soapbox v.1</strong></dd>
                                </div>

                                <div class="col-xl-7 col-md-12 mt-4">
                                    <div class="col-md-12 col-sm-6 col-xs-6">
                                        <select name="campaign" id="campaign" class="form-control">
                                            <option value="0">All Campaigns</option>
                                            <?php
                                                foreach ($campaign as $c){
                                                ?>
                                            <option value="<?=$c->id?>"><?=$c->proj_name?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xl-12 mt-5">
                                        <canvas id="myChart" class="chartjs-demo" style="position: relative; height:40vh;"></canvas>
                                    </div>
                                    <div class="row">
<!--                                         <div class="col-xl-6 mt-5">
                                            <dd class="text-big mt-2" id="total_sent"></dd>
                                            <dd class="text-big mt-2" id="total_undelivered"></dd>
                                            <dd class="text-big mt-2" id="total_replies"></dd>
                                        </div> -->
<!--                                         <div class="col-xl-6 mt-5">
                                            <dd class="text-big mt-2">Account Balance</dd>
                                            <dd class="text-big mt-2">Total Credits: <?= $credit? number_format($credit->tCredits,2):0?></dd>
                                            <dd class="text-big mt-2">Balance: <?= $credit? number_format($credit->creditBal,2):0?></dd>
                                        </div> -->
                                    </div>
                                    <div class="col-xl-12 mt-5">
                                        <h5><strong>Recent Replies</strong></h5>
                                        <table class="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th>ALL</th>
                                                <th>Positive</th>
                                                <th>Negative</th>
                                                <th>Time</th>
                                            </tr>
                                            </thead>
                                            <tbody id="recent-replies">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-md-12 mt-4">
                               <div class="col-md-8">
                                        <a href="<?php base_url()?>campaign" target="_blank" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modCampaign'])? (json_decode($authPermissionID,true)['modCampaign']===true? '':'hidden'): 'hidden') : ''?>><button class="btn btn-outline-dark btn-lg waves-effect" type="button" style="padding: 15px;">CREATE NEW CAMPAIGN</button><a>
                                    </div>
                                    <div class="col-md-12 mt-4">
                                        <a href="<?php base_url()?>admin/users?role=3" target="_blank" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modUser'])? : 'hidden' ) : '' ?>>
                                            <i class="ion ion-md-add"></i>
                                            <span class="camp-text ml-1">Add Texter</span>
                                        </a>
                                    </div>
                                    <div class="col-md-12 mt-4">
                                        <a href="<?php base_url()?>admin/contacts" target="_blank" <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modContacts'])||isset(json_decode($authPermissionID,true)['modGroup']))? '' : 'hidden' : '' ?>>
                                            <i class="ion ion-md-cloud-upload"></i>
                                            <span class="camp-text ml-1">Import Contacts</span>
                                        </a>
                                    </div>

<!--                                     <div class="col-md-12 mt-4">
                                        <a href="#">
                                            <i class="ion ion-ios-book"></i>
                                            <span class="camp-text ml-1">Generate Report</span>
                                        </a>
                                    </div><br> -->
                                    <div class="col-md-12 mt-5 row-bordered row-border-black">
                                        <div class="col-md-12 pb-2">
                                            <i class="ic-bulb ion ion-ios-bulb" style=""></i><span class="ml-2" style="font-size: 23px; font-weight: bold;"> Texting Tips</span>
                                            <p class="mt-3 text-small">Something not working?.Check out our knowledge base and clicking help in the upper right of your portal screen.</p>
                                        </div>
                                    </div><br>
                                    <div class="col-md-12 mt-5 row-bordered row-border-black">
                                        <div class="col-md-12 pb-2">
                                            <h5><strong>Recent Activities</strong></h5>
                                            <dd>Jhon Update the campaign into My Soapbox</dd>
                                        </div>
                                    </div>
                                </div>
                                <!-- 2nd row Start -->
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-xl-12 col-md-12">
                                    <h1 class="display-4">Good afternoon,<span style="font-weight: bolder;"><strong><?= $this->session->userdata('username'); ?></strong></h1>
                                </div>

                                <div class="col-xl-3 col-md-6">
                                    <div class="card mb-4 ui-proj mb-4">
                                        <div class="card-body">
                                            <div class="row align-items-center mb-3">
                                                <div class="col-auto">
                                                    <i class="fa fa-user text-primary display-4"></i>
                                                </div>
                                                <div class="col p-l-0">
                                                    <h5 class="mb-1 f-w-700">New Users</h5>
                                                    <h6 class="mb-0 text-primary">Registered</h6>
                                                </div>
                                            </div>
                                            <h6 class="pt-badge bg-primary bg-pattern-2"><?=$total_users?></h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="card mb-4 ui-proj mb-4">
                                        <div class="card-body">
                                            <div class="row align-items-center mb-3">
                                                <div class="col-auto">
                                                    <i class="fa fa-share-square text-success display-4"></i>
                                                </div>
                                                <div class="col p-l-0">
                                                    <h5 class="mb-1 f-w-700">Campaign</h5>
                                                    <h6 class="mb-0 text-success">Running</h6>
                                                </div>
                                            </div>
                                            <h6 class="pt-badge bg-success bg-pattern-2"><?=$total_campaigns?></h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="card mb-4 ui-proj mb-4">
                                        <div class="card-body">
                                            <div class="row align-items-center mb-3">
                                                <div class="col-auto">
                                                    <i class="fa fa-comment-alt text-danger display-4"></i>
                                                </div>
                                                <div class="col p-l-0">
                                                    <h5 class="mb-1 f-w-700">Messages</h5>
                                                    <h6 class="mb-0 text-danger">Sent today</h6>
                                                </div>
                                            </div>
                                            <h6 class="pt-badge bg-danger bg-pattern-2"><?=$total_messages?></h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="card mb-4 ui-proj mb-4">
                                        <div class="card-body">
                                            <div class="row align-items-center mb-3">
                                                <div class="col-auto">
                                                    <i class="fa fa-comment-dots text-warning display-4"></i>
                                                </div>
                                                <div class="col p-l-0">
                                                    <h5 class="mb-1 f-w-700">Clients </h5>
                                                    <h6 class="mb-0 text-warning">Feedback</h6>
                                                </div>
                                            </div>
                                            <h6 class="pt-badge bg-warning bg-pattern-2"><?=$total_clients?></h6>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <?php } ?>
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/libs/chart-am4/core.js"></script>
<script src="<?=base_url()?>assets/admin/libs/chart-am4/charts.js"></script>
<script src="<?=base_url()?>assets/admin/libs/chartjs/chartjs.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_dashboard.js"></script>
</body>
</html>
