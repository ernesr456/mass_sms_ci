<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>

<head>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/datatables/datatables.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/photoswipe/photoswipe.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-indicator.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-video.css">
    <style type="text/css">
        .list-group-item.activechat{
            background-color: #716aca;
            color: #ffff !important;
        }
        .material-style .chat-contacts .list-group-item.activechat .chat-status{
            color: #ffff !important;
        }
        .material-style .chat-contacts .list-group-item.activechat .badge-outline-success{
            color: #ffff !important;
        }
        .chat-contacts .list-group-item.activechat {
            font-weight: bold;
        }
        .list-group-item.activechat {
            background-color: #1E9FF2 !important;
        }
        .container-p-y:not([class^="pb-"]):not([class*=" pb-"]) {
            padding: 0rem !important;
        }
       .material-style .chat-contacts .list-group-item.activechat .badge-outline-success {
            box-shadow: 0 0 0 1px #ffffff inset;
        }
    </style>
    <style type="text/css">
        .loader,
        .loader:after {
            border-radius: 50%;
            width: 10em;
            height: 10em;
        }
        .loader {            
            margin: 60px auto;
            font-size: 10px;
            position: relative;
            text-indent: -9999em;
            border-top: 1.1em solid rgba(255, 255, 255, 0.2);
            border-right: 1.1em solid rgba(255, 255, 255, 0.2);
            border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);
            border-left: 1.1em solid #1E9FF2 !important;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-animation: load8 1.1s infinite linear;
            animation: load8 1.1s infinite linear;
        }
        @-webkit-keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        #loadingDiv {
            position:absolute;;
            top:0;
            left:0;
            width:100%;
            height:100%;
        }

        .is-hide{
            display:none;
        }

        .material-style .dropzone{
            border: none !important;
        }
        .dropzone{
            padding: 0 0 0 0 !important;
        }
        .dz-image-preview{
            margin: 15px !important;
        }
        .ui-chat img{
            margin: 0px !important;
        }
        .form-group {
            margin-bottom: 0 !important;
        }
        .note.needsclick{
            display: none;
        }
        .btn:not(:disabled):not(.disabled),#button_msg{
            max-height: 50px !important;
        }
        .card-body {
            padding: 0.9rem !important;
        }
        .dz-image-preview {
            margin: 5px !important;
        }
        a[itemprop="contentUrl"] img{
            width:auto !important;
            max-height: 150px;
            max-width: 200px;
        }
        .ui-chat .center-chat {
            text-align: center;
        }
        .ui-chat .received-chat .msg:after {
            border-bottom-color: rgba(24, 28, 33, 0.06) !important;
        }
        figure {
            margin: auto;
        }

        .ui-chat .send-chat .msg:after {
            content: "";
            position: absolute;
            right: -6px;
            top: -7px;
            -webkit-transform: rotate(315deg);
            transform: rotate(315deg);
            border: 7px solid transparent;
            border-bottom-color: #1E9FF2 !important;
        }
    </style>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/chat.css">
</head>
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>
    
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modConversation'])? (json_decode($authPermissionID,true)['modConversation']===true? '':show_404()): show_404()) : ''?>
    <!-- Modal template -->
    <div class="modal fade" id="show_all_contact_modal">
        <div class="modal-dialog modal-lg">
            <form class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">All Contact
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">
                    <table>
                        <thead>
                            
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="show_con_contact">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">
                    <div class="card ui-team mb-12">
                        <h4 class="card-header text-center">Forward Message</h4>
                        <div class="card-body">
                            <div class="row" id="forward_message_content">
  <!--                               <div class="col-md-6 col-xl-4">
                                    <div class="card bg-dark border-0 text-white" id="forward_img">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-8">
                                    <input type="hidden" id="con_forward_message" name="contact_id">
                                    <textarea id="autosize-demo" rows="3" placeholder="Type message here...." maxlength="1200" class="form-control forward_message" name="chatmessage" rows="10" ></textarea>
                                    <span id="forward_count">0/1200</span>
                                </div> -->
                            </div>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="template_id" value="0" name="">
                            <input type="hidden" id="forward_id" name="">
                            <input type="text" class="form-control form-control-md search_forward_contacts" placeholder="Search Contacts here...">
                            <div class="clearfix"></div>
                        </div>
                        <div class="media align-items-center">
                            <div class="nav-tabs-top">
                                <ul class="nav nav-tabs" style="text-align: center;">
                                    <li class="nav-item">
                                        <a class="nav-link active forward-list-contacts" data-title="single" data-toggle="tab" href="#">Contacts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link forward-list-contacts" data-title="group" data-toggle="tab" href="#">Groups</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="card-body table-responsive" id="team-member1" style="height:340px">
                            <table class="table card-table" id="table_contacts">
                                <tbody class="modal_contact_list">
                                    <input type="hidden">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
  <div class="modal fade" id="show_templates">
    <div class="modal-dialog modal-lg">
        <form class="modal-content">
             <div class="modal-header">
                <h5 class="modal-title">Choose a template
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive mb-4">
                    <table class="datatables-demo table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="15%">Name</th>
                                <th width="78%">Content</th>
                                <th width="5%">File</th>
                                <th style="text-align: center; width: 1%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($template as $t){
                            ?>
                            <tr>
                                <td><?=$t->subject?></td>
                                <td><?=$t->message?></td>
                                <td class="center" width="5%">
                                    <?php if($t->filename!=""){ ?>
                                        <button type="button" id="fetch_template" data-id='<?=$t->id?>' class="btn btn-xs btn-outline-primary">Click to view</button>
                                    <?php } ?>
                                </td>
                                <td class="center" width="5%">
                                    <div class="form-row">
                                        <button type="button" id="select_template" data-id='<?=$t->id?>' class="btn btn-xs btn-outline-primary">Select</button>

                                    </div>
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <!-- [ Layout content ] Start -->
                <div class="layout-content">
                    <!-- [ content ] Start -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <div class="row" style="height: 100% !important">
                            <div class="chat-wrapper col-md-9">

                                <!-- Make card full height of `.chat-wrapper` -->
                                <div class="card flex-grow-1 position-relative overflow-hidden">

                                    <!-- Make row full height of `.card` -->
                                    <div class="row no-gutters h-100">
                                        <div class="chat-sidebox col">
                                            <!-- Chat contacts header -->
                                            <div class="flex-grow-0 px-4">
                                                <div class="media align-items-center">
                                                    <div class="media-body">
                                                        <input type="text" class="form-control chat-search my-3" placeholder="Search...">
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <a href="javascript:void(0)" class="chat-sidebox-toggler d-lg-none d-block text-muted text-large font-weight-light pl-3">&times;</a>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div class="btn-group mb-2">
                                                        <input type="hidden" id="msg_filter_input" value="2" name="">
                                                        <button id="msg_filter" data-value="2" class="btn btn-default btn-xs dropdown-toggle waves-effect btn-cp" type="button" data-toggle="dropdown" aria-expanded="false">All</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 25px; left: 0px;">
                                                            <a class="dropdown-item" href="javascript:void(0)" id="message_filter" data-name="All" data-value="2">All</a>
                                                            <a class="dropdown-item" href="javascript:void(0)" id="message_filter" data-name="Unread" data-value="1">Unread</a>
                                                            <a class="dropdown-item" href="javascript:void(0)" id="message_filter" data-name="Read" data-value="0">Read</a>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="btn-group mb-2">
                                                        <button class="btn btn-default btn-xs dropdown-toggle waves-effect btn-cp" type="button" data-toggle="dropdown" aria-expanded="false">Filter by tag</button>
                                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 25px; left: 0px;">
                                                            <a class="dropdown-item" href="javascript:void(0)">Unread</a>
                                                            <a class="dropdown-item" href="javascript:void(0)">Read</a>
                                                        </div>
                                                    </div> -->
                                                </div>
                                                <hr class="border-light m-0">
                                            </div>
                                            <div class="flex-grow-0 px-4">
                                                <div class="media align-items-center">
                                                    <div class="nav-tabs-top">
                                                        <ul class="nav nav-tabs" style="text-align: center;">
                                                            <li class="nav-item">
                                                                <a class="nav-link active list-contacts" data-title="single" data-toggle="tab" href="#">Contacts</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link list-contacts" data-title="group" data-toggle="tab" href="#">Groups</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="last_id" name="">
                                            <!-- / Chat contacts header -->
                                            <!-- Wrap `.chat-scroll` to properly position scroll area. Remove this wtapper if you don't need scroll -->
                                            <div class="flex-grow-1 position-relative">
                                                <div id="chat-scroll" class="chat-contacts list-group chat-scroll py-3">
                                                </div>
                                                <!-- / .chat-contacts -->
                                            </div>

                                        </div>
                                        <div class="d-flex col flex-column">
                                            <input type="hidden" id="msg_sent_filter" value="0">
                                            <!-- Chat header -->
                                            <div class="flex-grow-0 py-3 pr-4 pl-lg-4">

                                                <div class="media align-items-center">
                                                    <a href="javascript:void(0)" class="chat-sidebox-toggler d-lg-none d-block text-muted text-large px-4 mr-2">
                                                        <i class="ion ion-md-more"></i>
                                                    </a>

                                                    <div class="position-relative">
                                                        <span class="badge badge-dot badge-success indicator"></span>
                                                        <!-- <img src="assets/img/avatars/4-small.png" class="ui-w-40 rounded-circle" alt=""> -->
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="media-body pl-3">
                                                        <strong id="contact_name" style="text-transform: capitalize;"></strong>
                                                        <div class="text-muted small">
                                                            <em id="contact_number"></em>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        
                                                        <div class="btn-group" id="btn-top">
                                                           <!--  <button type="button" class="btn btn-info btn-round icon-btn mr-1" id="show_all_contact" data-toggle="modal" data-target="#show_all_contact_modal" title="Contact List" style="display: none;">
                                                                <i class="ion-ios-list"></i>
                                                            </button> -->
                                                            <button type="button" class="btn btn-default btn-round icon-btn" data-toggle="dropdown" aria-expanded="false" title="Message Filter">
                                                                <i class="ion ion-ios-more"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: top, left; top: 25px; left: -135px;">
                                                                <a href="javascript:" class="dropdown-item" id="msg_sent_all" data-id="0">&nbsp; All</a>
                                                                <a href="javascript:" class="dropdown-item" id="msg_to" data-id="1">&nbsp; To</a>
                                                                <a href="javascript:" class="dropdown-item" id="msg_from" data-id="2">&nbsp; From</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <hr class="flex-grow-0 border-light m-0">
                                            <!-- / Chat header -->

                                            <!-- Wrap `.chat-scroll` to properly position scroll area. Remove this wtapper if you don't need scroll -->
                                            <div class="flex-grow-1 position-relative">

                                                <!-- Remove `.chat-scroll` and add `.flex-grow-1` if you don't need scroll -->
                                                <div id="photoswipe-example" itemscope itemtype="http://schema.org/ImageGallery" class="chat-messages ui-chat chat-scroll p-4">
                                                    <!-- <div style="" id="loadingDiv"><div class="loader">Loading...</div></div> -->
                                                </div>
                                                <!-- / .chat-messages -->
                                            </div>
                                        <form class="send_message_conversation" method="post">
                                            <!-- Chat footer -->
                                            <hr class="border-light m-0">
                                            <div class="flex-grow-0 py-3 px-4" style="padding-bottom: 0.5rem">
                                                <blockquote class="blockquote" id="blockquote" style="display: none;">
                                                    <div class="card-header with-elements" style="border-bottom-width: 0px !important; padding: 0rem !important;max-height: 0px !important">
                                                        <div class="card-header-elements ml-auto">
                                                            <div class="btn-group">
                                                                <button type="button" id="close_qoute" class="btn btn-sm btn-default icon-btn borderless btn-round md-btn-flat dropdown-toggle hide-arrow"><i class="ion ion-md-close"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="blockquote_content">
                                                        <img src="<?php echo base_url() ?>assets/admin/img/bg/1.jpg" class="figure-img img-fluid" alt="A generic square placeholder image with rounded corners in a figure." style="max-height: 100px !important">
                                                        <p class="mb-0" id="blockquote_message"></p>
                                                    </div>
                                                    <footer class="blockquote-footer">
                                                        <cite title="Source Title" id="blockquote_from"></cite>
                                                    </footer>
                                                </blockquote>

                                                <div class="input-group">
                                                    <input type="hidden" class="form-control quote_id" value="0" name="message" placeholder="Type your message">
                                                    <input type="text" class="form-control chatmessage" name="message" maxlength="160"  placeholder="Type your message">
                                                    <div class="input-group-append">
                                                        <button type="button" class="btn icon-btn btn-outline-info" title="Message Template" id="message_template" data-toggle="modal" data-target="#show_templates">
                                                            <span class="fas fa-paste"></span>
                                                        </button>
                                                        <button type="button" class="btn icon-btn btn-outline-secondary" id="show_attach" title="Attach File" data-value="0">
                                                            <span class="fas fa-link"></span>
                                                        </button>
                                                        <button type="submit" id="btn_sendMessage" class="btn icon-btn btn-primary" title="Send Message">
                                                            <span class="fas fa-paper-plane"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <span class="input-group">
                                                    <span id="chatmessage_count">0/160</span>
                                                </span>
                                            </div>
                                            <div action="http://localhost:4000/Admin/temp_file" class="dropzone needsclick" id="dropzone-add" style="display: none;"  >
                                                    <div class="dz-message needsclick">
                                                        Drop files here or click to upload
                                                    </div>
                                                    <div class="fallback">
                                                        <input name="file" type="file" name="" multiple>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            <!-- / Chat footer -->
                                        </form>
                                        </div>
                                    </div>
                                    <!-- / .row -->
                                </div>
                                <!-- / .card -->

                            </div>
                            <!-- / .chat-wrapper -->
                            <div class="col-md-3">
                                <input type="hidden" id="active_contact" name="">
                                <div class="d-flex justify-content-between align-items-center m-0 py-2 pr-2">
                                </div>
                                <div class="col-md-12">
                                    <h4>Assigned Texter</h4>
                                    <!-- <button onclick="showBottom()">Show bottom msg</button> -->
                                    <hr class="my-2">
                                    <div class="mb-4">
                                        <strong id="assign_texter" style="text-transform: capitalize;"></strong>
                                    </div>
                                    <h4>Contact Information</h4>
                                    <hr class="my-2">
                                    <p id="contact-name" style="text-transform: capitalize;"></p>
                                    <p id="contact-number"></p>
                                    <p id="contact-created" style="text-transform: capitalize;"></p>
                                    <p id="contact-email"></p>
                                    <p id="contact-group"></p>
                                    <p id="last-activity"></p>
                                    <br><br>
                                    <h4>Apply Tags</h4>
                                    <hr class="my-2">
                                    <input type="text" class="form-control" id="" placeholder="Add" value="" data-role="tagsinput">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- [ content ] End -->
                    <?php $this->load->view('admin/common/footer'); ?>

                </div>
                <!-- [ Layout content ] Start -->
            </div>
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->
<!-- JS Files -->
<!-- Core scripts -->
<?php $this->load->view('admin/common/js'); ?>

<!-- <script src="assets/js/analytics.js"></script> -->
<script src="<?=base_url()?>assets/admin/libs/autosize/autosize.js"></script>
<script src="<?=base_url()?>assets/admin/libs/select2/select2.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-select/bootstrap-select.js"></script>
<script src="<?=base_url()?>assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/pages_chat.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_conversation.js"></script>
<script src="<?=base_url()?>assets/admin/libs/dropzone/dropzone.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/tables_datatables.js"></script>
<script src="<?=base_url()?>assets/admin/libs/photoswipe/photoswipe.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-fullscreen.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-indicator.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-video.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-vimeo.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-youtube.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/ui_lightbox.js"></script>

<script type="text/javascript">
    function showBottom(){
       $('.convo-list776').attr('tabindex', -1).focus();
    }
</script>
</body>
</html>
