<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card bg-success text-white ui-hover-icon mb-4 bg-pattern-3">
                                    <div class="card-body text-center">
                                        <h2 id="total_receive"><?=$total_payment['total_payment']? number_format($total_payment['total_payment'], 2, '.', '') : '0.00' ?></h2> 
                                        <h6 class="mb-0">Total Payment</h6>
                                        <i class="fas fa-money-bill hov-icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card bg-danger text-white ui-hover-icon mb-4 bg-pattern-3">
                                    <div class="card-body text-center">
                                        <h2 id="total_receive"><?=$total_payment['total_credits']? $total_payment['total_credits'] : '0' ?></h2>
                                        <h6 class="mb-0">Total Credit</h6>
                                        <i class="fas fa-credit-card hov-icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card bg-warning text-white ui-hover-icon mb-4 bg-pattern-3">
                                    <div class="card-body text-center">
                                        <h2 id="total_receive"><?=$total_payment['credit_balance']? number_format($total_payment['credit_balance'], 2, '.', '') : '0.00'?></h2>
                                        <h6 class="mb-0">Total Balance</h6>
                                        <i class="fas fa-balance-scale hov-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-left mb-10">
                                    <?php if($_SESSION['user_group']==1){ ?>
                                        <a href="/admin/transaction"class="btn btn-primary">
                                        All Transaction</a>
                                    <?php } ?>
                                    <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#our_pricing">
                                    Our Pricing</a>
                                </div>
                                <?php if($transaction){ ?>
                                    <div class="card">
                                        <h6 class="card-header">All Transaction</h6> 
                                        <div class="card-datatable table-responsive">
                                            <table class="trans-table table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="5%">
                                                            Transaction ID
                                                        </th>
                                                        <th class="text-center">Username</th>
                                                        <th class="text-center">Payment Method</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-center">Total Payment</th>
                                                        <th class="text-center">Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($transaction as $t) {
                                                        $status = "";
                                                        if ($t['payment_state']=='completed') {
                                                            $status = '<button type="button" class="btn btn-sm btn-success" disabled>'.ucwords($t['payment_state']).'</button>';
                                                        }else{
                                                            $status = '<button type="button" class="btn btn-sm btn-danger" disabled>'.ucwords($t['payment_state']).'</button>';
                                                        }
                                                    ?>
                                                        <tr>
                                                            <td class="text-center"><?= $t['txn_id'] ?></td>
                                                            <td class="text-center"><a href="/admin/transaction/<?= $t['id'] ?>"><?= $t['username']?></a></td>
                                                            <td class="text-center"><?= ucwords($t['payment_method'])?></td>
                                                            <td class="text-center"><?= $status?></td>
                                                            <td class="text-center"><?= $t['total_payment']?></td>
                                                            <td class="text-center"><?= date('D, M d, Y - H:i a', strtotime($t['created_time']))?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="alert alert-dark-info">
                                        <strong>Empty! </strong> No transaction history recorded.
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="our_pricing">
                    <div class="modal-dialog">
                        <form class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <img style="max-width: 250px; max-height: 500px; "class="img-fluid" src="<?php base_url()?>/assets/Email_image/masssms.jpg">
                                </div>
                                <br>
                                <h3 class="modal-title text-center" style="text-align: center;">How Credit Works?</h3>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-center">
                                        <h5>Sending Message</h5>
                                        <p>Send texts with 150 characters cost <b>1 credit</b></p>
                                    </div>
                                    <br>
                                    <div class="col-md-6 text-center">
                                        <h5>Sending Message with File</h5>
                                        <p>Sending Message with file cost <b>3 credit</b></p>
                                    </div>
                                </div>  
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- [ Layout footer ] -->
                <?php $this->load->view('admin/common/footer'); ?>
            </div>
        </div>
    </div>
    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
    <!-- [ Layout wrapper] End -->
    <!-- JS Files -->
    <?php $this->load->view('admin/common/js'); ?>
</body>
</html>
