<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
							<div class="col-sm-12 mb-3">
								<div class="row">
									<div class="col-sm-6">
										<a href="<?=base_url();?>admin/campaigns" class="btn btn-primary">Back</a>
									</div>
									<div class="col-sm-6">
										<div class="d-flex justify-content-end align-content-end">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card overflow-hidden">
							<div class="row no-gutters row-bordered row-border-light">
								<div class="col-md-9">
									<form id="update-basic" method="POST"  class="tab-content">
										<div class="tab-pane fade show active" id="account-general">
											<div class="card-body">
												<div class="form-group">
													<label class="form-label">Campaign Title</label>
													<input type="text" class="form-control mb-1" name="campaign_title" id="campaign_title" value="<?=isset($campaign) ? $campaign->campaign_title : ''?>">
													<div class="clearfix"></div>
												</div>
												<div class="form-group">
													<label class="form-label">Campaign Text</label>
													<textarea class="form-control" id="campaign_text" name="campaign_text" rows="5"><?=isset($campaign) ? $campaign->campaign_text : ''?></textarea>
													<div class="clearfix"></div>
												</div>
												<div class="form-group">
													<label class="form-label mb-3">Assign To</label>
													<select class="users form-control " id="permission_name" >
														<option value="0" selected>Select Option</option>
														<?php
															foreach ($users as $i=>$u){
																?>
																<option value="<?=$u['id']?>" <?=(isset($campaign) && $campaign->user_id === $u['id']) ? 'selected' : ''?>><?=$u['username']?></option>
																<?php
															}
															?>
													</select>
												</div>
												<div class="form-group">
													<label class="form-label mt-3">Campaign Date</label>
													<input type="text" id="start_at" class="form-control" placeholder="DateTime" value="<?=isset($campaign) ? $campaign->start_at : ''?>" data-dtp="dtp_v7GPW">		
												</div>
												<div class="form-group">
													<label class="form-label mt-3">Apply Tags</label>
													<input type="text" placeholder="Add" value="<?=isset($campaign) ? $campaign->tags : ''?>" class="form-control" id="bs-tagsinput-2">
												</div>
												
												<div class="form-group">
													<label class="form-label mt-3">Assign Contact</label>
													<div>
														<label>Filter By:&nbsp;&nbsp;</label>
														<label class="form-check form-check-inline">
															<input class="form-check-input checkbox_check_gid" type="checkbox" onclick="getContact(1)" name="inline-radios-example" id="group" value="option1"/>
															<span class="form-check-label">Contact Group</span>
														</label>
														<label class="form-check form-check-inline">
															<input class="form-check-input checkbox_check_id" type="checkbox" onclick="getContact(2)" name="inline-radios-example" id="single" value="option2"/>
															<span class="form-check-label">Contacts</span>
														</label>
													</div>
													<input type="hidden" id="idSelected" value="<?=isset($campaign) ? $campaign->contacts_names : ''?>">
													<select class="form-control" id="contacts" value="<?=isset($campaign) ? $campaign->contacts : ''?>" multiple="multiple">
														<?php if(isset($campaign)){
															$data = explode(',', $campaign->contacts_names);
															foreach($data as $i =>$key) { ?>
																<option value="<?=$key?>" selected><?=$key?></option>
															<?php 
															} } ?>
													</select>
												</div>
												<div class="form-group">
													<label class="form-check form-check-inline">
														<input class="form-check-input checkbox_check_id" type="checkbox" name="inline-radios-example" id="auto_reply" <?=isset($campaign) ? ($campaign->auto_reply != '0') ? 'checked' : '' : ''?> />
													<span class="form-check-label">Default/Auto Reply Message</span>
													</label>
													<input type="hidden" value="<?=isset($campaign) ? $campaign->auto_reply : '' ?>" id="hide_auto_reply">
													<input type="text" class="form-control mb-1" name="auto_reply_text" id="auto_reply_text" value="<?=isset($campaign) ? $campaign->auto_reply_text : ''?>"  >
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<?php if($this->session->userdata("role_slug") === 'administrator' || strstr($this->session->userdata('permission'),'updateCampaign')): ?>
						<div class="text-right mt-3 mb-1">
							<button onclick="saveCampaign(<?=isset($campaign) ? $campaign->campaign_id : '0'?>)" class="btn btn-primary" data-style="slide-right">Save changes</button>&nbsp;
						</div>
						<?php endif ?>
                        <!-- [ Content End ] -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/js/init/init_campaign.js"></script>
</body>
</html>
