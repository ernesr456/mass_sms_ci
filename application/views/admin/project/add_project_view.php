<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/select2/select2.css">
<body>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="/project/project" class="btn btn-primary">Back</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="d-flex justify-content-end align-content-end">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form class="my-5 submit_project" id="save_project" autocomplete="off" method="POST">
                                            <div class="form-group">
                                                <label class="form-label">Name</label>
                                                <input type="hidden" name="validation-bs-tagsinput" id="template_id" value="<?php 
                                                if(isset($_GET['id'])){echo $_GET['id'];} ?>" class="form-control" autocomplete="none">
                                                <input type="text" id="edit_project" value="<?=isset($project) ? $project->proj_name : ''?>" name="name" class="form-control" autocomplete="none">
                                                <input type="hidden" id="project_id" value="<?=isset($project) ? $project->id : ''?>" name="name" class="form-control" autocomplete="none">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">To</label>
                                                <div>
                                                    <label>Filter By:&nbsp;&nbsp;</label>
                                                    <label class="form-check form-check-inline">
                                                        <input class="form-check-input checkbox_check_gid" type="checkbox" onclick="getContact(1)" name="inline-radios-example" id="group" value="option1"/>
                                                        <span class="form-check-label">Contact Group</span>
                                                    </label>
                                                </div>
                                                <select class="form-control" id="contacts" name="group_id" value="<?=isset($campaign) ? $campaign->contacts : ''?>" multiple="multiple">
                                                    <?php if(isset($project)){
                                                        $data = explode(',', $project->contacts_names);
                                                        foreach($data as $i =>$key) { ?>
                                                        <option value="<?=$key?>" selected><?=$key?></option>
                                                    <?php } } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Description</label>
                                                <textarea id="autosize-demo" rows="3" name="description"  class="form-control edit_description" autocomplete="none"><?=isset($project) ? $project->description : ''?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Message</label>
                                                <textarea id="autosize-demo" rows="3" name="message" class="form-control edit_message" maxlength="1200" autocomplete="none"><?=isset($project) ? $project->message : ''?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Project File</label>
                                                <div action="#" class="dropzone needsclick" id="dropzone-add">
                                                    <div class="dz-message needsclick">
                                                        Drop files here or click to upload
                                                    </div>
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <button type="submit" id="create_account" class="btn btn-primary"><?=isset($single) ? 'Update' : 'Save'?></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- [ Content End ] -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/libs/select2/select2.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#contacts').select2({
            placeholder:'Select Options',
            multiple: true
        });
    })
    function getContact(id){
        $.ajax({
            url: base_url+"Campaign/getContact",
            dataType: "json",
            type: "POST",
            data:{
                id:id
            },
            success:function(data){
                for(var i=0; i<data.length; i++){
                    if ($('#contacts').find("option[value='" + data[i].gid + "']").length) {
                        if($('input.checkbox_check_gid').is(':unchecked')) {
                            $("#contacts option[value='"+data[i].gid+"']").remove();
                        }
                    } else { 
                        var newOption = new Option(data[i].group_name, data[i].gid);
                        $('#contacts').append(newOption).trigger('change');
                    } 
                }
            }   
        })  
    }
</script>
</body>
</html>
