<!-- Add Contact -->
<div class="modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form action="javascript:;" class="add_campaign" method="POST" id="save_project">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-share-2"></i> <span id="modal_titles">Add New Campaign</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label class="form-label">Campaign Name</label>
                                <input type="text" id="projName" name="name" value="<?=isset($single) ? $single->name : ''?>" class="form-control" placeholder="Campaign Name" required>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Message</label>
                                <textarea id="autosize-demo" rows="3" name="message" maxlength="160" class="form-control message"></textarea>
                                <div class="clearfix"></div>
                                <span class="input-group">
                                    <span id="chatmessage_count">0/160</span>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Description</label>
                                <textarea id="description" rows="3" name="description" class="form-control"></textarea>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <!-- Range -->
    <!--                             <input type="text" id="alt_date_start" class="form-control">
                                <input type="text" id="alt_date_end" class="form-control"> -->
                                <label class="form-label">Campaign Date</label>
                                <div class="input-daterange input-group" id="datepicker-range">
                                    <input type="text" id="date_start" class="form-control" name="start">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">to</span>
                                    </div>
                                    <input type="text" id="date_end" class="form-control" name="end">
                                    <div class="clearfix"></div>
                                </div>
                                <!-- <label class="form-label mt-3">Campaign Date</label> -->
                                <!-- <input type="text" id="project_start" data-date-format="mm/dd/yyyy" class="form-control" placeholder="DateTime">      -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label class="form-label">Volunteer</label>
                                <select class="user_id form-control" name="assign" multiple style="width: 100%">
                                    <?php
                                        for($i=0;$i<count($users);$i++){
                                            if ($users[$i]['name']!="") {
                                                echo '<option value='.$users[$i]['id'].'>'.ucwords($users[$i]['name']).'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Group Contacts</label>
                                <select class="group_id form-control" name="group" multiple style="width: 100%">
                                    <?php
                                        for($i=0;$i<count($group_contact);$i++){
                                            if ($group_contact[$i]['group_name']!="") {
                                                echo '<option value='.$group_contact[$i]['gid'].'>'.$group_contact[$i]['group_name'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Choose File (Limit 1 only)</label>
                                <div action="#" class="dropzone needsclick" id="dropzone-add">
                                    <div class="dz-message needsclick">
                                        Drop files here or click to upload
                                    </div>
                                    <div class="fallback">
                                        <input name="file" type="file" multiple>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn_project" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>

<!-- Add Contact -->
<div class="modal fade" id="updateCampaign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form action="javascript:;" class="add_campaign" method="POST" id="save_project">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-share-2"></i> <span id="modal_titles">Update Campaign</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label class="form-label">Campaign Name</label>
                                <input type="hidden" name="id" id="id" value="">
                                <input type="text" id="update_name" name="name" value="<?=isset($single) ? $single->name : ''?>" class="form-control" placeholder="Campaign Name" required>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Message</label>
                                <textarea id="autosize-demo" rows="3" name="message" maxlength="160" class="form-control update_message"></textarea>
                                <div class="clearfix"></div>
                                <span class="input-group">
                                    <span id="chatmessage_count">0/160</span>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Description</label>
                                <textarea id="update_description" rows="3" name="description" class="form-control"></textarea>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Campaign Date</label>
                                <div class="input-daterange input-group" id="datepicker-range">
                                    <input type="text" id="update_date_start" class="form-control" name="start">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">to</span>
                                    </div>
                                    <input type="text" id="update_date_end" class="form-control" name="end">
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label class="form-label">Assign To</label>
                                <select class="update_user_id form-control" name="assign" multiple style="width: 100%">
                                    <?php
                                        for($i=0;$i<count($users);$i++){
                                            if ($users[$i]['name']!="") {
                                                echo '<option value='.$users[$i]['id'].'>'.ucwords($users[$i]['name']).'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Group</label>
                                <select class="update_group_id form-control" name="group" multiple style="width: 100%">
                                    <?php
                                        for($i=0;$i<count($group_contact);$i++){
                                            if ($group_contact[$i]['group_name']!="") {
                                                echo '<option value='.$group_contact[$i]['gid'].'>'.$group_contact[$i]['group_name'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Campaign File</label>
                                <div action="#" class="dropzone needsclick" id="dropzone-update">
                                    <div class="dz-message needsclick">
                                        Drop files here or click to upload
                                    </div>
                                    <div class="fallback">
                                        <input name="file" type="file" multiple>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn_project" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>


<div class="modal fade" id="send_campaign" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 1.25rem 2.5625rem 1.5rem 1.5625rem !important;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
            <h4 id="attemp">Attempted to Send:<span id="num_attemp"> </span></h4>
            <h4 id="success">Message Sent:<span id="num_sent"> </span></h4>
            <br>
            <p id="message"></p>
            <p id="contact"></p>
            <button type="submit" id="btn_send_text" class="btn btn-info send_txt">SEND TEXT</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="send_campaign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog-centered" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-share-2"></i> <span id="modal_titles">Add New Campaign</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn_project" class="btn btn-primary">Save</button>
                </div>
        </div>
    </div>
</div>-->

