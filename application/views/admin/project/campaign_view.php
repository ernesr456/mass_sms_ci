<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<head>
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/photoswipe/photoswipe.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-indicator.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-video.css">

	<style type="text/css">
		.chart-container {
		  position: relative;
		  margin: auto;
		  width: 100%;
		  /*width: 80vw;*/
		}
		.ui-chat .received-chat .msg:after {
			border-bottom-color: #1E9FF2 !important;
		}
		.mb-3, .my-3 {
		    margin-bottom: 0.5rem !important;
		}

	</style>
</head>
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>
	<?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modCampaign'])? (json_decode($authPermissionID,true)['modCampaign']===true? '':show_404()): show_404()) : ''?>

<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        
						<div class="col-sm-12 mb-3">

								<div class="media flex-wrap align-items-center py-1 px-2 pull-right" id="step1">
		                            <div class="media-body d-flex flex-wrap flex-basis-100 flex-basis-sm-auto">
		                            	<button type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted contact_refresh" data-target="#addContact" data-toggle="modal" title="Create Campaign" id="create_campaign">
		                                    <i class="sidenav-icon feather icon-share-2"></i>
		                                </button>
		                                <button type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted campaign_refresh" id="create_campaign" data-toggle="modal" title="Refresh">
		                                    <i class="ion ion-md-refresh"></i>
		                                </button>

<!-- 		                                <button type="button" id="contact_status" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" data-stat="2" title="Deactivate">
		                                    <i class="ion ion-md-close"></i>
		                                </button>
		                                <button id="delete_contact" type="button" id="contact_status" data-stat="2" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Delete">
		                                    <i class="ion ion-md-trash"></i>
		                                </button> -->
		                                <span class="filter-text">Filter Options: </span>
		                                <select class="custom-select filter-input" id="filter_campaign">
		                                    <option value="" hidden>Campaign</option>
											<option value="">All</option>

											<?php for($i=0;$i<count($project);$i++){?>
												<option value="<?php echo $project[$i]->id ?>"><?php echo $project[$i]->proj_name?></option>
											<?php } ?>
		                                </select>
		                                <select class="custom-select filter-input" name="by_status" id="filter_users">
		                                    <option value="" hidden>Created By</option>
											<option value="">All</option>
											<?php for($i=0;$i<count($users);$i++){?>
												<option value="<?php echo $users[$i]['id'] ?>"><?php echo ucwords($users[$i]['name']) ?></option>
											<?php } ?>
		                                </select>
		                                <select class="custom-select filter-input" name="created_by" id="filter_status">
		                                    <option value="" hidden>Status</option>
		                                    <option value="">All</option>
		                                    <option value="1">Upcoming Campaign</option>
		                                    <option value="2">Running Campaign</option>
		                                    <option value="3">Complete Campaign</option>
		                                </select>
		                            </div>
		                        </div>
							<span class="input-group">
							</span>
						</div>
						<div class="row">
							<div class="col-xl-5 col-md-12">
								<div class="row">
									<div class="col-xl-12 col-md-12">
	                                    <div class="card mb-4 ui-proj mb-4">
	                                        <div class="card-body">
	                                            <div class="row align-items-center mb-3">
	                                                <div class="col-auto">
	                                                    <i class="fa fa-share-square text-warning display-4"></i>
	                                                </div>
	                                                <div class="col p-l-0">
	                                                	<h6 class="mb-0 text-warning">Upcoming</h6>
	                                                    <h5 class="mb-1 f-w-700">Campaign</h5>
	                                                </div>
	                                            </div>
	                                            <h6 class="pt-badge bg-warning bg-pattern-2" id="total_upcoming">0</h6>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-xl-12 col-md-12">
	                                    <div class="card mb-4 ui-proj mb-4">
	                                        <div class="card-body">
	                                            <div class="row align-items-center mb-3">
	                                                <div class="col-auto">
	                                                    <i class="fa fa-share-square text-info display-4"></i>
	                                                </div>
	                                                <div class="col p-l-0">
	                                                	<h6 class="mb-0 text-info">Running</h6>
	                                                    <h5 class="mb-1 f-w-700">Campaign</h5>
	                                                </div>
	                                            </div>
	                                            <h6 class="pt-badge bg-info bg-pattern-2" id="total_running">0</h6>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-xl-12 col-md-12">
	                                    <div class="card mb-4 ui-proj mb-4">
	                                        <div class="card-body">
	                                            <div class="row align-items-center mb-3">
	                                                <div class="col-auto">
	                                                    <i class="fa fa-share-square text-success display-4"></i>
	                                                </div>
	                                                <div class="col p-l-0">
	                                                	<h6 class="mb-0 text-success">Completed</h6>
	                                                    <h5 class="mb-1 f-w-700">Campaign</h5>
	                                                </div>
	                                            </div>
	                                            <h6 class="pt-badge bg-success bg-pattern-2" id="total_completed">0</h6>
	                                        </div>
	                                    </div>
	                                </div>
									<div class="col-xl-12">
										<div class="card chart-container mb-4 ui-proj mb-4">
											<div class="chart-container">
												<canvas id="myChart" class="chartjs-demo" style="position: relative; height:40vh;"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-7 col-md-12 project_list" id="photoswipe-example" itemscope itemtype="http://schema.org/ImageGallery" style="max-height: 720px;"> </div>
						</div>
						<?php $this->load->view('admin/project/campaign_modal');?>
                        <!-- [ Content End ] -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/libs/chart-am4/core.js"></script>
<script src="<?=base_url()?>assets/admin/libs/chart-am4/charts.js"></script>
<script src="<?=base_url()?>assets/admin/libs/chartjs/chartjs.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_campaign.js"></script>
<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
<script src="<?=base_url()?>assets/admin/libs/dropzone/dropzone.js"></script>
<script src="<?=base_url()?>assets/admin/libs/photoswipe/photoswipe.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-fullscreen.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-indicator.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-video.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-vimeo.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-youtube.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/ui_lightbox.js"></script>
</body>
</html>
