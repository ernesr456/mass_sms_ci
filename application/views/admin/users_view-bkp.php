<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- 2nd row Start -->
<div class="row">
  <div class="col-sm-12 mb-3">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <div class="d-flex justify-content-end align-content-end">
                  <div class="btn-group all_action_btn" id="all_action_btn" style="display:none;">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" <?=(json_decode($authPermissionID,true)['updateUser']==="false")? 'disabled' : ''?>>Options
                    </button>
                    <div class="dropdown-menu" align="center">
                      <a href="javascript:;" id="activate_user" style="cursor:pointer;" class="dropdown-item">ACTIVATE USER</a>
                      <a href="javascript:;" id="deactivate_user" style="cursor:pointer;" class="dropdown-item">DEACTIVATE USER</a>
                    </div>
                  </div>&nbsp;
                    <a <?=(json_decode($authPermissionID,true)['addUser']==="false")? 'style="cursor: no-drop;" disabled' : 'href="/admin/add_user"'?> class="btn btn-primary">
                        Add
                    <?php  
                        if($_SESSION['user_group']==1){
                            echo "User";
                        }else{
                            echo "Volunteer";
                        }
                    ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <h6 class="card-header">All 
            <?php 
                if($_SESSION['user_group']==1){
                    echo "Users";
                }else{
                    echo "Volunteers";
                }
            ?></h6> 
            <!-- DataTable within card -->
                    <div class="card-datatable table-responsive">
                        <table class="datatables-demo table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th width="5%">
                                   <input name="select_all" value="1" id="select-all" type="checkbox" />
                                </th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Role</th>
                                <?php if($_SESSION['user_group']==1){ ?>
                                <th>Created By</th>
                                <?php } ?>
                                <th width="5%">Status</th>
                                <th class="center" width="5%">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach($users as $u){
                                            $switcher="";
                                if ($u->user_stat==1) {
                                  $switcher="unchecked";
                                }else{
                                  $switcher="checked";
                                }
                              ?>
                              <tr>
                                <td><input type="checkbox" id="selected_contact" value="<?= $u->id ?>" /></td>
                                <td><?= $u->name ?></td>
                                <td><?= $u->username?></td>
                                <td><?= $u->group_name?></td>
                                <?php if($_SESSION['user_group']==1){ ?>
                                <td><?= ucwords($u->created_name)?></td>
                                <?php } ?>
                                <td>
                                  <label class="switcher switcher-success">
                                      <input id="user_stat" data-id='<?= $u->id?>' data-stat='<?=$u->user_stat?>'type="checkbox" class="switcher-input" <?php echo $switcher; ?>>
                                      <input type="hidden" id="stat" value ="" />
                                      <span class="switcher-indicator">
                                          <span class="switcher-yes">
                                              <span class="ion ion-md-checkmark"></span>
                                          </span>
                                          <span class="switcher-no">
                                              <span class="ion ion-md-close"></span>
                                          </span>
                                      </span>
                                  </label>
                                </td>
                                <td>
                                  <a href="/admin/add_user?id=<?php echo $u->id ?>" class="btn icon-btn btn-sm btn-outline-primary">
                                        <span class="far fa-edit"></span>
                                  </a>
                                </td>  
                              </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
        <!-- Staustic card 3 Start -->
    </div>
</div>
<!-- 2nd row Start -->
