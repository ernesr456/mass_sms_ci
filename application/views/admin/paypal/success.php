<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="starter-template">
            	</div>
            	<div class="contact-form">
            	</div>                
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	function closeWindow() {
        swal({
            title: "Success!",
            text: "Your payment was successful, thank you for purchase.",
            icon: "success",
            button: false,
            closeModal: false,
        });
	    setTimeout(function() {
	       window.close();
	    }, 5000);
    }
    window.onload = closeWindow();
</script>