<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
  <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modTemplate'])? (json_decode($authPermissionID,true)['modTemplate']===true? '':'hidden'): 'hidden') : ''?>
  <!-- [ Layout wrapper ] Start -->
  <div class="layout-wrapper layout-2">
    <div class="layout-inner">
      <!-- [ Layout sidenav ] Start -->
      <?php $this->load->view('admin/common/leftnav'); ?>
      <!-- [ Layout container ] Start -->
      <div class="layout-container">
        <!-- [ Layout navbar ( Header ) ] -->
        <?php $this->load->view('admin/common/topbar'); ?>
        <!-- [ Layout content ] -->
        <div class="layout-content">
          <div class="container-fluid flex-grow-1 container-p-y>">
            <!-- Breadcrumbs -->
            <?php $this->load->view('admin/common/breadcrumbs'); ?>
            <!-- [ Content Start ] -->
            

            <div class="row">
              <div class="col-sm-12 mb-3">
                <div class="row">
                  <div class="col-sm-6"></div>
                  <div class="col-sm-6">
                    <div class="d-flex justify-content-end align-content-end">
                      <div class="btn-group all_action_btn" id="all_action_btn" style="display:none;">
                        <button type="button" id="delete_template" class="btn btn-info">DELETE
                        </button>
                      </div>&nbsp;
                      <?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'addTemplate')): ?>
                      <a href="<?=base_url()?>admin/add_template" class="btn btn-primary">Add Template</a>
                       <?php endif ?>

                    </div>

                <!-- <div class="d-flex justify-content-end align-content-end">
                  <div class="btn-group all_action_btn" id="all_action_btn" style="display:none;">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Options
                    </button>
                    <div class="dropdown-menu" align="center">
                      <a href="javascript:;" id="do_not_send" style="cursor:pointer;" class="dropdown-item">DO NOT SEND LIST</a>
                      <a href="javascript:;" id="delete_contact" style="cursor:pointer;" class="dropdown-item">DELETE CONTACT</a>
                      <a href="javascript:;" id="activate_contact" style="cursor:pointer;" class="dropdown-item">ACTIVATE CONTACT</a>
                    </div>
                  </div>
                    <a <?=(json_decode($authPermissionID,true)['addContacts']==="false")? 'style="cursor: no-drop;" disabled' : 'href="'.base_url().'admin/add_template"'?> class="btn btn-primary">Add Template</a>
                  </div> -->
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="card">
                <h6 class="card-header">All Templates</h6> 
                <div class="card-datatable table-responsive">
                  <table class="datatables-demo table table-striped table-bordered" id="view_template">
                    <thead>
                      <tr>
                        <th width="5%" align="center">
                          <input name="select_all" value="1" id="select-all" type="checkbox" />
  <!--                           <label class="custom-control custom-checkbox px-2 m-0" style="text-align: center;">
                              <input type="checkbox" name="select_all[]" id="select_all_temp" class="custom-control-input"><span class="custom-control-label"></span>
                            </label> -->
                          </th>
                          <th width="20%">Name</th>
                          <th>Content</th>
                          <th width="20%">File</th>
                          <th class="center" width="7%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- Staustic card 3 Start -->
              </div>
            </div>
            <!-- 2nd row Start -->

            <!-- Show File Modal -->
            <div class="modal fade" id="show_file_modal">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                  </div>
                  <div class="modal-body">
                    <div class="card-body" id="fetch_file_template">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END show file modal -->
            <!-- [ Content End ] -->
          </div>
          <!-- [ Layout footer ] -->
          <?php $this->load->view('admin/common/footer'); ?>
        </div>
      </div>
    </div>
    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
  </div>
  <!-- [ Layout wrapper] End -->
  <!-- JS Files -->
  <?php $this->load->view('admin/common/js'); ?>
</body>
</html>
