<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
      <div class="layout-inner">
         <!-- [ Layout sidenav ] Start -->
         <?php $this->load->view('admin/common/leftnav'); ?>
         <!-- [ Layout container ] Start -->
         <div class="layout-container">
            <!-- [ Layout navbar ( Header ) ] -->
            <?php $this->load->view('admin/common/topbar'); ?>
            <!-- [ Layout content ] -->
            <div class="layout-content">
               <div class="container-fluid flex-grow-1 container-p-y>">
                <!-- Breadcrumbs -->
                <?php $this->load->view('admin/common/breadcrumbs'); ?>
                <!-- [ Content Start ] -->
                <?php if(!isset($_GET['id'])){ ?>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="javascript:;" onclick="window.history.back()" class="btn btn-primary">Back</a>
                            </div>
                            <div class="col-sm-6">
                                <div class="d-flex justify-content-end align-content-end">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="my-5 <?=isset($single) ? 'update_account' : 'register'?>" id="register-form" autocomplete="off" method="POST">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Username</label>
                                            <input type="hidden" name="get_id" id="username" value="<?php if(isset($_GET['id'])){echo $_GET['id'];} ?>" class="form-control" autocomplete="none">
                                            <input type="text" name="username" id="username" value="<?=isset($single) ? $single->username : ''?>" class="form-control" autocomplete="none">
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Full Name</label>
                                            <input style="text-transform: capitalize" type="text" id="name" name="name"value="<?=isset($single) ? $single->name : ''?>"  class="form-control" autocomplete="none">
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label"><span>Password</span></label>
                                            <input type="password" name="password" value="" class="form-control">
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label"><span>Confirm Password</span></label>
                                            <input type="password" name="password2" class="form-control">
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label">Email</label>
                                            <input type="email" name="email"value="<?=isset($single) ? $single->email : ''?>" class="form-control" autocomplete="none">
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <?php if($_SESSION['user_group']==1): ?>
                                            <label class="form-label">Group</label>
                                            <select class="custom-select" id="group_id" name="group_id">
                                                <option value="">Select group</option>
                                                <?php
                                                foreach ($groups as $i=>$g){
                                                ?>
                                                <option value="<?=$g['id']?>" <?=(isset($single) && $single->user_group === $g['id']) ? 'selected' : ''?>><?=$g['name']?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" id="create_account" class="btn btn-primary"><?=isset($single) ? 'Update' : 'Save'?></button>
                                </div>
                            </form>
                            <!-- [ Form ] End -->
                        </div>
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            <div class="container-fluid flex-grow-1 container-p-y">
                <div class="col-sm-12 mb-3">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="javascript:;" onclick="window.history.back()" class="btn btn-primary">Back</a>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-end align-content-end">
                            </div>
                        </div>
                    </div>
                </div>
                <form class="tab-content update_account" id="register-form" autocomplete="off" method="POST">
                    <div class="card overflow-hidden">
                        <div class="row no-gutters row-bordered row-border-light">
                            <div class="col-md-3 pt-0">
                                <div class="list-group list-group-flush account-settings-links">
                                    <a class="list-group-item list-group-item-action active" data-toggle="list" href="#account-general">General</a>
                                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-change-password">Change password</a>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content"> 
                                    <div class="tab-pane fade show active" id="account-general">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label class="form-label">Full Name</label>
                                                <input style="text-transform: capitalize" type="text" id="name" name="name"value="<?=isset($single) ? $single->name : ''?>"  class="form-control" autocomplete="none">
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Username</label>
                                                <input type="hidden" name="get_id" id="username" value="<?php if(isset($_GET['id'])){echo $_GET['id'];} ?>" class="form-control" autocomplete="none">
                                                <input type="text" name="username" id="username" value="<?=isset($single) ? $single->username : ''?>" class="form-control" autocomplete="none">
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Email</label>
                                                <input type="email" name="email"value="<?=isset($single) ? $single->email : ''?>" class="form-control" autocomplete="none">
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <?php if($_SESSION['user_group']==1): ?>
                                                <label class="form-label">Group</label>
                                                <select class="custom-select" id="group_id" name="group_id">
                                                    <option value="">Select group</option>
                                                    <?php
                                                    foreach ($groups as $i=>$g){
                                                    ?>
                                                    <option value="<?=$g['id']?>" <?=(isset($single) && $single->user_group === $g['id']) ? 'selected' : ''?>><?=$g['name']?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="account-change-password">
                                    <div class="card-body pb-2">
                                        <div class="form-group">
                                            <label class="form-label">New password</label>
                                            <input type="password" id="password" name="password" class="form-control">
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Repeat new password</label>
                                            <input type="password" id="password2" name="password2" class="form-control">
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-right mt-3">
                    <button type="submit" id="create_account" class="btn btn-primary"><?=isset($single) ? 'Update' : 'Save'?></button>
                </div>
            </form>
        </div>


        <?php } ?>
        <!-- [ Content End ] -->
    </div>
    <!-- [ Layout footer ] -->
    <?php $this->load->view('admin/common/footer'); ?>
</div>
</div>
</div>
<!-- Overlay -->
<div class="layout-overlay layout-sidenav-toggle"></div>
</div>
<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
</body>
</html>
