<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">

<!-- CSS Header -->
<head>
    <title><?=$page_title;?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="<?=$page_description?>" />
    <meta name="keywords" content="<?=$page_keyword;?>">
    <meta name="author" content="Srthemesvilla" />
    <link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/admin/img/favicon.ico">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/fontawesome.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/ionicons.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/linearicons.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/open-iconic.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/feather.css">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap-material.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/shreerang-material.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/uikit.css">

    <!-- Libs -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-maxlength/bootstrap-maxlength.css">
    <!-- Page -->
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/authentication.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/toastr/toastr.css">
    <link href="<?=base_url();?>assets/admin/libs/ladda/ladda.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/admin/css/custom.css" rel="stylesheet">
    <!-- Basic universal javascripts -->
    <script src="<?=base_url()?>assets/admin/js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/toastr/toastr.js"></script>
    <input type="hidden" id="base_url" value="<?=base_url()?>">
</head>

<!-- Page Loader -->
<div class="page-loader">
    <div class="bg-primary"></div>
</div>

<body>
            <!-- [ content ] Start -->
            <div class="authentication-wrapper authentication-2 px-4">
                <div class="authentication-inner py-5">

                    <!-- [ Form ] Start -->
                    <div class="card">
                        <div class="p-4 p-sm-5">
                            <?php if ($error ==2):?>
                                 <div class="display-1 lnr lnr-cross-circle text-center text-danger mb-4"></div>
                                <p class="text-center text-big mb-4">This request was already applied<br>If you wish to request again, please request again</p>
                                <!-- <a href="<?php echo base_url('/auth'); ?>"><button type="button" class="btn btn-primary btn-block">Click here to login page</button></a> -->
                            <?php endif; ?>
                            <?php if($_GET['type']=='confirm_pass'&& $error==0): ?>
                                 <div class="display-1 lnr lnr-checkmark-circle text-center text-success mb-4"></div>
                                <p class="text-center text-big mb-4">Your password has been changed successfully.</p>
                                <input type="hidden" id="id" value = "<?php echo $_GET['id'] ?>" name="">
                                <a href="<?php echo base_url('/auth'); ?>"><button type="button" class="btn btn-primary btn-block">Click here to login</button></a>
                            <?php endif; ?>
                            <?php if($_GET['type']=='verification'&&$error==0):?>
                                <div class="display-1 lnr lnr-checkmark-circle text-center text-success mb-4"></div>
                                <p class="text-center text-big mb-4">Your email address has been successfully confirmed.</p>
                                <input type="hidden" id="id" value = "<?php echo $_GET['id'] ?>" name="">
                                <a href="<?php echo base_url('/auth'); ?>"><button type="button" class="btn btn-primary btn-block">Click here to login</button></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- [ Form ] End -->

                </div>
            </div>
            <!-- [ content ] End -->
    <!-- JS Files -->
    <?php //$this->load->view('admin/common/js'); ?>
    <script src="<?=base_url()?>assets/admin/js/pace.js"></script>
    <script src="<?=base_url()?>assets/admin/js/jquery-3.2.1.min.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/toastr/toastr.js"></script>
    <script src="<?=base_url()?>assets/admin/js/pages/jquery.timeago.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
    <script src="<?=base_url()?>assets/admin/js/bootstrap.js"></script>
    <script src="<?=base_url()?>assets/admin/js/sidenav.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/spin/spin.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/ladda/ladda.js"></script>
    <script src="<?=base_url()?>assets/admin/js/init/init_login.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/validate/validate.js"></script>
    <script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <script src="<?=base_url()?>assets/admin/js/layout-helpers.js"></script>
    <script src="<?=base_url()?>assets/admin/js/material-ripple.js"></script>
    <script src="<?=base_url()?>assets/admin/js/custom.js"></script>
</body>
</html>