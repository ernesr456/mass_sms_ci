<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
	<div class="col-sm-12 mb-3">
		<div class="row">
			<div class="col-sm-6">
				<a href="/admin/template" class="btn btn-primary">Back</a>
			</div>
			<div class="col-sm-6">
				<div class="d-flex justify-content-end align-content-end">
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
			 	<form id="save_template" autocomplete="off" enctype="">
                    <div class="form-group">
                         <label class="form-label">Name</label>
                         <input type="hidden" name="validation-bs-tagsinput" id="template_id" value="<?php 
                         if(isset($_GET['id'])){echo $_GET['id'];} ?>" class="form-control" autocomplete="none">
                         <input type="text" name="validation-bs-tagsinput" id="template_name" class="form-control" autocomplete="none">
                    </div>
                    <div class="form-group">
                         <label class="form-label">Message</label>
                         <textarea id="autosize-demo" name="validation-text" rows="3" class="form-control edit_message" maxlength="1200" autocomplete="none"></textarea>
                    </div>
                     <span class="input-group" style="float: left">
                        <span id="textarea_feedback">0/1200</span>
                    </span>
                    <div class="text-left">
                    	<!-- <button type="submit" id="add_temp_btn" class="btn btn-primary" <?=(json_decode($authPermissionID,true)['sendMessage']==="false")? 'disabled' : ''?>>Save</button> -->
                    </div>
                </form>
                <br>
                <form action="#" class="dropzone needsclick" id="dropzone-add">
                    <div class="dz-message needsclick">
                        Drop files here or click to upload
                    </div>
                    <div class="fallback">
                        <input name="file" type="file" multiple>
                        <div class="clearfix"></div>
                    </div>
                </form>
                <div class="d-flex justify-content-end">
                    <button type="submit" id="add_temp_btn" class="btn btn-primary" <?=(json_decode($authPermissionID,true)['sendMessage']==="false")? 'disabled' : ''?> style="margin-top: 10px;">Save</button>
                </div>
			</div>
		</div>
	</div>
</div>
<script>

</script>