<!-- Upload CSV Contact -->
<div class="modal fade" id="importContacts" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" method="POST" id="importContactsForm" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload"></i> Import Contacts</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="container">
                            <p>
                                <h6>The CSV file must have a (First Name, Last Name, Phone) refers to image below.</h6>
                                <img src="<?=base_url();?>assets/admin/img/t1.png" style="width:100%">
                                <h6>File must be <b> CSV (Comma Delimited) (*.csv)</b></h6>
                            </p>
                        </div>
                        <div class="container">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="form-label">Group</label>
                                    <select class="custom-select" name="group_id" id="import_group_id">
                                        <option value="">Select group</option>
                                        <?php foreach ($groups as $i=>$g){ ?>
                                            <option value="<?=$g['gid']?>"><?=$g['group_name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-label">Choose Files (Limit 1 only):</label>
                                    <div action="<?=base_url();?>Admin/temp_file" class="dropzone needsclick" id="dropzone-add">
                                        <div class="dz-message needsclick">
                                            Drop files here or click to upload
                                        </div>
                                        <div class="fallback">
                                            <input name="file" type="file" name="csv_file" id="csv_file" multiple>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="uploadImportedFile" name="uploadImportedFile">Upload File</button>
                </div>
            </form>      
        </div>
    </div>
</div>

<!-- Add Contact -->
<div class="modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" class="add_contact" method="POST" id="addContactForm">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-user-plus"></i> Add New Contact</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">First Name</label>
                            <input type="hidden" name="id" value="0">
                            <input type="text" id="firstName" name="firstName" value="<?=isset($single) ? $single->name : ''?>" class="form-control" placeholder="Enter First Name" required>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Last Name</label>
                            <input type="text" id="lastName" name="lastName" class="form-control" placeholder="Enter Last Name" required>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Group</label>
                            <select class="custom-select" name="group_id">
                                <option value="">Select group</option>
                                <?php foreach ($groups as $i=>$g){ ?>
                                    <option value="<?=$g['gid']?>"><?=$g['group_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Number</label>
                            <div class="input-group">
                                <input class="form-control" id="phonenumber" name="number" type="tel" placeholder="Enter Number" required >
                                <input name="old_number" type="hidden">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Email</label>
                            <input type="email" id="email" name="email" class="form-control" placeholder="Enter Email">
                            <input name="old_email" type="hidden">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 custom-input">
                            <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                <input type="checkbox" id="status_contact" name="cstatus" class="custom-control-input" checked>
                                <span class="custom-control-label">Status</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn_add_contact" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>

<!-- Update Contact -->
<div class="modal fade" id="updateContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" class="update_contact" method="POST" id="updateContactForm">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-user-plus"></i>Edit Contact</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">First Name</label>
                            <input type="hidden" id="update_ID" name="id" value="0">
                            <input type="text" id="update_firstName" name="firstName" value="" class="form-control" placeholder="First Name" required>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Last Name</label>
                            <input type="text" id="update_lastName" name="lastName" class="form-control" placeholder="Last Name" required>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Group</label>
                            <select class="custom-select" id="update_groupID" name="group_id">
                                <option value="">Select group</option>
                                <?php foreach ($groups as $i=>$g){ ?>
                                    <option value="<?=$g['gid']?>"><?=$g['group_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Number</label>
                            <div class="input-group">
                                <input class="form-control" id="update_phoneNumber" name="number" type="tel" required>
                                <input id="update_old_phoneNumber" name="old_number" type="hidden" >
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Email</label>
                            <input type="email" id="update_email" name="email" class="form-control" placeholder="Email">
                            <div class="clearfix"></div>
                            <input id="update_old_email" name="old_email" type="hidden">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 custom-input">
                            <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                <input type="checkbox" id="update_status" name="cstatus" class="custom-control-input">
                                <span class="custom-control-label">Status</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn_update" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>

<div class="modal fade" id="addContactGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="add_group" id="add_contactGroup"  method="post">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-user-plus"></i> Add New Contact Group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Contact Group Name</label>
                            <input type="hidden" name="id" id="group_id" value="0">
                            <input type="text" class="form-control" name="group_name" data-name="" id="group_name" placeholder="Group Name">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-12 custom-input">
                            <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                <input type="checkbox" id="status_contact" name="cstatus" class="custom-control-input" checked>
                                <span class="custom-control-label">Status</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_group" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>

<div class="modal fade" id="updateContactGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="add_group" id="add_contactGroup"  method="post">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="feather icon-user-plus"></i>Update Contact Group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Contact Group Name</label>
                            <input type="hidden" name="id" id="group_id" value="0">
                            <input type="text" class="form-control" name="group_name" data-name="" id="group_name" placeholder="Group Name">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-12 custom-input">
                            <label class="custom-control custom-checkbox m-0" style="z-index: 0">
                                <input type="checkbox" id="status_contact" name="cstatus" class="custom-control-input" checked>
                                <span class="custom-control-label">Status</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_group" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>