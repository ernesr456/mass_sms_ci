<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>

    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modContacts'])||isset(json_decode($authPermissionID,true)['modGroup'])? '' :show_404()) : '' ?> 

	<!-- [ Layout wrapper ] Start -->
	<div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
				<?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="card">
                            <h6 class="card-header">Contacts
		                        <div class="pull-right">
		                        	<a onclick="importContactstModal()" href="javascript:;" class="btn btn-primary btn-sm text-white"><i class="ion ion-ios-cloud-upload  text-white"></i> Import Contacts</a>&nbsp;
									<a data-target="#addContact" data-toggle="modal" href="javascript:;" class="btn btn-info btn-sm"><i class="ion ion-ios-add-circle text-white"></i> Add Contact</a>
								</div>
							</h6>
                            <!-- Controls -->
                            <div class="media flex-wrap align-items-center py-1 px-2">
                                <div class="media-body d-flex flex-wrap flex-basis-100 flex-basis-sm-auto">

                                    <button type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted contact_refresh" title="Refresh">
                                        <i class="ion ion-md-refresh"></i>
                                    </button>

                                    <button type="button" id="contact_status" data-type="contact" data-stat="1" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Activate/Restore">
                                        <i class="ion ion-md-checkmark"></i>
                                    </button>
                                    <button type="button" id="contact_status" data-type="contact" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" data-stat="2" title="Deactivate">
                                        <i class="ion ion-md-close"></i>
                                    </button>
                                    <button id="delete_contact" type="button" id="contact_status" data-type="contact" data-stat="2" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Delete">
                                        <i class="ion ion-md-trash"></i>
                                    </button>
                                    <span class="filter-text">Filter Options: </span>
                                    <select class="custom-select filter-input" id="filter_users">
                                        <option value="">Created by All <?=($this->session->userdata('user_group')==1)? 'User': 'Volunteer'?></option>
                                        <option value="<?=$this->session->userdata('user_id'); ?>">Created by Me</option>
                                        <optgroup label="Created By:">
											<?php for($i=0;$i<count($users);$i++){?>
												<option value="<?php echo $users[$i]['id'] ?>"><?php echo ucwords($users[$i]['name']) ?></option>
											<?php } ?>
                                        </optgroup>
                                    </select>
                                    <select class="custom-select filter-input" name="by_status" id="filter_status">
                                        <option value="" selected>All Status</option>
                                        <option value="1">Active</option>
                                        <option value="2">Deactivated</option>
                                        <option value="3">OptOut</option>
                                        <?php if($this->session->userdata('user_group')==1): ?>
                                        	<option value="5">Deleted</option>
                                        <?php endif; ?>
                                    </select>
                                    <select class="custom-select filter-input" name="created_by" id="filter_groups">
                                    	<option value="" hidden>Group Name</option>
                                        <option value="">All</option>
										<?php for($i=0;$i<count($groups);$i++){?>
											<option value="<?php echo $groups[$i]['gid'] ?>"><?php echo $groups[$i]['group_name'] ?></option>
										<?php } ?>
                                    </select>
                                </div>
                            </div>
                            <hr class="border-light m-0">
                            <div class="card-datatable">
                                <div class="tbl-responsive">
                                    <table class="datatables-demo table table-striped table-bordered" id="view_contacts">
                                        <thead>
                                            <tr>
                                                <th width="5%">
                                                    <input name="select_all" id="select_all" value="1" type="checkbox">
                                                </th>
                                                <th>First Name</th>
												<th>Last Name</th>
												<th>Number</th>
												<th>Email</th>
												<th>Group Name</th>
												<?php if($this->session->userdata('user_group')==1): ?>
												<th>Created By</th>
												<?php endif;?>
												<th width="5%">Status</th>
												<th class="text-center" width="5%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <input type="hidden" id="user_role" value="<?=isset($_GET['role'])?$_GET['role']:0;?>">
                                </div>
                            </div>
                        </div>
                        <!-- [ Content End ] -->
                    </div>
                    <?php $this->load->view('admin/contacts/contact_modal');?>					<!-- [ Layout footer ] -->
				</div>
					<br>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
	<!-- JS Files -->
	<?php $this->load->view('admin/common/js'); ?>
	<script src="<?=base_url()?>assets/admin/js/init/init_contacts.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
	<script src="<?=base_url()?>assets/admin/js/pages/tables_datatables.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/dropzone/dropzone.js"></script>
	<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
	<script src="<?=base_url()?>assets/build/js/intlTelInput.min.js"></script> 
	<script src="<?=base_url()?>assets/build/js/intlTelInput-jquery.min.js"></script> 
	<script>
		if (document.getElementById('phonenumber')) {
			var input = document.querySelector("#phonenumber");
			var phone = $("#phonenumber");
			$(document).ready(function(){
			var dial_code = $(".iti__country.iti__preferred.iti__active").data('dialCode');
			phone.val("+"+dial_code+" ");

			$(document).on('click', '.iti__country.iti__preferred' ,function(){
				var dial_code = $(this).data('dialCode');
				phone.val("+"+dial_code+" ");
			})
			})
			window.intlTelInput(input, {
				utilsScript: base_url+"assets/build/js/utils.js",
			});
		}
		if (document.getElementById('update_phoneNumber')) {
			var input2 = document.querySelector("#update_phoneNumber");
			var phone2 = $("#update_phoneNumber");
			$(document).ready(function(){
			var dial_code2 = $(".iti__country.iti__preferred.iti__active").data('dialCode');
			phone2.val("+"+dial_code2+" ");

			$(document).on('click', '.iti__country.iti__preferred' ,function(){
				var dial_code2 = $(this).data('dialCode');
				phone2.val("+"+dial_code2+" ");
			})
			})
			window.intlTelInput(input2, {
				utilsScript: base_url+"assets/build/js/utils.js",
			});
		}
	</script>

</body>
</html>
