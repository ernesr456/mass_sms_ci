<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
	<div class="col-sm-12 mb-3">
		<div class="row">
			<div class="col-sm-6">
				<a href="<?=base_url()?>admin/email_template" class="btn btn-primary">Back</a>
			</div>
			<div class="col-sm-6">
				<div class="d-flex justify-content-end align-content-end">
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
			 	<form id="save_contact_email_temp_form" autocomplete="off" enctype="">
                    <div class="form-group">
                         <label class="form-label">Template Name</label>
                         <input type="hidden" name="validation-bs-tagsinput" id="template_id" value="<?php 
                         if(isset($_GET['id'])){echo $_GET['id'];} ?>" class="form-control" autocomplete="none">
                         <input type="text" name="validation-bs-tagsinput" id="template_name" class="form-control" autocomplete="none">
                    </div>
                    <div class="form-group">
                         <label class="form-label pb-2">Email Content</label>
                         <textarea id="summernote" name="validation-text" rows="3" class="form-control edit_message summernote" autocomplete="none"></textarea>
                    </div>
                    <div class="text-left">
                    	<button type="submit" id="add_contact_email_temp_btn" class="btn btn-primary">Save Template</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>
