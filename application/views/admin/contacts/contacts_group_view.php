<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modContacts'])? (json_decode($authPermissionID,true)['modContacts']===true? '':show_404()): show_404()) : ''?>

	<!-- [ Layout wrapper ] Start -->
	<div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
				<?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->

				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
						<!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>

						<div class="card">
                            <h6 class="card-header">Contact Groups
		                        <div class="pull-right">
									<a data-target="#addContactGroup" data-toggle="modal" href="javascript:;" id="add_group" class="btn btn-info btn-sm"><i class="ion ion-ios-add-circle text-white"></i> Add Contact Group</a>
								</div>
							</h6>
                            <!-- Controls -->
                            <div class="media flex-wrap align-items-center py-1 px-2">
                                <div class="media-body d-flex flex-wrap flex-basis-100 flex-basis-sm-auto">

                                    <button type="button" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted contact_refresh" title="Refresh">
                                        <i class="ion ion-md-refresh"></i>
                                    </button>

                                    <button type="button" id="contact_status" data-stat="1" data-type="contacts_group" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Activate/Restore">
                                        <i class="ion ion-md-checkmark"></i>
                                    </button>
                                    <button type="button" id="contact_status" data-type="contacts_group" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" data-stat="2" title="Deactivate">
                                        <i class="ion ion-md-close"></i>
                                    </button>
                                    <button id="delete_contact" type="button" id="contact_status" data-type="contacts_group" data-stat="2" class="btn btn-default borderless md-btn-flat icon-btn messages-tooltip text-muted" title="Delete">
                                        <i class="ion ion-md-trash"></i>
                                    </button>
                                    <span class="filter-text">Filter Options: </span>
                                    <?php if($this->session->userdata('user_group')!=3): ?>
	                                    <select class="custom-select filter-input" id="filter_users">
	                                        <option value="">Created by All <?=($this->session->userdata('user_group')==1)? 'User': 'Volunteer'?></option>
	                                        <option value="<?=$this->session->userdata('user_id'); ?>">Created by Me</option>
	                                        <optgroup label="Created By:">
												<?php for($i=0;$i<count($users);$i++){?>
													<option value="<?php echo $users[$i]['id'] ?>"><?php echo ucwords($users[$i]['name']) ?></option>
												<?php } ?>
	                                        </optgroup>
	                                    </select>
                                	<?php endif; ?>
                                    <select class="custom-select filter-input" name="by_status" id="filter_status">
                                        <option value="" selected>All Status</option>
                                        <?php if($this->session->userdata('user_group')==1): ?>
                                        	<option value="0">Deleted</option>
                                        <?php endif; ?>
                                        <option value="1">Active</option>
                                        <option value="2">Deactivated</option>
                                        <!-- <?php if($this->session->userdata('user_group')==1): ?>
                                        	<option value="0">Deleted</option>
                                        <?php endif; ?> -->
                                    </select>
                                    <!-- <select class="custom-select filter-input" name="created_by" id="filter_groups">
                                    	<option value="" hidden>Group Name</option>
                                        <option value="">All</option>
										<?php for($i=0;$i<count($groups);$i++){?>
											<option value="<?php echo $groups[$i]['gid'] ?>"><?php echo $groups[$i]['group_name'] ?></option>
										<?php } ?>
                                    </select> -->
                                </div>
                            </div>
                            <hr class="border-light m-0">
                            <div class="card-datatable">
                                <div class="tbl-responsive">
                                    <table class="datatables-demo table table-striped table-bordered" id="view_contact_group">
                                        <thead>
												<tr>
													<th width="5%"><input name="select_all" id="select_all" value="1" type="checkbox"></th>
													<th>Group Name</th>
													<?php if($this->session->userdata('user_group')!=3): ?>
														<th>Created By</th>
													<?php endif; ?>
													<th width="5%">Status</th>
													<th class="text-center" width="5%">Action</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
                                    </table>
                                    <input type="hidden" id="user_role" value="<?=isset($_GET['role'])?$_GET['role']:0;?>">
                                </div>
                            </div>
                        </div>
						<?php $this->load->view('admin/contacts/contact_modal');?>
						<!-- [ Content End ] -->
					</div>
					<br>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
	<!-- JS Files -->
	<?php $this->load->view('admin/common/js'); ?>
	<script src="<?=base_url()?>assets/admin/js/init/init_contacts.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
	<script src="<?=base_url()?>assets/admin/js/pages/tables_datatables.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/dropzone/dropzone.js"></script>
	<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
</body>
</html>


