<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
  .note.needsclick{
      display: none;
  }
  .pl-2, .px-2 {
      padding-left: 3rem !important;
  }
</style>

<div class="row">
  <div class="col-sm-12 mb-3">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <div class="d-flex justify-content-end align-content-end">
                  
                    <?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'addTemplate')): ?>

                    <a href="<?base_url()?>admin/create_email_template" class="btn btn-primary">Create Template</a>
                    <?php endif ?>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <h6 class="card-header">All Templates</h6> 
            <div class="card-datatable table-responsive">
                <table class="datatables-demo table table-striped table-bordered" id="email_templates">
                      <thead>
                        <tr>
                          <th width="7%">
                            <label class="custom-control custom-checkbox px-2 m-0" style="text-align: center;">
                              <input type="checkbox" class="custom-control-input" name="selected_email_templates" id="selected_email_templates" value="">
                              <span class="custom-control-label"></span>
                            </label>
                          </th>
                          <th width="7%">Count</th>
                          <th>Template Title</th>
                          <th>Date Created</th>
                          <th class="center" width="10%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                </table>
            </div>
        </div>
        <!-- Staustic card 3 Start -->
    </div>
</div>
<!-- 2nd row Start -->