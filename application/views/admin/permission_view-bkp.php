
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h6 class="card-header"><?=$role?></h6>
            <div class="col-md-4 mt-3">
                <button type="button" <?=(json_decode($authPermissionID,true)['setGroupPermission']==="false")? 'style="cursor: no-drop;" disabled' : ''?> class="group-permission btn btn-outline-info" id="<?=($role=='Client')?'2':'3'?>" onclick="showmodal()">Set Permission For All Group</button>
            </div>
            <div class="card-datatable table-responsive">
                <table class="advence-datatables table table-striped table-bordered">
                    <thead>
                    <tr class="text-center">
                        <th>Full Name</th>
                        <th>Username</th>
                        <th>Permissions</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                    foreach ($users as $i=>$u) { ?>
                        <tr class="odd gradeX">
                            <td><?=$u['fullname']?></td>
                            <td><?=$u['username']?></td>
                            <td class="permissionTD">
                            <?php $color;$id; 
                                    //Dashboard Module
                                    (json_decode($u['permission'],true)['modDashboard']==='true')? $color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Dashboard</kbd>';
                                    //Campaign Module
                                    //Campaign Module
                                    (json_decode($u['permission'],true)['modCampaign']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Campaign</kbd>';
                                    (json_decode($u['permission'],true)['addCampaign']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Campaign</kbd>';
                                    (json_decode($u['permission'],true)['updateCampaign']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Campaign</kbd>';
                                    (json_decode($u['permission'],true)['deleteCampaign']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Campaign</kbd>';
                                    //Conversation Module
                                    (json_decode($u['permission'],true)['modConversation']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Conversation</kbd>';
                                    (json_decode($u['permission'],true)['viewMessage']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">View Message</kbd>';
                                    (json_decode($u['permission'],true)['sendMessage']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Send Message</kbd>';
                                    (json_decode($u['permission'],true)['deleteMessage']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Message</kbd>';
                                     //Contacts Module
                                     (json_decode($u['permission'],true)['modContacts']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Contact</kbd>';
                                    (json_decode($u['permission'],true)['addContacts']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Contact</kbd>';
                                    (json_decode($u['permission'],true)['updateContacts']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Contact</kbd>';
                                    (json_decode($u['permission'],true)['deleteContacts']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Contact</kbd>';
                                    //Group Module
                                     (json_decode($u['permission'],true)['modGroup']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Group</kbd>';
                                    (json_decode($u['permission'],true)['addGroup']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add Group</kbd>';
                                    (json_decode($u['permission'],true)['updateGroup']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Group</kbd>';
                                    (json_decode($u['permission'],true)['deleteGroup']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Group</kbd>';
                                    //User Module
                                    (json_decode($u['permission'],true)['modUser']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">User</kbd>';
                                    (json_decode($u['permission'],true)['addUser']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Add User</kbd>';
                                    (json_decode($u['permission'],true)['updateUser']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update User</kbd>';
                                    (json_decode($u['permission'],true)['deleteUser']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete User</kbd>';
                                    //Permission Module   
                                    (json_decode($u['permission'],true)['modPermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Permission</kbd>';
                                    (json_decode($u['permission'],true)['createPermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Create Permission</kbd>';
                                    (json_decode($u['permission'],true)['updatePermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Update Permission</kbd>';
                                    (json_decode($u['permission'],true)['deletePermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Delete Permission</kbd>';
                                    (json_decode($u['permission'],true)['setGroupPermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Group Permission</kbd>';   
                                    (json_decode($u['permission'],true)['setClientPermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Client Permission</kbd>';
                                    (json_decode($u['permission'],true)['removeClientPermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Remove Client Permission</kbd>';
                                    (json_decode($u['permission'],true)['setVolunteerPermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Set Volunteer Permission</kbd>';
                                    (json_decode($u['permission'],true)['removeVolunteerPermission']==='true')?$color='green':$color='red';
                                        echo '<kbd class="ml-1" style="background-color:'.$color.';font-size:9px;">Remove Volunteer Permission</kbd>';?></td>
                            <td><?=$u['role']?></td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Options
                                    </button>
                                    <div class="dropdown-menu">
                                        <button class="dropdown-item" onclick="showmodal(<?=$u['id']?>)"<?=($role==='Client')?(json_decode($authPermissionID,true)['setClientPermission']==="false")? 'style="cursor: no-drop;" disabled' : '':(json_decode($authPermissionID,true)['setVolunteerPermission']==="false")? 'style="cursor: no-drop;" disabled' : ''?>>Set Permissions</button>
                                        <button class="dropdown-item" onclick="userDeletePermission(<?=$u['id']?>)" 
                                        <?=($role==='Client')?(json_decode($authPermissionID,true)['removeClientPermission']==="false")? 'style="cursor: no-drop;" disabled' : '':(json_decode($authPermissionID,true)['removeVolunteerPermission']==="false")? 'style="cursor: no-drop;" disabled' : ''?>
                                        >Remove Permissions</button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Staustic card 3 Start -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="permissionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Set Permission</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="user_id" class="form-control">
        <select class="select2-demo form-control" id="permission_name" style="width: 100%" data-select2-id="76" tabindex="-1" onchange="permission()" aria-hidden="true">
            <option  value="0"  selected>Select Option</option>
            <?php foreach ($usersPermission as $i=>$u){
                    echo "<option value=".$u['upid'].">".$u['permission_name']."</option>";
            }?>
        </select>
        <div class="col-md-12 mt-3">
            <div class="row">
                <div class="col-md-12">
                    <label><strong>Module Permission</strong></label>
                </div>
                <div class="col-md-12">
                    <div class="permissionModal ml-3"></div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="userPermission()">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tdModal" tabindex="-1" role="dialog" aria-labelledby="tdModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Full Permission Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid permissionBody"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>