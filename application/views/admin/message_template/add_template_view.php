<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
            <!-- Breadcrumbs -->
            <?php $this->load->view('admin/common/breadcrumbs'); ?>
            <!-- [ Content Start ] -->
              <div class="row">
                  <div class="col-sm-12 mb-3">
                      <div class="row">
                          <div class="col-sm-6">
                              <a href="/admin/template" class="btn btn-primary">Back</a>
                          </div>
                          <div class="col-sm-6">
                              <div class="d-flex justify-content-end align-content-end">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="card">
                          <div class="card-body">
                              <form id="save_template" autocomplete="off" enctype="">
                                  <div class="form-group">
                                     <label class="form-label">Name</label>
                                     <input type="hidden" name="validation-bs-tagsinput" id="template_id" value="<?php 
                                     if(isset($_GET['id'])){echo $_GET['id'];} ?>" class="form-control" autocomplete="none">
                                     <input type="text" name="validation-bs-tagsinput" id="template_name" class="form-control" autocomplete="none">
                                 </div>
                                 <div class="form-group">
                                     <label class="form-label">Message</label>
                                     <textarea id="autosize-demo" name="validation-text" rows="3" class="form-control edit_message" maxlength="1200" autocomplete="none"></textarea>
                                 </div>
                                 <span class="input-group" style="float: left">
                                  <span id="textarea_feedback">0/1200</span>
                              </span>
                              <div class="text-left">
                                  <!-- <button type="submit" id="add_temp_btn" class="btn btn-primary" <?=(json_decode($authPermissionID,true)['sendMessage']==="false")? 'disabled' : ''?>>Save</button> -->
                              </div>
                          </form>
                          <br>
                          <div action="http://localhost:4000/Admin/temp_file" class="dropzone needsclick" id="dropzone-add"  >
                            <div class="dz-message needsclick">
                                Drop files here or click to upload
                            </div>
                            <div class="fallback">
                                <input name="file" type="file" name="" multiple>
                                <div class="clearfix"></div>
                            </div>
                          </div>
<!--                           <form action="#" class="dropzone needsclick" id="dropzone-add">
                              <div class="dz-message needsclick">
                                  Drop files here or click to upload
                              </div>
                              <div class="fallback">
                                  <input name="file" type="file" multiple>
                                  <div class="clearfix"></div>
                              </div>
                          </form> -->
                          <div class="d-flex justify-content-end">
                              <button type="submit" id="add_temp_btn" class="btn btn-primary" style="margin-top: 10px;">Save</button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- [ Content End ] -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/js/init/init_messageTemplate.js"></script>
<script src="<?=base_url()?>assets/admin/libs/dropzone/dropzone.js"></script>
</body>
</html>
