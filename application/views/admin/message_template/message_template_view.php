<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<head>
  <link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/datatables/datatables.css">
</head>
<body>
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modTemplate'])? (json_decode($authPermissionID,true)['modTemplate']===true? '':'hidden'): 'hidden') : ''?>
  <?php $this->load->view('admin/message_template/template_modal');?>

<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
            <div class="row">
              <!-- Display status message -->
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-xs-12" id="import_response"></div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <h6 class="card-header">My Template
                    <div class="pull-right">
                      <a data-target="#addTemplate" data-toggle="modal" href="javascript:;" class="btn btn-info btn-sm"><i class="ion ion-ios-add-circle text-white"></i> Add Template</a>
                    </div>
                  </h6>
                  
                  <div class="card-datatable table-responsive">
                    <table class="table mb-0 table table-striped table-bordered" id="view_template">
                          <thead>
                            <tr>
                              <th width="5%" align="center">
                                <input name="select_all" value="1" id="select-all" type="checkbox" />
                              </th>
                              <th width="20%">Name</th>
                              <th>Content</th>
                              <th width="7%">File</th>
                              <th class="center" width="7%">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
              <!-- <div class="row">
                <div class="col-sm-12 mb-3">
                      <div class="row">
                          <div class="col-sm-6"></div>
                          <div class="col-sm-6">
                              <div class="d-flex justify-content-end align-content-end">
                                <div class="btn-group all_action_btn" id="all_action_btn" style="display:none;">
                                  <button type="button" id="delete_template" class="btn btn-info">DELETE
                                  </button>
                                </div>&nbsp;
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <div class="card">
                          <div class="table-responsive mb-4">
                              <table class="table mb-0 table table-striped table-bordered" id="view_template">
                                    <thead>
                                      <tr>
                                        <th width="5%" align="center">
                                          <input name="select_all" value="1" id="select-all" type="checkbox" />
                                        </th>
                                        <th width="20%">Name</th>
                                        <th>Content</th>
                                        <th width="7%">File</th>
                                        <th class="center" width="7%">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div> -->
              <!-- 2nd row Start -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<script src="<?=base_url()?>assets/admin/js/init/init_messageTemplate.js"></script>
<script src="<?=base_url()?>assets/admin/libs/datatables/datatables.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/tables_datatables.js"></script>
<script src="<?=base_url()?>assets/admin/libs/dropzone/dropzone.js"></script>

</body>
</html>
