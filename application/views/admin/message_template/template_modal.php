<!-- Add Contact -->
<div class="modal fade" id="addTemplate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" method="POST" id="submitTemplate">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-envelope" id="icon-template-title"></i> Add New Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Subject</label>
                            <input type="text" id="template_subject" name="template" value="" class="form-control" placeholder="Subject Name" required>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Message</label>
                            <div class="input-group">
                               <textarea id="autosize-demo" name="validation-text" rows="3" class="form-control template_message" maxlength="160" autocomplete="none"></textarea>
                            </div>
                            <div class="clearfix"></div>
                            <span class="input-group" style="float: left">
                                <span id="textarea_feedback">0/160</span>
                            </span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">File:</label>
                            <div action="http://localhost:4000/Admin/temp_file" class="dropzone needsclick" id="dropzone-add"  >
                                <div class="dz-message needsclick">
                                    Drop files here or click to upload
                                </div>
                                <div class="fallback">
                                    <input name="file" type="file" name="" multiple>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>

<!-- Add Contact -->
<div class="modal fade" id="updateTemplate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="javascript:;" method="POST" id="submitTemplate">
                <input type="hidden" id="template_id" name="">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-envelope" id="icon-template-title"></i> Update Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Subject</label>
                            <input type="text" id="update_template_subject" name="template" value="" class="form-control" placeholder="Subject Name" required>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Message</label>
                            <div class="input-group">
                               <textarea id="autosize-demo" name="validation-text" rows="3" class="form-control update_template_message" maxlength="160" autocomplete="none"></textarea>
                            </div>
                            <div class="clearfix"></div>
                            <span class="input-group" style="float: left">
                                <span id="textarea_feedback">0/160</span>
                            </span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">File:</label>
                            <div action="http://localhost:4000/Admin/temp_file" class="dropzone needsclick" id="dropzone-update"  >
                                <div class="dz-message needsclick">
                                    Drop files here or click to upload
                                </div>
                                <div class="fallback">
                                    <input name="file" type="file" name="" multiple>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>      
        </div>
    </div>
</div>