<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/account.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/photoswipe/photoswipe.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-indicator.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-video.css">
<body>
    <audio id="myAudio">
      <source src="<?=base_url()?>assets/tone.mp3" type="audio/mp3">
    </audio>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
						 <!-- [ content ] Start -->
					    <div class="card overflow-hidden">
					        <div class="row no-gutters row-bordered row-border-light">
					            <div class="col-md-3 pt-0">
					                <div class="list-group list-group-flush account-settings-links">
					                    <a class="list-group-item list-group-item-action active" data-toggle="list" href="#account-general">General</a>
					                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-change-password">Change password</a>
					                </div>
					            </div>
					            <div class="col-md-9">
					                <div class="tab-content"> 
					                    <!-- <form id="register-form savebasic" method="POST"  class="tab-content savebasic" autocomplete="off">  -->
					                        <form class="tab-content savebasic" id="register-form" autocomplete="off" method="POST">
					                    <div class="tab-pane fade show active" id="account-general">
					                        <div class="card-body media align-items-center">
					                        	<div id="photoswipe-example" class="row profile_picture" itemscope itemtype="http://schema.org/ImageGallery" <?= isset($user->image)&&$user->image!="" ? '' : 'hidden' ?>>
						                            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-sm-3">
						                                <a href="<?= isset($user->image)&&$user->image!="" ? base_url()."assets/profile_img/".$_SESSION['user_id'].'/'.$user->image: '' ?>" itemprop="contentUrl" data-size="1920x1280">
						                                	<img src="<?= isset($user->image)&&$user->image!="" ? base_url()."assets/profile_img/".$_SESSION['user_id'].'/'.$user->image: '' ?>" itemprop="thumbnail" class="d-block ui-w-80" alt="Image description" id="output-img"></a>
						                            </figure>
						                        </div>
					                     		<div class="media-body ml-4">
                                                    <label class="btn btn-outline-primary btn-sm">
                                                        Change
                                                        <input type="file" class="account-settings-fileinput" id="profile_image" name="files">
                                                    </label> &nbsp;
                                                    <div class="text-light small mt-1">Allowed JPG, GIF or PNG. Max size of 800K</div>
                                                </div>
					                        </div>
					                        <hr class="border-light m-0">
					                        <div class="card-body">
					                            <div class="form-group">
					                            	<input type="hidden" class="form-control mb-1" autocomplete="none" name="user_id" value="<?=$user ? $user->id : ''?>">
					                            	<input type="hidden" class="form-control mb-1" autocomplete="none" name="user_group" value="<?=$user ? $user->user_group : ''?>">

					                                <label class="form-label">Username</label>
					                                <input type="hidden" class="form-control mb-1" autocomplete="none" name="old_username" value="<?=$user ? $user->username : ''?>">
					                                <input type="text" class="form-control mb-1" id="username" name="username" autocomplete="none" value="<?=$user ? $user->username : ''?>">
					                                <div class="clearfix"></div>
					                            </div>
					                            <div class="form-group">
					                                <label class="form-label">Name</label>
					                                <input type="text" class="form-control" id="name" name="name" autocomplete="none" value="<?=$user ? $user->name : ''?>">
					                                <div class="clearfix"></div>
					                            </div>
					                            <div class="form-group">
					                                <label class="form-label">E-mail</label>
					                                <input type="text" class="form-control mb-1" id="email" autocomplete="none" name="old_email" value="<?=$user ? $user->email : ''?>">
					                                <input type="hidden" class="form-control mb-1" autocomplete="none" name="email" value="<?=$user ? $user->email : ''?>">
					                            </div>
					                        </div>
					                    </div>
					                    <div class="tab-pane fade" id="account-change-password">
					                        <div class="card-body pb-2">
					<!--                             <div class="form-group">
					                                <label class="form-label">Current password</label>
					                                <input type="password" id="password2" name="current_password" class="form-control">
					                                <div class="clearfix"></div>
					                            </div> -->
					                            <div class="form-group">
					                                <label class="form-label">Old password</label>
					                                <input type="password" id="old_password" name="old_password" autocomplete="none" class="form-control">
					                                <div class="clearfix"></div>
					                            </div>
					                            <div class="form-group">
					                                <label class="form-label">New password</label>
					                                <input type="password" id="password" name="password" autocomplete="none" class="form-control">
					                                <div class="clearfix"></div>
					                            </div>
					                            <div class="form-group">
					                                <label class="form-label">Repeat new password</label>
					                                <input type="password" id="password2" name="password2" autocomplete="none" class="form-control">
					                                <div class="clearfix"></div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>

					    <div class="text-right mt-3">
					        <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
					        <button type="button" class="btn btn-default">Cancel</button>
					    </div>
					    </form>
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
<!-- <script src="<?=base_url()?>assets/admin/js/pages/pages_account-settings.js"></script> -->
<script src="<?=base_url()?>assets\admin\libs\bootstrap-sweetalert\bootstrap-sweetalert.js"></script>
<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
<script src="<?=base_url()?>assets/admin/js/init/init_register.js"></script>
<script src="<?=base_url()?>assets/admin/libs/photoswipe/photoswipe.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-fullscreen.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-indicator.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-video.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-vimeo.js"></script>
<script src="<?=base_url()?>assets/admin/libs/blueimp-gallery/gallery-youtube.js"></script>
<script src="<?=base_url()?>assets/admin/js/pages/ui_lightbox.js"></script>
<script>
	var fileInput = document.querySelector('input[type="file"]');
	var preview = document.getElementById('output-img');
	 
	fileInput.addEventListener('change', function(e) {
 		$('.profile_picture').removeAttr('hidden');
	    var url = URL.createObjectURL(e.target.files[0]);
	    preview.setAttribute('src', url);
	});

  // var loadFile = function(event) {
  //   var output = document.getElementById('output');
  //   var output_img = document.getElementById('output-img');
  //   output_img.src = URL.createObjectURL(event.target.files[0]);
  //   output.src = URL.createObjectURL(event.target.files[0]);
  // };
</script>
</body>
</html>
