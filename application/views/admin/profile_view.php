<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
    <?=($this->session->userdata('user_group')!=1)? (isset(json_decode($authPermissionID,true)['modUser'])? (json_decode($authPermissionID,true)['modUser']===true? '':show_404()): show_404()) : ''?>

<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- [ Layout sidenav ] Start -->
            <?php $this->load->view('admin/common/leftnav'); ?>
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
                <!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
                <!-- [ Layout content ] -->
                <div class="layout-content">
                    <div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
                        <?php $this->load->view('admin/common/breadcrumbs'); ?>
                         <!-- [ content ] Start -->
                        <div class="container-fluid flex-grow-1 container-p-y">
                            <div class="card overflow-hidden">
                                <div class="row no-gutters row-bordered row-border-light">
                                    <div class="col-md-3 pt-0">
                                        <div class="list-group list-group-flush account-settings-links">
                                            <a class="list-group-item list-group-item-action active" data-toggle="list" href="#account-general">General</a>
                                            <a class="list-group-item list-group-item-action" data-toggle="list" href="#account-change-password">Change password</a>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="tab-content"> 
                                            <form class="tab-content savebasic" id="register-form" autocomplete="off" method="POST">
                                                <div class="tab-pane fade show active" id="account-general">
                                                    <div class="card-body media align-items-center">
                                                        <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-80">
                                                        <div class="media-body ml-4">
                                                            <label class="btn btn-outline-primary">
                                                                Upload new photo
                                                                <input type="file" class="account-settings-fileinput">
                                                            </label> &nbsp;
                                                            <button type="button" class="btn btn-default md-btn-flat">Reset</button>
                                                            <div class="text-light small mt-1">Allowed JPG, GIF or PNG. Max size of 800K</div>
                                                        </div>
                                                    </div>
                                                    <div class="text-light small mt-1">Allowed JPG, GIF or PNG. Max size of 5MB</div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ Layout footer ] -->
                    <?php $this->load->view('admin/common/footer'); ?>
                </div>
            </div>
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>

<script src="<?=base_url()?>assets/admin/js/init/init_user.js"></script>
<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    var output_img = document.getElementById('output-img');
    output_img.src = URL.createObjectURL(event.target.files[0]);
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<!-- [ content ] End -->
</body>
</html>