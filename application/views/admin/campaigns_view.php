<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">
<!-- CSS Header -->
<?php $this->load->view('admin/common/css'); ?>
<body>
<!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
		<div class="layout-inner">
			<!-- [ Layout sidenav ] Start -->
			<?php $this->load->view('admin/common/leftnav'); ?>
			<!-- [ Layout container ] Start -->
			<div class="layout-container">
				<!-- [ Layout navbar ( Header ) ] -->
                <?php $this->load->view('admin/common/topbar'); ?>
				<!-- [ Layout content ] -->
				<div class="layout-content">
					<div class="container-fluid flex-grow-1 container-p-y>">
                        <!-- Breadcrumbs -->
						<?php $this->load->view('admin/common/breadcrumbs'); ?>
                        <!-- [ Content Start ] -->
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-6">
                                        <?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'addCampaign')): ?>
                                        <div class="d-flex justify-content-end align-content-end">
                                            <a href="<?=base_url();?>campaign/add_campaign" class="btn btn-primary">
                                                Create Campaign
                                            </a>
                                        </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <h6 class="card-header">Campaign Lists</h6> 
                                    <!-- DataTable within card -->
                                    <div class="card-datatable table-responsive">
                                        <table class="datatables-demo table table-striped table-bordered dataTable no-footer" id="table_campaign">
                                            <thead>
                                                <tr>
                                                    <th>Campaign Title</th>
                                                    <th>Campaign Text</th>
                                                    <th>Assigned To</th>
                                                    <th>Release Date</th>
                                                    <th>Status</th>
                                                    <th>Contact</th>
                                                    <th>Tags</th>
                                                    <th>Created By</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($campaign as $c){ ?>
                                                    <tr>
                                                        <td><?=$c->campaign_title?></td>
                                                        <td class="campaignText"><?=$c->campaign_text?></td>
                                                        <td><?=$c->b_username?></td>
                                                        <td><?=date('m-d-y h:i A',strtotime($c->start_at));?></td>
                                                        <td>
                                                            <?php $dateToday = date("Y-m-d");$start_at = date("Y/m/d",strtotime($c->start_at)); 
                                                            $strToday = strtotime($dateToday); 
                                                            $strStart = strtotime($start_at); 
                                                            if ($strToday > $strStart) 
                                                                echo '<span class="badge badge-pill badge-info">Completed</span>'; 
                                                            elseif($strToday === $strStart)
                                                                echo '<span class="badge badge-pill badge-success">Running</span>'; 
                                                            else
                                                                echo '<span class="badge badge-pill badge-warning">Upcoming</span>'; 
                                                            ?>     
                                                        </td>
                                                        <td class="permissionTD"><?=$c->contacts_names;?></td>
                                                        <td>
                                                            <?php $tags = explode(",",$c->tags);
                                                                foreach($tags as $value => $key) { ?>
                                                                    <span class="badge badge-pill badge-success"><?=$key?></span>
                                                            <?php } ?>
                                                        </td>
                                                        <td><?=$c->c_created_by;?></td>
                                                        <td>
                                                            <?php $url=($c->group_name!="")? 'campaign/add_campaign?id='.$c->campaign_id.'&contactType=1&contactID='.$c->contacts : 'campaign/add_campaign?id='.$c->campaign_id.'&contactType=2&contactID='.$c->contacts;?>
                                                            <?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'updateCampaign')): ?>
                                                            <a href="<?=base_url().$url?>" class="btn icon-btn btn-sm btn-outline-primary">
                                                                <span class="far fa-edit"></span>
                                                            </a>
                                                            <?php endif ?>
                                                            <?php if($this->session->userdata('role_slug')=='administrator'|| strstr($this->session->userdata('permission'),'deleteCampaign')): ?>
                                                            <button onclick="deleteCampaign(<?=$c->campaign_id?>)" href="" class="btn icon-btn btn-sm btn-outline-danger">
                                                                <span class="fas fa-trash-alt"></span>
                                                            </button>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [ Content End ] -->
					</div>
					<!-- [ Layout footer ] -->
					<?php $this->load->view('admin/common/footer'); ?>
				</div>
			</div>
		</div>
		<!-- Overlay -->
		<div class="layout-overlay layout-sidenav-toggle"></div>
	</div>
	<!-- [ Layout wrapper] End -->
<!-- JS Files -->
<?php $this->load->view('admin/common/js'); ?>
</body>
</html>
