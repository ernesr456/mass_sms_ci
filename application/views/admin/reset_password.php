<!DOCTYPE html>
<html lang="en" class="material-style layout-fixed">

<!-- CSS Header -->
<head>
	<title><?=$page_title;?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="<?=$page_description?>" />
	<meta name="keywords" content="<?=$page_keyword;?>">
	<meta name="author" content="Srthemesvilla" />
	<link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/admin/img/favicon.ico">

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

	<!-- Icon fonts -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/fontawesome.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/ionicons.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/linearicons.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/open-iconic.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/fonts/feather.css">

	<!-- Core stylesheets -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap-material.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/shreerang-material.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/uikit.css">

	<!-- Libs -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/libs/bootstrap-maxlength/bootstrap-maxlength.css">
	<!-- Page -->
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/pages/authentication.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/toastr/toastr.css">
	<link href="<?=base_url();?>assets/admin/libs/ladda/ladda.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/admin/css/custom.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/libs/bootstrap-sweetalert/bootstrap-sweetalert.css">
	<!-- Basic universal javascripts -->
	<input type="hidden" id="base_url" value="<?=base_url()?>">
</head>

<!-- Page Loader -->
<div class="page-loader">
	<div class="bg-primary"></div>
</div>

<body>
	<!-- [ content ] Start -->
<div class="authentication-wrapper authentication-3">
	<div class="authentication-inner">

		<!-- [ Side container ] Start -->
		<!-- Do not display the container on extra small, small and medium screens -->
		<div class="d-none d-lg-flex col-lg-8 align-items-center ui-bg-cover ui-bg-overlay-container p-5" style="background-image: url('<?=base_url()?>assets/admin/img/bg/21.jpg');">
			<div class="ui-bg-overlay bg-dark opacity-50"></div>
			<!-- [ Text ] Start -->
			<div class="w-100 text-white px-5">
				<h1 class="display-2 font-weight-bolder mb-4">JOIN <br>With Us</h1>
				<div class="text-large font-weight-light">
					As your trusted communication partner.
				</div>
			</div>
			<!-- [ Text ] End -->
		</div>
		<!-- [ Side container ] End -->

		<!-- [ Form container ] Start -->
		<div class="d-flex col-lg-4 align-items-center bg-white p-5">
			<!-- Inner container -->
			<!-- Have to add `.d-flex` to control width via `.col-*` classes -->
			<div class="d-flex col-sm-7 col-md-5 col-lg-12 px-0 px-xl-4 mx-auto">
				<div class="w-100">

					<!-- [ Logo ] Start -->
					<div class="d-flex justify-content-center align-items-center">
						<div class="ui-w-60">
							<div class="w-100 position-relative">
								<img src="<?=base_url()?>assets/admin/img/logo-dark.png" alt="Brand Logo" class="img-fluid">
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<!-- [ Logo ] End -->

					<h4 class="text-center text-lighter font-weight-normal mt-5 mb-0">Change your Password</h4>

					<!-- [ Form ] Start -->
					<form class="my-5 reset_pass" id="register-form" autocomplete="off" method="POST">
						<input type="hidden" id="id" value ="<?php echo $_GET['id'] ?>" name="id">
						<div class="form-group">
							<label class="form-label">Password</label>
							<input type="password" name="password" id="password" class="form-control" autocomplete="none">
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<label class="form-label d-flex justify-content-between align-items-end">
								<span>Confirm Password</span>
							</label>
							<input type="password" name="password2" id="password2" class="form-control">
							<div class="clearfix"></div>
						</div>
						<div class="d-flex justify-content-between align-items-center m-0">
							<button type="submit" id="password_reset" class="btn btn-block btn-primary">Save</button>
						</div>
					</form>
					<!-- [ Form ] End -->
					<div class="text-center text-muted">
						Already have an account?
						<a href="/login">Sign In</a>
					</div>
					<br/>
					<div class="text-center text-muted">
						Don't have an account yet?
						<a href="/auth/register">Sign Up</a>
					</div>

				</div>
			</div>
		</div>
		<!-- [ Form container ] End -->

	</div>
</div>
<!-- [ content ] End -->
	
	<!-- JS Files -->
	<?php //$this->load->view('admin/common/js'); ?>
	<script src="<?=base_url()?>assets/admin/js/pace.js"></script>
	<script src="<?=base_url()?>assets/admin/js/jquery-3.2.1.min.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/toastr/toastr.js"></script>
	<script src="<?=base_url()?>assets/admin/js/pages/jquery.timeago.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/admin/libs/popper/popper.js"></script>
	<script src="<?=base_url()?>assets/admin/js/bootstrap.js"></script>
	<script src="<?=base_url()?>assets/admin/js/sidenav.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/spin/spin.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/ladda/ladda.js"></script>
	<script src="<?=base_url()?>assets/admin/js/init/init_login.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/validate/validate.js"></script>
	<script src="<?=base_url()?>assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="<?=base_url()?>assets/admin/js/layout-helpers.js"></script>
	<script src="<?=base_url()?>assets/admin/js/material-ripple.js"></script>
	<script src="<?=base_url()?>assets/admin/js/custom.js"></script>
	<script src="<?=base_url()?>assets\admin\libs\bootstrap-sweetalert\bootstrap-sweetalert.js"></script>
</body>
</html>
