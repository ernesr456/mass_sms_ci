<?php

if ( ! function_exists('pre')) {
	function pre($content,$exit = false,$varDump=false){
		echo '<div style="padding:20px; border: 1px solid #ff0000;"><pre style="word-wrap: break-word; white-space: pre-wrap;">';
		$varDump ? var_dump($content) : print_r($content);
		echo '</pre></div>';
		if ($exit) {
			exit();
		}
	}
}
