<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

use Twilio\Rest\Client;
use Twilio\Twiml\MessagingResponse;

class CommonController extends CI_Controller
{

	protected $data = array();
	protected $tw;

	function __construct()
	{
		parent::__construct();

		//Load Site Settings 
		$this->load->config('twilio', TRUE);
        $site_settings 	   = $this->Admin_model->getConfigs();
	    $this->config->config['twilio']['account_sid'] =  $site_settings['twillio_account_sid'];
	    $this->config->config['twilio']['auth_token'] =  $site_settings['twillio_auth_token'];
        $this->config->set_item('send_grid_email', $site_settings['send_grid_email']);
        $this->config->set_item('send_grid_pass', $site_settings['send_grid_key']);
        
		//$this->tw = new Client($this->config->item('sid'), $this->config->item('authtoken'));

		/*
		 * Page meta contents
		 *
		 * page_title
		 * page_description
		 * page_keyword
		 * page_class
		 * */

		$this->data['page_title'] = $this->config->item('site_name');
		$this->data['page_head'] = $this->config->item('site_name');
		$this->data['usname'] = $this->session->userdata('username');
		$this->data['before_head'] = '';
		$this->data['before_body'] = '';

	}

	public function setBreadcumb($bredcumbdata)
	{
		$this->data['breadcumb'][$bredcumbdata[0]] = $bredcumbdata;
	}

	public function setBeforeHead($beforehead)
	{
		$this->data['before_head'] .= $beforehead;
	}

	public function setBeforeBody($beforebody)
	{
		$this->data['before_body'] .= $beforebody;
	}

	protected function render($the_view = NULL, $title = NULL, $meta = NULL, $hasBreadcrumbs = true, $template = 'admin_master')
	{
		$pagetitle = $title ? $title . ' | ' . $this->config->item('site_name') : $this->config->item('site_name');
		$this->data['page_title'] = $pagetitle;
		$this->data['page_head'] = $title;
		$this->data['page'] = $meta['page'];
		$this->data['meta'] = $meta;
		$this->data['hasBreadcrumbs'] = $hasBreadcrumbs;

		if ($template == 'json' || $this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo json_encode($this->data);
		} else {
			$this->data['the_view_content'] = (is_null($the_view)) ? '' : $this->load->view($the_view, $this->data, TRUE);
			$this->load->view('templates/' . $template . '_view', $this->data);
		}
	}

	public function setData($key, $data)
	{
		$this->data[$key] = $data;
	}

}

class FrontendController extends CommonController
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('session');
	}
}

class BackendController extends CommonController
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	protected function render($the_view = NULL, $title = NULL, $meta = NULL, $hasBreadcrumbs = true, $template = 'admin_master')
	{
		$this->load->helper('url');
		if (!$this->session->userdata('is_logged_in')) {
			redirect('/login');
		}
		if ($the_view === 'admin/dashboard_view'||$the_view==='admin/project/project_view') {
			parent::setBeforeHead($this->generateCommoncss());
			parent::setBeforeBody($this->generateCommonjs());
		}
		parent::render($the_view, $title, $meta, $hasBreadcrumbs, $template);
	}

	public function generateCommoncss()
	{
		$commons = '';
		$commons .= '<link rel="stylesheet" href="' . base_url() . 'assets/admin/libs/flot/flot.css">';
		return $commons;
	}

	public function generateCommonjs()
	{
		$commons = '';
		$commons .= '<script src="' . base_url() . 'assets/admin/libs/chartjs/chartjs.js"></script>';
		$commons .= '<script src="' . base_url() . 'assets/admin/js/pages/charts_chartjs.js"></script>';
		return $commons;
	}

	// send sms twillio
	public function sendSms($number, $msg)
	{
		$this->tw->messages->create(
		// the number you'd like to send the message to
			$number,
			array(
				// A Twilio phone number you purchased at twilio.com/console
				'from' => $this->config->item('twnumber'),
				// the body of the text message you'd like to send
				'body' => $msg
			)
		);
	}

	// create call twillio
	public function twcall($number)
	{
		$this->tw->calls->create(
			$number, // Call this number
			$this->config->item('twnumber'), // From a valid Twilio number
			array(
				'url' => 'https://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient'
			)
		);
	}
}
