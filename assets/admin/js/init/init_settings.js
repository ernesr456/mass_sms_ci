function saveSettings(){
    var btn = Ladda.create(document.querySelector('#saveSettingsBtn'));
	btn.start();
	$.ajax({
		url:base_url+'admin/save_site_settings',
		method:'POST',
		data: $('#settingsForm').serialize(),
        success: (res) => {
            btn.stop();
            if(res==1){
                toastr["success"]("Settings saved successfully.");
            }else{
                toastr["error"]("A problem occued. Please try again.");
            }
        }
	});
}