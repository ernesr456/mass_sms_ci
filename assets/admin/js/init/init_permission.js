$(function() {
    $('.project_id').each(function() {
        $(this).wrap('<div class="position-relative"></div>')
            .select2({
                placeholder: 'Select project',
                dropdownParent: $(this).parent(),
                tokenSeparators: [',', ' ']
            });
    })
})

//search for contacts
$(document).on('keyup','.chat-search', function(e){
    e.preventDefault();
    var value = $(this).val().toLowerCase();
    $('.chat-scroll a').filter(function(){
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
})

$(document).ready(function(){
    $(document).on('change','#modCampaign,#modDashboard,#modConversation,#modTemplate,#modUser,#modGroup',function(){
    	if($('#modDashboard').is(":checked")){
            $('#modDashboard').prop('checked', true);
        }else{
            $('#modDashboard').prop('checked', false);
        }

        if($('#modCampaign').is(":checked")){
            $('#addCampaign,#updateCampaign,#deleteCampaign,#sendCampaign').removeAttr("disabled");
            $('#addCampaign,#updateCampaign,#deleteCampaign,#sendCampaign').prop('checked', true);
        }else{
            $("#addCampaign,#updateCampaign,#deleteCampaign,#sendCampaign").attr("disabled", true);
            $('#addCampaign,#updateCampaign,#deleteCampaign,#sendCampaign').prop('checked', false);
        }

        if($('#modConversation').is(":checked")){
            $('#addConversation,#sendMessage,#updateConversation').removeAttr("disabled");
            $('#addConversation,#sendMessage,#updateConversation').prop('checked', true);
        }else{
            $("#addConversation,#sendMessage,#updateConversation").attr("disabled", true);
            $('#addConversation,#sendMessage,#updateConversation').prop('checked', false);
        }

        if($('#modTemplate').is(":checked")){
            $('#addTemplate,#updateTemplate,#deleteTemplate').removeAttr("disabled");
            $('#addTemplate,#updateTemplate,#deleteTemplate').prop('checked', true);
        }else{
            $("#addTemplate,#updateTemplate,#deleteTemplate").attr("disabled", true);
            $('#addTemplate,#updateTemplate,#deleteTemplate').prop('checked', false);
        }

        if($('#modUser').is(":checked")){
            $("#addUser,#updateUser,#deleteUser,#updatePermission").attr("disabled", false);
            $('#addUser,#updateUser,#deleteUser,#updatePermission').prop('checked', true);
        }else{
            $("#addUser,#updateUser,#deleteUser,#updatePermission").attr("disabled", true);
            $('#addUser,#updateUser,#deleteUser,#updatePermission').prop('checked', false);
        }

        if($('#modGroup').is(":checked")){
            $("#addGroup,#updateGroup,#deleteGroup").attr("disabled", false);
            $('#addGroup,#updateGroup,#deleteGroup').prop('checked', true);
        }else{
            $("#addGroup,#updateGroup,#deleteGroup").attr("disabled", true);
            $('#addGroup,#updateGroup,#deleteGroup').prop('checked', false);
        }

        var permission_list = $('input[name^="permission"]').serializeArray();
        $.ajax({
            url: "/permission/update_permission",
            dataType: "json",
            type: "POST",
            data:{
                id: $('#id').val(),
                permission: permission_list
            },
            success:function(data){
            }
        })
    })
    $(document).on('click','#selected_user',function(e){
        const ths = $(this);
        $('.activechat').removeClass('activechat');
        ths.addClass('activechat');
        var id_array = [];
        $('input[name=check_user]:checked').each(function() {
            id_array.push($(this).val());
            $(this).parent().parent().addClass('activechat');
        });
        var id = $(this).data('id');
        $('#id').val(id);
        $('#group_id').val($(this).data('group'));
        $.ajax({
            url: "/permission/show_user",
            dataType: "json",
            type: "POST",
            data:{
                id:$('#id').val(),
                group_id: $('#group_id').val()
            },
            success:function(data){
                for(i=0;i<data.length;i++){
                    $('#contact_name').text(data[i].name);
                     if (data[i].verify==1) {
                        user_status = '<span id="user_status">Unverified</span>';
                        badge = '<span class="badge badge-dot badge-danger indicator"></span><div class="clearfix"></div>';
                    }else if (data[i].status==1) {
                        user_status = '<span id="user_status">Deactive</span>';
                        badge = '<span class="badge badge-dot badge-danger indicator"></span><div class="clearfix"></div>';
                    }else{
                        user_status = '<span id="user_status">Active</span>';
                        badge = '<span class="badge badge-dot badge-success indicator"></span><div class="clearfix"></div>';
                    }

                    var info_status = '';
                    if (data[i].verify==1){
                        status = '<span class="badge badge-dot badge-danger"></span>&nbsp; Unverified';
                        checkbox = 'disabled';
                        disabled = '';
                        info_status = '<a href="javascript:void(0)" class="badge badge-danger" style="pointer-events: none;cursor: default;">Unverified</a>';
                    }else if (data[i].user_stat==1){
                        status = '<span class="badge badge-dot badge-danger"></span>&nbsp; Deactive';
                        checkbox = 'value="'+data[i].id+'" id="check_user"';
                        info_status = '<a href="javascript:void(0)" class="badge badge-danger" style="pointer-events: none;cursor: default;">Deactive</a>';
                    }else if (data[i].user_stat==0) {
                        status = '<span class="badge badge-dot"></span>&nbsp; Active';
                        online = 'online';
                        checkbox = 'value="'+data[i].id+'" id="check_user"';
                        info_status = '<a href="javascript:void(0)" class="badge badge-success" style="pointer-events: none;cursor: default;">Active</a>';
                    }else if (data[i].user_stat==2) {
                        status = '<span class="badge badge-dot"></span>&nbsp; Deleted';
                        checkbox = 'value="'+data[i].id+'" id="check_user"';
                        info_status = '<a href="javascript:void(0)" class="badge badge-danger" style="pointer-events: none;cursor: default;">Deleted</a>';

                    }
                    
                    $('#user_badge').html(badge);
                    $('#user_status').html(user_status);
                    $('#user-name').text('Name: '+data[i].name);
                    $('#user-username').text('Username: '+data[i].username);
                    $('#user-email').text('Email: '+data[i].email);
                    $('#user-status').html('Status: '+info_status);
                    $('#user-group').html('User Type: '+data[i].group_name);
                        if (data[i].group_id==1) {
                            $('input:checkbox').not('#check_user').prop('checked',true);
                        }else if (data[i].user_permission!==null) {
                            var obj = JSON.parse(data[i].user_permission);
                            if (obj.modDashboard===true||data[i].group_id!=3) {
                                $('#modDashboard').prop('checked', true);
                            }else{
                                $('#modDashboard').prop('checked', false);
                            }
                            //Campaign
                            if (obj.modCampaign==true||data[i].group_id!=3) {
                                $('#modCampaign').prop('checked', true);
                            }else{
                                $('#modCampaign').prop('checked', false);
                            }

                            if (obj.addCampaign==true||data[i].group_id!=3) {
                                $('#addCampaign').prop('checked', true);
                            }else{
                                $('#addCampaign').prop('checked', false);
                            }

                            if (obj.updateCampaign==true||data[i].group_id!=3) {
                                $('#updateCampaign').prop('checked', true);
                            }else{
                                $('#updateCampaign').prop('checked', false);
                            }

                            if (obj.deleteCampaign==true||data[i].group_id!=3) {
                                $('#deleteCampaign').prop('checked', true);
                            }else{
                                $('#deleteCampaign').prop('checked', false);
                            }

                            if (obj.sendCampaign==true||data[i].group_id!=3) {
                                $('#sendCampaign').prop('checked', true);
                            }else{
                                $('#sendCampaign').prop('checked', false);
                            }

                            //Conversation
                            if (obj.modConversation==true||data[i].group_id!=3) {
                                $('#modConversation').prop('checked', true);
                            }else{
                                $('#modConversation').prop('checked', false);
                            }

                            if (obj.addConversation==true||data[i].group_id!=3) {
                                $('#addConversation').prop('checked', true);
                            }else{
                                $('#addConversation').prop('checked', false);
                            }

                            if (obj.sendMessage==true||data[i].group_id!=3) {
                                $('#sendMessage').prop('checked', true);
                            }else{
                                $('#sendMessage').prop('checked', false);
                            }

                            if (obj.updateConversation==true||data[i].group_id!=3) {
                                $('#updateConversation').prop('checked', true);
                            }else{
                                $('#updateConversation').prop('checked', false);
                            }

                            //Template
                            if (obj.modTemplate==true||data[i].group_id!=3) {
                                $('#modTemplate').prop('checked', true);
                            }else{
                                $('#modTemplate').prop('checked', false);
                            }

                            if (obj.addTemplate==true||data[i].group_id!=3) {
                                $('#addTemplate').prop('checked', true);
                            }else{
                                $('#addTemplate').prop('checked', false);
                            }

                            if (obj.updateTemplate==true||data[i].group_id!=3) {
                                $('#updateTemplate').prop('checked', true);
                            }else{
                                $('#updateTemplate').prop('checked', false);
                            }

                            if (obj.deleteTemplate==true||data[i].group_id!=3) {
                                $('#deleteTemplate').prop('checked', true);
                            }else{
                                $('#deleteTemplate').prop('checked', false);
                            }

                            //User
                            if (obj.modUser==true||data[i].group_id!=3) {
                                $('#modUser').prop('checked', true);
                            }else{
                                $('#modUser').prop('checked', false);
                            }

                            if (obj.addUser==true||data[i].group_id!=3) {
                                $('#addUser').prop('checked', true);
                            }else{
                                $('#addUser').prop('checked', false);
                            }

                            if (obj.updateUser==true||data[i].group_id!=3) {
                                $('#updateUser').prop('checked', true);
                            }else{
                                $('#updateUser').prop('checked', false);
                            }

                            if (obj.deleteUser==true||data[i].group_id!=3) {
                                $('#deleteUser').prop('checked', true);
                            }else{
                                $('#deleteUser').prop('checked', false);
                            }

                            if (obj.updatePermission==true||data[i].group_id!=3) {
                                $('#updatePermission').prop('checked', true);
                            }else{
                                $('#updatePermission').prop('checked', false);
                            }

                            //Contact Group
                            if (obj.modGroup==true||data[i].group_id!=3) {
                                $('#modGroup').prop('checked', true);
                            }else{
                                $('#modGroup').prop('checked', false);
                            }

                            if (obj.addGroup==true||data[i].group_id!=3) {
                                $('#addGroup').prop('checked', true);
                            }else{
                                $('#addGroup').prop('checked', false);
                            }

                            if (obj.updateGroup==true||data[i].group_id!=3) {
                                $('#updateGroup').prop('checked', true);
                            }else{
                                $('#updateGroup').prop('checked', false);
                            }

                            if (obj.deleteGroup==true||data[i].group_id!=3) {
                                $('#deleteGroup').prop('checked', true);
                            }else{
                                $('#deleteGroup').prop('checked', false);
                            }
                        }else{
                            $('input:checkbox').not('#check_user').prop('checked',false);
                        }
                }
            }
        })
        
    })
    show_user();
    function show_user(){
        $.ajax({
            url: "/permission/show_user",
            dataType: "json",
            type: "POST",
            success:function(data){
                var list = '';
                var user_status = '';
                var badge = '';

                for(i=0;i<data.length;i++){
                    var status = '';
                    var online = '';
                    var checkbox = '';
                    var info_status = '';
                    if (data[i].verify==1){
                        status = '<span class="badge badge-dot badge-danger"></span>&nbsp; Unverified';
                        checkbox = 'disabled';
                        disabled = '';
                        info_status = '<a href="javascript:void(0)" class="badge badge-danger" style="pointer-events: none;cursor: default;">Unverified</a>';
                    }else if (data[i].user_stat==1){
                        status = '<span class="badge badge-dot badge-danger"></span>&nbsp; Deactive';
                        checkbox = 'value="'+data[i].id+'" id="check_user"';
                        info_status = '<a href="javascript:void(0)" class="badge badge-danger" style="pointer-events: none;cursor: default;">Deactive</a>';
                    }else if (data[i].user_stat==0) {
                        status = '<span class="badge badge-dot"></span>&nbsp; Active';
                        online = 'online';
                        checkbox = 'value="'+data[i].id+'" id="check_user"';
                        info_status = '<a href="javascript:void(0)" class="badge badge-success" style="pointer-events: none;cursor: default;">Active</a>';
                    }else if (data[i].user_stat==2) {
                        status = '<span class="badge badge-dot"></span>&nbsp; Deleted';
                        checkbox = 'value="'+data[i].id+'" id="check_user"';
                        info_status = '<a href="javascript:void(0)" class="badge badge-danger" style="pointer-events: none;cursor: default;">Deleted</a>';

                    }

                    if (i==0) {

                        if (data[i].group_id==1) {
                            $('input[type=permission]').not('#check_user').each(function () {
                                $(this).prop('checked', true);
                            });
                        }else if (data[i].permission!=""&&data[i].permission!==undefined) {
                        	console.log(data[i].permission);
                            var obj = JSON.parse(data[i].permission);
                            if (obj.modDashboard==true||data[i].group_id!=3) {
                            	console.log(obj.modDashboard);
                                $('#modDashboard').prop('checked', true);
                            }else{
                            	console.log(obj.modDashboard);
                                $('#modDashboard').prop('checked', false);
                            }
                            //Campaign
                            if (obj.modCampaign==true||data[i].group_id!=3) {
                                $('#modCampaign').prop('checked', true);
                            }else{
                                $('#modCampaign').prop('checked', false);
                            }

                            if (obj.addCampaign==true||data[i].group_id!=3) {
                                $('#addCampaign').prop('checked', true);
                            }else{
                                $('#addCampaign').prop('checked', false);
                            }

                            if (obj.updateCampaign==true||data[i].group_id!=3) {
                                $('#updateCampaign').prop('checked', true);
                            }else{
                                $('#updateCampaign').prop('checked', false);
                            }

                            if (obj.deleteCampaign==true||data[i].group_id!=3) {
                                $('#deleteCampaign').prop('checked', true);
                            }else{
                                $('#deleteCampaign').prop('checked', false);
                            }

                            if (obj.sendCampaign==true||data[i].group_id!=3) {
                                $('#sendCampaign').prop('checked', true);
                            }else{
                                $('#sendCampaign').prop('checked', false);
                            }

                            //Conversation
                            if (obj.modConversation==true||data[i].group_id!=3) {
                                $('#modConversation').prop('checked', true);
                            }else{
                                $('#modConversation').prop('checked', false);
                            }

                            if (obj.addConversation==true||data[i].group_id!=3) {
                                $('#addConversation').prop('checked', true);
                            }else{
                                $('#addConversation').prop('checked', false);
                            }

                            if (obj.sendMessage==true||data[i].group_id!=3) {
                                $('#sendMessage').prop('checked', true);
                            }else{
                                $('#sendMessage').prop('checked', false);
                            }

                            if (obj.updateConversation==true||data[i].group_id!=3) {
                                $('#updateConversation').prop('checked', true);
                            }else{
                                $('#updateConversation').prop('checked', false);
                            }

                            //Template
                            if (obj.modTemplate==true||data[i].group_id!=3) {
                                $('#modTemplate').prop('checked', true);
                            }else{
                                $('#modTemplate').prop('checked', false);
                            }

                            if (obj.addTemplate==true||data[i].group_id!=3) {
                                $('#addTemplate').prop('checked', true);
                            }else{
                                $('#addTemplate').prop('checked', false);
                            }

                            if (obj.updateTemplate==true||data[i].group_id!=3) {
                                $('#updateTemplate').prop('checked', true);
                            }else{
                                $('#updateTemplate').prop('checked', false);
                            }

                            if (obj.deleteTemplate==true||data[i].group_id!=3) {
                                $('#deleteTemplate').prop('checked', true);
                            }else{
                                $('#deleteTemplate').prop('checked', false);
                            }

                            //User
                            if (obj.modUser==true||data[i].group_id!=3) {
                                $('#modUser').prop('checked', true);
                            }else{
                                $('#modUser').prop('checked', false);
                            }

                            if (obj.addUser==true||data[i].group_id!=3) {
                                $('#addUser').prop('checked', true);
                            }else{
                                $('#addUser').prop('checked', false);
                            }

                            if (obj.updateUser==true||data[i].group_id!=3) {
                                $('#updateUser').prop('checked', true);
                            }else{
                                $('#updateUser').prop('checked', false);
                            }

                            if (obj.deleteUser==true||data[i].group_id!=3) {
                                $('#deleteUser').prop('checked', true);
                            }else{
                                $('#deleteUser').prop('checked', false);
                            }

                            if (obj.updatePermission==true||data[i].group_id!=3) {
                                $('#updatePermission').prop('checked', true);
                            }else{
                                $('#updatePermission').prop('checked', false);
                            }

                            //Contact Group
                            if (obj.modGroup==true||data[i].group_id!=3) {
                                $('#modGroup').prop('checked', true);
                            }else{
                                $('#modGroup').prop('checked', false);
                            }

                            if (obj.addGroup==true||data[i].group_id!=3) {
                                $('#addGroup').prop('checked', true);
                            }else{
                                $('#addGroup').prop('checked', false);
                            }

                            if (obj.updateGroup==true||data[i].group_id!=3) {
                                $('#updateGroup').prop('checked', true);
                            }else{
                                $('#updateGroup').prop('checked', false);
                            }

                            if (obj.deleteGroup==true||data[i].group_id!=3) {
                                $('#deleteGroup').prop('checked', true);
                            }else{
                                $('#deleteGroup').prop('checked', false);
                            }
                        }else{
                            $('#addCampaign,#updateCampaign,#deleteCampaign,#sendCampaign').prop('checked', false);
                        }

                        $('#id').val(data[i].id);
                        $('#group_id').val(data[i].group);
                        $('#contact_name').text(data[i].name);
                         if (data[i].verify==1) {
                            user_status = '<span id="user_status">Unverified</span>';
                            badge = '<span class="badge badge-dot badge-danger indicator"></span><div class="clearfix"></div>';
                        }else if (data[i].status==1) {
                            user_status = '<span id="user_status">Deactive</span>';
                            badge = '<span class="badge badge-dot badge-danger indicator"></span><div class="clearfix"></div>';
                        }else{
                            user_status = '<span id="user_status">Active</span>';
                            badge = '<span class="badge badge-dot badge-success indicator"></span><div class="clearfix"></div>';
                        }
                        $('#user_badge').html(badge);
                        $('#user_status').html(user_status);
                        $('#user-name').text('Name: '+data[i].name);
                        $('#user-username').text('Username: '+data[i].username);
                        $('#user-email').text('Email: '+data[i].email);
                        $('#user-status').html('Status: '+info_status);
                        $('#user-group').html('User Type: '+data[i].group_name);
                    }

                    list += 
                    '<a href="javascript:void(0)" id="selected_user" data-id="'+data[i].id+'" data-group="'+data[i].group_id+'" data-verify="'+data[i].verify+'" data-status="'+data[i].status+'" class="list-group-item list-group-item-action '+online+'">'+
                        '<div class="media-body ml-3" style="text-transform: capitalize">'+
                            data[i].name+
                            '<div class="chat-status small">'+
                                status+
                            '</div>'+
                        '</div>'+
                        '<label class="custom-control custom-checkbox m-0">'+
                           ' <input type="checkbox" name="check_user" class="custom-control-input" value="'+data[i].id+'" id="check_user">'+
                            '<span class="custom-control-label"></span>'+
                        '</label>'+
                    '</a>';
                }
                $('.chat-contacts').html(list);
                $('#selected_user').first().addClass('activechat');
            }
        })
    }
    $(document).on('click','#update_user',function(){
        var id_array = [];
        var status = $(this).data('status')
        $('input[name=check_user]:checked').each(function() {
            id_array.push($(this).val());
        });

        if (id_array.length==0) {
            id_array.push($('.activechat').data('id'));
        }

        if (id_array.length>0||$('.activechat').data('id')!="") {
            var text = '';
            var btn = '';
            var type = '';
            if (status==0) {
                text = "activate";
                btn = 'info';
                type = 'warning';
            }else if (status==1) {
                text = "deactivate";
                 btn = 'danger';
            }else{
                text = "delete";
                btn = 'danger';
            }
            swal({
                title: "Are you sure?",
                text: "You want to "+text+" this user?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-"+btn,
                confirmButtonText: "Yes "+text+" it!",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "/admin/delete_user",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_array: id_array,
                            status: status
                        },
                        success: function(data) {
                            show_user();
                            if (data==true) {
                                if (status==2) {
                                    swal("Deleted!", "User successfully deleted.", "success");
                                }else if(status==0){
                                    swal("Activate!", "User successfully activated.", "success");
                                }else if (status==1) {
                                    swal("Deactivate!", "User successfully deactivate.", "success");
                                }
                            }else if (data==2){
                                swal("Error!", "Your can't delete your own account.", "error");
                            }else{
                                swal("Error!", "You can't update user right now.", "error");
                            }
                        }
                    })
                }
            });
        }
    })
})