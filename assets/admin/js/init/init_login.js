var base_url = $('#base_url').val();

$(document).on('submit','.login_form',function(e){
	var button = Ladda.create(document.querySelector('#create_account'));
	button.start();
	e.preventDefault();
	var formData = new FormData(this);
	var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	formData.append("timezone",timezone);
	$.ajax({
		url: base_url+"auth/login_user",
		dataType: "json",
		type: "POST",
		data:formData,
		processData:false,
		contentType:false,
		success:function(data){
            button.stop();
			if (data==true) {
				toastr.success(
                    'Login successfully!'
				);
                setTimeout(function(){ window.location.href = "/dashboard"; }, 1000);
			}
			else if (data==3) {
				toastr.warning(
                    'Please check your email.',
                    'Your account isn\'t verify.'
				);
			}else if(data == false){
				toastr.error(
                    'Please check username and password.',
                    'Invalid Login Credentials.'
				);
			}else if(data==4){
				toastr.error(
                    'Sorry we couldn\'t find your account.'
				);
			}else{
				toastr.error(
                    'Please contact your administrator','You can\'t login right now'
				);
			}
		}
	})
})

const forgotPassword= (e)=>{
	var email = $("#recover_email").val();
	var button = Ladda.create(e);
	button.start();
	if (email=="") {
		toastr["error"]("Please enter your email");
	}else{
		$.ajax({
			url: base_url+"Auth/get_key",
			type: "POST",
			dataType: "json",
			data: {
				email: email
			},
			success: function(data){
				if (data == true) {
					button.stop();
					swal("Success!", "We've sent a password reset link on that email.", "success");
					$("#recover_email").val("");
				}else if(data==false){
					button.stop();
					toastr["error"]("Sorry, we couldn't find your email");
				}else{
					toastr.error(
                        'Please contact our administrator.',
                        'Your account is deactivated.'
					);
				}
			}
		})
	}
}

const loginUser = (e)=>{
    var button = Ladda.create(e);
    var username = $("#username1").val();
	var password = $("#password1").val();
	var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	if (username||password) {
		button.start();
		$.ajax({
			url: base_url+"auth/login_user",
			dataType: "json",
			type: "POST",
			data:{
				username:username,
				password:password,
				timezone: timezone
			},
			success:function(data){
                button.stop();
				if (data==true) {
					toastr.success(
                        'Login successfully!'
					);
	                setTimeout(function(){ window.location.href = "/masssms/admin"; }, 1000);
				}
				else if (data==3) {
					toastr.warning(
                        'Please check your email.',
                        'Your account isn\'t verify.'
					);
				}else if(data == false){
					toastr.error(
                        'Please check username and password.',
                        'Invalid Login Credentials.'
					);
				}else if(data==4){
					toastr.error(
                        'Sorry we couldn\'t find your account.'
					);
				}else{
					toastr.error(
                        'Please contact your administrator','You can\'t login right now'
					);
				}
			}
		})
	}else{
        toastr.success("All fields are required");
    }
}
$(document).on("submit", ".reset_pass",function(e){
	e.preventDefault();
	var data = $(this).serializeArray();
	$.ajax({
		url: base_url+"Auth/change_pass",
		type: "POST",
		dataType: "json",
		data:{
			data:data
		},
		success:function(data) {
			if (data == true) {
				swal("Password changed successfully!", "Pleae check your email to confirm.", "success");
				$("#password").val("");
				$("#password2").val("");
			}else{
				swal("Please checked your email", "You password is already changed.", "error");
			}
		}
	})

})
