function validateCredits(){
	var price = $('#credit_price').val();
	var amount = $('#credit_amount').val();
	if (price&&amount) {
		if ( price>0 && amount>0 ) {
			$('#saveCreditsBtn').removeAttr('disabled');
		}else{
			toastr.warning(
	            'Price and Amount of Credits should be greater than zero.',
	            'Please check your inputs.'
			);
			$('#saveCreditsBtn').attr('disabled','disabled');
		}
	}else{
		$('#saveCreditsBtn').attr('disabled','disabled');
	}
}

const saveCredits = ()=>{
	var btn = Ladda.create(document.querySelector('#saveCreditsBtn'));
	btn.start();
	$.ajax({
		url: base_url+'admin/add_update_credits',
		type: 'POST',
		data: $('#add-credits-form').serialize(),
		success: (res)=>{
			btn.stop();
			if(res==1){
				swal({ 
					title: "Saved!", 
					text: "Credit was successfully saved.",
					type: "success",
					allowEscapeKey : false,
					allowOutsideClick: false
				}, function() { window.location.href = base_url+'admin/credits'; });
			}else{
				toastr.warning(
					'Unable to save, please try again.',
					'Please try again.'
				);
			}
		}
	});
}

const delete_credit = (credit_id)=>{
    swal({
        title: "Delete this credit?",
        text: "This will not be recovered. Continue?",
        type: "warning",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        confirmButtonText: "Delete",
        closeOnConfirm: false,
        confirmButtonColor: "#e11641"
    },
    ()=>{
        $.ajax({
			url: base_url+'admin/delete_credit/'+credit_id,
			success: (res)=>{
				if(res==1){
					$('#credit'+credit_id).remove();
					swal('Deleted!', 'Credit deleted successfully.', 'success');
				}else{
					toastr.warning(
						'Unable to delete, please try again.',
						'Please try again.'
					);
				}
			}
		});
    });
}

var total_pay = [];
var total_credit= [];
/* Get payment settings using ajax */
var payment_settings = ()=>{
    var result;
    $.ajax({
        url: '/admin/get_payment_settings',
        dataType: 'JSON',
        async: false,
        success: (res)=>{
            result = res;
        }
    });
    return result;
}

/* Show paypal payment settins using ajax */
var res = payment_settings();
function renderPaymentBtn(){
    var env = (res['paypal_sandbox']==1) ? 'sandbox' : 'production';
    paypal.Button.render({
        style: {
            size: 'responsive',
            color: 'black'
        },
        env: env, 
        client: {
            sandbox: res['paypal_client_id'],
            production: res['paypal_client_id']
        },
        locale: 'en_US',
        commit: true, // Show a 'Pay Now' button
        payment: function(data, actions) {
            // Set up the payment here
            var amount = (eval(total_pay.join('+'))) ? eval(total_pay.join('+')) : 1;
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: amount, currency: 'USD' }
                        }
                    ]
                }
            });
        },
        onAuthorize: function(data, actions) {
            // Execute the payment here
            return actions.payment.execute().then(function(payment){
				// Violet Loader Color
				H5_loading.show({color:"rgb(129, 122, 220)",background:"rgba(0, 0, 0, 0.55)",timeout:0.5,scale:0.8});
				$('#paymentModal').modal('hide');
                $.ajax({
                    url: 				'/payments/purchase_credits/', // 1 Paypal
                    data: {
						txn_id:			payment.id,
						credit_id: 		$('#credit_id').val(),
						payment_method: payment.payer.payment_method,
						total_payment: 	eval(total_pay.join('+')),
						payer_email: 	payment.payer.payer_info.email,
						payer_status: 	payment.payer.status,
						created_time:  	payment.create_time,
						total_credit: 	eval(total_credit.join('+')),
					},
                    type: 'POST',
                    success:(res)=>{
                        H5_loading.hide();
                        if(res==1){
                            swal({ title: "Success!", text: 'Credits loaded succesfully. Click "OK" to refresh your page.', type: "success" }, function() { location.reload(); });
                        }else{
                            swal("Oops!", 'Payment failed.', "error");
                        }
                    }
                });
            });
        }
    }, '#paypal-button');
}
renderPaymentBtn();
const payWithPaypal = (credit_id)=>{
	// var btn = Ladda.create(document.querySelector('#creidt_btn'+credit_id));
	// btn.start();
    $.ajax({
        url: '/payments/get_single_credit/'+credit_id,
        dataType: 'JSON',
        success: (res)=>{
			// btn.stop();
			if(res!=0){
				$('#package_name').html(res['credit_name']);
				$('#no_of_credits').html(res['credit_amount']);
				$('#credit_price').html(res['credit_price']);
				$('#credit_total_price').val(res['credit_price']);
				$('#credit_id').val(res['id']);
				$('#paymentModal').modal('show');
				total_pay=[];
				total_credit = [];
				total_pay.push(res['credit_price']);
				total_credit.push(res['credit_amount']);
			}else{
				toastr["error"]("A problem occued. Please try again.");
			}
        }
    });
}

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "$" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "$" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}


    $(document).ready(function(){
        var table_credit = $('#view_transaction').DataTable({
            "pageLength": 10,
            "serverSide": true,
            "ajax": {   
                "url": '/payments/show_credit_transaction',
                "type": 'POST',
            },
            'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'width': '1%',
            'className': 'dt-body-center',
        }],
        'order': [[1, 'asc']],
        'rowCallback': function(row, data, dataIndex){
            // Get row ID
            var rowId = data[0];

        }
    });

	show_credit();
	function show_credit(){
		$.ajax({
			url: base_url+"payments/show_credit",
			type: "POST",
			success:function(data) {
                if(data!=0){
                    $('#credit_card').html(data);
                }else{
                    $('#credit_card').html('');
                }
			}
		})
	}

	$(document).on('submit','.add_credit',function(e){
		var btn = Ladda.create(document.querySelector('#btn_credits'));
		var id = $('#id').val();
		var type = '';
		var message = '';
		if (id!=="") {
			type = 'update';
			message = 'updated';
		}else{
			type = 'insert';
			message = 'added'
		}

		btn.start();
		e.preventDefault();
		var all_data = $(this).serializeArray();
	   	$.ajax({
			url: "/payments/submit_credit",
			type: "POST",
			dataType: "json",
			data:{
				data:all_data,
				type:type
			},
			success:function(data) {
				btn.stop();
				show_credit();
				if (data==1) {
					swal("Success!", "Credit "+message+" successfully!", "success");
					$(".add_credit")[0].reset() 
				}
			}
		})
	})

	$(document).on('click','#delete_credit',function(){
		var id = $(this).data('id');
		swal({
	      title: "Are you sure?",
	      text: "You want to delete this credit?",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonClass: "btn-danger",
	      confirmButtonText: "Yes, delete it!",
	      cancelButtonText: "No",
	      closeOnConfirm: false,
	      closeOnCancel: true
	    },
	    function(isConfirm) {
	      if (isConfirm) {
	        $.ajax({
	          url: "/payments/update_credit",
	          type: "POST",
	          dataType: "json",
	          data: {
	            id  : id,
	            type: 'delete'
	          },
	          success: function(data) {
	            show_credit();
	            if (data==1) {
	              swal("Success!", "Credit deleted successfully!", "success");
	            }
	          }
	        })
	        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
	      }
	    });
	})
	$(document).on('click','#edit_plan',function(){
		var id = $(this).data('id');
		$.ajax({
          	url: "/payments/get_single_credit/"+id,
          	type: "POST",
          	dataType: "json",
          	success: function(data) {
        		$('#id').val(data['id']);
        		$('#credit_name').val(data['credit_name']);
        		$('#currency-field').val('$'+data['credit_price']);
        		$('#total_credit').val(data['credit_amount']);
        		$('#modal_credit').modal('show');
            }
      
        })
	})

})


