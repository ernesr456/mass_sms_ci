var base_url = $('#base_url').val();
$(document).on('click','#close_qoute',function(){
	$('#blockquote').hide(500);
	$('.quote_id').val(0);
})

$(document).on('click','#quote_message',function(){
	$('#blockquote').show(500);
	var message = $(this).data('message');
	var filename = $(this).data('file');
	var from_message = $(this).data('from');

	var content = '';
	if (filename!="") {
		content = '<img src="'+base_url+"assets/MMS_Images/"+filename+'" class="figure-img img-fluid" alt="A generic square placeholder image with rounded corners in a figure." style="max-height: 100px !important"><p class="mb-0" id="blockquote_message" style="font-size: 15px !important"><i>'+message+'</i></p>';
	}else{
		content = '<p class="mb-0" id="blockquote_message"><i>'+message+'</i></p>';
	}
	$('.blockquote_content').html(content);
	$('#blockquote_from').text(from_message);
	$('.quote_id').val($(this).data('id'));
})


$('.chatmessage, .forward_message').on('keydown keyup', function(){
	var character = $(this).val().length;
	$('#forward_count').text(character+'/160');
	$('#chatmessage_count').text(character+'/160');
});


$(document).ready(function(){
    
	var convo_limit = 6;
	//global variable contacts and forward contacts
	var search = $('.chat-search').val();
	var limit_contact = 15 ;
	var limit_contact2 = 15;
	var scroll_bottom = 0;

	var filter = $('#msg_filter').data('value');
	var contact = $('.active.list-contacts').data('title');

	var id = 0;
	var active_id  = $('#active_contact').val();

	listContacts(contact,limit_contact,search,active_id);
  	contact_list(contact,id,limit_contact2);

  	//end of global

	// Dropzone
	var all_file = new FormData();
	var update_all_file = new FormData();
	var edit_all_file = new Array();
		Dropzone.autoDiscover = false;
		if (document.getElementById('dropzone-add')) {
		  	var editDropzone = new Dropzone("#dropzone-add",{
			    parallelUploads: 2,
			    maxFilesize: 5,
			    createImageThumbnails: true,
			    clickable: true,
		    	thumbnailWidth: 200,
		    	thumbnailHeight: 200,
			    filesizeBase:    1000,
			    addRemoveLinks:  true,
			    maxFiles: 1,
			    acceptedMimeTypes: "image/jpeg,image/gif,image/png,image/bmp,text/vcard,text/csv,.csv,,text/rtf,.rtf,.vfu,text/richtext,text/calendar,text/directory,application/pdf,audio/basic,audio/L24,audio/mp4,.mp4,audio/mpeg,audio/ogg,audio/vorbis,audio/vnd.rn-realaudio,audio/vnd.wave,audio/3gp,audio/3gpp2,audio/ac3,audio/vnd.wave,audio/webm,audio/amr-nb,audio/amr",
			    // acceptedFiles: ".gif,.png,.jpg,.gif,.au,.mp4a,.mpga,.oga,.3ga,.3gpp,.ac3,.wav,.weba,.awb,.amr,.video,.mpeg,.mp4,.qt,.webm,.3gp,.3g2,.3gpp-tt,.h261,.h263,.h264,.vcf,.csv,.rtf,.rtx,.ics,.pdf",
			    removedfile: function(file) {
			    	if (file==edit_all_file[0]) {
		    			edit_all_file.pop();

			    	}

			    	var files = all_file.getAll("files[]");
			    	all_file.delete('files[]');
			    	for(i=0;i<files.length;i++){
			    		if (files[i].name==file.name) {
			    		}else{
			    			all_file.append('files[]',file,file.name);
			    		}
			    	}

			    	var files = update_all_file.getAll("files[]");
			    	update_all_file.delete('files[]');
			    	for(i=0;i<files.length;i++){
			    		if (files[i].name==file.name) {
			    		}else{
			    			update_all_file.append('files[]',file,file.name);
			    		}
			    	}

				    var _ref;
				    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			    },
			});
		}
	  	// Mock the file upload progress (only for the demo)
	  	Dropzone.prototype.uploadFiles = function(files) {

		    var minSteps         = 6;
		    var maxSteps         = 60;
		    var timeBetweenSteps = 100;
		    var bytesPerStep     = 100000;
		    var isUploadSuccess  = Math.round(Math.random());

		    var self = this;

		    for (var i = 0; i < files.length; i++) {

		      	var file = files[i];
		      	var totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));
		      	for (var step = 0; step < totalSteps; step++) {
		        	var duration = timeBetweenSteps * (step + 1);
		        	setTimeout(function(file, totalSteps, step) {
			         	return function() {
				            file.upload = {
				              progress: 100 * (step + 1) / totalSteps,
				              total: file.size,
				              bytesSent: (step + 1) * file.size / totalSteps
				            };
			            	self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
				            if (file.upload.progress == 100) {
				            	if (edit_all_file.length>0) {
				            		file.status = Dropzone.ERROR;
		            				self.emit('error', file, 'You can not upload any more file', null);
									self.emit('complete', file);
									self.processQueue();
				            	}else{
				            		update_all_file.append('files[]',file,file.name);
				            		file.status =  Dropzone.SUCCESS;
					                self.emit('success', file, 'success', null);
					              	self.emit('complete', file);
					             	self.processQueue();
				            	}

				            	all_file.append('files[]',file,file.name);
				            }
			          	};
		        	}(file, totalSteps, step), duration);
		    	}
	    	}
		};
	//End of Dropzone
	var table_template = $('#view_template').DataTable({
        "pageLength": 10,
        "serverSide": true,
        "ajax": {   
            "url": '/conversation/show_template',
            "type": 'POST',
            "data":function(data) {
                data.type= "conversation";
            },
        },
        'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'width': '1%',
         'className': 'dt-body-center',
         'render': function(data, type, full, meta) {
            return '<input type="checkbox" name="selected_id" id="selected" value="' +
                $('<div/>').text(data).html() + '">';
        }
      }],
      'order': [[1, 'asc']],
      'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];
      }
   });
   
	function reload_table_template(){
		table_template.ajax.reload(null,false);
	}
	//send message
	$(document).on('submit','.send_message_conversation',function(e){
		var send_message = new FormData();
		e.preventDefault();
		var message = $('.chatmessage').val();
		var group = $('.active.list-contacts').data('title');
		var contact = $('.chatuser.activechat').data('id');
		var quote = $('.quote_id').val();
		var btn = Ladda.create(document.querySelector('#btn_sendMessage'));
		btn.start();

		if ($('#template_id').val()>0) {
			update_all_file.append('timezone', Intl.DateTimeFormat().resolvedOptions().timeZone);
			update_all_file.append('message',message);
			update_all_file.append('contact',contact);
			update_all_file.append('group',group);
			update_all_file.append('quote',quote);
			update_all_file.append('file',JSON.stringify(edit_all_file));
			send_message = update_all_file;
		}else{
			all_file.append('timezone', Intl.DateTimeFormat().resolvedOptions().timeZone);
			all_file.append('message',message);
			all_file.append('contact',contact);
			all_file.append('group',group);
			all_file.append('quote',quote);
			send_message = all_file;
		}
		$.ajax({
			url: "/conversation/send_message",
			dataType: "json",
			type: "POST",
			data:send_message,
			contentType:false,
			cache:false,
			processData:false,
			success:function(data){
				btn.stop();
				if (data==true) {
					var bottom = 0;
					scroll_bottom--;
					var contact = $('.online.chatuser.activechat').data('id');
					conversation(contact,convo_limit,bottom);
					$('.chatmessage').val('');
					$('.quote_id').val(0);
					$('#blockquote').hide(500);
					$("#dropzone-add").hide(500);
				}else if(data==false){
					swal("Error!", "You can't send a message please contact your administrator.", "error");
				}else if (data==2) {
					swal("Failed!", "You don't have enough credit to send this message.", "info");
				}

			},
            error: function (jqXHR, exception) {
				btn.stop();
				swal("Error!", "You can't send a message please contact your administrator.", "error");
			}
		})
	})

	//show dropzone file upload
	$(document).on('click','#show_attach',function(){
		// $("#dropzone-add").show(500);
		if ($("#dropzone-add").is(":visible")) {
			$("#dropzone-add").hide(500);
		}else{
			$("#dropzone-add").show(500);
		}
	})

    $(document).on('click','#select_template',function(){
    	edit_all_file=[];
    	var btn = Ladda.create(this);
    	btn.start();
    	Dropzone.forElement('#dropzone-add').removeAllFiles(true);
    	$('#template_id').val($(this).data('id'));
    	var id = $(this).data('id');
    	$.ajax({
    		url: "/template/fetch_template",
    		dataType: "json",
    		type: "POST",
    		data:{
    			id:id
    		},
    		success:function(data){
    			if (data.length>1) {
					$("#dropzone-add").show(500);
    				var filename = data[1].name;
    				var path = base_url+''+data[1].path;
    				var size = data[1].size;
    				var mockFile = { name: filename, size: size, path:path };
		    		editDropzone.files.push(mockFile);
		          	editDropzone.emit("addedfile", mockFile);
		          	editDropzone.emit("thumbnail", mockFile, path);
		          	editDropzone.emit("complete", mockFile);
		          	edit_all_file.push(mockFile);
    			}else{
    				$("#dropzone-add").hide(500);
    				btn.stop();
    				$('#show_templates').modal('hide');
    				$('.chatmessage').val(data[0].message);
    			}
    		}
    	})
    })


	$(document).on('click','#message_filter',function(){
		$('#active_contact').val('');
		$("#msg_filter").text($(this).data('name'));
		var value = $(this).data('value');
		$('#msg_filter_input').val(value);
		var contact = $('.active.list-contacts').data('title');
		var search = $('.chat-search').val();
		var active_id  = $('#active_contact').val();
		$(".chat-messages").empty();
		$("#contact_name").empty();
		$("#contact_number").empty();
		$("#contact-name").empty();
		$("#contact-number").empty();
		$("#contact-created").empty();
		$("#contact-group").empty();
		$("#last-activity").empty();
		$("#contact-email").empty();
		$('.chat-contacts').empty();
		scroll_bottom--;
		convo_limit = 6;
		listContacts(contact,limit_contact,search,active_id);
	})

	$("#msg_sent_all,#msg_to,#msg_from").click(function(){
		$('#msg_sent_filter').val($(this).data('id'));
		var contact = $('.online.chatuser.activechat').data('id');
		var bottom = 0;
		conversation(contact,convo_limit,bottom);
	});

	$(document).on('click','#selected_contact',function(e){
		$('.chat-messages').empty();
		$('.online.activechat').removeClass('activechat');
		$('#active_contact').val($(this).data('id'));
		// $("a").removeClass("active");
		var date = $(this).data('date');
		var d = "";
		var contact = $('.active.list-contacts').data('title');
		if (date===null) {
			d = "No Activity";
		}else{
			d = date;
		}

		var group = "";

		$('#last-activity').text('Last Activity: '+d);
		$('#contact_name').text($(this).data('name'));
		if (contact=='single'){
			if ($(this).data('group')===null) {
				group = "none";
			}else{
				group = $(this).data('group');
			}
			$('#contact_number').text('+'+$(this).data('number'));
			$('#assign_texter').text($(this).data('assign'));
			$('#assign_texter').text($(this).data('assign'));
			$('#contact-name').text('Name: '+$(this).data('name'));
			$('#contact-number').text('Contact Number: +'+$(this).data('number'));
			$('#contact-email').text('Email: '+$(this).data('email_address'));
			$('#contact-group').text('Group: '+group);
		}else{
			$('#contact_number').text('with '+$(this).data('total')+' contacts')
			$('#assign_texter').text($(this).data('assign'));
			$('#contact-name').text('Group Name: '+$(this).data('name'));
			$('#contact-number').text('Total Contacts: '+$(this).data('total'));
			$('#contact-email').text('');
		}
		const ths = $(this);
		ths.addClass('activechat');
		var id = $(this).data('id');
		var convo_limit = 6;
		var bottom = 0;
		scroll_bottom--;
		conversation(id,convo_limit,bottom);
	})
	$(document).on('click','.list-contacts',function(){
		$('.chat-messages').empty();
		$("#con_id").val("");
		$('#active_contact').val("");		
		var title = $(this).data('title');
		var contact = $('#msg_filter').data('value');
		//empty chat content and table contact content
		$(".chat-content").empty();
		$("#table_contact").empty();

		$("#assign_texter").empty();
		$("#contact-name").empty();
		$("#contact-number").empty();
		$("#contact-created").empty();
		$("#contact-email").empty();
		$("#last-activity").empty();
		$("#contact-group").empty();
		$("#type_message").val($(this).data('title'));
		$("#thecontact_name").html("");

		if (title=='group') {
			$('#show_all_contact').show();
		}else{
			$('#show_all_contact').hide();
		}
		scroll_bottom--;
		convo_limit = 6;
		var active_id  = $('#active_contact').val();
		var saerch = $('.chat-search').val();
		listContacts(title,limit_contact,search,active_id);

	})

	//search for contacts
	$(document).on('keyup','.chat-search', function(e){
		var filter = $('#msg_filter').data('value');
		active_id = "";
	    var search = $(this).val();
	    $('#active_id').val('');
	    active_id = '';
		var contact = $('.active.list-contacts').data('title');

	    listContacts(contact,limit_contact,search,active_id);
	})

  	function listContacts(contact,limit_contact,search,active_id){
  		var filter = $('#msg_filter_input').val()
  		const queryString = window.location.search;
  		const urlParams = new URLSearchParams(queryString)
  		const url_id = urlParams.get('id');
  		$.ajax({
  			url:"/conversation/listContacts",
			dataType: "json",
			type: "POST",
			data:{
				filter:filter,
				contact:contact,
				limit_contact:limit_contact,
				search: search
			},
			success:function(data){
				var contact = '';
				for(i=0;i<data.length;i++){
					unread = "";
					if (data[i].count_status>0) {
						unread = '<div class="badge badge-outline-success">'+data[i].count_status+'</div>';
					}
					if ($('.active.list-contacts').data('title')=='single') {
						contact +=
	                    '<a href="javascript:void(0)" id="selected_contact" class="list-group-item list-group-item-action online chatuser" data-id="'+data[i].id+'" data-name="'+data[i].firstName+' '+data[i].lastName+'" data-session="'+data[i].created_by+'" data-sent="'+data[i].sent_time+'" data-email_address="'+data[i].email+'" data-status="'+data[i].status+'" data-assign="'+data[i].assign_name+'" data-date="'+data[i].last_activity+'" data-group="'+data[i].group_name+'" data-number = "'+data[i].number+'">'+
	                        '<div class="media-body ml-3" style="text-transform:capitalize">'+
	                            data[i].firstName+' '+data[i].lastName+
	                            '<div class="chat-status small">'+
	                                '<span></span>&nbsp; +'+data[i].number+'</div>'+
	                        '</div>'+
	                        unread+
	                    '</a>';
	                }else{
	                	contact +=
	                    '<a href="javascript:void(0)" id="selected_contact" class="list-group-item list-group-item-action online chatuser" data-id="'+data[i].gid+'" data-name="'+data[i].group_name+'" data-total="'+data[i].total_contacts+'" data-assign="'+data[i].assign_name+'" data-date="'+data[i].last_activity+'">'+
	                        '<div class="media-body ml-3">'+
	                            data[i].group_name+
	                            '<div class="chat-status small">'+
	                                '<span></span> with '+data[i].total_contacts+' contacts</div>'+
	                        '</div>'+
	                        unread+
	                    '</a>';
	                }
				}
				$('.chat-contacts').empty();
				$('.chat-contacts').html(contact);
				var id = "";
				if (data.length>0) {
					if (active_id!=="") {
						$('a[data-id = '+active_id+']').addClass('activechat');
						id = active_id;
					}else if(url_id!==null) {
						$('a[data-id = '+url_id+']').addClass('activechat');
						id = url_id;
					}else{
						$('#active_contact').val($('#selected_contact').first().data('id'));
						$('#selected_contact').first().addClass('activechat');
						var name = $('#selected_contact').first().data('name');
						var contact = $('.list-contacts.active').data('title');
						var date = $('#selected_contact').first().data('date');

						var d = "";
						if (date===null) {
							d = "No Activity";
						}else{
							d = date;
						}
						$('#last-activity').text('Last Activity: '+d);
						if (contact=='single'){
							var group = "";
							if ($('#selected_contact').first().data('group')===null) {
								group = "none";
							}else{
								group = $('#selected_contact').first().data('group');
							}
							$('#contact_name').first().text($('#selected_contact').first().data('name'));
							$('#contact_number').text('+'+$('#selected_contact').first().data('number'));
							$('#assign_texter').text($('#selected_contact').first().data('assign'));
							$('#assign_texter').text($('#selected_contact').first().data('assign'));
							$('#contact-name').text('Name: '+$('#selected_contact').first().data('name'));
							$('#contact-number').text('Contact Number: +'+$('#selected_contact').first().data('number'));
							$('#contact-email').text('Email: '+$('#selected_contact').first().data('email_address'));
							// $('#contact-email').text('Email: +'+$('#selected_contact').first().data('email_address'));
							$('#contact-group').text('Group: '+group);
						}else{
							$('#contact_name').first().text(name);
							$('#contact_number').first().text('with '+$('#selected_contact').first().data('total')+' contacts');
							$('#assign_texter').text($('#selected_contact').first().data('assign'));
							$('#contact-name').text('Group Name: '+$('#selected_contact').first().data('name'));
							$('#contact-number').text('Total Contacts: '+$('#selected_contact').first().data('total'));
							$('#contact-email').text('');
						}
					}

					//load conversation
					id = $('.online.chatuser.activechat').data('id');
					$('#btn_sendMessage, #show_attach, #message_template').removeAttr('disabled');
				}else{
					var html = '';
					html =
						'<div class="card-body">'+
							'<h5 class="display-5" style="text-align: center;">No contact information found</h5>'+
						'</div>';
					$('.chat-contacts').html(html);
					$('#btn_sendMessage, #show_attach, #message_template').attr("disabled", true);
				}
				var bottom = 0;
				conversation(id,convo_limit,bottom);
			}
  		})
  	}

    $('.chat-scroll,#team-member1').on('scroll', function() {
    	var ths = $(this);
    	var bottom = 0;
		if(Math.round(ths.scrollTop() + ths.innerHeight(), 10) >= Math.round(ths[0].scrollHeight, 10)&&$('.chat-contacts').is(':hover')) {
            limit_contact = limit_contact + 10;
    		var filter = $('#msg_filter').data('value');
			var contact = $('.active.list-contacts').data('title');
			var active_id  = $('#active_contact').val();
			var search = $('.chat-search').val();
		  	listContacts(contact,limit_contact,search,active_id);
        }else if(Math.round(ths.scrollTop() + ths.innerHeight(), 10) >= Math.round(ths[0].scrollHeight, 10)&&$('#team-member1').is(':hover')){
            limit_contact2 = limit_contact2 + 10;
		  	var contact = $('.active.list-contacts').data('title');
		  	var id = 0;
		  	var active_id  = $('#active_contact').val();
		  	contact_list(contact,id,limit_contact2)
        }

	    var scrollTop = $(this).scrollTop();
		if (scrollTop <= 0&&$('.chat-messages').is(':hover')) {
			convo_limit = convo_limit +5;
			var contact = $('.online.chatuser.activechat').data('id');
			conversation(contact,convo_limit,bottom);
		   
		}
    })
    setInterval(function(){
		var contact = $('.active.list-contacts').data('title');
		var filter = $('#msg_filter').data('value');
		var search = $('.chat-search').val();
        var active_id  = $('#active_contact').val();
        listContacts(contact,limit_contact,search,active_id);
        reload_table_template();
	},60000);

	$(function() {
    	new PerfectScrollbar(document.getElementById('team-member1'));
	});

  	function conversation(contact,convo_limit,bottom){
  		var filter = $('#msg_sent_filter').val();
  		$.ajax({
  			url:"/conversation/show_conversation",
			dataType: "json",
			type: "POST",
			data:{
				contact:contact,
				filter:filter,
				type: $('.active.list-contacts').data('title'),
				limit: convo_limit
			},
			success:function(data){
				$("#loadingDiv").fadeOut();
				var chat = '';
				var data1 = data.reverse();
				var date = "";
				var date_time = '';
				for(i=0;i<data.length;i++){
					var tem_qoute= '';

					for(a=0;a<data1.length;a++){
						var id = data[i].contact_id;
						var data1_type = parseInt(data1[a].msg_type);
						var from = data1[a].from_id;
						if(data[i].parent_id==data1[a].id){
							if (data1[a].contact_id==data1[a].to_id) {
								sent_to = '<p class="mb-0" style="text-align: left; text-transform: capitalize; font-size:14px !important"> Send From: '+data[i].firstName+' '+data[i].lastName+', +'+data1[a].number+'</p><br>';
							}else{
								sent_to = '<p class="text-white mb-0" style="text-align: left; text-transform: capitalize; font-size:14px !important"> Sent to: '+data[i].firstName+' '+data[i].lastName+', +'+data1[a].number+'</p><br>';
							}
							if (data1[a].filename!="") {
								src = base_url+"assets/MMS_Images/"+data1[a].filename;

								file = 
								'<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">'+
                                	'<a href="'+src+'" itemprop="contentUrl" data-size="1920x1280"><img src="'+src+'" itemprop="thumbnail" alt="Image description"></a>'+
                            	'</figure>';
							}else{
								file = '';
							}
							tem_qoute = 
							'<blockquote class="blockquote" style="max-height: unset !important">'+
								sent_to+
								file+
								'<p class="mb-0" style="font-size:15px;"><i>'+data1[a].message+'</i></p>'+
                               '<footer class="blockquote-footer text-white" style="font-size: 13.5px !important">'+data[i].sent_time+
                               ' </footer>'+
                            '</blockquote>'+
    						'<hr>';
						}else if(data[i].parent_id<1){
							tem_qoute = '';
						}
					}

					// if (date_time==data[i].sent_time) {
					// 	date_time ="";
					// }else{
					// 	date_time = data[i].sent_time
					// }

					var contact_id = data[i].contact_id;
					var from_id = data[i].from_id;
					var time = '';
					var file = '';
					var message = '';

					if (data[i].message!="") {
						message = '<p class="mb-0">'+data[i].message+'</p>';
					}

					if (data[i].filename!="") {
						src = base_url+"assets/MMS_Images/"+data[i].filename;
						var src_size = get_image_size(src);
						
						file = '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">'+
                                '<a href="'+src+'" itemprop="contentUrl" data-size="1920x1280"><img src="'+src+'" itemprop="thumbnail" alt="Image description"></a>'+
                            '</figure>';
					}else{
						file = '';
					}
					
					if (data[i].status==2) {
						time = '<p class="text-muted mb-0"><i class="fas fa-exclamation mr-2" style="color:red;"></i> Failed to send a message</p>';
					}else if(data[i].status==3){
						time = '<p class="text-muted mb-0"><i class="fas fa-exclamation mr-2" style="color:red;"></i> Unverified Number</p>';
					}else{
						time = '<p class="text-muted mb-0 convo-list'+data[i].id+'"><i class="ion ion-ios-timer mr-2"></i>'+data[i].sent_time+'</p>';
					}

					if (data[i].contact_id==data[i].from_id&&data[i].sms_status=='received') {
						chat +=
                    	'<div class="row mb-3 received-chat" data-id="'+data[i].id+'" data-status="'+data[i].status+'">'+
                            '<div class="col-auto p-r-0">'+
                                '<div class="btn-group">'+
                                    '<button type="button" class="btn btn-sm btn-default icon-btn borderless btn-round md-btn-flat dropdown-toggle hide-arrow" data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></button>'+
                                    '<div class="dropdown-menu dropdown-menu-left" id="dropdown-option">'+
                                    	'<a id="forward_message" data-id="'+data[i].id+'" data-from="'+data[i].contact_id+'" class="dropdown-item" data-toggle="modal" data-target="#show_con_contact">'+
                                    		'<i class="fas fa-reply text-muted"></i> &nbsp; Forward'+
                                    	'</a>'+
                                    	'<a id="quote_message" data-id="'+data[i].id+'" data-message="'+data[i].message+'" data-file="'+data[i].filename+'" data-from=" Sent from:'+data[i].firstName+' '+data[i].lastName+' , +'+data[i].number+'" data-from_id="'+data[i].contact_id+'" class="dropdown-item">'+
	                                		'<i class="ion ion-md-quote text-muted"></i> &nbsp; Quote'+
	                                	'</a>'+
                                    '</div>'+
                             	'</div>'+
                            '</div>'+
                            '<div class="col">'+
                                '<div class="msg bg-light ">'+
                                    '<p class="text-muted mb-0" style="text-align: left; text-transform: capitalize;"> Sent From: '+data[i].firstName+' '+data[i].lastName+', +'+data[i].number+'</p><br>'+
                                    file+
                                    message+
                                '</div>'+
                               	time+
                            '</div>'+
                        '</div>';
					}else{
						chat +=
                    	'<div class="row mb-3 send-chat text-white" data-id="'+data[i].id+'">'+
                            '<div class="col">'+
                                '<div class="msg bg-info">'+
                                	tem_qoute+
                                	'<p class="mb-0" style="text-align: left; text-transform: capitalize"> Sent to: '+data[i].firstName+' '+data[i].lastName+', +'+data[i].number+'</p><br>'+
                                	file+
                                    message+
                                '</div>'+
                                time+
                           ' </div>'+
                            '<div class="col-auto p-l-0">'+
                                '<div class="btn-group">'+
	                                '<button type="button" class="btn btn-sm btn-default icon-btn borderless btn-round md-btn-flat dropdown-toggle hide-arrow" data-toggle="dropdown">'+
	                                	'<i class="fas fa-ellipsis-v"></i>'+
	                                '</button>'+
	                                '<div class="dropdown-menu dropdown-menu-right" id="dropdown-option">'+
	                                	'<a id="forward_message" data-id="'+data[i].id+'" data-message="'+data[i].message+'" data-file="'+data[i].filename+'" data-from="'+data[i].contact_id+'" class="dropdown-item" data-toggle="modal" data-target="#show_con_contact">'+
	                                		'<i class="fas fa-reply text-muted"></i> &nbsp; Forward'+
	                                	'</a>'+
	                                	'<a id="quote_message" data-id="'+data[i].id+'" data-message="'+data[i].message+'" data-file="'+data[i].filename+'" data-from=" Sent to:'+data[i].firstName+' '+data[i].lastName+', +'+data[i].number+'" data-from_id="'+data[i].contact_id+'" class="dropdown-item">'+
	                                		'<i class="ion ion-md-quote text-muted"></i> &nbsp; Quote'+
	                                	'</a>'+
	                            	'</div>'+
                            	'</div>'+
                            '</div>'+
                        '</div>';
					}
					
				}
				$('.chat-messages').html(chat);
				if(data.length>0){
				    to_bottom(data[data.length-1].id);
				}
				// $('.convo-list'+data[data.length-1].id).attr('tabindex', -1).focus();
			}
		})
  	}
  	function to_bottom(last_id){
  		if (scroll_bottom==0) {
  			$('.convo-list'+last_id).attr('tabindex', -1).focus();
  			scroll_bottom++;
  		}
  	}

  	function get_image_size(src){
	    var img = new Image();
	    var size = new Array();
	    img.addEventListener("load", function(){
	    	size.push(this.naturalWidth+'x'+this.naturalHeight);
	    	// size = this.naturalWidth+'x'+this.naturalHeight;
	        // alert( this.naturalWidth +' '+ this.naturalHeight );
	    });
	    img.src = src;
	    return size
  	}
  	
	//search for contacts
	$(document).on('keyup','.search_forward_contacts', function(e){
		e.preventDefault();
		var value = $(this).val().toLowerCase();
		$('.modal_contact_list tr').filter(function(){
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	})
  	$(document).on('click','#forward_message',function(){
  		var id = $(this).data('id');
  		var con_id = $(this).data('from');
  		$('#forward_id').val(id);
  		$.ajax({
    		url:"/conversation/conversation_thread",
			dataType: "json",
			type: "POST",
			data:{
				id:id
			},
			success:function(data){
				var src = base_url+'assets/MMS_Images/'+data['filename'];
				var html = '';
				if (data['filename']!=""){
					html +=
              		'<div class="col-md-6 col-xl-4">'+
                        '<div class="card bg-dark border-0 text-white" id="forward_img">'+
                        	'<img class="card-img" src='+src+' alt="Card image">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-6 col-xl-8">'+
                        '<input type="hidden" id="con_forward_message" name="contact_id">'+
                        '<textarea id="autosize-demo" rows="3" placeholder="Type message here...." maxlength="1200" class="form-control forward_message" name="chatmessage" rows="10" >'+data['message']+'</textarea>'+
                    '</div>';
				}else{
					html+=
                    '<div class="col-md-6 col-xl-12">'+
                        '<input type="hidden" id="con_forward_message" name="contact_id">'+
                        '<textarea id="autosize-demo" rows="3" placeholder="Type message here...." maxlength="1200" class="form-control forward_message" name="chatmessage" rows="10" >'+data['message']+'</textarea>'+
                    '</div>';
				}
				
				$('#forward_message_content').html(html);
			}
    	})
  	})
  	$(document).on('click','.forward-list-contacts',function(){
  		var contact = $(this).data('title');
  		var id = $(this).data('id');
  		contact_list(contact,id,limit_contact2);
  	})

	function contact_list(contact,id,limit_contact){
		var filter = 2;
		$('#loadingDiv').show();
		$.ajax({
			url:"/conversation/listContacts",
			dataType: "json",
			type: "POST",
			data: {
				contact:contact,
				filter:filter,
				limit_contact:limit_contact
			},
			success:function(data){
				var modal_contact ='';
				for(i=0; i<data.length; i++){
					var disable = '';
					var text = '';
					var number = '';
					var name = '';
					if (contact=='single') {
						if (data[i].cstatus==2||data[i].cstatus==0) {
							disable = 'disabled';
							text = 'text-danger';
						}else{
							text = 'text-muted';
						}
						number = '<p class="'+text+' mb-0"> +'+data[i].number+'</p>';
						name = data[i].firstName+' '+data[i].lastName;
					}else{
						name = data[i].group_name;
						number = '<p class="text-muted" mb-0"> with '+data[i].total_contacts+' contacts</p>';
					}
					modal_contact +=
    			        '<tr>'+
    			        	'<td width="50%">'+
                                '<div class="d-inline-block align-middle">'+
                                    '<h6 class="mb-1">'+name+'</h6>'+
                                    number+
                                '</div>'+
                            '</td>'+

                            '<td width="50%"><button type="button" class="btn btn-round btn-info pull-right" id="send_forward_msg" data-title='+data[i].title+' data-id='+data[i].id+' '+disable+' '+disable+'>Send</button></td>'+
                        '</tr>';
	                $('.modal_contact_list').html(modal_contact);
	            }
			}
		});
	}
	$(document).on('mouseover','.received-chat',function(){
		var status = $(this).data('status');
		var id = $(this).data('id');
		if (status==1) {
			$.ajax({
				url: "/conversation/update_message",
				dataType: "json",
				type: "POST",
				data:{
					id:id
				},success:function(data){
					if(data==true){
						var filter = $('#msg_filter').data('value');
						var contact = $('.active.list-contacts').data('title');
						listContacts(filter,contact,limit_contact,search);
					}
				}
			})
		}
	})
	$(document).on('click','#send_forward_msg',function(e){
		var button = Ladda.create(this);
		var contact_id = $('.chatuser.activechat').data('id');
		var id = $('#forward_id').val();
		var chat_id  = $(this).data('id');
		var message = $('.forward_message').val();
		var group = $('.active.forward-list-contacts').data('title');
		var folder_id = $('.chatuser.activechat').data('id');
		button.start();
		$.ajax({
			url: "/conversation/send_message",
			dataType: "json",
			type: "POST",
			data:{
				forward_message: true,
				id:id,
				group:contact,
				contact:chat_id,
				message:message,
				quote:0,
				group:group,
				folder_id:folder_id,
				timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,

			},success:function(data){
				button.stop();


				// showChat($('.chatuser.activechat').data('id'),
				// 	$('.chatuser.activechat').data('title'),
				// 	$('.chatuser.activechat').data('session'));
				// if (data==true) {
				// 	$('#save_message').trigger('reset');
				// 	button.stop();
				// 	$('.chatmessage').val("");
				// }else if(data==false){
				// 	button.stop();
				// 	toastr.error(
				// 		'You don&#39;t have enough credit to send a message'
				// 	);
				// }
				// else{
				// 	button.stop();
				// 	if (title=='single') {
				// 		toastr.error(
				// 			'This number is deactivated'
				// 		);
				// 	}else{
				// 		toastr.warning(
				// 			'Some of number are deactivated'
				// 		);
				// 	}
				// }
			},
			error: function (jqXHR, exception) {
				button.stop();
				// toastr.error(
				// 	'Please contact your administrator',
				// 	'Twilio Credential is Invalid'
				// );
			}
		})
	})
})
