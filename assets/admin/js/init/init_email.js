$(document).on("change", "#send_to_type", function() {
    var send_to_type = $(this).val();
    if (send_to_type == 'specific_emails') {
        $('#select_emails_group').removeClass('d-none');
    } else {
        $('#select_emails_group').addClass('d-none');
    }
});

$(function() {
    $('.select2-demo').each(function() {
        $(this)
        .wrap('<div class="position-relative"></div>')
        .select2({
            placeholder: 'Choose emails',
            dropdownParent: $(this).parent()
        });
    })
});

if (!window.Quill) {
    $('#message-editor,#message-editor-toolbar').remove();
    $('#message-editor-fallback').removeClass('d-none');
} else {
    $('#message-editor-fallback').remove();
    var compose_msg = new Quill('#message-editor', {
        modules: {
            toolbar: {
                container:'#message-editor-toolbar',
                handlers: {
                    'image': imageHandler
                  }
            }
        },
        placeholder: 'Type your message...',
        theme: 'snow',
    });
}

function imageHandler() {
    let fileInput = this.container.querySelector('input.ql-image[type=file]');
    if (fileInput == null) {
      fileInput = document.createElement('input');
      fileInput.setAttribute('type', 'file');
      fileInput.setAttribute('accept', 'image/png, image/jpeg, image/bmp, image/x-icon');
      fileInput.classList.add('ql-image');
      fileInput.addEventListener('change', () => {
        if (fileInput.files != null && fileInput.files[0] != null) {
          let reader = new FileReader();
          reader.onload = (e) => {
            var _this = this;
            $.ajax({
                url: base_url+'email/save_image',
                data: {email_img:e.target.result},
                type:'POST',
                success:function(data){
                    var range = _this.quill.getSelection(true);
                    var Delta = Quill.import('delta');
                    _this.quill.updateContents(new Delta()
                      .retain(range.index)
                      .delete(range.length)
                      .insert({ image: base_url+'assets/admin/img/email/'+data })
                    , Quill.sources.USER);
                    fileInput.value = "";
                }
            })
            
          }
          reader.readAsDataURL(fileInput.files[0]);
        }
      });
      this.container.appendChild(fileInput);
    }
    fileInput.click();
}

const send_message = ()=>{
    var getMessage   = compose_msg.root.innerHTML; // Get the html content from the editor
    var from_name    = $('#from_name').val();
    var send_to_type = $('#send_to_type').val();
    var send_type    = $('#send_type').val();
    var send_to      = $('#send_to').val();
    var subject      = $('#subject').val();
    if(from_name&&send_to_type&&send_type&&subject){
        if(send_to_type=='specific_emails'){
            if(send_to.length > 0){
                send_this_message(getMessage);
            }else{
                $('#send_to').focus();
                toastr["error"]("Please choose emails to send.");
            }
        }else{
            send_this_message(getMessage);
        }
    }else{
        $('#subject').focus();
        toastr["error"]("Please fill up all fields below.");
    }
}

const send_this_message = (getMessage)=>{
    if(getMessage!=='<p><br></p>'){
        var sendBtn = Ladda.create(document.querySelector('#send_msg_btn'));
        var draftBtn = Ladda.create(document.querySelector('#draft_msg_btn'));
        sendBtn.start();
        draftBtn.start();
        var data  = $('#send_message_form').serializeArray();
        data.push({name: 'message', value: getMessage});
        $.ajax({
            url: base_url+'email/send_mail_message',
            type: 'POST',
            data: data,
            success:(res)=>{
                if(res==1){
                    toastr["success"]("Email sent successfully!");
                    setInterval('window.location.href=base_url+"email/sent"',1000);
                }else{
                    sendBtn.stop();
                    draftBtn.stop();
                    toastr["error"]("There was a problem sending your message. Please login again!");
                }
            },
            error: (err)=>{
                sendBtn.stop();
                draftBtn.stop();
                toastr["error"]("There was a problem sending your message. Please login again!");
            }
        });
    }else{
        toastr["error"]("Your message is required. Please write something.");
    }
}

const save_mail_as_draft = ()=>{
    var getMessage   = compose_msg.root.innerHTML;
    var sendBtn = Ladda.create(document.querySelector('#send_msg_btn'));
    var draftBtn = Ladda.create(document.querySelector('#draft_msg_btn'));
    sendBtn.start();
    draftBtn.start();
    var data  = $('#send_message_form').serializeArray();
    data.push({name: 'message', value: getMessage});
    $.ajax({
        url: base_url+'email/save_mail_as_draft',
        type: 'POST',
        data: data,
        success:(res)=>{
            if(res==1){
                toastr["success"]("Message saved to drafts!");
                setInterval('window.location.href=base_url+"email/drafts"',1000);
            }else{
                sendBtn.stop();
                draftBtn.stop();
                toastr["error"]("There was a problem saving the message. Please login again!");
            }
        },
        error: (err)=>{
            sendBtn.stop();
            draftBtn.stop();
            toastr["error"]("There was a problem saving the message. Please login again!");
        }
    });
}

const saveAllAsDraft = (page)=>{
    var items=[];
    $.each($(".msg_id:checked"), function(){
        items.push($(this).val());
    });
    var msg_id     = items.join(",");
    var cntItemChk = $(".msg_id:checked").length;
    if(cntItemChk > 0){
        $.ajax({
            url: base_url+'email/save_all_as_draft',
            type: 'POST',
            data: {msg_id: msg_id},
            success:(res)=>{
                if(res==1){
                    $.each($(".msg_id:checked"), function(){
                        $(this).closest("li").remove();
                    });
                    var cntItemChk = $(".msg_id").length;
                    if(cntItemChk==0){
                        $('.messages-list').html(
                            '<li class="list-group-item px-4">'+
                                '<a href="javascript:;" class="message-sender flex-shrink-1 d-block text-dark">No emails found.</a>'+
                                '<div class="message-date text-muted">Today</div>'+
                            '</li>'
                        );
                        $('#pagination').remove();
                    }
                    updateMailSidebox(page);
                    toastr["success"]("All messages saved to draft!");
                }else{
                    toastr["error"]("There was a problem saving the message. Please login again!");
                }
            },
            error: (err)=>{
                toastr["error"]("There was a problem saving the message. Please login again!");
            }
        });
    }else{ 
        toastr["error"]("No item selected.");
    }
}

const moveToTrash = (page)=>{
    var items=[];
    $.each($(".msg_id:checked"), function(){
        items.push($(this).val());
    });
    var msg_id     = items.join(",");
    var cntItemChk = $(".msg_id:checked").length;
    if(cntItemChk > 0){
        $.ajax({
            url: base_url+'email/move_to_trash',
            type: 'POST',
            data: {msg_id: msg_id},
            success:(res)=>{
                if(res==1){
                    $.each($(".msg_id:checked"), function(){
                        $(this).closest("li").remove();
                    });
                    var cntItemChk = $(".msg_id").length;
                    if(cntItemChk==0){
                        $('.messages-list').html(
                            '<li class="list-group-item px-4">'+
                                '<a href="javascript:;" class="message-sender flex-shrink-1 d-block text-dark">No emails found.</a>'+
                                '<div class="message-date text-muted">Today</div>'+
                            '</li>'
                        );
                        $('#pagination').remove();
                    }
                    updateMailSidebox(page);
                    toastr["success"]("All messages are moved to trash!");
                }else{
                    toastr["error"]("There was a problem saving the message. Please login again!");
                }
            },
            error: (err)=>{
                toastr["error"]("There was a problem saving the message. Please login again!");
            }
        });
    }else{ 
        toastr["error"]("No item selected.");
    }
}

const deletePermanently = (page)=>{
    var items=[];
    $.each($(".msg_id:checked"), function(){
        items.push($(this).val());
    });
    var msg_id     = items.join(",");
    var cntItemChk = $(".msg_id:checked").length;
    if(cntItemChk > 0){
        swal({
            title: "Delete Permamently?",
            text: "These mails will not be recovered. Continue?",
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete!",
            closeOnConfirm: false,
        },
        ()=>{
            $.ajax({
                url: base_url+'email/delete_permanently',
                type: 'POST',
                data: {msg_id: msg_id},
                success:(res)=>{
                    if(res==1){
                        $.each($(".msg_id:checked"), function(){
                            $(this).closest("li").remove();
                        });
                        var cntItemChk = $(".msg_id").length;
                        if(cntItemChk==0){
                            $('.messages-list').html(
                                '<li class="list-group-item px-4">'+
                                    '<a href="javascript:;" class="message-sender flex-shrink-1 d-block text-dark">No emails found.</a>'+
                                    '<div class="message-date text-muted">Today</div>'+
                                '</li>'
                            );
                            $('#pagination').remove();
                        }
                        updateMailSidebox(page);
                        swal('Deleted!','Messages deleted successfully!','success');
                    }else{
                        swal('Error!','There was a problem while deleting. Please login again!','error');
                    }
                },
                error: (err)=>{
                    swal('Error!','There was a problem while deleting. Please login again!','error');
                }
            });
        });
    }else{ 
        toastr["error"]("No item selected.");
    }
}

const updateMailSidebox = (page)=>{
    $.ajax({
        url: base_url+'email/update_mail_sidebox/',
        type: 'POST',
        data: { page:page },
        success:(res)=>{
            if(res!=0){
                $('#mail-sidebox').html(res);
            }
        }
    });
}

const selectAllMsg = (type)=>{
    if(type==1){
        $('#select-btn').attr('onclick', 'selectAllMsg(2)');
        $(".msg_id").prop('checked', true);
    }else{
        $('#select-btn').attr('onclick', 'selectAllMsg(1)');
        $(".msg_id").prop('checked', false);
    }
}