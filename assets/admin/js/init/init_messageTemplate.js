$(document).ready(function(){
	var base_url = $('#base_url').val();
	//Message Template Editor
	table_template = $('#view_template').DataTable({
	    "pageLength" : 10,
	    "serverSide": true,
	    "order": [[0, "asc" ]],
	    "ajax":{
	      url :  '/template/show_template',
	      type : 'POST'
	    },
	    'columnDefs': [{
	         'targets': 0,
	         'searchable':false,
	         'orderable':false,
	         'className': 'dt-body-center',
	         'render': function (data, type, full, meta){
	             return '<input type="checkbox" name="id[]" value="' 
	                + $('<div/>').text(data).html() + '">';
	         }
	      }],
	      'order': [1, 'asc'],
	      "createdRow": function(row, data, dataIndex){
	         $(row).attr("id", "tblRow_" + data[0]);
	    }
	}); // End of DataTable
	 function reload_table_template(){
		table_template.ajax.reload(null,false);
	}
	// Dropzone
	var all_file = new FormData();
	var update_all_file = new FormData();
	var edit_all_file = new Array();
		Dropzone.autoDiscover = false;
		if (document.getElementById('dropzone-add')) {
		  	var editDropzone = new Dropzone("#dropzone-add",{
			    parallelUploads: 2,
			    maxFilesize: 5,
			    createImageThumbnails: true,
			    clickable: true,
		    	thumbnailWidth: 200,
		    	thumbnailHeight: 200,
			    filesizeBase:    1000,
			    addRemoveLinks:  true,
			    maxFiles: 1,
			    acceptedMimeTypes: "image/jpeg,image/gif,image/png,image/bmp,text/vcard,text/csv,.csv,,text/rtf,.rtf,.vfu,text/richtext,text/calendar,text/directory,application/pdf,audio/basic,audio/L24,audio/mp4,.mp4,audio/mpeg,audio/ogg,audio/vorbis,audio/vnd.rn-realaudio,audio/vnd.wave,audio/3gp,audio/3gpp2,audio/ac3,audio/vnd.wave,audio/webm,audio/amr-nb,audio/amr",
			    // acceptedFiles: ".gif,.png,.jpg,.gif,.au,.mp4a,.mpga,.oga,.3ga,.3gpp,.ac3,.wav,.weba,.awb,.amr,.video,.mpeg,.mp4,.qt,.webm,.3gp,.3g2,.3gpp-tt,.h261,.h263,.h264,.vcf,.csv,.rtf,.rtx,.ics,.pdf",
			    removedfile: function(file) {
			    	if (file==edit_all_file[0]) {
		    			edit_all_file.pop();

			    	}

			    	var files = all_file.getAll("files[]");
			    	all_file.delete('files[]');
			    	for(i=0;i<files.length;i++){
			    		if (files[i].name==file.name) {
			    		}else{
			    			all_file.append('files[]',file,file.name);
			    		}
			    	}

			    	var files = update_all_file.getAll("files[]");
			    	update_all_file.delete('files[]');
			    	for(i=0;i<files.length;i++){
			    		if (files[i].name==file.name) {
			    		}else{
			    			update_all_file.append('files[]',file,file.name);
			    		}
			    	}

				    var _ref;
				    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			    },
			});
		}
		if (document.getElementById('dropzone-update')) {
		  	var editDropzone = new Dropzone("#dropzone-update",{
			    parallelUploads: 2,
			    maxFilesize: 5,
			    createImageThumbnails: true,
			    clickable: true,
		    	thumbnailWidth: 200,
		    	thumbnailHeight: 200,
			    filesizeBase:    1000,
			    addRemoveLinks:  true,
			    maxFiles: 1,
			    acceptedMimeTypes: "image/jpeg,image/gif,image/png,image/bmp,text/vcard,text/csv,.csv,,text/rtf,.rtf,.vfu,text/richtext,text/calendar,text/directory,application/pdf,audio/basic,audio/L24,audio/mp4,.mp4,audio/mpeg,audio/ogg,audio/vorbis,audio/vnd.rn-realaudio,audio/vnd.wave,audio/3gp,audio/3gpp2,audio/ac3,audio/vnd.wave,audio/webm,audio/amr-nb,audio/amr",
			    // acceptedFiles: ".gif,.png,.jpg,.gif,.au,.mp4a,.mpga,.oga,.3ga,.3gpp,.ac3,.wav,.weba,.awb,.amr,.video,.mpeg,.mp4,.qt,.webm,.3gp,.3g2,.3gpp-tt,.h261,.h263,.h264,.vcf,.csv,.rtf,.rtx,.ics,.pdf",
			    removedfile: function(file) {
			    	if (file==edit_all_file[0]) {
		    			edit_all_file.pop();

			    	}

			    	var files = all_file.getAll("files[]");
			    	all_file.delete('files[]');
			    	for(i=0;i<files.length;i++){
			    		if (files[i].name==file.name) {
			    		}else{
			    			all_file.append('files[]',file,file.name);
			    		}
			    	}

			    	var files = update_all_file.getAll("files[]");
			    	update_all_file.delete('files[]');
			    	for(i=0;i<files.length;i++){
			    		if (files[i].name==file.name) {
			    		}else{
			    			update_all_file.append('files[]',file,file.name);
			    		}
			    	}

				    var _ref;
				    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			    },
			});
		}


	  	// Mock the file upload progress (only for the demo)
	  	Dropzone.prototype.uploadFiles = function(files) {

		    var minSteps         = 6;
		    var maxSteps         = 60;
		    var timeBetweenSteps = 100;
		    var bytesPerStep     = 100000;
		    var isUploadSuccess  = Math.round(Math.random());

		    var self = this;

		    for (var i = 0; i < files.length; i++) {

		      	var file = files[i];
		      	var totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));
		      	for (var step = 0; step < totalSteps; step++) {
		        	var duration = timeBetweenSteps * (step + 1);
		        	setTimeout(function(file, totalSteps, step) {
			         	return function() {
				            file.upload = {
				              progress: 100 * (step + 1) / totalSteps,
				              total: file.size,
				              bytesSent: (step + 1) * file.size / totalSteps
				            };
			            	self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
				            if (file.upload.progress == 100) {
				            	if (edit_all_file.length>0) {
				            		file.status = Dropzone.ERROR;
		            				self.emit('error', file, 'You can not upload any more file', null);
									self.emit('complete', file);
									self.processQueue();
				            	}else{
				            		update_all_file.append('files[]',file,file.name);
				            		file.status =  Dropzone.SUCCESS;
					                self.emit('success', file, 'success', null);
					              	self.emit('complete', file);
					             	self.processQueue();
				            	}

				            	all_file.append('files[]',file,file.name);
				            }
			          	};
		        	}(file, totalSteps, step), duration);
		    	}
	    	}
		};
	//End of Dropzone

	$(document).on('submit','#submitTemplate',function(e){
		var id = $('#template_id').val();
		var frmData = new FormData();
		var url = "";
		if (id!="") {
			update_all_file.append('subject', $('#update_template_subject').val());
			update_all_file.append('id', id);
			update_all_file.append('message', $('textarea.update_template_message').val());
			frmData = update_all_file;
			url = "update_template";
		}else{
			all_file.append('subject', $('#template_subject').val());
			all_file.append('id', id);
			all_file.append('message', $('textarea.template_message').val());
			frmData = all_file;
			url = "save_template";
		}

		$.ajax({
			url: "/template/"+url,
			dataType: "json",
			type: "POST",
			data:frmData,
			contentType:false,
			cache:false,
			processData:false,
			success:function(data){
				if (data==true) {
					Dropzone.forElement('#dropzone-add').removeAllFiles(true)
					$('#template_id').val('');
					$('#template_subject').val('');
					$('textarea.template_message').val('');
					reload_table_template();
					if (id!="") {
						swal("Updated!", "Your template has been updated.", "success");
					}else{
						swal("Success!", "Your template has been added.", "success");
					}


				}
			}

		})
	})
    $(document).on('click','#delete_template', function(e){
    	var value = [];
		if ($(this).data('id')!==undefined&&$(this).data('id')!="") {
			var id = parseInt($(this).data('id'));
			value.push(id);
		}else{
			$('input[type=checkbox]:checked').not('#user_stat, #contact_stat, #contact_msg_stat, #select-all').each(function () {
	            value.push($(this).val());
	        });
	    }
	    swal({
	      title: "Are you sure?",
	      text: "You want to delete this template?",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonClass: "btn-danger",
	      confirmButtonText: "Yes delete it!",
	      cancelButtonText: "No",
	      closeOnConfirm: false,
	      closeOnCancel: true
	    },
	    function(isConfirm) {
	      if (isConfirm) {
	        $.ajax({
	            url: "/template/delete_template",
	            dataType:"json",
	            type:"POST",
	            data:{
	                data:value
	            },success:function(data){
	            	$('#all_action_btn').hide();
	                reload_table_template();
	                swal("Deleted!", "Your template has been deleted.", "success");
	            }
	        })
	      }
	    });
    })
    $(document).on('click','#edit_template', function(){
    	var id = $(this).data('id');
    	Dropzone.forElement('#dropzone-update').removeAllFiles(true)
    	$.ajax({
            url: "/template/fetch_template",
            dataType:"json",
            type:"POST",
            data:{
                id:id
            },success:function(data){
            	console.log(data);
    			if (data.length>1) {
    				$('#template_id').val(data[0].id);
    				var filename = data[1].name;
    				var path = base_url+''+data[1].path;
    				var size = data[1].size;
    				var mockFile = { name: filename, size: size, path:path };
		    		editDropzone.files.push(mockFile);
		          	editDropzone.emit("addedfile", mockFile);
		          	editDropzone.emit("thumbnail", mockFile, path);
		          	editDropzone.emit("complete", mockFile);
		          	edit_all_file.push(mockFile);
    				$('#update_template_subject').val(data[0].subject);
    				$('.update_template_message').val(data[0].message);
    			}else{
    				$('#template_id').val(data[0].id);
    				$('#update_template_subject').val(data[0].subject);
    				$('.update_template_message').val(data[0].message);
    			}
            }
        })
    })
    $(document).on('keydown keyup','.template_message',function(e){
		var character = $(this).val().length;
		$('#textarea_feedback').text(character+'/160');
	})
    $(document).on('click','#add_template',function(){
  //   	var node = $('#icon-template-title').get(0).nextSibling;
		// node.parentNode.removeChild(node);
		$('#template_id').val('');
		$('#modal_title').text(' Add New Template');
		$('#template_subject').val('');
		$('.template_message').val('');
    	edit_all_file=[];
    	Dropzone.forElement('#dropzone-add').removeAllFiles(true)
    })
})
