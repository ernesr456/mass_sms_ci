var base_url = $('#base_url').val();
var validNumber = new RegExp(/^\d*\.?\d*$/);
lastValid = "";
function validateNumber(elem) {
    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}

$(".numOnly").keypress(function(event) {
    return /\d/.test(String.fromCharCode(event.keyCode));
});

$(document).ready(function(){
	var limit = 6;
	var x = document.getElementById("myAudio"); 
	function playAudio() { 
	  x.play(); 
	}
	show_profile();
	function show_profile(){
		$.ajax({
			url: "/profile/show_profile",
			dataType: "json",
			type: "POST",
			success:function(data){
				if (data.username!==null||data.username!==undefined) {
					$('#profile_name').text(data.username);
				}

			}
		})
	}
	total_credit();
	function total_credit(){
		$.ajax({
			url: "/payments/total_credit",
			dataType: "json",
			type: "POST",
			success:function(data){
				var total = 0;
				if (data['total_credit']!=null) {
					total = data['total_credit'];
				}
				$('#topbar_credit').text('Credit: '+total);
			}
		})
	}
	show_new_message(limit);
	$('#list_message').on('scroll', function() {
		var ths = $(this);
		if(Math.round(ths.scrollTop() + ths.innerHeight(), 10) >= Math.round(ths[0].scrollHeight, 10)&&$('#list_message').is(':hover')) {
			limit = limit + 5;
			show_new_message(limit);
		}
    })
	function show_new_message(limit){
	    var type  = "info";

	    var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	    var dt = new Date();

		$.ajax({
  			url:"/conversation/show_new_message",
			dataType: "json",
			type: "POST",
			data:{
				limit:limit
			},
			success:function(data){
				var list = '';
         		var dt = new Date();
				for(i=0;i<data.length;i++){
					var message = '';
					if (data[i].message!="") {
						message = data[i].message;
					}else if (data[i].filename!="") {
						message = "Sent a Image";
					}
					list+=
						'<a href="'+$('#base_url').val()+'admin/conversation/?id='+data[i].contact_id+'" class="list-group-item list-group-item-action media d-flex align-items-center">'+
                            '<i class="fas fa-user"></i>'+
                            '<div class="media-body ml-3">'+
                                '<div class="text-dark line-height-condenced">'+message+'</div>'+
                                '<div class="text-light small mt-1">'+
                                    '<span style="text-transform:capitalize">'+data[i].firstName+' '+data[i].lastName+'</span> &nbsp;·&nbsp; '+jQuery.timeago(""+data[i].sent_time)+
                                '</div>'+
                            '</div>'+
                        '</a>';
                    var sent_time = new Date(data[i].sent_time);

                  	var diff =(dt.getTime() - sent_time.getTime()) / 1000;
                  	var total = Math.abs(Math.round(diff));
                  	if (total<=43200&&total>=43194) {
                  		playAudio();
                  		var msg = data[i].message;
                  		var title = data[i].firstName+' '+data[i].lastName+', +'+data[i].number;
                  		var type = "info";

          			    toastr[type](msg, title, {
					      positionClass:     "toast-bottom-right",
					      closeButton:       true,
					      progressBar:       true,
					      preventDuplicates: false,
					      newestOnTop:       true,
					      rtl:               $('body').attr('dir') === 'rtl' || $('html').attr('dir') === 'rtl'
					    });
                  	}
				}
				if (data.length>0) {
					$('#new_message').text(data.length+' New Messages');
				}else{
					$('#new_message').text('You have 0 new message');
				}

				$('#list_message').html(list);
			}
		})
	}
	$(document).on('submit','.savebasic',function(e){
		e.preventDefault();
		var file = $("#profile_image").val();
		var formData = new FormData(this);
		formData.append('files', $('input[type=file]')[0].files[0]);
		// formData.append('files[]',file,file.name);


		swal({
	      title: "Are you sure?",
	      text: "You want to update this profile?",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonClass: "btn-info",
	      confirmButtonText: "Yes update it!",
	      cancelButtonText: "No",
	      closeOnConfirm: false,
	      closeOnCancel: true,
	      showLoaderOnConfirm: true
	    },
	    function(isConfirm) {
	        if (isConfirm) {
	            $.ajax({
					url: "/auth/updateProfile",
					type: "POST",
					data: formData,
					processData:false,
					contentType:false,
					dataType: "JSON",
					success: function(res) {

						if(res['code']==200){
							$("#old_password").val("");
							$("#password").val("");
							$("#password2").val("");
							swal(
						      'Changed!',
						      res['msg'],
						      'success'
						    )
						    show_profile();
			            }else{
			            	swal(
						      'Failed!',
						      res['msg'],
						      'error'
						    )
			            }     					
					}
				})
	        }
	    });
	})

	setInterval(function(){
		show_new_message(limit);
		total_credit();
	},5000);
})

// The function you currently have
$('.charDashOnly').keypress(function (e) {
    var allowedChars = new RegExp("^[a-zA-Z0-9\-]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (allowedChars.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
}).keyup(function() {
    // the addition, which whill check the value after a keyup (triggered by Ctrl+V)
    // We take the same regex as for allowedChars, but we add ^ after the first bracket : it means "all character BUT these"
    var forbiddenChars = new RegExp("[^a-zA-Z0-9\-]", 'g');
    if (forbiddenChars.test($(this).val())) {
        $(this).val($(this).val().replace(forbiddenChars, ''));
    }
});