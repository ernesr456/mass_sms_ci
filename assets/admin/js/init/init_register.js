var base_url = $('#base_url').val();

$(document).on('submit','.register',function(e){
	var btn = Ladda.create(document.querySelector('#create_account'));
	btn.start();
	e.preventDefault();
	var formData = new FormData(this);
	$.ajax({
		url: "/auth/create_account",
		dataType: "json",
		type: "POST",
		data:formData,
		processData:false,
		contentType:false,
		success:function(data){
			btn.stop();
			if(data==true){
				swal({
		            title: "Registered Successfully!",
		            text: "Please check your email for verification.",
		            type: "success"
		        }, function() {
		            window.location = base_url+"login";
		        });
			}
        	else if(data==4){
        		toastr.error(
        			'Please use another username',
	            	'Username is already taken'
				);
				toastr.error(
        			'Please use another email',
	            	'Email is already taken'
				);
        	}else if(data==6){
        		toastr.error(
        			'Please use another email',
	            	'Email is already taken'
				);
        	}else if(data==7){
        		toastr.error(
        			'Please use another username',
	            	'Username is already taken'
				);
        	}else if(data==0){
        		toastr.error(
        			'Please contact your administrator',
					'You can not register right now'
				);
        	}
		},
		error: function (jqXHR, exception) {
			btn.stop();
    		toastr.error(
    			'Please contact your administrator',
				'You can not register right now'
			);
		}
	});
})
