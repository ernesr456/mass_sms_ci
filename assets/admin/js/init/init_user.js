/* MJ Codes ==================================================================================================== */
// Tooltip
$('.messages-tooltip').tooltip();

$(document).on('submit','.register',function(e){
    e.preventDefault();
    var btn = Ladda.create(document.querySelector('#addUserButton'));
    btn.start();
    $.ajax({
        url: "/auth/add_new_user",
        type: "POST",
        dataType: "JSON",
        data: $('#register-form').serialize(),
        success: function(res){
            if(res['code']==200){
                toastr["success"](res['msg']);
                $('#addUser').modal('hide');
            }else{
                toastr["error"](res['msg']);
            }
            reloadUsers();
            btn.stop();
        }
    })
})

$(document).on('submit','.update_user',function(e){
    e.preventDefault();
    var btn = Ladda.create(document.querySelector('#updateBtn'));
    btn.start();
    $.ajax({
        url: "/auth/update_this_user",
        type: "POST",
        dataType: "JSON",
        data: $('#update-form').serialize(),
        success: function(res){
            if(res['code']==200){
                toastr["success"](res['msg']);
                $('#update_user_form').modal('hide');
                reloadUsers();
            }else{
                toastr["error"](res['msg']);
            }
            btn.stop();
        }
    })
})

const addUser = ()=>{
    $('#register-form').submit();
}

const updateUser = (id)=>{
    $('#update-form').submit();
}

const editUser = (id)=>{
    $.ajax({
        url: "/auth/get_user",
        type: "POST",
        dataType: "json",
        data: { id:id, },
        success: function(res){
            if(res!=0 || res.length>0){
                $('#id').val(res['id']);
                $('#update_name').val(res['name']);
                $('#update_username').val(res['username']);
                $('#update_old_username').val(res['username']);
                $('#update_email').val(res['email']);
                $('#update_old_email').val(res['email']);
                $('#update_created_by').val(res['created_by']);
                $("#update_group_id option").removeAttr('selected');
                $("#update_group_id option[value='"+res['user_group']+"']").prop('selected','selected');
                $("#update_group_id option:not(:selected)").prop("disabled", true);
                $("#project_id option").removeAttr('selected');
                $("#project_id option[value='"+res['project_id']+"']").prop('selected','selected');
                $('#updateBtn').attr('onclick', 'updateUser('+id+')');
                $('#update_user_form').modal('show');
            }else{ toastr["error"]("A problem occured. Please login again."); }
        }
    })
}

const deleteUser = (id, status)=>{
    swal({
        title: "Delete User?",
        text: "This user will be deleted but you can still recover it. Continue?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        showLoaderOnConfirm: true,
        confirmButtonText: "Delete!",
        closeOnConfirm: false,
        confirmButtonColor: "#e11641"
    },
    ()=>{
        $.ajax({
            url: base_url+"admin/delete_user",
            type: "POST",
            data: { id: id },
            success: function(res) {
                $('#statusText'+id).html('Deleted');
                $('#statusText'+id).attr('class', 'switcher-label text-danger');
                $('#statSwitch'+id).attr('onclick', 'changeStatus('+id+', 2)');
                $('#statSwitch'+id).prop('checked', false);
                if (res==1) {
                    swal("Deleted!", "The user was successfully deleted.", "success");
                }else{
                    swal("Error!", "A problem occured. Please login again.", "error");
                }
            }
        })
    });
}

const activateSelectedUsers = ()=>{
    var items=[];
    $.each($(".usersID:checked"), function(){
        items.push($(this).val());
    });
    var usersID     = items.join(",");
    var cntItemChk = $(".usersID:checked").length;
    if(cntItemChk > 0){
        swal({
            title: "Activate selected users?",
            text: "Selected users will be activated. Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            showLoaderOnConfirm: true,
            confirmButtonText: "Activate!",
            closeOnConfirm: false,
            confirmButtonColor: "#e11641"
        },
        ()=>{
            $.ajax({
                url: "/auth/change_selected_users_status",
                type: "POST",
                data: { id: usersID, status: 0 },
                success: function(res) {
                    if(res==1){
                        reloadUsers();
                        swal('Activated!','Users activated successfully!','success');
                    }else{
                        swal('Error!','There was a problem while activating. Please login again!','error');
                    }
                },
                error: (err)=>{
                    swal('Error!','There was a problem while activating. Please login again!','error');
                }
            })
        });
    }else{
        toastr["error"]("No users selected.");
    }
}

const deactivateSelectedUsers = ()=>{
    var items=[];
    $.each($(".usersID:checked"), function(){
        items.push($(this).val());
    });
    var usersID     = items.join(",");
    var cntItemChk = $(".usersID:checked").length;
    if(cntItemChk > 0){
        swal({
            title: "Deactivate selected users?",
            text: "Selected users will be deactivated. Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            showLoaderOnConfirm: true,
            confirmButtonText: "Deactivate!",
            closeOnConfirm: false,
            confirmButtonColor: "#e11641"
        },
        ()=>{
            $.ajax({
                url: "/auth/change_selected_users_status",
                type: "POST",
                data: { id: usersID, status: 1 },
                success: function(res) {
                    if(res==1){
                        reloadUsers();
                        swal('Deactivated!','Users deactivated successfully!','success');
                    }else{
                        swal('Error!','There was a problem while deactivating. Please login again!','error');
                    }
                },
                error: (err)=>{
                    swal('Error!','There was a problem while deactivating. Please login again!','error');
                }
            })
        });
    }else{
        toastr["error"]("No users selected.");
    }
}

const deleteSelectedUsers = ()=>{
    var items=[];
    $.each($(".usersID:checked"), function(){
        items.push($(this).val());
    });
    var usersID     = items.join(",");
    var cntItemChk = $(".usersID:checked").length;
    if(cntItemChk > 0){
        swal({
            title: "Delete selected users?",
            text: "Selected users will be deleted but you can still recover it. Continue?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            showLoaderOnConfirm: true,
            confirmButtonText: "Delete!",
            closeOnConfirm: false,
            confirmButtonColor: "#e11641"
        },
        ()=>{
            $.ajax({
                url: "/auth/change_selected_users_status",
                type: "POST",
                data: { id: usersID, status: 2 },
                success: function(res) {
                    if(res==1){
                        reloadUsers();
                        swal('Deleted!','Users deleted successfully!','success');
                    }else{
                        swal('Error!','There was a problem while deleting. Please login again!','error');
                    }
                },
                error: (err)=>{
                    swal('Error!','There was a problem while deleting. Please login again!','error');
                }
            })
        });
    }else{
        toastr["error"]("No users selected.");
    }
}

const changeStatus = (id, status)=>{
    $.ajax({
        url: "/auth/update_user_status",
        type: "POST",
        data: { id:id, status: status},
        success: function(res){
            if(res==1){
                var newStatus = (status==1 || status==2) ? 0 : 1;
                var statusText  = (status==0) ? 'Deactivated':'Active';
                var statusClass = (status==0) ? 'text-warning':'text-success';
                $('#statusText'+id).html(statusText);
                $('#statusText'+id).attr('class', 'switcher-label '+statusClass+'');
                $('#statSwitch'+id).attr('onclick', 'changeStatus('+id+','+newStatus+')');
                if(newStatus==1){
                    toastr["error"]("User deactivated");
                }else{
                    toastr["success"]("User activated");
                }
            }else{
                toastr["error"]("A problem occured. Please login again.");
            }
        }
    })
}

const selectAllUsers = (type)=>{
    if(type==1){
        $('#select-btn').attr('onclick', 'selectAllUsers(2)');
        $(".usersID").prop('checked', true);
    }else{
        $('#select-btn').attr('onclick', 'selectAllUsers(1)');
        $(".usersID").prop('checked', false);
    }
}

/* Serverside Datatables ========================================================================================================== */
// Users Table
var tableUsers = $('#view_users').DataTable({
    "order": [[ 1, "asc" ]],
    "processing": true,
    "serverSide": true,
    "bDestroy": true,
    "bJQueryUI": true,
    "ajax": {
        "url": "/auth/get_user_lists/",
        "type": "POST",
        "data": function (d) {
            d.created_by = $('#created_by').val();
            d.by_status  = $('#by_status').val();
            d.user_role  = $('#user_role').val();
        }
    },
    "columns": [
        { "data": "id"},
        { "data": "name"},
        { "data": "username"},
        { "data": "email"},
        { "data": "user_group"},
        { "data": "created_by"},
        { "data": "status"},
        { "data": "id"},
    ],
    "fnCreatedRow": function( nRow, data, iDataIndex) {
        $(nRow).attr('id', 'data_'+data['id']);
    },
    "columnDefs" : [
        { // Checkbox
            "bSortable": false,
            "targets" : 0,
            "render" : function (url, type, data) {
                return '<div class="message-checkbox mr-1">'+
                    '<label class="custom-control custom-checkbox">'+
                        '<input type="checkbox" class="custom-control-input usersID" name="usersID[]" value="'+data['id']+'">'+
                        '<span class="custom-control-label"></span>'+
                    '</label>'+
                '</div>';
            }
        },
        { // Name
            "targets" : 1,
            "render" : function (url, type, data) {
                return data['name'];
            }
        },
        { // Username
            "targets" : 2,
            "render" : function (url, type, data) {
                return data['username'];
            }
        },
        { // Email
            "targets" : 3,
            "render" : function (url, type, data) {
                return data['email'];
            }
        },
        { // Role name
            "targets" : 4,
            "render" : function (url, type, data) {
                return data['role_name'];
            }
        },
        { // Created By
            "targets" : 5,
            "render" : function (url, type, data) {
                return data['created_by_name'];
            }
        },
        { // Status
            "className": 'text-center',
            "targets" : 6,
            "render" : function (url, type, data) {
                var checked     = (data['status']==0) ? 'checked':'';
                var statusText  = (data['status']==0) ? 'Active':((data['status']==1)?'Deactivated':'Deleted');
                var statusClass = (data['status']==0) ? 'text-success':((data['status']==1)?'text-warning':'text-danger');
                return '<label class="switcher switcher-success">'+
                    '<input id="statSwitch'+data['id']+'" onclick="changeStatus('+data['id']+','+data['status']+')" type="checkbox" class="switcher-input" '+checked+'>'+
                    '<span class="switcher-indicator">'+
                        '<span class="switcher-yes">'+
                            '<span class="ion ion-md-checkmark"></span>'+
                        '</span>'+
                        '<span class="switcher-no">'+
                            '<span class="ion ion-md-close"></span>'+
                        '</span>'+
                    '</span>'+
                    '<span class="switcher-label '+statusClass+'" id="statusText'+data['id']+'">'+statusText+'</span>'+
                '</label>'
            }
        },
        { // Action
            "bSortable": false,
            "className": 'text-center',
            "targets" : 7,
            "render" : function (url, type, data) {
                // var createdUsers = ($('#user_role').val()==3)?'':'<a class="dropdown-item" href="javascript:void(0)">View Created Users</a>'; // Do not delete
                return '<div class="btn-group">'+
                    '<a onclick="editUser('+data['id']+')" href="javascript:void(0)" class="btn btn-primary btn-sm">Edit</a>'+
                    '<button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown"></button>'+
                    '<div class="dropdown-menu dropdown-menu-right">'+
                        '<a onclick="editUser('+data['id']+')" class="dropdown-item" href="javascript:void(0)">Edit</a>'+
                        '<a onclick="deleteUser('+data['id']+', '+data['status']+')" class="dropdown-item" href="javascript:void(0)">Delete</a>'+
                    '</div>'+
                '</div>';
            }
        }
    ]
});

const reloadUsers = ()=>{
    $('#select-btn').attr('onclick', 'selectAllUsers(1)');
    $(".usersID").prop('checked', false);
    tableUsers.ajax.reload();
}
/* Close Serverside Datatables ==================================================================================================== */