var base_url = $('#base_url').val();
$(function () {
    $("#mySelect2").select2("val", "0");
    $('#contacts').select2({
        placeholder:'Select Options',
        multiple: true
    });
    $('#start_at').bootstrapMaterialDatePicker({
        weekStart: 1,
        format : 'DD-MM-YYYY HH:mm',
        shortTime: true,
        nowButton : true,
        minDate : new Date()
    });
    $('.users').select2({
        placeholder: 'Select value',
    });
})
function getContact(id){
    $.ajax({
        url: base_url+"Campaign/getContact",
        dataType: "json",
        type: "POST",
        data:{ id:id },
        success:function(data){
            if(id != 1){
                for(var i=0; i<data.length; i++){
                    if ($('#contacts').find("option[value='" + data[i].id + "']").length) {
                        if($('input.checkbox_check_id').is(':unchecked')) {
                            $("#contacts option[value='"+data[i].id+"']").remove();
                        }
                    } else { 
                        var newOption = new Option(data[i].email, data[i].id);
                        $('#contacts').append(newOption).trigger('change');
                    } 
                }
            }else{
                for(var i=0; i<data.length; i++){
                    if ($('#contacts').find("option[value='" + data[i].gid + "']").length) {
                        if($('input.checkbox_check_gid').is(':unchecked')) {
                            $("#contacts option[value='"+data[i].gid+"']").remove();
                        }
                    } else { 
                        var newOption = new Option(data[i].group_name, data[i].gid);
                        $('#contacts').append(newOption).trigger('change');
                    } 
                }
            }
            if(contactID.length!=1){
                $("#contacts").val($("#idSelected").val().split(","));
            }
        }	
    });
}