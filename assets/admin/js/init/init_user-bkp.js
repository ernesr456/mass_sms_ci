$(function() {
    $('.project_id').each(function() {
        $(this).wrap('<div class="position-relative"></div>')
            .select2({
                placeholder: 'Select project',
                dropdownParent: $(this).parent(),
                tokenSeparators: [',', ' ']
            });
    })
})

$(document).ready(function(){
    var table_user = $('#view_user').DataTable({
        "pageLength": 10,
        "serverSide": true,
        "ajax": {   
            "url": '/admin/show_user',
            "type": 'POST',
            "data":function(data) {
                data.role= $('.nav-link.show.active').data('id');
            },
        },
        'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'width': '1%',
         'className': 'dt-body-center',
         'render': function(data, type, full, meta) {
            return '<input type="checkbox" name="selected_id" id="selected" value="' +
                $('<div/>').text(data).html() + '">';
        }
      }],
      'order': [[1, 'asc']],
      'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];

         // If row ID is in the list of selected row IDs
         // if($.inArray(rowId, rows_selected) !== -1){
         //    $(row).find('input[type="checkbox"][id="selected"]').prop('checked', true);
         //    $(row).addClass('selected');
         // }
      }
    });
    function reload_table_user(){
        table_user.ajax.reload(null,false);
    }
    // table_user = $('#view_user').DataTable({
    //     "pageLength" : 10,
    //     "serverSide": true,
    //     "order": [[0, "asc" ]],
    //     "ajax":{
    //           url :  '/admin/show_user/'+$('.nav-link.show.active').data('id'),
    //           type : 'POST'
    //     },
    //     'columnDefs': [{
    //          'targets': 0,
    //          'searchable':false,
    //          'orderable':false,
    //          'className': 'dt-body-center custom-control-label',
    //          'render': function (data, type, full, meta){
    //              return '<input type="checkbox" name="id[]" value="' 
    //                 + $('<div/>').text(data).html() + '">';
    //          }
    //     }],
    //    'order': [1, 'asc'],
    //     "createdRow": function(row, data, dataIndex){
    //          $(row).attr("id", "tblRow_" + data[0]);
    //     }
    // }); // End of DataTable

    $(document).on('submit','.register',function(e){
        var btn = Ladda.create(document.querySelector('#create_account'));
        btn.start();
        e.preventDefault();

        var data = $('.project_id').select2('data').map(function(elem){ 
            return elem.text
        });

        var project_text = data.join(",");

        var project_id = $(".project_id").val().join(",");

        btn.stop();
        $.ajax({
            url: "/auth/create_account",
            dataType: "json",
            type: "POST",
            data:{
                id: $('#id').val(),
                name: $('#name').val(),
                username: $('#username').val(),
                email: $('#email').val(),
                password: $('#password').val(),
                project_text: project_text,
                project_id : project_id
            },
            success:function(data){
                btn.stop();
                if(data==3){
                    swal("Registered Successfully!", "Please check your email for verification.", "success");
                }
                else if(data==4){
                    toastr.error(
                        'Please use another username',
                        'Username is already taken'
                    );
                    toastr.error(
                        'Please use another email',
                        'Email is already taken'
                    );
                }else if(data==6){
                    toastr.error(
                        'Please use another email',
                        'Email is already taken'
                    );
                }else if(data==7){
                    toastr.error(
                        'Please use another username',
                        'Username is already taken'
                    );
                }else if(data==5){
                    toastr.error(
                        'Please contact your administrator',
                        'You can not register right now'
                    );
                }
            },
            error: function (jqXHR, exception) {
                btn.stop();
                toastr.error(
                    'Please contact your administrator',
                    'You can not register right now'
                );
            }
        });
    })
    $(document).on('click','#select_role',function(){
        reload_table_user();
    })
    $(document).on('click','#update_user',function(){
        var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "You want to delete this contact?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes delete it!",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: base_url+"Admin/update_user",
                    type: "POST",
                    dataType: "json",
                    data: {
                        id: id,
                    },
                    success: function(data) {
                        reload_table_user();
                        if (data==true) {
                            swal("Deleted!", "Your contact has been deleted.", "success");
                        }
                    }
                })
            }
        });
    })
    $(document).on("click","#user_stat",function() {
        var id =  $(this).data('id');
        var stat = $(this).data('stat');
        $.ajax({
            url: base_url+"Admin/updateUserStatus",
            type: "POST",
            dataType: "json",
            data: {
                id:id,
                stat:stat
            },
            success: function(data){
                if(data==0){
                    toastr["error"]("User deactivate status");
                    reload_table_user();
                }else{
                    toastr["success"]("User activate status");
                    reload_table_user();
                }
            }
        })
    });
    $(document).on("click","#edit_user",function() {
        var id =  $(this).data('id');
        $.ajax({
            url: base_url+"Admin/getUsers",
            type: "POST",
            dataType: "json",
            data: {
                id:id,
            },
            success: function(data){
                for(i=0;i<data.length;i++){
                    $('#id').val(data[i].id);
                    $('#update_name').val(data[i].name);
                    $('#update_username').val(data[i].username);
                    $('#update_email').val(data[i].email);
                    $('#update_group_id').val(data[i].group_id);
                }
            }
        })
    });
    $(document).on('submit','.update_user',function(e){
        var btn = Ladda.create(document.querySelector('#create_account'));
        btn.start();
        e.preventDefault();

        var data = $('.project_id').select2('data').map(function(elem){ 
            return elem.text
        });

        var project_text = data.join(",");

        var project_id = $(".project_id").val().join(",");

        btn.stop();
        $.ajax({
            url: "/auth/create_account",
            dataType: "json",
            type: "POST",
            data:{
                id: $('#id').val(),
                name: $('#update_name').val(),
                username: $('#update_username').val(),
                email: $('#update_email').val(),
                password: $('#password').val(),
                project_text: project_text,
                project_id : project_id
            },
            success:function(data){
                btn.stop();
                if (data==true) {
                    swal("Updated Successfully!", "Your user updated successfully.", "success");
                }else if(data==3){
                    swal("Registered Successfully!", "Please check your email for verification.", "success");
                }
                else if(data==4){
                    toastr.error(
                        'Please use another username',
                        'Username is already taken'
                    );
                    toastr.error(
                        'Please use another email',
                        'Email is already taken'
                    );
                }else if(data==6){
                    toastr.error(
                        'Please use another email',
                        'Email is already taken'
                    );
                }else if(data==7){
                    toastr.error(
                        'Please use another username',
                        'Username is already taken'
                    );
                }else if(data==5){
                    toastr.error(
                        'Please contact your administrator',
                        'You can not register right now'
                    );
                }
            },
            error: function (jqXHR, exception) {
                btn.stop();
                toastr.error(
                    'Please contact your administrator',
                    'You can not register right now'
                );
            }
        });
    })
})