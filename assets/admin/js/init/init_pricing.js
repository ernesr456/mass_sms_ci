// $(function() {
//   	vanillaTextMask.maskInput({
// 	    inputElement: $('#text-mask-number')[0],
// 	    mask: textMaskAddons.createNumberMask({
// 	      prefix: '$'
// 	    })
//   	});
// })
$(document).ready(function(){
    get_package();
    function get_package(){
      $.ajax({
        url: "/Payments/get_package",
        type: "POST",
        dataType: "json",
        success: function(data) {
          const oneDay = 1000 * 60 * 60 * 24;

          for(i=0;i<data.length;i++){
            // var date = new Date(data[i].datetime);
            // var dt = new Date();
            // const start = Date.UTC(dt.getFullYear(), dt.getMonth(), dt.getDate());
            // const end = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
            // console.log((start - end) / oneDay);

            var timer;
            var start_date = new Date(data[i].datetime);
            var compareDate = new Date();
            compareDate.setDate(start_date.getDate() + 30); //just for this demo today + 7 days

            timer = setInterval(function() {
              timeBetweenDates(compareDate);
            }, 1000);

            function timeBetweenDates(toDate) {
              var dateEntered = toDate;
              var now = new Date();
              var difference = dateEntered.getTime() - now.getTime();

              if (difference <= 0) {

                // Timer done
                clearInterval(timer);
              
              } else {
                
                var seconds = Math.floor(difference / 1000);
                var minutes = Math.floor(seconds / 60);
                var hours = Math.floor(minutes / 60);
                var days = Math.floor(hours / 24);

                hours %= 24;
                minutes %= 60;
                seconds %= 60;

                $("#days").text(days);
                $("#hours").text(hours);
                $("#minutes").text(minutes);
                $("#seconds").text(seconds);
              }
            }
          }
        }
      })
    }
    var table_transaction = $('#view_transaction').DataTable({
        "pageLength": 10,
        "serverSide": true,
        "ajax": {   
            "url": '/payments/show_transaction',
            "type": 'POST',
        },
        'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'width': '1%',
         'className': 'dt-body-center',
      }],
      'order': [[1, 'asc']],
      'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];

      }
   });

  function reload_table_transaction() {
    table_transaction.ajax.reload(null, false);
  }
  show_pricing();
  function show_pricing(){
    var session_group = $('#session_group').val();
    $.ajax({
      url: "/Payments/show_pricing",
      type: "POST",
      dataType: 'json',
      success: function(data) {
        $.ajax({
          url: "/Payments/my_transaction",
          type: "POST",
          dataType: 'json',
          success: function(transaction) {
            var card ='';
            for(i=0;i<data.length;i++){
              var button = '';
              var button1 = '';
              if (transaction.length<1&&session_group!=1) {
                button = '<a href="javascript:;" onclick="payWithPaypal('+data[i]['id']+')" ><button type="button" class="btn btn-info">Get this Plan</button></a>';
              }else if(session_group!=1){
                for(j=0;j<transaction.length;j++){
                  if (data[i]['id']==transaction[j]['id']) {
                    button = '<a href="javascript:;" ><button type="button" class="btn btn-info"><i class="fas fa-check"></i> Avail</button></a>';
                  }else{
                    button = '<a href="javascript:;" onclick="payWithPaypal('+data[i]['id']+')" ><button type="button" class="btn btn-info"><i class="fas fa-arrow-up"></i> Upgrade</button></a>';
                  }
                }
              }

              if (session_group==1) {
                var button1 = 
                '<div class="card-body pull-right" style="padding: 1rem 1.5rem 0rem 1.5rem;">'+
                  '<a href="javascript:void(0)" data-id="'+data[i].id+'" class="btn icon-btn btn-sm btn-outline-danger" id="delete_plan" title="Delete">'+
                        '<span class="fas fa-trash"></span>'+
                  '</a>&nbsp;'+
                  '<a data-target="#planModal" data-toggle="modal" href="javascript:void(0)" data-id="'+data[i].id+'" class="btn icon-btn btn-sm btn-outline-warning" id="edit_plan" title="Edit">'+
                        '<span class="fas fa-pencil-alt"></span>'+
                  '</a>'+
                '</div>';
              }

              var Dashboard     = '<i class="ion ion-md-close"></i>';
              var Campaigns     = '<i class="ion ion-md-close"></i>';
              var Conversation  = '<i class="ion ion-md-close"></i>';
              var Template      = '<i class="ion ion-md-close"></i>';
              var Email         = '<i class="ion ion-md-close"></i>';
              var Volunteer     = '<i class="ion ion-md-close"></i>';
              var Contacts      = '<i class="ion ion-md-close"></i>';
              var Group         = '<i class="ion ion-md-close"></i>';
              var Permission    = '<i class="ion ion-md-close"></i>';

              var iterator = Object.keys(data[i]['permission']); 
              for (let key of iterator) { 
                if (key=='modDashboard') {
                  Dashboard = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modCampaign') {
                  Campaigns = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modConversation') {
                  Conversation = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modEmail') {
                  Email = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modVolunteer') {
                  Volunteer = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modContacts') {
                  Contacts = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modGroup') {
                  Group = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modPermission') {
                  Permission = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modUser') {
                  Volunteer = '<i class="ion ion-md-checkmark"></i>';
                }
                if (key=='modTemplate') {
                  Template = '<i class="ion ion-md-checkmark"></i>';
                }
              } 

              card += 
              '<div class="col-md" style="margin-bottom: 0.75rem !important">'+
                '<div class="card">'+
                    '<div class="">'+
                      button1+
                    '</div>'+
                    '<div class="card-body pricing-table">'+
                        '<p class="card-text text-center">'+
                            '<span class="pricing-currency">$</span> '+
                            '<span class="pricing-amount">'+data[i]['pricing']+'</span>'+
                            '<span class="pricing-period">/mo</span>'+
                        '</p>'+
                        '<p class="pricing-title text-center">'+
                            '<span>'+data[i]['name']+'</span>'+
                        '</p>'+
                    '</div>'+
                    '<ul class="list-group list-group-flush">'+
                        '<li class="list-group-item">Dashboard <span class="pull-right">'+Dashboard+'</span></li>'+
                        '<li class="list-group-item">Campaigns <span class="pull-right">'+Campaigns+'</span></li>'+
                        '<li class="list-group-item">Conversation <span class="pull-right">'+Conversation+'</span></li>'+
                        '<li class="list-group-item">Message Template <span class="pull-right">'+Template+'</span></li>'+
                        '<li class="list-group-item">Email <span class="pull-right">'+Email+'</span></li>'+
                        '<li class="list-group-item">Volunteer <span class="pull-right">'+Volunteer+'</span></li>'+
                        '<li class="list-group-item">Contacts <span class="pull-right">'+Contacts+'</span></li>'+
                        '<li class="list-group-item">Group Contacts <span class="pull-right">'+Group+'</span></li>'+
                        '<li class="list-group-item">Volunteer Permission <span class="pull-right">'+Permission+'</span></li>'+
                    '</ul>'+
                    '<div class="card-body text-center">'+
                        button+
                    '</div>'+
                '</div>'+
              '</div>';
            }
            $('#pricing_card').html(card);
          }
        });
      }
    })
  }
  $(document).on('click', '#delete_plan', function() {
    var id = $(this).data('id');
    swal({
      title: "Are you sure?",
      text: "You want to delete this plan?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: "/Payments/update_pricing",
          type: "POST",
          dataType: "json",
          data: {
            id  : id,
            type: 'delete'
          },
          success: function(data) {
            show_pricing();
            if (data==true) {
              swal("Success!", "Package deleted successfully!", "success");
            }
          }
        })
        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
      }
    });
  });
  $(document).on('click','#add_plan',function(){
    $('#pricing_name').val('');
    $('#currency-field').val('');
    $('#id').val('');
    $('#dashboard,#campaign,#conversation,#email,#template,#user,#group_contacts,#volunteer_permission').prop('checked', true);
  })
  $(document).on('click','#edit_plan',function(){
    $('#modal_title').text('Edit Package Pricing')
    var id = $(this).data('id');
    $.ajax({
      url: "/Payments/getPricing",
      type: "POST",
      dataType: "json",
      data: {
        id  : id,
      },
      success: function(data) {
        $('#pricing_name').val(data['name']);
        $('#currency-field').val('$'+data['price']);
        $('#id').val(data['id']);

        var obj = JSON.parse(data['permission']);
        if (obj.modDashboard==true) {
            $('#dashboard').prop('checked', true);
        }else{
            $('#dashboard').prop('checked', false);
        }
        //Campaign
        if (obj.modCampaign==true) {
            $('#campaign').prop('checked', true);
        }else{
            $('#campaign').prop('checked', false);
        }
        //Conversation
        if (obj.modConversation==true) {
            $('#conversation').prop('checked', true);
        }else{
            $('#conversation').prop('checked', false);
        }

        //Email
        if (obj.modEmail==true) {
            $('#email').prop('checked', true);
        }else{
            $('#email').prop('checked', false);
        }

        //Template
        if (obj.modTemplate==true) {
            $('#template').prop('checked', true);
        }else{
            $('#template').prop('checked', false);
        }

        //User
        if (obj.modUser==true) {
            $('#user').prop('checked', true);
        }else{
            $('#user').prop('checked', false);
        }

       //Contact Group
        if (obj.modGroup==true) {
            $('#group_contacts').prop('checked', true);
        }else{
            $('#group_contacts').prop('checked', false);
        }
        //Contact Group
        if (obj.modPermission==true) {
            $('#volunteer_permission').prop('checked', true);
        }else{
            $('#volunteer_permission').prop('checked', false);
        }
      }
    })
  })
  $(document).on('submit','.add_pricing',function(e){
    var btn = Ladda.create(document.querySelector('#btn_pricing'));
    e.preventDefault();
    var price = $('#currency-field').val();
    var number = price.replace("$", "");
    var permission_list = $('input[name^="permission"]').serializeArray();
    $.ajax({
      url: "/Payments/submit_pricing",
      type: "POST",
      dataType: "json",
      data: {
        id          : $('#id').val(),
        name        : $('#pricing_name').val(),
        pricing     : number,
        permission  : permission_list
      },
      success: function(data) {
        btn.stop();
        show_pricing();
        if (data==true) {
          if ($('#id').val()!="") {
            swal("Success!", "Package added successfully!", "success");
          }else{
            swal("Success!", "Package updated successfully!", "success");
          }
        }
      }
    })
  })
    /* Get payment settings using ajax */
  var payment_settings = ()=>{
      var result;
      $.ajax({
          url: base_url+'admin/get_payment_settings',
          dataType: 'JSON',
          async: false,
          success: (res)=>{
              result = res;
          }
      });
      return result;
  }

  /* Show paypal payment settins using ajax */
  var res = payment_settings();
  function renderPaymentBtn(){
    var env = (res['paypal_sandbox']==1) ? 'sandbox' : 'production';
    paypal.Button.render({
        style: {
            size: 'responsive',
            color: 'black'
        },
        env: env, 
        client: {
            sandbox: res['paypal_client_id'],
            production: res['paypal_client_id']
        },
        locale: 'en_US',
        commit: true, // Show a 'Pay Now' button
        payment: function(data, actions) {
            // Set up the payment here
            var amount = (eval(total_pay.join('+'))) ? eval(total_pay.join('+')) : 1;
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: amount, currency: 'USD' }
                        }
                    ]
                }
            });
        },
        onAuthorize: function(data, actions) {
            // Execute the payment here
            return actions.payment.execute().then(function(payment){
        // Violet Loader Color
        H5_loading.show({color:"rgb(129, 122, 220)",background:"rgba(0, 0, 0, 0.55)",timeout:0.5,scale:0.8});
        $('#paymentModal').modal('hide');
          $.ajax({
            url: '/Payments/purhcase_pricing/', // 1 Paypal
            data: {
              txn_id:     payment.id,
              pricing_id:    $('#credit_id').val(),
              payment_method: payment.payer.payment_method,
              total_payment:  eval(total_pay.join('+')),
              payer_email:  payment.payer.payer_info.email,
              payer_status:   payment.payer.status,
              created_time:   payment.create_time,
            },
            type: 'POST',
            success:(res)=>{
                H5_loading.hide();
                if(res==1){
                    swal("Success!", "You purchase plan.", "success");
                    setTimeout(function(){
                      location.reload(true);
                      
                    }, 5000);  
                    show_pricing();
                    reload_table_transaction();
                }else{
                    swal("Oops!", 'Payment failed.', "error");
                }
            }
          });
        });
      }
    }, '#paypal-button');
  }
  renderPaymentBtn();
})
var total_pay = [];

const payWithPaypal = (credit_id)=>{
    $.ajax({
      url: '/Payments/get_pricing/'+credit_id,
      dataType: 'JSON',
      success: (res)=>{
        if(res!=0){
          total_pay=[];
          $('#paymentModal').modal('show');
          total_pay.push(res['price']);
          $('#package_name').text(res['name']);
          $('#credit_price').text(res['price']);
          $('#package_amount').val(res['price']);
          $('#credit_id').val(credit_id);
        }else{
          toastr["error"]("A problem occued. Please try again.");
        }
      }
    });
}

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "$" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "$" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}