$(function() {

  $('.chartjs-demo').each(function() {
    $(this).wrap($('<div style="height:' + this.getAttribute('height') + 'px"></div>'));
  });

  var campaign = $('#campaign').val();
  $('#campaign')

  if (campaign!==null) {
      // sms_status(campaign);
      get_percent_reply(campaign);
      get_reply_status(campaign);
      all_count_message(campaign);
      $(document).on('change','#campaign',function(){
        var campaign = $(this).val();
        // sms_status(campaign);
        // get_percent_reply(campaign);
        // get_reply_status(campaign);
        all_count_message(campaign);
      })

      function all_count_message(campaign){
        $.ajax({
          url: "/dashboard/sms_status",
          type: "POST",
          dataType: "json",
          data: {
            campaign:campaign,
          },
          success: function(data){
            console.log(data);
            var label = [];
            var total = [];
            var undelivered = [];
            var received = [];
            for(i=0;i<data.length;i++){

              label.push(data[i].month);
              total.push(data[i].sent);
              undelivered.push(data[i].undelivered);
              received.push(data[i].received);
            }
              data_message(label,total,undelivered,received);
          }
        })
      }
      // function sms_status(campaign){
      //   $.ajax({
      //   url: "/dashboard/sms_status",
      //   type: "POST",
      //   dataType: "json",
      //   data: {
      //     campaign:campaign,
      //   },
      //   success: function(data){
      //     for(i=0;i<data.length;i++){
      //       if (data[i].sent===null&&data[i].undelivered===null) {
      //         $('#total_sent').text("Total Sent: 0.00%");
      //         $('#total_undelivered').text("Total Undelivered: 0.00%")
      //       }else{
      //         $('#total_sent').text("Total Sent: "+data[i].sent+"%");
      //         $('#total_undelivered').text("Total Undelivered: "+data[i].undelivered+"%");
      //       }
      //     }
      //   }
      // })
      // }
      function get_percent_reply(campaign){
        $.ajax({
        url: "/admin/get_percent_reply",
        type: "POST",
        dataType: "json",
        data: {
          campaign:campaign,
        },
        success: function(data){
          for(i=0;i<data.length;i++){
            if (data[i].all_reply===null) {
              $('#total_replies').text("Total Replies: 0.00%");
            }else{
              $('#total_replies').text("Total Replies: "+data[i].all_reply+"%");
            }
          }
        }
      })
      }
      function get_reply_status(campaign){
        $.ajax({
          url: "/admin/get_reply_status",
          type: "POST",
          dataType: "json",
          data: {
            campaign:campaign,
          },
          success: function(data){
            var table = '';
            var negative = '';
            var positive = '';
            for(i=0;i<data.length;i++){
              console.log(data[i].id);

              if (data[i].sms_reply_status=='1') {
                negative = 'No';
                positive = 'Yes';
              }else{
                negative = 'Yes';
                positive = 'No';
              }
              console.log(data[i]);
              table +=
              '<tr>'+
                '<td><a href="'+base_url+'admin/conversation?contact_id='+data[i].id+'">'+data[i].name+'</a><br>'+data[i].message+'</td>'+
                '<td>'+positive+'</td>'+
                '<td>'+negative+'</td>'+
                '<td>'+jQuery.timeago(""+data[i].sent_time)+'</td>'+
              '</tr>'
            }
            $('#recent-replies').html(table);
          }
        })
      }
      function data_message(label,total,undelivered,received){
        data_label = "";
        if (label=="") {
          data_label = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "November", "December"];
        }else{
          data_label = label;
        }
        var data = {
            labels: data_label,
            datasets: [{
                label: "Total Sent",
                backgroundColor: 'rgba(40, 208, 148, 1)',
                borderWidth: 1,
                data: total,
            }]
        };

        var ctx = $("#myChart").get(0).getContext("2d");
        var myBarChart = new Chart(ctx, {
            title: "Text",
            type: "bar",
            data: data,
            options: {
              title: {
                  display: true,
                  text: 'Text Graph',
                  fontSize: 25,
                  fontFamily: 'Arial'
              },
              scales: {

                  xAxes: [{
                      gridLines: {
                          display: false // This removes vertical grid lines
                      },
                  }],
                  yAxes: [{
                      gridLines:{
                          display: false
                      },
                      ticks : {
                          min: 0,
                          max: 100,
                          callback: function(value){return value+ "%"}
                      },
                  }]
              }
            },
        });

        var received = {
            label: "Total Replies",
            backgroundColor: 'rgba(29, 151, 230,1)',
            borderWidth: 1,
            data: received,
        }
        data.datasets.push(received);
        myBarChart.update();

        var Undelivered = {
            label: "Total Undelivered",
            backgroundColor: 'rgba(242, 69, 92,1)',
            borderWidth: 1,
            data: undelivered,
        }

        data.datasets.push(Undelivered);
            myBarChart.update();
            function resizeCharts() {
            myBarChart.resize()
          }

          // Initial resize
          resizeCharts();

          // For performance reasons resize charts on delayed resize event
          window.layoutHelpers.on('resize.charts-demo', resizeCharts);
      }

  }

  // Resizing charts
});
