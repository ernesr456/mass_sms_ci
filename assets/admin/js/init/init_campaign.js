var base_url = $('#base_url').val();
$(function() {
    if (document.getElementById('photoswipe-example')) {
        new PerfectScrollbar(document.getElementById('photoswipe-example'));
    }
});

$('.message').on('keydown keyup', function(){
    var character = $(this).val().length;
    $('#chatmessage_count').text(character+'/160');
});

$(function() {

    // $('#project_start').bootstrapMaterialDatePicker({
    //     weekStart: 1,
    //     format: 'dddd MMMM DD YYYY - hh:mm a',
    //     altFormat: 'DD-MM-YYYY HH:mm',
    //     shortTime: true,
    //     nowButton: true,
    //     minDate: new Date()
    // });

    $('.user_id, .update_user_id').each(function() {
        $(this).wrap('<div class="position-relative"></div>')
            .select2({
                allowClear: true,
                placeholder: 'Select value',
                dropdownParent: $(this).parent(),
                tokenSeparators: [',', ' ']
            });
    })
    $('.group_id, .update_group_id').each(function() {
        $(this).wrap('<div class="position-relative"></div>')
            .select2({
                allowClear: true,
                placeholder: 'Select value',
                dropdownParent: $(this).parent(),
                tokenSeparators: [',', ' ']
            });
    })
})

$(document).ready(function() {
    // Dropzone
    var all_file = new FormData();
    var update_all_file = new FormData();
    var edit_all_file = new Array();
    Dropzone.autoDiscover = false;
    if (document.getElementById("dropzone-add")) {
        var addDropzone = new Dropzone("#dropzone-add", {
            parallelUploads: 2,
            maxFilesize: 5,
            createImageThumbnails: true,
            clickable: true,
            thumbnailWidth: 200,
            thumbnailHeight: 200,
            filesizeBase: 1000,
            addRemoveLinks: true,
            maxFiles: 1,
            acceptedMimeTypes: "image/jpeg,image/gif,image/png,image/bmp,text/vcard,text/csv,.csv,,text/rtf,.rtf,.vfu,text/richtext,text/calendar,text/directory,application/pdf,audio/basic,audio/L24,audio/mp4,.mp4,audio/mpeg,audio/ogg,audio/vorbis,audio/vnd.rn-realaudio,audio/vnd.wave,audio/3gp,audio/3gpp2,audio/ac3,audio/vnd.wave,audio/webm,audio/amr-nb,audio/amr",
            // acceptedFiles: ".gif,.png,.jpg,.gif,.au,.mp4a,.mpga,.oga,.3ga,.3gpp,.ac3,.wav,.weba,.awb,.amr,.video,.mpeg,.mp4,.qt,.webm,.3gp,.3g2,.3gpp-tt,.h261,.h263,.h264,.vcf,.csv,.rtf,.rtx,.ics,.pdf",
            removedfile: function(file) {
                if (file == edit_all_file[0]) {
                    edit_all_file.pop();
                }

                var files = all_file.getAll("files[]");
                all_file.delete('files[]');
                for (i = 0; i < files.length; i++) {
                    if (files[i].name == file.name) {} else {
                        all_file.append('files[]', file, file.name);
                    }
                }

                // var files = update_all_file.getAll("files[]");
                // update_all_file.delete('files[]');
                // for (i = 0; i < files.length; i++) {
                //     if (files[i].name == file.name) {} else {
                //         update_all_file.append('files[]', file, file.name);
                //     }
                // }

                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
        });
    }

    if (document.getElementById("dropzone-update")) {
        var editDropzone = new Dropzone("#dropzone-update", {
            parallelUploads: 2,
            maxFilesize: 5,
            createImageThumbnails: true,
            clickable: true,
            thumbnailWidth: 200,
            thumbnailHeight: 200,
            filesizeBase: 1000,
            addRemoveLinks: true,
            maxFiles: 1,
            acceptedMimeTypes: "image/jpeg,image/gif,image/png,image/bmp,text/vcard,text/csv,.csv,,text/rtf,.rtf,.vfu,text/richtext,text/calendar,text/directory,application/pdf,audio/basic,audio/L24,audio/mp4,.mp4,audio/mpeg,audio/ogg,audio/vorbis,audio/vnd.rn-realaudio,audio/vnd.wave,audio/3gp,audio/3gpp2,audio/ac3,audio/vnd.wave,audio/webm,audio/amr-nb,audio/amr",
            // acceptedFiles: ".gif,.png,.jpg,.gif,.au,.mp4a,.mpga,.oga,.3ga,.3gpp,.ac3,.wav,.weba,.awb,.amr,.video,.mpeg,.mp4,.qt,.webm,.3gp,.3g2,.3gpp-tt,.h261,.h263,.h264,.vcf,.csv,.rtf,.rtx,.ics,.pdf",
            removedfile: function(file) {
                update_all_file.delete("filename");
                if (file == edit_all_file[0]) {
                    edit_all_file.pop();

                }

                var files = all_file.getAll("files[]");
                all_file.delete('files[]');
                for (i = 0; i < files.length; i++) {
                    if (files[i].name == file.name) {} else {
                        all_file.append('files[]', file, file.name);
                    }
                }

                var files = update_all_file.getAll("files[]");
                update_all_file.delete('files[]');
                for (i = 0; i < files.length; i++) {
                    if (files[i].name == file.name) {} else {
                        update_all_file.append('files[]', file, file.name);
                    }
                }

                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
        });
    }
    // Mock the file upload progress (only for the demo)
    Dropzone.prototype.uploadFiles = function(files) {
        var minSteps = 6;
        var maxSteps = 60;
        var timeBetweenSteps = 100;
        var bytesPerStep = 100000;
        var isUploadSuccess = Math.round(Math.random());

        var self = this;

        for (var i = 0; i < files.length; i++) {

            var file = files[i];
            var totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));
            for (var step = 0; step < totalSteps; step++) {
                var duration = timeBetweenSteps * (step + 1);
                setTimeout(function(file, totalSteps, step) {
                    return function() {
                        file.upload = {
                            progress: 100 * (step + 1) / totalSteps,
                            total: file.size,
                            bytesSent: (step + 1) * file.size / totalSteps
                        };
                        self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
                        if (file.upload.progress == 100) {
                            if (edit_all_file.length > 0) {
                                file.status = Dropzone.ERROR;
                                self.emit('error', file, 'You can not upload any more file', null);
                                self.emit('complete', file);
                                self.processQueue();
                            } else {
                                update_all_file.append('files[]', file, file.name);
                                file.status = Dropzone.SUCCESS;
                                self.emit('success', file, 'success', null);
                                self.emit('complete', file);
                                self.processQueue();
                            }

                            all_file.append('files[]', file, file.name);
                        }
                    };
                }(file, totalSteps, step), duration);
            }
        }
    };
    $(document).on('click','.campaign_refresh',function(){
        show_campaign();
    })
    //End of Dropzone

    show_campaign();

    function show_campaign() {
        var campaign = "";
        var user = "";
        var status = "";


        $.ajax({
            url: "/campaign/show_campaign",
            dataType: "json",
            type: "POST",
            data:{
                campaign: $('#filter_campaign').val(),
                user: $('#filter_users').val(),
                status: $('#filter_status').val(),
                type: 'show',
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
            },
            success: function(data) {

                var accordion = "";
                var total_upcoming = 0;
                var total_running = 0;
                var total_completed= 0;
                var total_sent = 0;
                var total_undelivered= 0;
                var total_received = 0;
                var total_all = 0;
                var sent = [];
                var undelivered = [];
                var received = [];
                var total = [];
                var label = ["Total Sent", "Total Undelivered", "Total Received"];
                var src ='';

                for (i = 0; i < data.length; i++) {
                    var badge = '';
                    var disabled = "";
                    var data_attribute = "";
                    var dt = new Date();
                    var project_date = '';
                    var project_time = '';

                    if (data[i].status!=3||data[i].status!=2) {
                        total_sent = total_sent + parseInt(data[i].total_sent);
                        total_undelivered = total_undelivered + parseInt(data[i].total_undelivered);
                        total_received = total_received + parseInt(data[i].total_received);
                    }
                    var time = dt.getFullYear()+"-"+dt.getMonth()+"-"+dt.getHours()+" "+dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
                    if (dt > new Date(data[i].project_end)) {
                        badge = '<a href="javascript:void(0)" class="badge badge-pill badge-success text-white">Completed</a>';
                        total_completed = total_completed + 1;
                        disabled = "disabled";
                        project_date = data[i].end_date;
                        project_time = data[i].end_time;
                    }else if (dt > new Date(data[i].project_start)){
                        badge = '<a href="javascript:void(0)" class="badge badge-pill badge-info text-white">Running</a>';
                        total_running = total_running + 1;
                        data_attribute = 'data-id="'+data[i].id+'" data-message="'+data[i].message+'" data-filename = "'+data[i].filename+'" data-contacts="'+data[i].total_contacts+'" id="campaign_send_text"';
                        project_date = data[i].end_date;
                        project_time = data[i].end_time;
                    }else{
                        badge = '<a href="javascript:void(0)" class="badge badge-pill badge-warning text-white">Upcoming</a>';
                        total_upcoming = total_upcoming + 1;
                        disabled = "disabled";
                        project_date = data[i].start_date;
                        project_time = data[i].start_time;
                    }

                    var status = '';
                    var button = '';
                    if (data[i].status == 1) {
                        status = 'checked';
                    } else {
                        status = 'unchecked';
                    }

                    if (data[i].filename != "") {
                        button = 
                        // '<div id="photoswipe-example" class="row" itemscope itemtype="http://schema.org/ImageGallery">'+
                            '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" class="col-sm-3">'+
                                '<a href="'+base_url+'assets/project_file/'+data[i].created_by+'/'+data[i].filename+'" itemprop="contentUrl" data-size="1920x1280"><img src="'+base_url+'assets/project_file/'+data[i].created_by+'/'+data[i].filename+'" itemprop="thumbnail" alt="Image description"></a>'+
                            '</figure>';
                        // '</div>';
                        // button = '<button type="button" class="btn btn-xs btn-info">Click to view</button>';
                    } else {
                        button = '<div class="col-md-10"><button type="button" class="btn btn-xs btn-info" disabled>No File</button></div>';
                    }

                    accordion
                        += 
                            '<div class="card review-card mb-4" id="accordion2">' +
                                '<div class="card-header with-elements">' +
                                    badge+ '&nbsp;'+
                                    '<h6 class="card-header-title mb-0">' + data[i].proj_name + '</h6>' +
                                    '<div class="card-header-elements ml-auto">' +
                                        '<label class="switcher switcher-success">' +
                                            '<input type="checkbox" class="switcher-input" id="status_campaign" data-status="' + data[i].status + '" data-id="' + data[i].id + '" ' + status + '>' +
                                            '<span class="switcher-indicator">' +
                                                '<span class="switcher-yes">' +
                                                    '<span class="ion ion-md-checkmark"></span>' +
                                                    '</span>' +
                                                    '<span class="switcher-no">' +
                                                    '<span class="ion ion-md-close"></span>' +
                                                '</span>' +
                                            '</span>' +
                                        '</label>' +
                                        '<a href="javascript:;" data-target="#updateCampaign" data-toggle="modal" id="edit_campaign" data-id="'+data[i].id+'" title="Edit Campaign" class="btn icon-btn btn-sm btn-outline-warning">' +
                                            '<span class="far fa-edit"></span>' +
                                        '</a>' +
                                        '<a href="#" title="Delete" id="delete_campaign" data-id="' + data[i].id + '" class="btn icon-btn btn-sm btn-outline-danger">' +
                                            '<span class="fas fa-trash"></span>' +
                                        '</a>' +
                                        '<a class="collapsed d-flex justify-content-between text-dark" data-toggle="collapse" href="#accordion2-' + data[i].id + '"><div class="collapse-icon"></div></a>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="card-body" style="padding: 1.5rem 1.5rem 1.5rem 0rem !important;">' +
                                    '<div class="review-block">' +
                                        '<div class="row pt-0 pb-2" style="padding-left: 0px !important">' +
                                            '<div class="col-md-2 pb-2">' +
                                            '<div>' +
                                            '<i class="feather icon-message-square ml-3 text-primary" title="Message"> '+data[i].count_message+'</i>' +
                                        '</div>' +
                                    '<div>' +
                                    '<i class="feather icon-calendar ml-3 text-primary" title="Date"> ' + project_date + '</i>' +
                                '</div>' +
                                '<div>' +
                                    '<i class="feather icon-clock ml-3 text-primary" title="Time"> ' + project_time + '</i>' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-sm-auto pb-2">' +
                                '<p class="m-b-15 text-muted ml-3">' + data[i].description + '</p>' +
                            '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="review-block">' +
                            '<div id="accordion2-' + data[i].id + '" class="collapse" data-parent="#accordion2">' +
                                '<div class="card-header with-elements">' +
                                    '<div class="card-header-elements ml-auto ml-5">' +
                                        '<div class="btn-group">' +
                                            '<button class="btn btn-info btn-round" '+data_attribute+' '+disabled+'>Start Texting</buttom>'+
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-sm-auto pb-2">' +
                                    '<div class="card-body">' +
                                        '<div class="row mb-12">' +
                                            '<div class="col-md-2 text-muted">To:</div>' +
                                            '<div class="col-md-10"> Campaign: '+data[i].proj_name+' with '+data[i].total_contacts+ ' contacts.'+
                                        '</div>' +
                                    '</div>' +
                                '<br>' +
                                '<div class="row mb-12">' +
                                    '<div class="col-md-2 text-muted">Message:</div>' +
                                    '<div class="col-md-10">' +
                                        '<div class="ui-chat">' +
                                            ' <div class="row mb-3 received-chat">' +
                                                '<div class="col">' +
                                                    '<div class="msg bg-info text-white ">' +
                                                        '<p class="mb-0">' + data[i].message + '</p>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        ' </div>' +
                                    '</div>' +
                            '</div>' +
                            '<div class="row mb-12">' +
                                '<div class="col-md-2 text-muted">File:</div>' +
                                    button +
                                '</div>' +
                                '<br>' +
                                ' <div class="table-responsive mb-4">' +
                                    '<table class="table mb-0">' +
                                        '<thead>' +
                                            ' <tr>' +
                                                '<th>Pre-Approved Responses</th>' +
                                                '<th>Corresponding Tags</th>' +
                                            ' </tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                            '<tr>' +
                                                '<th scope="row">Thanks for you support!</th>' +
                                                '<td><a href="javascript:void(0)" class="badge badge-pill badge-info">Yes</a></td>' +
                                            '</tr>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '</div>' +
                        '<div class="card-footer review-block">' +
                        '<p class="m-b-15" style="text-transofrm: capitalize"><span class="text-muted">Created By:</span> <a href="javascript:;" class="mr-4">'+data[i].name+'</a></p>' +
                        '</div>' +
                        '</div>' ;
                }

                $('.project_list').html(accordion);
                $('#total_upcoming').text(total_upcoming);
                $('#total_running').text(total_running);
                $('#total_completed').text(total_completed);

                sent.push(total_sent/2);
                undelivered.push(total_undelivered/2);
                received.push(total_received/2);
                total_all = total_sent + total_undelivered + total_received;
                total.push(total_sent);
                data_chart(sent,undelivered,received);
            }
        });
        
    }
    function data_chart(sent,undelivered,received){
        var ctx = document.getElementById('myChart').getContext('2d');
        // This chart would show a line only for the third dataset
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [{
                    label: 'Total Sent',
                    data: sent,
                    backgroundColor: 'rgba(40, 208, 148, 1)',
                    borderWidth: 1,
                }, {
                    label: 'Total Undelivered',
                    data: undelivered,
                    backgroundColor: 'rgba(242, 69, 92,1)',
                    borderWidth: 1,
                }, {
                    label: 'Total Replies',
                    data: received,
                    backgroundColor: 'rgba(29, 151, 230,1)',
                    borderWidth: 1,
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false // This removes vertical grid lines
                        },
                        ticks : {
                            beginAtZero : true
                        },
                    }],
                    yAxes: [{
                        gridLines:{
                            display: false
                        },
                        ticks : {
                            beginAtZero : true
                        },
                    }]
                }
            }
        });
    }

    $('#filter_campaign,#filter_users,#filter_status').change(function(){
        show_campaign();
    })

    var opts = {
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        altFormat: "yy-mm-dd"
    };
    var start_date = "";
    $('#date_end').bootstrapMaterialDatePicker({
        weekStart: 1, 
        format: 'YYYY-MM-DD HH:mm',
        shortTime: true,
        nowButton: true,
        minDate: new Date(),
    });

    $('#date_start').bootstrapMaterialDatePicker({
        weekStart: 1, 
        format: 'YYYY-MM-DD HH:mm',
        shortTime : true,
        nowButton: true,
        minDate: new Date(),
    }).on('change', function(e, date) {
        $('#date_end').bootstrapMaterialDatePicker('setMinDate', date);
    });

    // $('#date_end').bootstrapMaterialDatePicker({
    //     weekStart : 1,
    //     format: 'YYYY-MM-DD HH:mm',
    //     // format: 'dddd MMMM DD YYYY - hh:mm a',
    //     altFormat: 'YYYY-MM-DD HH:mm',
    //     altField: 'alt_date_end',
    //     shortTime: true,
    //     nowButton: true,
    //     minDate: new Date(),
    // });
    // $('#date_start').bootstrapMaterialDatePicker({
    //     weekStart : 1,
    //     // format: 'dddd MMMM DD YYYY - hh:mm a',
    //     format: 'YYYY-MM-DD HH:mm',
    //     altField: 'alt_date_start',
    //     shortTime: true,
    //     nowButton: true,
    //     minDate: new Date()
    // }).on('change', function(e, date)
    // {
    // $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    // });
    $(document).on('click','#create_campaign',function(){
        $('#id').val('');
    })
    $(document).on('submit', '.add_campaign', function(e) {

        e.preventDefault();
        // var dates = $('#project_start').datepicker('getDate').toISOString();
        var date = Date.parse($("#scheduleDate").datepicker({ dateFormat: 'dd,MM,yyyy' }).val());
        var id = $('#id').val();
        file = new FormData();
        if (id=="") {
            var data = $('.group_id').select2('data').map(function(elem){ 
                return elem.text
            });
            var group_text = data.join(",");
            var data1 = $('.user_id').select2('data').map(function(elem){ 
                return elem.text
            });
            var user_text = data1.join(",");
            var group_id = $(".group_id").val().join(",");
            var user_id = $(".user_id").val().join(",");

            all_file.append('name', $('#projName').val());
            all_file.append('message', $('.message').val());
            all_file.append('id', id);
            all_file.append('description', $('#description').val());
            all_file.append('start_at', $('#date_start').val());
            all_file.append('end_at', $('#date_end').val());
            all_file.append('timezone', Intl.DateTimeFormat().resolvedOptions().timeZone);
            all_file.append('group_id', group_id);
            all_file.append('user_id', user_id);
            all_file.append('group_text', group_text);
            all_file.append('user_text', user_text);
            file = all_file;
        }else{
            var data = $('.update_group_id').select2('data').map(function(elem){ 
                return elem.text
            });
            var group_text = data.join(",");
            var data1 = $('.update_user_id').select2('data').map(function(elem){ 
                return elem.text
            });
            var user_text = data1.join(",");
            var group_id = $(".update_group_id").val().join(",");
            var user_id = $(".update_user_id").val().join(",");

            update_all_file.append('name', $('#update_name').val());
            update_all_file.append('message', $('.update_message').val());
            update_all_file.append('id', id);
            update_all_file.append('description', $('#update_description').val());
            update_all_file.append('start_at', $('#update_date_start').val());
            update_all_file.append('end_at', $('#update_date_end').val());
            update_all_file.append('timezone', Intl.DateTimeFormat().resolvedOptions().timeZone);
            update_all_file.append('group_id', group_id);
            update_all_file.append('user_id', user_id);
            update_all_file.append('group_text', group_text);
            update_all_file.append('user_text', user_text);
            file = update_all_file;
        }
        var btn = Ladda.create(document.querySelector('#btn_project'));
        btn.start();
        $.ajax({
            url: "/campaign/insert_campaign",
            dataType: "json",
            type: "POST",
            data: file,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                show_campaign();
                btn.stop();
                if (data==true) {
                    if (id=="") {
                        swal("Success!", "Your campaign added successfully.", "success");
                        $('#projName').val('');
                        $('#projName').val('');
                        $('textarea.message').val('');
                        $('textarea#description').val('');
                        $('#date_start').val('');
                        $('#date_end').val('');
                        $('.user_id').val('').trigger("change");
                        $('.group_id').val('').trigger("change");
                    }else{
                        swal("Updated!", "Your campaign updated successfully.", "success");
                    }
                }else if(data==2){
                    swal("Failed!", "Campaign name is already exist please use another name.", "error");
                }else{
                   if (id!="") {
                        swal("Failed!", "Your can't create campaign right now please contact your administrator.", "error");
                    }else{
                        swal("Failed!", "Your can't update campaign right now please contact your administrator.", "error");
                    }
                }


            },
            error: function(jqXHR, exception) {
                btn.stop();

            }
        })
    })
    $(document).on('click', '#status_campaign', function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        $.ajax({
            url: "/campaign/update_status",
            dataType: "json",
            type: "POST",
            data: {
                id: id,
                status: status
            },
            success: function(data) {
                show_campaign();
                if (data == 0) {
                    toastr["error"]("Campaign deactivated successfully");
                } else {
                    toastr["success"]("Campaign activated successfully");
                }
            }
        })
    })
    $(document).on('click', '#delete_campaign', function() {
        var id = $(this).data('id');

        swal({
            title: "Are you sure?",
            text: "You want to delete this campaign?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes delete it!",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "/campaign/delete_campaign",
                    dataType: "json",
                    type: "POST",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        show_campaign();
                        if (data == true) {
                            swal("Deleted!", "Your campaign has been deleted.", "success");
                        } else if (data == false) {
                            swal("Ops!", "You can't delete this campaign right now.", "error");
                        }
                    }
                })
            }
        });
    })
    $(document).on('click','#campaign_send_text',function(){
        var id = $(this).data('id');
        $.ajax({
            url: "/campaign/get_contacts",
            dataType: "json",
            type: "POST",
            data:{
                id: id
            },
            success: function(data) {
                if (data.length>0) {
                    $('#send_campaign').modal('toggle');
                    $('#send_campaign').modal('show');
                    send_text(data,id);
                    // var length = data.length;
                    $('#num_attemp').text(0);
                    $('#num_sent').text(0);
                    for (i = 0; i < 1; i++) {
                        $('#message').text(data[i].message);
                        $('#contact').text('+'+data[i].number+', '+data[i].firstName+' '+data[i].lastName);
                    }
                }else{
                    swal("No Contacts!", "This campaign don't have contact.", "error");
                }
            },
            error: function (jqXHR, exception) {
                btn.stop();
                toastr.error(
                    'You can not send text right now'
                );
            }
        })
        
    });
    function send_text(data,id){
        var i = 0;
        var y = 0;
        var sent = 0;
        (function() {
            var btn = Ladda.create(document.querySelector('#btn_send_text'));
            if (i<data.length) {
                $(document).on('click','#btn_send_text',loadmore);
            }
            // $("#btn_project").on("click", loadmore);
            function loadmore() {
                btn.start();
                if (i<data.length) {
                    y++;
                    $('#message').text(data[i].message);
                    $('#contact').text('+'+data[i].number+', '+data[i].firstName+' '+data[i].lastName);
                    $('#num_attemp').text(y);
                    $.ajax({
                        url: "/campaign/send_text",
                        dataType: "json",
                        type: "POST",
                        data:{
                            timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                            id: id,
                            contact_id : data[i].contact_id
                        },
                        success: function(data) {
                            if (data===true) {
                                sent++
                                $('#num_sent').text(sent);
                            }
                            btn.stop();
                        }
                    })
                    i++;
                }else{
                    $('#send_campaign').modal('toggle');
                    $('#send_campaign').modal('hide');
                    swal({
                        title: "Total Message Sent: "+sent,
                        text:  "",
                        type:  "success"
                    }, function() {
                        // $('#send_campaign').modal('toggle');
                        // $('#send_campaign').modal('hide');
                    });
                    // $('#send_campaign').modal('toggle');
                    // $('#send_campaign').modal('hide');
                    i = i - data.length;
                    y = y - data.length;
                }
            }
        })();
    }

    $(document).on('click','#edit_campaign',function(){
        edit_all_file=[];
        Dropzone.forElement('#dropzone-update').removeAllFiles(true)
        var id = $(this).data('id');
        var user = "";
        var status = "";
        $.ajax({
            url: "/campaign/get_single_campaign",
            dataType: "json",
            type: "POST",
            data:{
                campaign: id,
            },
            success: function(data) {
                var array_group = [];
                var array_user = [];
                $('#id').val(data.id);
                $('#update_name').val(data.proj_name);
                $('.update_message').val(data.message);
                $('#update_description').val(data.description);
                $('#update_date_start').val(data.project_start);
                $('#update_date_end').val(data.project_end);

                var group = data.group_id.split(',');
                var user = data.assign_id.split(',');
                for(i=0;i<group.length;i++){
                    array_group.push(group[i]);
                }

                for(i=0;i<user.length;i++){
                    array_user.push(user[i]);
                }
                $('.update_group_id').val(array_group);
                $('.update_group_id').trigger('change');
                $('.update_user_id').val(array_user);
                $('.update_user_id').trigger('change');
                if (data.filename!="") {
                    var filename = data.filename;
                    var path = base_url+''+data.path;
                    var size = data.size;
                    var mockFile = { name: filename, size: size, path:path };
                    editDropzone.files.push(mockFile);
                    editDropzone.emit("addedfile", mockFile);
                    editDropzone.emit("thumbnail", mockFile, path);
                    editDropzone.emit("complete", mockFile);
                    edit_all_file.push(mockFile);
                    update_all_file.append('filename', filename);
                }

            }
        })
    })
})

function startIntro(){
    var intro = introJs();
      intro.setOptions({
        steps: [
          { 
            element: '#step1',
            intro: "Hello world!"
          },
          {
            element: '#step2',
            // element: document.querySelector('#step1'),
            intro: "This is a tooltip."
          },
          {
            element: '#step3',
            // element: document.querySelectorAll('#step2')[0],
            intro: "Ok, wasn't that fun?",
            position: 'right'
          },
          {
            element: '#step4',
            intro: 'More features, more fun.',
            position: 'left'
          },
          {
            element: '#step5',
            intro: "Another step.",
            position: 'bottom'
          },
          {
            element: '#step6',
            intro: 'Get it, use it.'
          }
        ]
      });

    intro.start();
}