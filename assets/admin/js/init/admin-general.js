var base_url = $('#base_url').val();
var auto_reply = 0;
var parts = window.location.search;
var lastSegment  = parts.substr(1);

$(document).ready(function() {
 	table_groupContacts = $('#view_contact_group').DataTable({
        "pageLength" : 10,
        "serverSide": true,
        "order": [[0, "asc" ]],
        "ajax":{
	          url :  base_url+'admin/show_groupContacts',
	          type : 'POST'
	    },
	    'columnDefs': [{
	         'targets': 0,
	         'searchable':false,
	         'orderable':false,
	         'className': 'dt-body-center',
	         'render': function (data, type, full, meta){
	             return '<input type="checkbox" name="id[]" value="' 
	                + $('<div/>').text(data).html() + '">';
	         }
	    }],
	   'order': [1, 'asc'],
	    "createdRow": function(row, data, dataIndex){
	         $(row).attr("id", "tblRow_" + data[0]);
	    }
    }); // End of DataTable
    function reload_table_groupContacts(){
        table_groupContacts.ajax.reload(null,false);
    }

    $(document).on('click','#delete_groupContacts', function(){
    	var id = $(this).data('id');
    	Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to delete this contact group?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  	if (result.value) {
			  	$.ajax({
					url: base_url+"Admin/delete_groupContacts",
					type: "POST",
					dataType: "json",
					data: {
						id:id
					},success:function(data){
						if (data==true) {
							 Swal.fire(
						      'Deleted!',
						      'Your group contact has been deleted.',
						      'success'
						    )
							 reload_table_groupContacts();
						}
					}
			  	})
		  	}
		})
    })
    $(document).on('click','#groupContacts_status',function(){
    	var id = $(this).data('id');
    	var status = $(this).data('stat');
    	$.ajax({
    		url: base_url+"Admin/groupContacts_status",
    		type: "POST",
    		dataType: "json",
    		data:{
    			id:id,
    			status:status
    		},success:function(data){
    			if (data==1) {
    				toastr["success"]("Contact group activate status");
    			}else if(data==0){
    				toastr["warning"]("Contact group deactivate status");
    			}else{
    				Swal.fire(
				      	'Ops!',
				      	'Something is wrong, please contact your administrator.',
				      	'success'
				    )
    			}
    			reload_table_groupContacts();
    		}
    	})
    });
    $(document).on('submit','.add_contactGroup',function(e){
		var btn = Ladda.create(document.querySelector('#save_group'));
		btn.start();
    	e.preventDefault();
    	$.ajax({
    		url: base_url+"Admin/add_contactGroup",
    		type: "POST",
    		dataType: "json",
    		data:{
    			id:$('#group_id').val(),
    			name:$('#group_name').val(),
    			data_name:$('#group_name').data('name')
    		},success:function(data){
    			btn.stop();
    			if (data==true) {
    				if ($('#group_name').data('name')=="") {
    					toastr["success"]("Contact group added successfully");
    				}else{
    					toastr["success"]("Contact group updated successfully");
    				}
    				reload_table_groupContacts();
    				$('#group_name').val("");
    			}else if(data==2){
    				toastr["error"]("Group Contact already exist");
    			}
    		}
    	})
    })
    
    $(document).on('click','#edit_groupContacts',function(){
    	var id = $(this).data('id');
    	var name = $(this).data('name');
    	$('#group_name').val(name);
    	$('#group_id').val(id);
    	$('#group_name').attr("data-name",name);
    })

	$(".name_alert").hide();

	($("#hide_auto_reply").val() === "0") ? $("#auto_reply_text").hide() : $("#auto_reply_text").show();

    table_user = $('#view_user').DataTable({
        "pageLength" : 10,
        "serverSide": true,
        "order": [[0, "asc" ]],
        "ajax":{
	          url :  base_url+'admin/show_user/'+lastSegment,
	          type : 'POST'
	    },
	    'columnDefs': [{
	         'targets': 0,
	         'searchable':false,
	         'orderable':false,
	         'className': 'dt-body-center custom-control-label',
	         'render': function (data, type, full, meta){
	             return '<input type="checkbox" name="id[]" value="' 
	                + $('<div/>').text(data).html() + '">';
	         }
	    }],
	   'order': [1, 'asc'],
	    "createdRow": function(row, data, dataIndex){
	         $(row).attr("id", "tblRow_" + data[0]);
	    }
    }); // End of DataTable

    $("#select-all").click(function(){
    	$("input[type=checkbox]").not('#user_stat, #contact_stat, #contact_msg_stat').prop('checked', $(this).prop('checked'));
    	if($('input[type="checkbox"]').not('#user_stat, #contact_stat, #contact_msg_stat').is(':checked')){
    		$('#all_action_btn, #msg_all_btn').show();
		}else{
		 	$('#all_action_btn').hide();
		}
	});
	$("#selected_contact").click(function(){
    	if($('input[type="checkbox"]').not('#user_stat, #contact_stat, #contact_msg_stat').is(':checked')){
    		$('#all_action_btn, #msg_all_btn').show();
		}else{
		 	$('#all_action_btn').hide();
		}
	});

    function reload_table_user(){
        table_user.ajax.reload(null,false);
     }
     $(document).on("click","#user_stat",function() {
		var id =  $(this).data('id');
		var stat = $(this).data('stat');
		$.ajax({
			url: base_url+"Admin/updateUserStatus",
			type: "POST",
			dataType: "json",
			data: {
				id:id,
				stat:stat
			},
			success: function(data){
				if(data==0){
					toastr["error"]("User deactivate status");
					reload_table_user();
				}else{
					toastr["success"]("User activate status");
					reload_table_user();
				}
			}
		})
	});
    $(document).on('keydown keyup','.edit_message',function(e){
		var character = $(this).val().length;
		$('#textarea_feedback').text(character+'/1200');
	})

	$("#show_attach").click(function(e){
		var value = $(this).data('value');
		if (value==0) {
			$("#dropzone-add").show(500);
			$(this).data('value',1);
		}else{
			$("#dropzone-add").hide(500);
			$(this).data('value',0);
		}
	});
	view_contact();
	function view_contact(){
		$.ajax({
			url: base_url+"Admin/getContacts",
			type: "POST",
			dataType: "json",
			success:function(data){
				var table = '';
				for(i=0;i<data.length;i++){
					var switcher = '';
					switcher="";
		        	if (data[i].status=="0"||data[i].status=="2") {
		        		switcher="unchecked";
		        	}else{
		        		switcher="checked";
		        	}
					table +=
					'<tr>'+
						'<td><input type="checkbox"/></td>'+
						'<td>'+data[i].name+'</td>'+
						'<td>'+data[i].number+'</td>'+
						'<td>'+data[i].email+'</td>'+
						'<td>'+data[i].name+'</td>'+
						'<td>'+data[i].group_name+'</td>'+
						'<td>'+
						    '<label class="switcher switcher-success">'+
		                       ' <input id="contact_stat" data-id='+data[i].id+' data-stat='+data[i].status+' type="checkbox" class="switcher-input" '+switcher+'>'+
		                        '<input type="hidden" id="stat" value ="" />'+
		                        '<span class="switcher-indicator">'+
		                            '<span class="switcher-yes">'+
		                                '<span class="ion ion-md-checkmark"></span>'+
		                            '</span>'+
		                            '<span class="switcher-no">'+
		                                '<span class="ion ion-md-close"></span>'+
		                           ' </span>'+
		                        '</span>'+
		                    '</label>'+
						'</td>'+
						'<td>'+data[i].id+'</td>'+
					'</tr>';
				}
				$('#view_contacts').html(table);
			}
		})
	}

	table_contact = $('#view_contact').DataTable({
        "pageLength" : 10,
        "serverSide": true,
        "order": [[0, "asc" ]],
        "ajax":{
	          url :  base_url+'admin/show_contact',
	          type : 'POST'
	    },
	      'columnDefs': [{
	         'targets': 0,
	         'searchable':false,
	         'orderable':false,
	         'className': 'dt-body-center',
	         'render': function (data, type, full, meta){
	             return '<input type="checkbox" name="id[]" value="' 
	                + $('<div/>').text(data).html() + '">';
	         }
	      }],
	      'order': [1, 'asc'],
	      "createdRow": function(row, data, dataIndex){
	         // $(row).attr("id", "tblRow_" + data[0]);
	         $(row).attr({name:"id","tblRow_":+data[0]});
	      }
    }); // End of DataTable
    function reload_table_contact(){
        table_contact.ajax.reload(null,false);
    }
    
    $(document).on('click','#activate_user',function(){
		var data = " ";
		if ($(this).data('id')!==undefined&&$(this).data('id')!="") {
			console.log($(this).data('id'));
			data = $(this).data('id');
		}else{
			var value = [];
			$('input[type=checkbox]:checked').not('#user_stat, #contact_stat, #contact_msg_stat, #select-all').each(function () {
	            value.push($(this).val());
	        });
	        data = value;
	    }

		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to activate these users?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!'
		}).then((result) => {

		  if (result.value) {
		  	$.ajax({
	        	url: base_url+"Admin/activate_user",
	        	type: "POST",
				dataType: "json",
				data: {
					data:data,
				},
				success: function(data){
					reload_table_contact()
					Swal.fire(
				      'Activated!',
				      'User has been activate.',
				      'success'
				    ).then(function(){
							location.reload(true);
					  });
				}
	        })
		  }
		})
	})

	$(document).on('click','#deactivate_user',function(){
		var data = " ";
		if ($(this).data('id')!==undefined&&$(this).data('id')!="") {
			console.log($(this).data('id'));
			data = $(this).data('id');
		}else{
			var value = [];
			$('input[type=checkbox]:checked').not('#user_stat, #contact_stat, #contact_msg_stat, #select-all').each(function () {
	            value.push($(this).val());
	        });
	        data = value;
	    }

		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to deactivate these users?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, deactivate it!'
		}).then((result) => {

		  if (result.value) {
		  	$.ajax({
	        	url: base_url+"Admin/deactivate_user",
	        	type: "POST",
				dataType: "json",
				data: {
					data:data,
				},
				success: function(data){
					reload_table_contact()
					Swal.fire(
				      'Deactivated!',
				      'Users has been deactivated.',
				      'success'
				    ).then(function(){
							location.reload(true);
					  });
				}
	        })
		  }
		})
	})
	
	$(document).on('click','#delete_contact',function(){
		var data = " ";
		if ($(this).data('id')!==undefined&&$(this).data('id')!="") {
			console.log($(this).data('id'));
			data = $(this).data('id');
		}else{
			var value = [];
			$('input[type=checkbox]:checked').not('#user_stat, #contact_stat, #contact_msg_stat, #select-all').each(function () {
	            value.push($(this).val());
	        });
	        data = value;
	    }

		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to delete these contacts?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {

		  if (result.value) {
		  	$.ajax({
	        	url: base_url+"Admin/delete_contact",
	        	type: "POST",
				dataType: "json",
				data: {
					data:data,
				},
				success: function(data){
					reload_table_contact()
					Swal.fire(
				      'Deleted!',
				      'Your contact has been deleted.',
				      'success'
				    ).then(function(){
							window.location.href=base_url+"admin/contacts";
					  });
				}
	        })
		  }
		})
	})
	$(document).on('click','#delete_single_contact',function(){
		var data = " ";
		var id = $(this).data('id');
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to delete these contacts?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {

		  if (result.value) {
		  	$.ajax({
	        	url: base_url+"Admin/delete_single_contact",
	        	type: "POST",
				dataType: "json",
				data: {
					id:id,
				},
				success: function(data){
					reload_table_contact()
					Swal.fire(
				      'Deleted!',
				      'Your contact has been deleted.',
				      'success'
				    ).then(function(){
							window.location.href=base_url+"admin/contacts";
					  });
				}
	        })
		  }
		})
	})

	$(document).on('click','#do_not_send',function(){
		var value=[];
		$('input[type=checkbox]').not('#user_stat, #contact_stat, #contact_msg_stat, #select-all').each(function () {
            value.push($(this).val());
        });
        Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to deactivate these contacts?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, deactivate it!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
		        	url: base_url+"Admin/do_not_send",
		        	type: "POST",
					dataType: "json",
					data: {
						data:value,
					},
					success: function(data){
						$('input[type=checkbox]').not('#user_stat, #contact_stat, #contact_msg_stat').prop("checked", false);
						// toastr["error"]("Contact status changed");
						reload_table_contact();
						Swal.fire(
					      'Activated!',
					      'Your contact has been deactivated.',
					      'success'
					    ).then(function(){
								window.location.href=base_url+"admin/contacts";
						});
					}
		        })
	    	}
		})
	})

	$(document).on('click','#activate_contact',function(){
		var value=[];
		$('input[type=checkbox]').not('#user_stat, #contact_stat, #contact_msg_stat, #select-all').each(function () {
            value.push($(this).val());
        });
        Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to active these contacts?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, activate it!'
		}).then((result) => {

		  if (result.value) {
				$.ajax({
		        	url: base_url+"Admin/activate_contact",
		        	type: "POST",
					dataType: "json",
					data: {
						data:value,
					},
					success: function(data){
						$('input[type=checkbox]').not('#user_stat, #contact_stat, #contact_msg_stat').prop("checked", false);
						// toastr["success"]("Contact status activate");
						reload_table_contact();
						Swal.fire(
					      'Activated!',
					      'Your contact has been activated.',
					      'success'
					    ).then(function(){
								window.location.href=base_url+"admin/contacts";
						});
					}
		        })
		  
		  }
		})
	})
	$(document).on('click','#contact_stat',function(){
		var id = $(this).data('id');
		var stat = $(this).data('stat');
		$.ajax({
        	url: base_url+"Admin/contact_stat",
        	type: "POST",
			dataType: "json",
			data: {
				id:id,
				stat:stat
			},
			success: function(data){
				if (data==0||data==2) {
					toastr["success"]("Contact status activate");
				}else{
					toastr["error"]("Contact status deactivate");
				}
			}
        })
	})

	$(document).on('click','#contact_msg_stat',function(){
		var id = $(this).data('id');
		var stat = $(this).data('stat');
		$.ajax({
        	url: base_url+"Admin/contact_msg_stat",
        	type: "POST",
			dataType: "json",
			data: {
				id:id,
				stat:stat
			},
			success: function(data){
				if (stat==0) {
					toastr["error"]("Contact status changed");
				}else{
					toastr["success"]("Contact status changed");
				}
				reload_table_contact();
			}
        })
	})
	$('#importContactsForm').on('submit',function(evt){
		
		evt.preventDefault();
		$("#response").html("");
		var fileType = ".csv";
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+("+ fileType + ")$");
		if (!regex.test($("#csv_file").val().toLowerCase())) {
			$("#response").addClass('alert-warning').html('<strong>Oops! </strong>The file you have selected is invalid.'+
								  	'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
								    	'<span aria-hidden="true">&times;</span>'+
								  	'</button>');
			return false;
		}else{
			$.ajax({
				url: base_url+'admin/import_contacts',
				method: 'POST',
				data:new FormData(this),
				contentType:false,
				cache:false,
				processData:false,
				beforeSend: function(){
					$('#uploadImportedFile').html('Importing, please wait').attr('disabled','disabled');
				},
				success:function(res){
					if(res==1){
						$('#importContacts').modal('hide');
	            		toastr.success(
	                        'Contact successfully import'
						);
						reload_table_contact();
						$("#csv_file").val('');
						$('#uploadImportedFile').html('Upload File').removeAttr('disabled');
	            	}
	            	else if(res==4){
	            		toastr.error(
	            			'Please use another email',
	            			'Contact email are already exists'
						);
	            		toastr.error(
	            			'Please use another number',
	            			'Contact number are already exists'
						);
						$("#csv_file").val('');
						$('#uploadImportedFile').html('Upload File').removeAttr('disabled');
	            	}else if(res==3){
	            		toastr.error(
	            			'Please use another number',
	            			'Contact number are already exists'
						);
						$("#csv_file").val('');
						$('#uploadImportedFile').html('Upload File').removeAttr('disabled');
	            	}else if(res==2){
	            		toastr.error(
	            			'Please use another email',
	            			'Contact email are already exists'
						);
						$("#csv_file").val('');
						$('#uploadImportedFile').html('Upload File').removeAttr('disabled');
	            	}else{
	            		toastr.error(
							'Your CSV format is not valid'
						);
						$("#csv_file").val('');
						$('#uploadImportedFile').html('Upload File').removeAttr('disabled');
	            	}
            	    setTimeout(function(){
				        location.reload();
				    }, 2000)       
				},error: function (jqXHR, exception) {
					$("#csv_file").val('');
					$('#uploadImportedFile').html('Upload File').removeAttr('disabled');
					toastr.error(
						'You can not create contact right now'
					);
				}
			});
		}
		return true;
	});
	show_image();
	function show_image(){
		$.ajax({
        	url: base_url+"Admin/show_image",
        	type: "POST",
			dataType: "json",
			success: function(data){
				var img = '';
				var src = '';
				if (data.image!="") {
					var src = base_url+'assets/profile_img/'+data.id+'/'+data.image;
					var img = '<img id="output" src="'+src+'" alt="" class="d-block ui-w-30 rounded-circle">';
				}
				$('#topbar_profile_img').html(''+img+'<span class="px-1 mr-lg-2 ml-2 ml-lg-0">'+data.username+'</span>');
			}
        })
	}
	$(document).on('submit','.add_contact',function(e){
		var btn = Ladda.create(document.querySelector('#create_contact'));
		btn.start();

		e.preventDefault();
		var formData = new FormData(this);
		$.ajax({
			url: base_url+"/Admin/create_contact",
			dataType: "json",
			type: "POST",
			data:formData,
			processData:false,
			contentType:false,
			success:function(data){
				btn.stop();
				if(data===true){
					console.log(data);
            		toastr.success(
                        'Contact Inserted Successfully'
					);
					setTimeout(function(){ window.location.href = "/admin/contacts"; }, 2000);
            	}
            	else if(data==4){
            		toastr.error(
            			'Please use another email',
            			'Contact email is already exists'
					);
            		toastr.error(
            			'Please use another number',
            			'Contact number already exists'
					);
            	}else if(data==3){
            		toastr.error(
            			'Please use another number',
            			'Contact number already exists'
					);
            	}else if(data==2){
            		toastr.error(
            			'Please use another email',
            			'Contact email is already exists'
					);
            	}else{
            		toastr.error(
						'You can not create contact right now'
					);
            	}

			},
			error: function (jqXHR, exception) {
				btn.stop();
				toastr.error(
					'You can not create contact right now'
				);
			}
		});
	})

	$(document).on('submit','.update_contact',function(e){
		var btn = Ladda.create(document.querySelector('#create_contact'));
		btn.start();
		e.preventDefault();
		var formData = new FormData(this);
		Swal.fire({
			  title: 'Are you sure do you want update this user?',
			  text: "",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes'
			}).then((result) => {
	  		if (result.value) {
	  			$.ajax({
					url: base_url+"/Admin/update_contact",
					dataType: "json",
					type: "POST",
					data:formData,
					processData:false,
					contentType:false,
					success:function(data){
						btn.stop();
						if(data===true){
		        			Swal.fire(
							      'Updated!',
							      'Contact Updated Successfully.',
							      'success'
							).then(function(){
				                    setTimeout(function(){ window.location.href = "/admin/contacts"; });
			              	});

		            	}
		            	else if(data==4){
		            		toastr.error(
		            			'Please use another email',
		            			'Contact email is already exists'
							);
		            		toastr.error(
		            			'Please use another number',
		            			'Contact number already exists'
							);
		            	}else if(data==3){
		            		toastr.error(
		            			'Please use another number',
		            			'Contact number already exists'
							);
		            	}else if(data==2){
		            		toastr.error(
		            			'Please use another email',
		            			'Contact email is already exists'
							);
		            	}else{
		            		toastr.error(
								'You can not create contact right now'
							);
		            	}
					},
					error: function (jqXHR, exception) {
						btn.stop();
						toastr.error(
							'You can not create contact right now'
						);
					}
				});
		  	}else{
		  		btn.stop();
		  	}
		})
	})

	$(document).on('submit','.register',function(e){
		var btn = Ladda.create(document.querySelector('#create_account'));
		btn.start();
		e.preventDefault();
		var formData = new FormData(this);
		$.ajax({
			url: base_url+"Auth/create_account",
			dataType: "json",
			type: "POST",
			data:formData,
			processData:false,
			contentType:false,
			success:function(data){
				btn.stop();
				if(data==3){
            		toastr.success(
                    	'Please check your email for verification',
                        'Registered Successfully'
					);
					if($("#group_id").val() == 2 ){
						location.href = base_url+'admin/users?val=client';	
					}else if($("#group_id").val() == 1){
						location.href = base_url+'admin/users?val=administrator';	
					}else if($("#group_id").val() == 3){
						location.href = base_url+'admin/users?val=volunteers';	
					}else{
						location.href = base_url+'admin/users?val=volunteers';	
					}
				}
            	else if(data==4){
            		toastr.error(
            			'Please use another username',
		            	'Username is already taken'
					);
					toastr.error(
            			'Please use another email',
		            	'Email is already taken'
					);
            	}else if(data==6){
            		toastr.error(
            			'Please use another email',
		            	'Email is already taken'
					);
            	}else if(data==7){
            		toastr.error(
            			'Please use another username',
		            	'Username is already taken'
					);
            	}else if(data==5){
            		toastr.error(
            			'Please contact your administrator',
						'You can not register right now'
					);
            	}
			},
			error: function (jqXHR, exception) {
				btn.stop();
        		toastr.error(
        			'Please contact your administrator',
					'You can not register right now'
				);
			}
		});
	})

	$(document).on('submit','.update_account',function(e){
		var btn = Ladda.create(document.querySelector('#create_account'));
		btn.start();
		e.preventDefault();
		var formData = new FormData(this);
		Swal.fire({
			  title: 'Are you sure do you want update this user?',
			  text: "",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes'
			}).then((result) => {
	  		if (result.value) {
	  			$.ajax({
					url: base_url+"Auth/update_account",
					dataType: "json",
					type: "POST",
					data:formData,
					processData:false,
					contentType:false,
					success:function(data){
						btn.stop();
						if(data===true){
		        			Swal.fire(
							      'Update!',
							      'Profile has been changed.',
							      'success'
							)
		            	}
		            	else if(data==4){
		            		toastr.error(
		            			'Please use another username',
				            	'Username is already taken'
							);
							toastr.error(
		            			'Please use another email',
				            	'Email is already taken'
							);
		            	}else if(data==3){
		            		toastr.error(
		            			'Please use another email',
				            	'Email is already taken'
							);
		            	}else if(data==2){
		            		toastr.error(
		            			'Please use another username',
				            	'Username is already taken'
							);
		            	}else{
		            		toastr.error(
		            			'Please contact your administrator',
								'You can not register right now'
							);
		            	}
					},
					error: function (jqXHR, exception) {
						btn.stop();
						toastr.error(
	            			'Please contact your administrator',
							'You can not register right now'
						);
					}
				});
		  	}else{
		  		btn.stop();
		  	}
		})
	})
});

$(document).on('keypress', '#name', function (event) {
    var regex = new RegExp("^[a-zA-Z ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}); 
//var savingprofile = Ladda.bind("#login", {timeout: 2500});
$(document).on("click", "#login", function(){
	var username = $("#username1").val();
	var password = $("#password1").val();
	var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	var button = Ladda.create(this);
    $('input[type="submit"]').attr('disabled','disabled');
	if (username == "" && password =="") {
		toastr["error"]("All fields are required");
	}
	if (username !="" || password!="") {
		button.start();
		$.ajax({
			url: base_url+"Auth",
			dataType: "json",
			type: "POST",
			data:{
				username:username,
				password:password,
				timezone: timezone
			},
			success:function(data){
				button.stop();
				if (data==true) {
					toastr.success(
                        'Login successfully!'
					);
	                setTimeout(function(){ window.location.href = "/masssms/admin"; }, 2000);
				}
				else if (data==3) {
					toastr.warning(
                        'Please check your email.',
                        'Your account doesn&#39;t verify.'
					);
				}else if(data == false){
					toastr.error(
                        'Please check username and password.',
                        'Invalid Login Credentials.'
					);
				}else if(data==4){
					toastr.error(
                        'Sorry we couldn&#39;t find you account.'
					);
				}else{
					toastr.error(
                        'Please contact your administrator','You can&#39;t login right now'
					);
				}
			}
		})
	}
})
$(document).on("submit", ".reset_pass",function(e){
	e.preventDefault();
	var password = $("#password").val();
	var password2= $("#password2").val();
	var data = $(this).serializeArray();

	$.ajax({
		url: base_url+"Auth/change_pass",
		type: "POST",
		dataType: "json",
		data:{
			data:data
		},
		success:function(data) {
			if (data == true) {
				toastr["success"]("Pleae check your email to confirm","Password changed successfully");
				$("#password").val("");
				$("#password2").val("");
			}else{
				toastr["error"]("Please checked your email","You password is already changed");
			}
		}
	})

})
$(document).on("click","#get_key",function(){
	var email = $("#recover_email").val();
	var button = Ladda.create(this);
	button.start();
	if (email=="") {
		toastr["error"]("Please enter your email");
	}else{
		$.ajax({
			url: base_url+"Auth/get_key",
			type: "POST",
			dataType: "json",
			data: {
				email: email
			},
			success: function(data){
				if (data == true) {
					button.stop();
					toastr["success"]("We've sent a password reset link on that email");
					$("#recover_email").val("");
				}else if(data==false){
					button.stop();
					toastr["error"]("Sorry, we couldn't find your email");
				}else{
					toastr.error(
                        'Please contact our administrator.',
                        'Your account is deactivated.'
					);
				}
			}
		})
	}
})


function newFunction(stat) {
	alert(stat);
}

function save(){

	if($("#permissionName").val() === ""){
		$(".name_alert").show();
		return;
	}
	$.ajax({
			url: base_url+"permission/savePermission",
			type: "POST",
			data: {
				permissionID: $("#idPermission").val(),
				permissionName:$("#permissionName").val(),
	        	modDashboard:$("#modDashboard").prop('checked'),
	        	modCampaign:$("#modCampaign").prop('checked'),
	        	addCampaign:$("#addCampaign").prop('checked'),
	        	updateCampaign:$("#updateCampaign").prop('checked'),
	        	deleteCampaign:$("#deleteCampaign").prop('checked'),
	        	modConversation:$("#modConversation").prop('checked'),
	        	modConversation:$("#modConversation").prop('checked'),
	        	viewMessage:$("#viewMessage").prop('checked'),
	        	sendMessage:$("#sendMessage").prop('checked'),
	        	deleteMessage:$("#deleteMessage").prop('checked'),
	        	modUser:$("#modUser").prop('checked'),
	        	addUser:$("#addUser").prop('checked'),
	        	updateUser:$("#updateUser").prop('checked'),
	        	deleteUser:$("#deleteUser").prop('checked'),
	        	modPermission:$("#modPermission").prop('checked'),
	        	createPermission:$("#createPermission").prop('checked'),
	        	updatePermission:$("#updatePermission").prop('checked'),
	        	deletePermission:$("#deletePermission").prop('checked'),
	        	setGroupPermission:$("#setGroupPermission").prop('checked'),
	        	setClientPermission:$("#setClientPermission").prop('checked'),
	        	removeClientPermission:$("#removeClientPermission").prop('checked'),
	        	setVolunteerPermission:$("#setVolunteerPermission").prop('checked'),
	        	removeVolunteerPermission:$("#removeVolunteerPermission").prop('checked'),
	        	modContacts:$("#modContacts").prop('checked'),
                addContacts:$("#addContacts").prop('checked'),
                updateContacts:$("#updateContacts").prop('checked'),
                deleteContacts:$("#deleteContacts").prop('checked'),
                modGroup:$("#modGroup").prop('checked'),
                addGroup:$("#addGroup").prop('checked'),
                updateGroup:$("#updateGroup").prop('checked'),
                deleteGroup:$("#deleteGroup").prop('checked'),
               	modEmail:$("#modEmail").prop('checked'),
                addEmail:$("#addEmail").prop('checked'),
                sendEmail:$("#sendEmail").prop('checked'),
                deleteEmail:$("#deleteEmail").prop('checked'),
                modProject:$("#modProject").prop('checked'),
                addProject:$("#addProject").prop('checked'),
                updateProject:$("#updateProject").prop('checked'),
                deleteProject:$("#deleteProject").prop('checked'),
                modTemplate:$("#modTemplate").prop('checked'),
                addTemplate:$("#addTemplate").prop('checked'),
                updateTemplate:$("#updateTemplate").prop('checked'),
                deleteTemplate:$("#deleteTemplate").prop('checked'),
			},
			success: (resp) => {
				$("#updatePermssionModal").modal('hide');
			    Swal.fire(
	                'Good job!',
	                'Permission Save',
	                'success'
	                ).then(function(){

	                    location.reload();
              	});
			}
		})
}
function permission(){
	$(".permissionModal").html('');
	$.ajax({
			url: base_url+"permission/getPermissionID",
			type: "POST",
	        dataType: "json",
			data: {
				id:$("#permission_name").val()
			},
			success: (resp) => {
				var mConversation=[];
				var color;

				//Dashboard Module
				resp.modDashboard==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Dashboard</kbd><br>');

				//Campaign Module
				resp.modCampaign==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;"> Campaign</kbd><br>');
				resp.addCampaign==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Add Campaign</kbd>');
				resp.updateCampaign==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Update Campaign</kbd>');
				resp.deleteCampaign==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete Campaign</kbd><br>');


				//Conversation Module
				if(resp.modConversation==="true"){
					(resp.deleteMessage==="true") ? mConversation.push('Delete Message'):'';
					(resp.viewMessage==="true") ? mConversation.push('View Message'):'';
					(resp.sendMessage==="true") ? mConversation.push('Send Message'):'';
					if (mConversation.length == 0){
						$(".permissionModal").append('<kbd class="ml-1" style="background-color:red;font-size:9px;">Conversation</kbd>')
					}else{
						$(".permissionModal").append('<kbd class="ml-1" style="background-color:green;font-size:9px;">'+mConversation.join()+'</kbd><br>')
					}
				}else{
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:red;font-size:9px;">Conversation</kbd><br>')
				};

				//Cotacts Module
				resp.modContacts==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Contacts</kbd>');
				resp.addContacts==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Add Contacts</kbd>');
				resp.updateContacts==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Update Contacts</kbd>');
				resp.deleteContacts==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete Contacts</kbd><br>');

				//Template Module
				resp.modTemplate==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Template</kbd>');
				resp.addTemplate==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Add Template</kbd>');
				resp.updateTemplate==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Update Template</kbd>');
				resp.deleteTemplate==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete Template</kbd><br>');

				//Project Module
				resp.modProject==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Project</kbd>');
				resp.addProject==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Add Project</kbd>');
				resp.updateProject==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Update Project</kbd>');
				resp.deleteProject==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete Project</kbd><br>');

				//Email Module
				resp.modEmail==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Email</kbd>');
				resp.sendEmail==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Send Email</kbd>');
				resp.addEmail==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Add Email</kbd>');
				resp.deleteEmail==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete Email</kbd><br>');
	

				//Group Module
				resp.modGroup==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Group</kbd>');
				resp.addGroup==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Add Group</kbd>');
				resp.updateGroup==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Update Group</kbd>');
				resp.deleteGroup==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete Group</kbd><br>');

				//User Module
				resp.modUser==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">User</kbd>');
				resp.addUser==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Add User</kbd>');
				resp.updateUser==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Update User</kbd>');
				resp.deleteUser==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete User</kbd><br>');

				//Permission Module
				resp.modPermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Permission</kbd>');
				resp.createPermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Create Permission</kbd>');
				resp.updatePermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Update Permission</kbd>')
				resp.deletePermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Delete Permission</kbd><br>');
				resp.setGroupPermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Set Group Permission</kbd>');
				resp.setClientPermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Set Client Permission</kbd>');
				resp.removeClientPermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Remove Client Permission</kbd><br>');
				resp.setVolunteerPermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Set Volunteer Permission</kbd>');
				resp.removeVolunteerPermission==="true" ? color="green" : color="red";
					$(".permissionModal").append('<kbd class="ml-1" style="background-color:'+color+';font-size:9px;">Remove Volunteer Permission</kbd>');
			}
		})
}
var group,group_id;
function showmodal(id){
	$('#permissionModal').modal('show');
	$("#user_id").val(id);
	group_id = $(".group-permission").attr('id');
	(typeof id === 'undefined')? group = true : console.log('false');
}
function userPermission(){

		if($("#permission_name").val() == 0){
			$('#permissionModal').modal('hide');
			Swal.fire(
	                '',
	                'No permission selected',
	                'error'
	                ).then(function(){
	                    $('#permissionModal').modal('show');
              	});
			return;
		}
		var url,id;
		if(group == true){
			url = base_url+"permission/groupPermission";
			id = group_id;
			console.log('Group');
		}else{
			url = base_url+"permission/userPermission";
			id = $("#user_id").val();
			console.log('User')
		}
		$.ajax({
			url: url,
			type: "POST",
			data: {
				permission_id:$("#permission_name").val(),
	        	user_id:id
			},
			success: (resp) => {
				$('#permissionModal').modal('hide');
			    Swal.fire(
	                'Good job!',
	                'User Permission Save',
	                'success'
	                ).then(function(){
	                    location.reload();
              	});
			}
	})
}
function deletePermission(id){
	Swal.fire({
			title: 'Are you sure?',
			text: "You want to delete this permission",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: base_url+"permission/deletePermission",
						type: "POST",
						data: {
							permission_id:id
						},
						success: (resp) => {
						    Swal.fire(
				                'Good job!',
				                'Permission Deleted',
				                'success'
				                ).then(function(){
				                    location.reload();
			              	});
						}
				})
			}
		})
}
function userDeletePermission(id){
	$.ajax({
			url: base_url+"permission/userDeletePermission",
			type: "POST",
			data: {
				user_id:id
			},
			success: (resp) => {
			    Swal.fire(
	                'Good job!',
	                'Permission Deleted',
	                'success'
	                ).then(function(){
	                    location.reload();
              	});
			}
	})
}

$("#saveNew").change(function() {
    if(this.checked) {
       $("#permissionName").val('');
       $("#idPermission").val('');
    }
});

$('#auto_reply').change(function(){
  ($("#auto_reply").prop("checked") == true) ? auto_reply = 1 : auto_reply = 0;
  (auto_reply == 1) ? $("#auto_reply_text").show() : $("#auto_reply_text").hide();
});

function saveCampaign(id){
	var data = $('#contacts').select2('data').map(function(elem){ 
        return elem.text
   	});
   	var contacts = $("#contacts").val().join(",");
   	var datatext = data.join(",");
   	console.log(datatext);
    $("#saveNew").prop('checked', false);
    if($("#campaign_title").val()==="" || $("#campaign_text").val() === "" || $("#start_at").val() === ""){
        Swal.fire(
            'Ooops!',
            'Fill all data',
            'error'
            )
        return; 
    }

    $.ajax({
            url: base_url+"Campaign/saveCampaign",
            type: "POST",
            data: {
                id:id,
                campaign_title:$("#campaign_title").val(),
                campaign_text:$("#campaign_text").val(),
                assign_to:$(".users").val(),
                start_at:$("#start_at").val(),
                tags: $("#bs-tagsinput-2").val(),
             	contacts: contacts,
             	contacts_names: datatext,
             	auto_reply: auto_reply,
             	auto_reply_text: $("#auto_reply_text").val()
            },
            success: (resp) => {
                Swal.fire(
                    'Good job!',
                    'Campaign Save',
                    'success'
                    ).then(function(){
                    	window.location.href=base_url+'admin/campaigns';
                  });    
            }
    })
}
function deleteCampaign(id){
			Swal.fire({
					title: 'Are you sure?',
					text: "You want to delete this campaign",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
					}).then((result) => {
							if (result.value) {
							Swal.fire(
							'Deleted!',
							'Campaign has been deleted.',
							'success'
							).then(function(){
								window.location.href=base_url+"campaign/deleteCampaign?id="+id;
						  });
						}
					})
}
$(".editButton").click(function() {
	var data = $(this).closest("tr").find(".permissionTD");
	$("#permissionName").val(data.closest('tr')['prevObject'][0].previousElementSibling.textContent);
	$("#idPermission").val(data.closest('tr')['prevObject'][0].previousElementSibling.id);
	var i=1;
	$("#updatePermssionModal").modal("show");
	(data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modDashboard').prop('checked', true) :  $('#modDashboard').prop('checked', false);

   	//Campaign Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modCampaign').prop('checked', true) :  $('#modCampaign').prop('checked', false);
  	(data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#addCampaign').prop('checked', true) :  $('#addCampaign').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#updateCampaign').prop('checked', true) :  $('#updateCampaign').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteCampaign').prop('checked', true) :  $('#deleteCampaign').prop('checked', false);

    //Conversation Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modConversation').prop('checked', true) :  $('#modConversation').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#viewMessage').prop('checked', true) :  $('#viewMessage').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#sendMessage').prop('checked', true) :  $('#sendMessage').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteMessage').prop('checked', true) :  $('#deleteMessage').prop('checked', false);

     //Contact Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modContacts').prop('checked', true) :  $('#modContacts').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#addContacts').prop('checked', true) :  $('#addContacts').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#updateContacts').prop('checked', true) :  $('#updateContacts').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteContacts').prop('checked', true) :  $('#deleteContacts').prop('checked', false);

     //Group Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modGroup').prop('checked', true) :  $('#modGroup').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#addGroup').prop('checked', true) :  $('#addGroup').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#updateGroup').prop('checked', true) :  $('#updateGroup').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteGroup').prop('checked', true) :  $('#deleteGroup').prop('checked', false);

    //User Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modUser').prop('checked', true) :  $('#modUser').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#addUser').prop('checked', true) :  $('#addUser').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#updateUser').prop('checked', true) :  $('#updateUser').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteUser').prop('checked', true) :  $('#deleteUser').prop('checked', false);

    //Permission Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modPermission').prop('checked', true) :  $('#modPermission').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#createPermission').prop('checked', true) :  $('#createPermission').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#updatePermission').prop('checked', true) :  $('#updatePermission').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deletePermission').prop('checked', true) :  $('#deletePermission').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#setGroupPermission').prop('checked', true) :  $('#setGroupPermission').prop('checked', false);


    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#setClientPermission').prop('checked', true) :  $('#setClientPermission').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#removeClientPermission').prop('checked', true) :  $('#removeClientPermission').prop('checked', false);
   	(data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#setVolunteerPermission').prop('checked', true) :  $('#setVolunteerPermission').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#removeVolunteertPermission').prop('checked', true) :  $('#removeVolunteerPermission').prop('checked', false);


     //Template Module
 	(data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modTemplate').prop('checked', true) :  $('#modTemplate').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#addTemplate').prop('checked', true) :  $('#addTemplate').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#updateTemplate').prop('checked', true) :  $('#updateTemplate').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteTemplate').prop('checked', true) :  $('#deleteTemplate').prop('checked', false);

    //Project Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modProject').prop('checked', true) :  $('#modProject').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#addProject').prop('checked', true) :  $('#addProject').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#updateProject').prop('checked', true) :  $('#updateProject').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteProject').prop('checked', true) :  $('#deleteProject').prop('checked', false);

    //Email Module
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#modEmail').prop('checked', true) :  $('#modEmail').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#sendEmail').prop('checked', true) :  $('#sendEmail').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#addEmail').prop('checked', true) :  $('#addEmail').prop('checked', false);
    (data[0].childNodes[i++].style['backgroundColor'] === "green")? $('#deleteEmail').prop('checked', true) :  $('#deleteEmail').prop('checked', false);

});
$('.table').on('click', 'tbody td.permissionTD', function() {
    $("#permissionName").val($(this).closest('tr')['prevObject'][0].previousElementSibling.innerHTML);
    $("#idPermission").val($(this).closest('tr')['prevObject'][0].previousElementSibling.id);
    $("#saveNew").prop('checked', false);
   var i=1;
    //Dashboad Module
   	(this.childNodes[i++].style['backgroundColor'] === "green")? $('#modDashboard').prop('checked', true) :  $('#modDashboard').prop('checked', false);

   	//Campaign Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modCampaign').prop('checked', true) :  $('#modCampaign').prop('checked', false);
  	(this.childNodes[i++].style['backgroundColor'] === "green")? $('#addCampaign').prop('checked', true) :  $('#addCampaign').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#updateCampaign').prop('checked', true) :  $('#updateCampaign').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteCampaign').prop('checked', true) :  $('#deleteCampaign').prop('checked', false);

    //Conversation Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modConversation').prop('checked', true) :  $('#modConversation').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#viewMessage').prop('checked', true) :  $('#viewMessage').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#sendMessage').prop('checked', true) :  $('#sendMessage').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteMessage').prop('checked', true) :  $('#deleteMessage').prop('checked', false);

     //Contact Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modContacts').prop('checked', true) :  $('#modContacts').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#addContacts').prop('checked', true) :  $('#addContacts').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#updateContacts').prop('checked', true) :  $('#updateContacts').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteContacts').prop('checked', true) :  $('#deleteContacts').prop('checked', false);

     //Group Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modGroup').prop('checked', true) :  $('#modGroup').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#addGroup').prop('checked', true) :  $('#addGroup').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#updateGroup').prop('checked', true) :  $('#updateGroup').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteGroup').prop('checked', true) :  $('#deleteGroup').prop('checked', false);

    //User Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modUser').prop('checked', true) :  $('#modUser').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#addUser').prop('checked', true) :  $('#addUser').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#updateUser').prop('checked', true) :  $('#updateUser').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteUser').prop('checked', true) :  $('#deleteUser').prop('checked', false);

    //Permission Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modPermission').prop('checked', true) :  $('#modPermission').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#createPermission').prop('checked', true) :  $('#createPermission').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#updatePermission').prop('checked', true) :  $('#updatePermission').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deletePermission').prop('checked', true) :  $('#deletePermission').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#setGroupPermission').prop('checked', true) :  $('#setGroupPermission').prop('checked', false);

    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#setClientPermission').prop('checked', true) :  $('#setClientPermission').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#removeClientPermission').prop('checked', true) :  $('#removeClientPermission').prop('checked', false);
   	(this.childNodes[i++].style['backgroundColor'] === "green")? $('#setVolunteerPermission').prop('checked', true) :  $('#setVolunteerPermission').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#removeVolunteertPermission').prop('checked', true) :  $('#removeVolunteerPermission').prop('checked', false);
 
    //Template Module
 	(this.childNodes[i++].style['backgroundColor'] === "green")? $('#modTemplate').prop('checked', true) :  $('#modTemplate').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#addTemplate').prop('checked', true) :  $('#addTemplate').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#updateTemplate').prop('checked', true) :  $('#updateTemplate').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteTemplate').prop('checked', true) :  $('#deleteTemplate').prop('checked', false);

    //Project Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modProject').prop('checked', true) :  $('#modProject').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#addProject').prop('checked', true) :  $('#addProject').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#updateProject').prop('checked', true) :  $('#updateProject').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteProject').prop('checked', true) :  $('#deleteProject').prop('checked', false);

    //Email Module
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#modEmail').prop('checked', true) :  $('#modEmail').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#sendEmail').prop('checked', true) :  $('#sendEmail').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#addEmail').prop('checked', true) :  $('#addEmail').prop('checked', false);
    (this.childNodes[i++].style['backgroundColor'] === "green")? $('#deleteEmail').prop('checked', true) :  $('#deleteEmail').prop('checked', false);

 })

$('#applied-tags').multiselect({
	buttonWidth: '100%',
	buttonClass: 'btn btn-primary',
	includeSelectAllOption: true,
	allSelectedText: 'Added All',
	onChange: function(element, checked) {                
		if(checked === true){
			console.log('element', element[0]);
		}
	}
});                

