const importContactstModal = () => {
    $('#importContacts').modal('show');
}

const addContactModal = () => {
    $('#addContact').modal('show');
}

function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

$(document).ready(function() {
    // Dropzone
    var all_file = new FormData();
    var update_all_file = new FormData();
    var edit_all_file = new Array();
    Dropzone.autoDiscover = false;
    if (document.getElementById('dropzone-add')) {
        var editDropzone = new Dropzone("#dropzone-add", {
            parallelUploads: 2,
            createImageThumbnails: true,
            clickable: true,
            thumbnailWidth: 200,
            thumbnailHeight: 200,
            filesizeBase: 1000,
            addRemoveLinks: true,
            maxFiles: 1,
            acceptedMimeTypes: ".csv",
            // acceptedFiles: ".gif,.png,.jpg,.gif,.au,.mp4a,.mpga,.oga,.3ga,.3gpp,.ac3,.wav,.weba,.awb,.amr,.video,.mpeg,.mp4,.qt,.webm,.3gp,.3g2,.3gpp-tt,.h261,.h263,.h264,.vcf,.csv,.rtf,.rtx,.ics,.pdf",
            removedfile: function(file) {
                if (file == edit_all_file[0]) {
                    edit_all_file.pop();
                }
                var files = all_file.getAll("files[]");
                all_file.delete('files[]');
                for (i = 0; i < files.length; i++) {
                    if (files[i].name == file.name) {} else {
                        all_file.append('files[]', file, file.name);
                    }
                }
                var files = update_all_file.getAll("files[]");
                update_all_file.delete('files[]');
                for (i = 0; i < files.length; i++) {
                    if (files[i].name == file.name) {} else {
                        update_all_file.append('files[]', file, file.name);
                    }
                }
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
        });
    }
    // Mock the file upload progress (only for the demo)
    Dropzone.prototype.uploadFiles = function(files) {

        var minSteps = 6;
        var maxSteps = 60;
        var timeBetweenSteps = 100;
        var bytesPerStep = 100000;
        var isUploadSuccess = Math.round(Math.random());

        var self = this;

        for (var i = 0; i < files.length; i++) {

            var file = files[i];
            var totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));
            for (var step = 0; step < totalSteps; step++) {
                var duration = timeBetweenSteps * (step + 1);
                setTimeout(function(file, totalSteps, step) {
                    return function() {
                        file.upload = {
                            progress: 100 * (step + 1) / totalSteps,
                            total: file.size,
                            bytesSent: (step + 1) * file.size / totalSteps
                        };
                        self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
                        if (file.upload.progress == 100) {
                            if (edit_all_file.length > 0) {
                                file.status = Dropzone.ERROR;
                                self.emit('error', file, 'You can not upload any more file', null);
                                self.emit('complete', file);
                                self.processQueue();
                            } else {
                                update_all_file.append('files[]', file, file.name);
                                file.status = Dropzone.SUCCESS;
                                self.emit('success', file, 'success', null);
                                self.emit('complete', file);
                                self.processQueue();
                            }

                            all_file.append('files[]', file, file.name);
                        }
                    };
                }(file, totalSteps, step), duration);
            }
        }
    };
    //End of Dropzone

    // Array holding selected row IDs
    var rows_selected = [];

    var filter_user = '';

    if (document.getElementById('filter_user')) {
      filter_user = $('#filter_users').val();
    }


    var table_groupContacts = $('#view_contact_group').DataTable({
        "pageLength": 10,
        "serverSide": true,
        "ajax": {   
            "url": '/admin/show_group',
            "type": 'POST',
            "data":function(data) {
                // data.group= $('#filter_groups').val();
                data.users= document.getElementById('filter_users')? $('#filter_users').val() : '';
                data.status= document.getElementById('filter_status')? $('#filter_status').val() : '';
                // data.status = $('#filter_status').val();
            },
        },
        'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'width': '1%',
         'className': 'dt-body-center',
         'render': function(data, type, full, meta) {
            return '<input type="checkbox" name="selected_id" id="selected" value="' +
                $('<div/>').text(data).html() + '">';
        }
      }],
      'order': [[1, 'asc']],
      'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];

         // If row ID is in the list of selected row IDs
         if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"][id="selected"]').prop('checked', true);
            // $(row).addClass('selected');
         }
      }
   });

    // var table_groupContacts = $('#view_contact_group').DataTable({
    //     "pageLength": 10,
    //     "serverSide": true,
    //     "order": [
    //         [0, "asc"]
    //     ],
    //     "ajax": {
    //         url: '/admin/show_group',
    //         type: 'POST'
    //     },
    //     'columnDefs': [{
    //         'targets': 0,
    //         'searchable': false,
    //         'orderable': false,
    //         'className': 'dt-body-center',
    //         'render': function(data, type, full, meta) {
    //           return '<input type="checkbox" name="selected_id" id="selected" value="' +
    //             $('<div/>').text(data).html() + '">';
    //         }
    //     }],
    //     'order': [1, 'asc'],
    //     "createdRow": function(row, data, dataIndex) {
    //         $(row).attr("id", "tblRow_" + data[0]);
    //     }
    // }); // End of DataTable

    var table_contacts = $('#view_contacts').DataTable({
        "pageLength": 10,
        "serverSide": true,
        "ajax": {   
            "url": '/admin/show_contacts',
            "type": 'POST',
            "data":function(data) {
                data.group= $('#filter_groups').val();
                data.users= filter_user;
                data.status = $('#filter_status').val();
            },
        },
        'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'width': '1%',
         'className': 'dt-body-center',
         'render': function(data, type, full, meta) {
            return '<input type="checkbox" name="selected_id" id="selected" value="' +
                $('<div/>').text(data).html() + '">';
        }
      }],
      'order': [[1, 'asc']],
      'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];

         // If row ID is in the list of selected row IDs
         if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"][id="selected"]').prop('checked', true);
            // $(row).addClass('selected');
         }
      }
   });

   // // Handle click on checkbox
   // $('#view_contacts tbody').on('click', 'input[type="checkbox"][id="selected"]', function(e){
   //    var $row = $(this).closest('tr');

   //    // Get row data
   //    var data = table_contacts.row($row).data();

   //    // Get row ID
   //    var rowId = data[0];

   //    // Determine whether row ID is in the list of selected row IDs
   //    var index = $.inArray(rowId, rows_selected);

   //    // If checkbox is checked and row ID is not in list of selected row IDs
   //    if(this.checked && index === -1){
   //       rows_selected.push(rowId);

   //    // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
   //    } else if (!this.checked && index !== -1){
   //       rows_selected.splice(index, 1);
   //    }

   //    if(this.checked){
   //       $row.addClass('selected');
   //    } else {
   //       $row.removeClass('selected');
   //    }

   //    // Update state of "Select all" control
   //    updateDataTableSelectAllCtrl(table_contacts);

   //    // Prevent click event from propagating to parent
   //    e.stopPropagation();
   // });

   // // Handle click on table cells with checkboxes
   // $('#view_contacts').on('click', 'tbody td, thead th:first-child', function(e){
   //    $(this).parent().find('input[type="checkbox"][id="selected"]').trigger('click');
   // });

   // // Handle click on "Select all" control
   // $('thead input[name="select_all"]', table_contacts.table().container()).on('click', function(e){
   //    if(this.checked){
   //       $('#view_contacts tbody input[type="checkbox"][id="selected"]:not(:checked)').trigger('click');
   //    } else {
   //       $('#view_contacts tbody input[type="checkbox"][id="selected"]:checked').trigger('click');
   //    }

   //    // Prevent click event from propagating to parent
   //    e.stopPropagation();
   // });

   // // Handle table draw event
   // table_contacts.on('draw', function(){
   //    // Update state of "Select all" control
   //    updateDataTableSelectAllCtrl(table_contacts);
   // });

    function reload_table_contacts() {
        table_contacts.ajax.reload(null, false);
    }
    function reload_table_groupContacts() {
        table_groupContacts.ajax.reload(null, false);
    }

    $('#filter_groups,#filter_users,#filter_status').change(function(){
        reload_table_contacts();
        reload_table_groupContacts();
    })
    $('.contact_refresh').on('click',function(){
      reload_table_contacts();
      reload_table_groupContacts()
    })
    $(document).on('click', '#delete_groupContacts', function() {
        var id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "You want to delete this contact group?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes delete it!",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "/admin/delete_groupContacts",
                    type: "POST",
                    dataType: "json",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data == true) {
                            swal("Deleted!", "Your contact group has been deleted.", "success");
                            reload_table_groupContacts();
                        }
                    }
                })
            }
        });
    })

    $(document).on('click', '#groupContacts_status', function() {
        var id = $(this).data('id');
        var status = $(this).data('stat');
        $.ajax({
            url: "/admin/groupContacts_status",
            type: "POST",
            dataType: "json",
            data: {
                id: id,
                status: status
            },
            success: function(data) {
                if (data == 1) {
                    toastr["success"]("Contact group activate status");
                } else if (data == 0) {
                    toastr["error"]("Contact group deactivate status");
                } else {
                    swal("Error!", "Your can't delete contact right now.", "error");
                }
                reload_table_groupContacts();
            }
        })
    });
    $(document).on('submit', '#add_contactGroup', function(e) {
        e.preventDefault();
        var btn = Ladda.create(document.querySelector('#save_group'));
        btn.start();
        $.ajax({
            url: "/admin/add_contactGroup",
            type: "POST",
            dataType: "json",
            data: {
                id: $('#group_id').val(),
                name: $('#group_name').val(),
                old_name: $('#group_name').data('name')
            },
            success: function(data) {
                btn.stop();
                if (data == true) {
                    if ($('#group_id').val() == "") {
                        toastr["success"]("Contact group added successfully");
                    } else {
                        toastr["success"]("Contact group updated successfully");
                    }
                    reload_table_groupContacts();
                    $('#group_name').val("");
                } else if (data == 2) {
                    toastr["error"]("Group Contact already exist");
                }
            }
        })
    })

    $(document).on('click', '#edit_groupContacts', function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $('#group_name').val(name);
        $('#group_id').val(id);
        $('#group_name').attr("data-name", name);
    })
    
    $(document).on('click', '#add_group', function() {
        $('#group_name').val('');
        $('#group_id').val('');
        $('#group_name').attr("data-name", ' ');
    })
    // Delete Contact
    $(document).on('click', '#delete_contact', function() {
        var data = [];
        var type = $(this).data('type');
        if ($(this).data('id') !== undefined && $(this).data('id') != "") {
            data.push($(this).data('id'));
        } else {
            $('input[type=checkbox]:checked').not('#user_stat, #contact_stat, #contact_msg_stat, #select-all').each(function() {
                data.push($(this).val());
            });
        }

        swal({
          title: "Are you sure?",
          text: type=="contact"? 'You want to delete this Contact' : 'You want to delete this Contact Group',
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes delete it!",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: true,
          showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "/admin/delete_contact",
                    type: "POST",
                    dataType: "json",
                    data: {
                        data: data,
                        type: type
                    },
                    success: function(data) {
                        reload_table_groupContacts();
                        reload_table_contacts();
                        swal("Deleted!", "Your contact has been deleted.", "success");
                    }
                })
            }
        });
    })
    $(document).on('submit', '#importContactsForm', function(e) {
        var btn = Ladda.create(document.querySelector('#uploadImportedFile'));
        var grp_id = $('#import_group_id').val();
        btn.start();
        e.preventDefault();
        if(grp_id){
            all_file.append('group_id', grp_id);
            $.ajax({
                url: "/admin/importContacts",
                type: "POST",
                data: all_file,
                contentType: false,
                cache: false,
                processData: false,
                success: function(res) {
                    btn.stop();
                    if(res==1){
                        toastr["success"]('Your file was successfully imported!');
                        reload_table_contacts();
                        $('#importContacts').modal('hide');
                    }else if(res==2){
                        toastr["error"]('This file is already imported or some contacts are already existing.');
                    }else{
                        toastr["error"]('Unable to import the file. CSV data format is invalid.');
                    }
                }
            })
        }else{
            btn.stop();
            toastr["error"]('Please select any group.');
        }
    })
    // Add Contact
    $(document).on('submit', '.add_contact', function(e) {
        e.preventDefault();
        var frmData = new FormData(this);
        var btn = Ladda.create(document.querySelector('#btn_add_contact'));
        btn.start();
        $.ajax({
            url: "/admin/submitContact",
            type: "POST",
            dataType: "json",
            data: frmData,
            contentType: false,
            cache: false,
            processData: false,
            success: function(res) {
                btn.stop();
                if (res==1) {
                    reload_table_contacts();
                    toastr["success"]('New contact was successfully added!');
                    $('#addContact').modal('hide');
                    $('#addContactForm')[0].reset();
                    $('#phonenumber').val('+1 ');
                }else if(res==2){
                    toastr["error"]('Email or number already exists!');
                }else{
                    toastr["error"]('A problem occured. Please try again.');
                }
            }
        })
    });
    $(document).on('click', '#edit_contact', function() {
        var id = $(this).data('id');
        $.ajax({
            url: "/admin/getsingleContact",
            type: "POST",
            dataType: "json",
            data: {
                id: id,
            },
            success: function(data) {
                var cstatus = data.cstatus;
                var status = "";
                if (cstatus == 1) {
                    $("#update_status").prop("checked", true);
                } else{
                    $("#update_status").prop("checked", false);
                }

                $('#update_ID').val(data.id);
                $('#update_groupID').val(data.group_id).trigger('change');
                $('#update_firstName').val(data.firstName);
                $('#update_lastName').val(data.lastName);
                $('#update_phoneNumber').val(data.number);
                $('#update_old_phoneNumber').val(data.number);
                $('#update_email').val(data.email);
                $('#update_old_email').val(data.email);
            }
        })
    })
    // Update Contact
    $(document).on('submit', '.update_contact', function(e) {
        var btn = Ladda.create(document.querySelector('#btn_update'));
        btn.start()
        e.preventDefault();
        var frmData = new FormData(this);
        $.ajax({
            url: "/admin/submitContact",
            type: "POST",
            data: frmData,
            contentType: false,
            cache: false,
            processData: false,
            success: function(res) {
                btn.stop();
                if (res==1) {
                    reload_table_contacts();
                    toastr["success"]('The contact was successfully updated!');
                    $('#updateContact').modal('hide');
                    $('#updateContactForm')[0].reset();
                }else if(res==2){
                    toastr["error"]('Email or number already exists!');
                }else{
                    toastr["error"]('A problem occured. Please try again.');
                }
            }
        })
    })
    $(document).on('click','#restore_contact',function(){
      id = $(this).data('id');
      var type = $(this).data('type');
      if(id){
          $.ajax({
              url: "/admin/restore_contact",
              type: "POST",
              data: { id: id,type:type },
              success: function(res) {
                  if(res==1){
                      reload_table_contacts();
                      toastr["success"]("Contact restored.");
                  }else{
                      toastr["error"]("A problem occured. Please refresh your page.");
                  }
              }
          })  
      }else{
          toastr["error"]("A problem occured. Please refresh your page.");
      }
    })

    $(document).on('click', '#contact_status', function() {
        var check = $('input[name=selected]:checked');
        var status = $(this).data('stat');
        var stat = '';
        var type = $(this).data('type');
        var id = [];

        if ($(this).data('id') !== undefined && $(this).data('id') != "") {
            id.push($(this).data('id'));
        } else {
            $('input[type=checkbox]:checked').not('#user_stat, #contact_status, #contact_msg_stat, #select_all, #status_contact').each(function() {
                id.push($(this).val());
            });
        }
        if(status==0){
          stat = 'delete';
        }else if(status==1){
          stat='activate';
        }else if(status==2){
          stat='deactivate';
        }else{
          stat='block'
        }

        if (id.length>0) {
          console.log(id);
          swal({
            title: "Are you sure?",
            text: type=="contact"? 'You want to '+stat+' this Contact' : 'You want to '+stat+' this Contact Group',
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Yes update it!",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
            showLoaderOnConfirm: true
          },
          function(isConfirm) {
              if (isConfirm) {
                  $.ajax({
                      url: "/admin/contact_status",
                      type: "POST",
                      dataType: "json",
                      data: {
                          id: id,
                          status: status,
                          type: type
                      },
                      success: function(data) {
                          if (data === true) {
                              swal("Success!", "Your contact updated successfully!", "success");
                          }
                          reload_table_groupContacts();
                          reload_table_contacts();
                      }
                  })
              }
          });
        }else{
          toastr["error"]("Please select contact to update.");
        }
    });

    $(document).on('click', '#contact_stat', function() {
        var status = $(this).data('stat');

        var id = [];
        if ($(this).data('id') !== undefined && $(this).data('id') != "") {
            id.push($(this).data('id'));
        } else {
            $('input[type=checkbox]:checked').not('#user_stat, #contact_status, #contact_msg_stat, #example-select-all').each(function() {
                id.push($(this).val());
            });
        }
        console.log(id);
        $.ajax({
            url: "/admin/contact_status",
            type: "POST",
            dataType: "json",
            data: {
                id: id,
                status: status
            },
            success: function(data) {
                if (data === true) {
                    swal("Success!", "Your contact updated successfully!", "success");
                }
                reload_table_contacts();
            }
        })
    });
})
