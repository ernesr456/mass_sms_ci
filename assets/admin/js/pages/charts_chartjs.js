$(function() {

  $('.chartjs-demo').each(function() {
    $(this).wrap($('<div style="height:' + this.getAttribute('height') + 'px"></div>'));
  });


  // var pieChart = new Chart(document.getElementById('chart-pie').getContext("2d"), {
  //   type: 'pie',
  //   data: {
  //     labels: [ 'Delivered', 'Landline', 'Spam' ],
  //     datasets: [{
  //       data: [ 192,4,0 ],
  //       backgroundColor: ['#00b3b3','#c7c5c5','#757575'],
  //     }]
  //   },
  //   // Demo
  //   options: {
  //     responsive: false,
  //     maintainAspectRatio: false,
  //     legend: false
  //   }
  // });
    var campaign = $('#campaign').val();

  if (campaign!==null) {
      sms_status(campaign);
      get_percent_reply(campaign);
      get_reply_status(campaign);
      all_count_message(campaign);
      $(document).on('change','#campaign',function(){
        var campaign = $(this).val();
        sms_status(campaign);
        get_percent_reply(campaign);
        get_reply_status(campaign);
      })

      function all_count_message(campaign){
        $.ajax({
          url: base_url+"Admin/all_count_message",
          type: "POST",
          dataType: "json",
          data: {
            campaign:campaign,
          },
          success: function(data){
            var label = [];
            var total = [];
            var undelivered = [];
            var received = [];
            for(i=0;i<data.length;i++){
              label.push(data[i].month);
              total.push(data[i].total_text);
              undelivered.push(data[i].undelivered);
              received.push(data[i].received);
            }
              data_message(label,total,undelivered,received);
          }
        })
      }
      function sms_status(campaign){
        $.ajax({
        url: base_url+"Admin/sms_status",
        type: "POST",
        dataType: "json",
        data: {
          campaign:campaign,
        },
        success: function(data){
          for(i=0;i<data.length;i++){
            if (data[i].sent===null&&data[i].undelivered===null) {
              $('#total_sent').text("Total Sent: 0.00%");
              $('#total_undelivered').text("Total Undelivered: 0.00%")
            }else{
              $('#total_sent').text("Total Sent: "+data[i].sent+"%");
              $('#total_undelivered').text("Total Undelivered: "+data[i].undelivered+"%");
            }
          }
        }
      })
      }
      function get_percent_reply(campaign){
        $.ajax({
        url: base_url+"Admin/get_percent_reply",
        type: "POST",
        dataType: "json",
        data: {
          campaign:campaign,
        },
        success: function(data){
          for(i=0;i<data.length;i++){
            if (data[i].all_reply===null) {
              $('#total_replies').text("Total Replies: 0.00%");
            }else{
              $('#total_replies').text("Total Replies: "+data[i].all_reply+"%");
            }
          }
        }
      })
      }
      function get_reply_status(campaign){
        $.ajax({
          url: base_url+"Admin/get_reply_status",
          type: "POST",
          dataType: "json",
          data: {
            campaign:campaign,
          },
          success: function(data){
            var table = '';
            var negative = '';
            var positive = '';
            for(i=0;i<data.length;i++){
              console.log(data[i].id);

              if (data[i].sms_reply_status=='1') {
                negative = 'No';
                positive = 'Yes';
              }else{
                negative = 'Yes';
                positive = 'No';
              }
              console.log(data[i]);
              table +=
              '<tr>'+
                '<td><a href="'+base_url+'admin/conversation?contact_id='+data[i].id+'">'+data[i].name+'</a><br>'+data[i].message+'</td>'+
                '<td>'+positive+'</td>'+
                '<td>'+negative+'</td>'+
                '<td>'+jQuery.timeago(""+data[i].sent_time)+'</td>'+
              '</tr>'
            }
            $('#recent-replies').html(table);
          }
        })
      }
      function data_message(label,total,undelivered,received){
        data_label = "";
        if (label=="") {
          data_label = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "November", "December"];
        }else{
          data_label = label;
        }
        console.log(label);
        var data = {
            labels: data_label,
            datasets: [{
                label: "Total Sent",
                backgroundColor: 'rgb(214,236,251)',
                borderColor: 'rgb(155,209,245)',
                borderWidth: 1,
                data: total,
            }]
        };

        var ctx = $("#myChart").get(0).getContext("2d");
        var myBarChart = new Chart(ctx, {
            type: "bar",
            data: data,
            options: {
                scales: {
                    xAxes: [{
                        gridLines: {
                            offsetGridLines: true
                        }
                    }]
                }
            },
        });

        var received = {
            label: "Total Replies",
            backgroundColor: 'rgba(99, 255, 132, 0.2)',
            borderColor: 'rgba(99, 255, 132, 1)',
            borderWidth: 1,
            data: received,
        }
        data.datasets.push(received);
        myBarChart.update();

        var Undelivered = {
            label: "Total Undelivered",
            backgroundColor: 'rgb(255,224,230)',
            borderColor: 'rgb(255,177,194)',
            borderWidth: 1,
            data: undelivered,
        }

        data.datasets.push(Undelivered);
            myBarChart.update();
            function resizeCharts() {
            // graphChart.resize();
            // barsChart.resize();
            // radarChart.resize();
            // polarAreaChart.resize();
            myBarChart.resize()
            // pieChart.resize();
            // doughnutChart.resize();
          }

          // Initial resize
          resizeCharts();

          // For performance reasons resize charts on delayed resize event
          window.layoutHelpers.on('resize.charts-demo', resizeCharts);
      }

  }

  // Resizing charts
});
