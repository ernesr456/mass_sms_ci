$(function() {
    // Enable tooltips
    $('.messages-tooltip').tooltip();

    $('.messages-scroll').each(function() {
        new PerfectScrollbar(this, {
            suppressScrollX: true,
            wheelPropagation: true
        });
    });

    $('.messages-sidebox-toggler').click(function(e) {
        e.preventDefault();
        $('.messages-wrapper, .messages-card').toggleClass('messages-sidebox-open');
    });
});
