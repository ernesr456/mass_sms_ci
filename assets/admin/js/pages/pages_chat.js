$(function() {

  	$('.chat-scroll').each(function() {
	    new PerfectScrollbar(this, {
	      suppressScrollX: true,
	      wheelPropagation: true
	    });
    });
    
  	const chatdiv = document.getElementById('chat-scroll');
	chatdiv.scrollTop = chatdiv.scrollHeight;

  	$('.chat-sidebox-toggler').click(function(e) {
    	e.preventDefault();
    	$('.chat-wrapper').toggleClass('chat-sidebox-open');
  	});
});
