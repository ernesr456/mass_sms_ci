$(function() {
	(function() {
		window.layoutHelpers.setAutoUpdate(true);
		window.attachMaterialRippleOnLoad();
	})();
	// Initialize login validation
	$('.add_group').validate({
		focusInvalid: false,
		rules: {
			'group_name': {
				required: true,
			},
		},

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize login validation
	$('#importContactsForm').validate({
		focusInvalid: false,
		rules: {
			'group_id': {
				required: true,
			},
			'csv_file':{
				required: true,
			},
		},

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize login validation
	$('#add_package').validate({
		focusInvalid: false,
		rules: {
			'pricing_name': {
				required: true,
			},
			'currency-field': {
				required: true,
			},
		},

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize login validation
	$('#login-form-bkp').validate({
		focusInvalid: false,
		rules: {
			'username': {
				required: true,
			},
			'password': {
				required: true,
				minlength: 5,
				maxlength: 20
			},
		},

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});
		// Initialize register validation
	$('#addUser-form').validate({
		focusInvalid: false,
		rules: {
			'username': {
				required: true,
			},
			'password': {
				required: true,
				minlength: 5,
				maxlength: 20
			},
			'password2': {
				required: true,
				minlength: 5,
				maxlength: 20,
				equalTo: 'input[name="password"]'
			},
			'email': {
				required: true,
				email: true
			},
			'name': {
				required: true,
			},
			'group_id': {
				required: true,
			}
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize register validation
	$('#register-form').validate({
		focusInvalid: false,
		rules: {
			'username': {
				required: true,
			},
			'password': {
				required: true,
				minlength: 5,
				maxlength: 20
			},
			'password2': {
				required: true,
				minlength: 5,
				maxlength: 20,
				equalTo: 'input[name="password"]'
			},
			'old_password':{
				required: true,
				minlength: 5,
				maxlength: 20,
			},
			'email': {
				required: true,
				email: true
			},
			'name': {
				required: true,
			},
			'user_group': {
				required: true,
			}
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize register validation
	$('#update-form').validate({
		focusInvalid: false,
		rules: {
			'username': {
				required: true,
			},
			'password': {
				minlength: 5,
				maxlength: 20
			},
			'password2': {
				minlength: 5,
				maxlength: 20,
				equalTo: 'input[name="password"]'
			},
			'email': {
				required: true,
				email: true
			},
			'name': {
				required: true,
			},
			'group_id': {
				required: true,
			}
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize register validation
	$('#addContactForm').validate({
		focusInvalid: false,
		rules: {
			'firstName': {
				required: true,
			},
			'lastName':{
				required: true,
			},
			'group_id':{
				required: true,
			},
			'number':{
				required: true,
			},
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize register validation
	$('#updateContactForm').validate({
		focusInvalid: false,
		rules: {
			'firstName': {
				required: true,
			},
			'lastName':{
				required: true,
			},
			'group_id':{
				required: true,
			},
			'number':{
				required: true,
			},
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});


	// Initialize register validation
	$('#update_project').validate({
		focusInvalid: false,
		rules: {
			'name': {
				required: true,
			},
			'description':{
				required: true,
			},
			'message':{
				required: true,
			},
			'start':{
				required: true,
			},
			'end':{
				required: true,
			},
			'assign':{
				required: true,
			},
			'group':{
				required: true,
			},
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// Initialize register validation
	$('#save_project').validate({
		focusInvalid: false,
		rules: {
			'name': {
				required: true,
			},
			'description':{
				required: true,
			},
			'message':{
				required: true,
			},
			'start':{
				required: true,
			},
			'end':{
				required: true,
			},
			'assign':{
				required: true,
			},
			'group':{
				required: true,
			},
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});


	// Credit Validation
	$('#add_credit').validate({
		focusInvalid: false,
		rules: {
			'credit_name': {
				required: true,
			},
			'currency-field': {
				required: true,
			},
			'credit':{
				required: true,
			}
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});

	// update basic profile

	// Initialize update-basic validation
	$('#update-basic').validate({
		focusInvalid: false,
		rules: {
			'username': {
				required: true,
			},
			'email': {
				required: true,
				email: true,
			},
			'name':{
				required: true,
			},
			'group_id': {
				required: true,
			}
		},

		// Errors
		//

		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');

			$el.addClass('is-invalid');

			// Select2 and Tagsinput
			if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
				$el.parent().addClass('is-invalid');
			}
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});	
	
	$("#recover").on('click',function(e){
		var email = $('#recover_email').val();
		$.ajax({
			type : "POST",
			url: "/admin/forget_password",
			dataType: "JSON",
			data : {email:email}
		})
	})
	$("#reset_all").click(function(){
		$(".savebasic").trigger("reset");
	})
	//var savingprofile = Ladda.bind("#savebasic", {timeout: 2000});

	function updateProfile(){
		alert();
	}

	$(document).ready(function(){
		$("#savebasic").attr('disabled','disabled');

	})
	$("#group_id").change(function(){
		if (this.value=="") {
			$("#savebasic").attr('disabled','disabled');
		}else{
			$("#savebasic").removeAttr("disabled");
		}

	})
	$("#username,#email,#name,#password,#password2,#group_id").keydown(function(){
		if (this.value=="") {
			$("#savebasic").attr('disabled','disabled');
		}else{
			$("#savebasic").removeAttr("disabled");
		}
	})
	$("#username,#email,#name,#password,#password2,#group_id").keyup(function(){
		if (this.value=="") {
			$("#savebasic").attr('disabled','disabled');
		}else{
			$("#savebasic").removeAttr("disabled");
		}
	})

	// validation for create contact group
	$('#create-contact-group').validate({
		focusInvalid: false,
		rules: {
			'group_name': {
				required: true,
			},
		},
		errorPlacement: function errorPlacement(error, element) {
			var $parent = $(element).parents('.form-group');

			// Do not duplicate errors
			if ($parent.find('.jquery-validation-error').length) { return; }

			$parent.append(
				error.addClass('jquery-validation-error small form-text invalid-feedback')
			);
		},
		highlight: function(element) {
			var $el = $(element);
			var $parent = $el.parents('.form-group');
			$el.addClass('is-invalid');
		},
		unhighlight: function(element) {
			$(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
		}
	});


	// Add phone validator
	$.validator.addMethod(
		'phone_format',
		function(value, element) {
			return this.optional(element) || /^\(\d{4}\)[ ]\d{3}\-\d{4}$/.test(value);
		},
		'Invalid phone number.'
	);

});
$("#select_all").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
    if($('#all_action_btn').is(":hidden")) {
    	$('#all_action_btn').show();
    }else{
    	$('#all_action_btn').hide();
    }
});
var validNumber = new RegExp(/^\d*\.?\d*$/);
var lastValid = "";

function validateNumber(elem) {
    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}