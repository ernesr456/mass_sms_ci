-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2020 at 10:14 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masssms`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_credits`
--

CREATE TABLE `account_credits` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_credits` float(10,2) NOT NULL,
  `credit_balance` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_credits`
--

INSERT INTO `account_credits` (`id`, `user_id`, `total_credits`, `credit_balance`) VALUES
(1, 64, 10.00, 10.00),
(2, 62, 5.00, 999.63);

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `contacts` varchar(255) NOT NULL,
  `contacts_names` varchar(255) NOT NULL,
  `campaign_title` text NOT NULL,
  `campaign_text` text NOT NULL,
  `tags` text NOT NULL,
  `start_at` text NOT NULL,
  `campaign_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL,
  `created_by` text NOT NULL,
  `auto_reply` varchar(255) NOT NULL,
  `auto_reply_text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chatthread`
--

CREATE TABLE `chatthread` (
  `id` int(11) NOT NULL,
  `convo_id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` bigint(20) NOT NULL,
  `firstName` char(255) NOT NULL,
  `lastName` char(255) NOT NULL,
  `number` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `cstatus` int(11) NOT NULL COMMENT '0 = delete| 1= activate | 2 = deactivate | 3 = blocklist',
  `tags` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `firstName`, `lastName`, `number`, `email`, `group_id`, `cstatus`, `tags`, `created_by`) VALUES
(44, 'Jake', 'Page', 639436462946, 'ernestoalmario20@gmail.com', 34, 1, '', 1),
(45, 'Mj', 'Tbl', 639229989394, 'ernestoalmario20@gmail.com', 34, 1, '', 1),
(46, 'Ernesto', 'Almario', 145644232, 'dsadas@gmail.com', 34, 1, '', 1),
(47, 'Ernesto', 'Almario', 132132131, 'jpage96@me.com', 35, 1, '', 2),
(48, 'Dean', 'Almario', 14324242342, 'dsadas@gmail.com', 35, 1, '', 2),
(49, 'Ernesto', 'Anast', 154353453, 'admin@gmail.com', 35, 1, '', 2),
(50, 'Dean', 'Anast', 16563534534534, 'dsadas@g.c', 35, 1, '', 2),
(51, 'Shella', 'Bill', 119705141041, 'shella@yopmail.com', 0, 0, '', 210),
(52, 'Mark', 'Yu', 113179436289, 'mark@yopmail.com', 0, 0, '', 210),
(53, 'Daniel', 'Song', 119189486122, 'daniel@yopmail.com', 0, 0, '', 210),
(54, 'Jongku', 'Sok', 113177951582, 'jongku@yopmail.com', 0, 0, '', 210),
(55, 'Carl', 'Song', 114156530010, 'carl@yopmail.com', 0, 0, '', 210),
(56, 'Dean', 'Almario', 1321312312312, 'ernestoalmario20@gmail.com', 0, 1, '', 1),
(57, 'Ernesto', 'Anast', 16394364625943, 'dsadas@gmail.com', 0, 1, '', 1),
(58, 'Ernesto', 'Anast', 1321321321312321, 'admin@gmail.com', 0, 1, '', 1),
(59, 'Ernesto', 'Almario', 639436462946, 'ernestoalmario20@gmail.com', 38, 1, '', 210);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) NOT NULL,
  `name` text DEFAULT NULL,
  `number` varchar(200) DEFAULT NULL,
  `email` text DEFAULT NULL,
  `group_id` bigint(20) NOT NULL,
  `cstatus` int(1) NOT NULL DEFAULT 1,
  `msg_status` int(11) NOT NULL COMMENT '0 = don''t send | 1= Can send',
  `created_by` bigint(20) DEFAULT NULL,
  `tags` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contacts_group`
--

CREATE TABLE `contacts_group` (
  `gid` bigint(20) NOT NULL,
  `group_name` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `tags` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts_group`
--

INSERT INTO `contacts_group` (`gid`, `group_name`, `status`, `tags`, `created_by`) VALUES
(34, 'Group 1', 0, '', 1),
(35, 'Group 10', 1, '', 2),
(36, 'Group 1', 0, '', 210),
(37, 'Group 11', 1, '', 1),
(38, 'Group 12', 1, '', 234);

-- --------------------------------------------------------

--
-- Table structure for table `conversation`
--

CREATE TABLE `conversation` (
  `id` bigint(11) NOT NULL,
  `message` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `from_id` bigint(20) NOT NULL,
  `to_id` bigint(20) NOT NULL,
  `sent_time` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0 read | 1= unread| 2 = failed to send|3 = unverfied number',
  `sms_sid` varchar(50) NOT NULL COMMENT 'Twilio SMS ID',
  `group_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `extension` char(20) NOT NULL,
  `sms_reply_status` int(11) NOT NULL COMMENT '0 = negative | 1 = positive',
  `sms_status` char(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conversation`
--

INSERT INTO `conversation` (`id`, `message`, `filename`, `from_id`, `to_id`, `sent_time`, `status`, `sms_sid`, `group_id`, `parent_id`, `extension`, `sms_reply_status`, `sms_status`) VALUES
(820, 'test14', '', 1, 44, '2020-04-25 01:37:45', 0, '', 34, 0, '', 0, ''),
(819, 'test15', '', 1, 44, '2020-04-23 00:00:00', 2, '', 34, 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `conversation_file`
--

CREATE TABLE `conversation_file` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `convo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credits`
--

CREATE TABLE `credits` (
  `id` int(11) NOT NULL,
  `credit_name` text NOT NULL,
  `credit_desc` text NOT NULL,
  `credit_amount` int(11) NOT NULL,
  `credit_price` decimal(10,2) NOT NULL,
  `credit_currency` varchar(5) NOT NULL,
  `created_by` int(11) NOT NULL,
  `credit_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 = active, 1 = inactive',
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credits`
--

INSERT INTO `credits` (`id`, `credit_name`, `credit_desc`, `credit_amount`, `credit_price`, `credit_currency`, `created_by`, `credit_status`, `created_at`) VALUES
(15, 'Credit 1', '', 500, '123.20', '', 0, 0, '2020-04-14 21:02:22'),
(16, 'Credit 2', '', 1500, '1.00', '', 0, 0, '2020-04-14 21:02:30'),
(17, 'Credit 3', '', 600, '32.20', '', 0, 0, '2020-04-14 21:02:42');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_name` varchar(255) NOT NULL,
  `send_to_type` varchar(255) NOT NULL,
  `send_type` varchar(255) NOT NULL,
  `send_to` text NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 Sent | 2 Drafts | 3 Deleted',
  `date_send` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `user_id`, `from_name`, `send_to_type`, `send_type`, `send_to`, `subject`, `message`, `status`, `date_send`) VALUES
(15, 1, 'test', 'specific_emails', 'group_send', '[\"45\"]', 'test', '<p>Hi this is</p>', 1, '2020-04-13 01:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `message_template`
--

CREATE TABLE `message_template` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `message_template`
--

INSERT INTO `message_template` (`id`, `subject`, `message`, `filename`, `file_type`, `status`, `created_by`) VALUES
(9, 'test', 'dsadasdas', 'Untitled_Diagram_(4).png', 'png', 1, 1),
(10, 'test', 'dsadasdasda', 'Untitled_Diagram_(2).png', 'png', 1, 210);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pricing_id` int(11) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `payment_method` text NOT NULL,
  `payment_state` varchar(20) NOT NULL,
  `total_payment` decimal(10,2) NOT NULL,
  `payer_email` text NOT NULL,
  `payer_mail` text NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `payer_status` text NOT NULL,
  `created_time` datetime NOT NULL,
  `update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments_credit`
--

CREATE TABLE `payments_credit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `credit_id` int(11) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `payment_method` text NOT NULL,
  `payment_state` varchar(20) NOT NULL,
  `total_payment` decimal(10,2) NOT NULL,
  `total_credit` int(11) NOT NULL,
  `payer_email` text NOT NULL,
  `payer_mail` text NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `payer_status` text NOT NULL,
  `created_time` datetime NOT NULL,
  `update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pricing_table`
--

CREATE TABLE `pricing_table` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pricing_table`
--

INSERT INTO `pricing_table` (`id`, `name`, `price`, `permission`, `status`) VALUES
(17, 'Pricing 1', '123.10', '{\"modDashboard\":true,\"modCampaign\":true,\"modConversation\":true,\"modTemplate\":true,\"modEmail\":true,\"modContacts\":true,\"modGroup\":true,\"modUser\":true,\"modPermission\":true}', 0),
(18, 'GOLD plan', '12.00', '{\"modDashboard\":true,\"modCampaign\":true,\"modConversation\":true,\"modTemplate\":true,\"modEmail\":true,\"modContacts\":true,\"modGroup\":true,\"modUser\":true,\"modPermission\":true}', 2),
(19, 'Premium Plan', '123.20', '{\"modDashboard\":true,\"modCampaign\":true,\"modConversation\":true,\"modTemplate\":true,\"modEmail\":true,\"modContacts\":true,\"modGroup\":true,\"modUser\":true,\"modPermission\":true}', 0),
(20, 'Pricing 5', '123.10', '{\"modDashboard\":true,\"modCampaign\":true,\"modConversation\":true,\"modTemplate\":true,\"modEmail\":true,\"modContacts\":true,\"modGroup\":true,\"modUser\":true,\"modPermission\":true}', 0),
(21, 'Pricing 5', '123.10', '{\"modDashboard\":true,\"modCampaign\":true,\"modConversation\":true,\"modTemplate\":true,\"modEmail\":true,\"modContacts\":true,\"modGroup\":true,\"modUser\":true,\"modPermission\":true}', 0),
(22, 'Pricing 5', '123.10', '{\"modDashboard\":true,\"modCampaign\":true,\"modConversation\":true,\"modTemplate\":true,\"modEmail\":true,\"modContacts\":true,\"modGroup\":true,\"modUser\":true,\"modPermission\":true}', 0),
(23, 'Pricing 5', '123.10', '{\"modDashboard\":true,\"modCampaign\":true,\"modConversation\":true,\"modTemplate\":true,\"modEmail\":true,\"modContacts\":true,\"modGroup\":true,\"modUser\":true,\"modPermission\":true}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `proj_name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `group_id` varchar(255) NOT NULL,
  `groups_name` varchar(255) NOT NULL,
  `assign_id` varchar(255) NOT NULL,
  `assign_name` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `project_start` datetime NOT NULL,
  `project_end` datetime NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `proj_name`, `filename`, `description`, `group_id`, `groups_name`, `assign_id`, `assign_name`, `message`, `project_start`, `project_end`, `created_time`, `created_by`, `status`) VALUES
(32, 'ernesto almario', 'ERD_Massage.png', 'dsadsadsadsa', '', '', '', '', 'dsadasda', '2020-05-02 13:46:00', '2020-05-31 13:46:00', '2020-05-02 13:56:22', 210, 2),
(33, 'Hi campaign12', 'Untitled_Diagram_(2).png', 'dsadasdasdas', '38', 'Group 12', '233,234', 'Dasdasdasdsa,Ernesthree', 'dsadasdasdasda', '2020-05-02 14:03:00', '2020-05-28 14:03:00', '2020-05-02 17:58:36', 210, 1),
(34, 'dsadasdas', '', 'asdasdsadsa', '38', 'Group 12', '233,234', 'Dasdasdasdsa,Ernesthree', 'dsadasd', '2020-05-02 16:37:00', '2020-05-29 16:37:00', '2020-05-02 16:37:28', 210, 1),
(35, 'Contact1', '', 'dasdas', '38', 'Group 12', '233,234', 'Dasdasdasdsa,Ernesthree', 'dsadsa', '2020-05-02 16:40:00', '2020-05-31 16:40:00', '2020-05-02 16:40:55', 210, 1),
(36, 'ernesto almario', 'myw3schoolsimage1.jpg', 'dasdasdas', '38', 'Group 12', '233,234', 'Dasdasdasdsa,Ernesthree', 'dasdasdas', '2020-05-02 16:44:00', '2020-05-31 16:44:00', '2020-05-02 17:15:52', 210, 1),
(37, 'Contact1', '', '2', '38', 'Group 12', '233', 'Dasdasdasdsa', '2', '2020-05-02 17:58:00', '2020-05-31 17:58:00', '2020-05-02 17:59:03', 210, 1),
(38, 'Hi campaign12', '', 'dadada', '38', 'Group 12', '233', 'Dasdasdasdsa', 'dada', '2020-05-02 17:59:00', '2020-05-31 17:59:00', '2020-05-02 17:59:32', 210, 1),
(39, 'Hi campaign1', '', '2', '38', 'Group 12', '233', 'Dasdasdasdsa', '2', '2020-05-02 18:00:00', '2020-05-31 18:01:00', '2020-05-02 18:01:05', 210, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_assign_id`
--

CREATE TABLE `project_assign_id` (
  `id` bigint(20) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_assign_id`
--

INSERT INTO `project_assign_id` (`id`, `project_id`, `user_id`) VALUES
(62, 32, 0),
(85, 34, 233),
(86, 34, 234),
(87, 35, 233),
(88, 35, 234),
(93, 36, 233),
(94, 36, 234),
(141, 33, 233),
(142, 33, 234),
(143, 37, 233),
(144, 38, 233),
(145, 39, 233);

-- --------------------------------------------------------

--
-- Table structure for table `project_group_contact`
--

CREATE TABLE `project_group_contact` (
  `id` bigint(20) NOT NULL,
  `project_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_group_contact`
--

INSERT INTO `project_group_contact` (`id`, `project_id`, `group_id`) VALUES
(57, 32, 0),
(78, 34, 38),
(79, 35, 38),
(82, 36, 38),
(106, 33, 38),
(107, 37, 38),
(108, 38, 38),
(109, 39, 38);

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE `reset_password` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = verified | 1 = not verify'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(11) NOT NULL,
  `twillio_auth_token` varchar(255) NOT NULL,
  `twillio_account_sid` varchar(255) NOT NULL,
  `twilio_account_number` bigint(20) NOT NULL,
  `mailchimp_api_key` varchar(255) NOT NULL,
  `send_grid_email` varchar(255) NOT NULL,
  `send_grid_key` varchar(255) NOT NULL,
  `paypal_sandbox` int(10) NOT NULL COMMENT '1 Sandbox, 0 Live',
  `paypal_email` text NOT NULL,
  `paypal_client_id` text NOT NULL,
  `stripe_testmode` int(10) NOT NULL COMMENT '1 Test mode, 0 Live mode',
  `stripe_test_skey` text NOT NULL,
  `stripe_test_pubkey` text NOT NULL,
  `stripe_live_skey` text NOT NULL,
  `stripe_live_pubkey` text NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` varchar(255) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`id`, `twillio_auth_token`, `twillio_account_sid`, `twilio_account_number`, `mailchimp_api_key`, `send_grid_email`, `send_grid_key`, `paypal_sandbox`, `paypal_email`, `paypal_client_id`, `stripe_testmode`, `stripe_test_skey`, `stripe_test_pubkey`, `stripe_live_skey`, `stripe_live_pubkey`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(12, '8d3ecabcc5268d1f23eeccdf667b6862', 'AC3b9173a9b0843f3c9352fffe2d990f47', 14847256617, '', '', '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', 1, 'billing-facilitator@tbldevelopmentfirm.com', 'ARZEOYBKIZk-UVoC70dh4rO84VJN6jsj2k3MMUaegnt-zBGplbKoW9i8cRfJCQiqt5wwooIGjf0bBXQj', 1, '', '', '', '', '1', '2020-01-28 11:28:18', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `template_file`
--

CREATE TABLE `template_file` (
  `tf_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_signin` int(11) DEFAULT NULL,
  `created_date` int(11) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `verification_key` varchar(255) NOT NULL,
  `user_group` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `status` int(3) NOT NULL COMMENT '0 = activate, 1 = deactivate',
  `verify` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `credits` bigint(20) NOT NULL,
  `timezone` varchar(255) NOT NULL,
  `campaign_id` varchar(255) NOT NULL,
  `campaign_text` varchar(255) NOT NULL,
  `user_permission` text DEFAULT NULL,
  `total_credit` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `last_signin`, `created_date`, `ip`, `verification_key`, `user_group`, `name`, `image`, `address`, `address2`, `city`, `state`, `zip`, `status`, `verify`, `created_by`, `credits`, `timezone`, `campaign_id`, `campaign_text`, `user_permission`, `total_credit`) VALUES
(1, 'admin', 'a1fa99a1724242d0931d4f9c62dd56a6', 'thomaswoodfin@tbldevelopmentfirm.com', 2147483647, 123132121, '::1', 'dfasdfa3a33a', 1, 'test1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '\'Asia/Manila\'', '', '', NULL, '0.00'),
(116, 'tom2', '9f49d2450881a0b304e471b08fc4a59c', 'tom2@yopmail.com', 2147483647, 1585057288, '::1', '0YJ910zjfY', 2, 'Tomas', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 'Asia/Singapore', '', '', NULL, '0.00'),
(119, 'asdsadsa', '17ffcc954acd2f63794f539076659a01', 'dasdsadasdSA@g.c', NULL, 0, NULL, '', 2, 'dsadsasad', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(187, 'dsadasdsadas', '17ffcc954acd2f63794f539076659a01', 'dsadasas@g.c', NULL, 0, NULL, '', 2, 'dsadsa', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(188, 'dsadasdsadas1', '17ffcc954acd2f63794f539076659a01', 'dsadasas1@g.c', NULL, 0, NULL, '', 2, 'dsadsa', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(189, 'dsadasdsadas11', '17ffcc954acd2f63794f539076659a01', 'dsadasa1s1@g.c', NULL, 0, NULL, '', 2, 'dsadsa1', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(190, 'dsadasdsadas112', '17ffcc954acd2f63794f539076659a01', 'dsadasa12s1@g.c', NULL, 0, NULL, '', 2, 'dsadsa1', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(191, 'dsadasdsadas1123', '17ffcc954acd2f63794f539076659a01', 'dsadasa123s1@g.c', NULL, 0, NULL, '', 2, 'dsadsa13', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(192, 'dsadasdsadas11234', '17ffcc954acd2f63794f539076659a01', 'dsadasa3123s1@g.c', NULL, 0, NULL, '', 2, 'dsadsa134', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(193, 'dsadasdsadas112345', '17ffcc954acd2f63794f539076659a01', 'dsadasa53123s1@g.c', NULL, 0, NULL, '', 2, 'dsadsa1345', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(194, 'dsadasdsadas1123455', '17ffcc954acd2f63794f539076659a01', 'dsadas5a53123s1@g.c', NULL, 0, NULL, '', 2, 'dsadsa13545', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(195, 'dsadasdsadas11234551', '17ffcc954acd2f63794f539076659a01', 'dsadas5a531123s1@g.c', NULL, 0, NULL, '', 2, 'dsadsa135451', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, '', '', '', NULL, '0.00'),
(210, 'ernesr963', '28e79462c6e1a01139d298e4c58439a0', 'ernestoalmario21@gmail.com', 2147483647, 0, '::1', 'D1rJR7qD5f', 2, 'ernesto almario', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, '\'Asia/Manila\'', '', '', NULL, '-500.00'),
(222, 'ernesr123', '17ffcc954acd2f63794f539076659a01', 'ernestoalmario20@gmail.com', 2147483647, 0, '::1', 'J2LHbrVUp5', 2, 'ernesto almario', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, '\'Asia/Manila\'', '', '', NULL, '0.00'),
(232, 'admin1', '', 'thomaswoodfin@tbldevelopmentfirm.com', NULL, 0, NULL, '', 1, 'test1', '', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '', '', '', NULL, '0.00'),
(233, 'dasdsadasda', '17ffcc954acd2f63794f539076659a01', 'dsadasda@g.c', NULL, 0, NULL, '', 3, 'dasdasdasdsa', '', NULL, NULL, NULL, NULL, NULL, 0, 0, 210, 0, '', '', '', NULL, '0.00'),
(234, 'ernesr321', '17ffcc954acd2f63794f539076659a01', 'ernestoalmario232@g.c', 2147483647, 0, '::1', '', 3, 'ernesthree', '', NULL, NULL, NULL, NULL, NULL, 0, 0, 210, 0, '\'Asia/Manila\'', '', '', '{\"modCampaign\":true,\"addCampaign\":true,\"updateCampaign\":true,\"deleteCampaign\":true,\"sendCampaign\":true}', '0.00'),
(235, 'ernesr789', '28e79462c6e1a01139d298e4c58439a0', 'fines24639@tgres24.com', 2147483647, 0, '::1', '', 3, 'ernesto almario', '', NULL, NULL, NULL, NULL, NULL, 2, 0, 210, 0, '\'Asia/Manila\'', '', '', NULL, '0.00'),
(236, 'ernesr741', '17ffcc954acd2f63794f539076659a01', 'mosaye3504@tgres24.com', 2147483647, 0, '::1', '2leECBX5MM', 2, 'ernesto', '', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '\'Asia/Manila\'', '', '', NULL, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role_slug` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `name`, `role_slug`) VALUES
(1, 'Administrator', 'administrator'),
(2, 'Client', 'client'),
(3, 'Volunteers', 'volunteers');

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

CREATE TABLE `user_permission` (
  `id` int(11) NOT NULL,
  `permission_name` varchar(255) NOT NULL,
  `permission` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_permission`
--

INSERT INTO `user_permission` (`id`, `permission_name`, `permission`, `created_at`, `update_at`, `created_by`) VALUES
(55, 'permission1', 'modDashboard,modCampaign,addCampaign,updateCampaign,deleteCampaign,modProject,addProject,updateProject,deleteProject,modConversation,viewMessage,sendMessage,deleteMessage,modTemplate,addTemplate,updateTemplate,deleteTemplate,modEmail,addEmail,sendEmail,deleteEmail,modContacts,addContacts,updateContacts,deleteContacts,modGroup,addGroup,updateGroup,deleteGroup,modUser,addUser,updateUser,deleteUser,modPermission,createPermission,updatePermission,deletePermission,setGroupPermission,setClientPermission,removeClientPermission,setVolunteerPermission,removeVolunteerPermission', '2020-03-17 11:44:20', '', 1),
(56, 'permission1', 'modDashboard,modCampaign,addCampaign,updateCampaign,deleteCampaign,modProject,addProject,updateProject,deleteProject,modConversation,viewMessage,sendMessage,deleteMessage,modTemplate,addTemplate,updateTemplate,deleteTemplate,modEmail,addEmail,sendEmail,deleteEmail,modContacts,addContacts,updateContacts,deleteContacts,modGroup,addGroup,updateGroup,deleteGroup,modUser,addUser,updateUser,deleteUser,modPermission,createPermission,updatePermission,deletePermission,setGroupPermission,setClientPermission,removeClientPermission,setVolunteerPermission,removeVolunteerPermission', '2020-03-17 11:44:31', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_credits`
--
ALTER TABLE `account_credits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatthread`
--
ALTER TABLE `chatthread`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts_group`
--
ALTER TABLE `contacts_group`
  ADD PRIMARY KEY (`gid`);

--
-- Indexes for table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversation_file`
--
ALTER TABLE `conversation_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_template`
--
ALTER TABLE `message_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments_credit`
--
ALTER TABLE `payments_credit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricing_table`
--
ALTER TABLE `pricing_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_assign_id`
--
ALTER TABLE `project_assign_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_group_contact`
--
ALTER TABLE `project_group_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template_file`
--
ALTER TABLE `template_file`
  ADD PRIMARY KEY (`tf_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_permission`
--
ALTER TABLE `user_permission`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_credits`
--
ALTER TABLE `account_credits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chatthread`
--
ALTER TABLE `chatthread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `contacts_group`
--
ALTER TABLE `contacts_group`
  MODIFY `gid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=821;

--
-- AUTO_INCREMENT for table `conversation_file`
--
ALTER TABLE `conversation_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credits`
--
ALTER TABLE `credits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `message_template`
--
ALTER TABLE `message_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `payments_credit`
--
ALTER TABLE `payments_credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pricing_table`
--
ALTER TABLE `pricing_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `project_assign_id`
--
ALTER TABLE `project_assign_id`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `project_group_contact`
--
ALTER TABLE `project_group_contact`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `template_file`
--
ALTER TABLE `template_file`
  MODIFY `tf_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=237;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_permission`
--
ALTER TABLE `user_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
